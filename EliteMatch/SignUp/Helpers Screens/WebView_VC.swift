//
//  WebView_VC.swift
//  Tamaas
//
//  Created by Krescent Global on 16/07/18.
//  Copyright © 2018 Krescent Global. All rights reserved.
//

import UIKit
import WebKit
import FTIndicator

class WebView_VC: UIViewController {
    
    //MARK:- Outlets
    @IBOutlet weak var webKitView: WKWebView! {
        didSet{
            webKitView.navigationDelegate = self
        }
    }
    
    //MARK:- Variables
    var strTitle = String()
    var strUrl:String = String()
    
    //MARK:- UIView life-cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
     
        if strTitle == "Privacy Policy" {
           strUrl = "https://www.nimapp.co/privacy.html"
        }
        else if strTitle == "About Us" {
            strUrl = "https://www.nimapp.co/about_us.html#admission"
        }
        else {
           strUrl = "https://www.nimapp.co/terms.html"
        }
        
        let url = URL(string: strUrl)!
        webKitView.load(URLRequest(url: url))
        webKitView.allowsBackForwardNavigationGestures = true
    }
    
    //MARK:- UIButton Actions
    @IBAction func didPress_back(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
   
}

extension WebView_VC: WKNavigationDelegate {

    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
        FTIndicator.showToastMessage("Loading....")
        print("Strat to load")
  }

    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
      print("finish to load")
        FTIndicator.dismissToast()
  }

    private func webView(webView: WKWebView, didFailProvisionalNavigation navigation: WKNavigation!, withError error: NSError) {
          print(error.localizedDescription)
      }
}
