//
//  RegisterPhone_Chaperon_VC.swift
//  EliteMatch
//
//  Created by Kitlabs-M-0002 on 2/25/19.
//  Copyright © 2019 Kitlabs-M-0002. All rights reserved.
//

import UIKit
import FTIndicator
import CountryPicker

class RegisterPhone_Chaperon_VC: UIViewController {
    
    //MARK:- Outlets
    @IBOutlet weak var txtFieldUserName: UITextField!
    @IBOutlet weak var txtFieldPhoneNo: UITextField!
    @IBOutlet weak var btnCountryCode: UIButton!
    @IBOutlet weak var btn_Continue: FormButton!
    @IBOutlet weak var picker: CountryPicker!
    @IBOutlet weak var vwCountryPicker: UIView!
    @IBOutlet weak var btnFemale: UIButton!
    @IBOutlet weak var btnMale: UIButton!
    
    
    //MARK:- UIView life-cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        UserDefaults.standard.removeObject(forKey: "countryCode")
        //init
        self.txtFieldPhoneNo.delegate = self
        self.configureCountryCodePicker()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
         btn_Continue.isHidden = true
        //init
        self.navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    //MARK:- Custom Methods
    func isValid() -> Bool {
        if self.txtFieldUserName.text!.count == 0  {
            FTIndicator.showToastMessage("Enter your name")
            return false
        }
        if self.txtFieldPhoneNo.text!.count == 0 || self.txtFieldPhoneNo.text!.count < 10 {
            FTIndicator.showToastMessage("Valid Phone no. is required")
            return false
        }
        
        if self.btnMale.isSelected == false && self.btnFemale.isSelected == false {
            FTIndicator.showToastMessage("Please choose your gender!")
            return false
        }
        return true
    }
    
    func configureCountryCodePicker(){
        //init Picker
        picker.displayOnlyCountriesWithCodes = ["CA", "FR", "TR", "IN","GB","US","AE","SA","QA"]
        picker.countryPickerDelegate = self
        picker.showPhoneNumbers = true
        picker.setCountry("US")
        let theme = CountryViewTheme(countryCodeTextColor: .black, countryNameTextColor: .black, rowBackgroundColor: .clear, showFlagsBorder: false)        //optional for UIPickerView theme changes
        picker.theme = theme
        //picker.setCountry(code!)
    }
    
    func showHide_countryPicker(isHidden:Bool){
        self.vwCountryPicker.isHidden = isHidden
    }
    
    //MARK:- UIButton Actions
    @IBAction func didPressDone_countryPicker(_ sender: UIButton) {
        self.showHide_countryPicker(isHidden: true)
    }
    
    @IBAction func didPressCountryCode(_ sender: UIButton) {
        self.view.endEditing(true)
       self.showHide_countryPicker(isHidden: false)
    }
    
    @IBAction func didPressContinue(_ sender: UIButton) {
        if self.isValid() {
            var phoneNo = (self.btnCountryCode.titleLabel?.text)! + self.txtFieldPhoneNo.text!
            phoneNo = phoneNo.replacingOccurrences(of: "+", with: "", options: NSString.CompareOptions.literal, range: nil)
            phoneNo = phoneNo.replacingOccurrences(of: "-", with: "", options: NSString.CompareOptions.literal, range: nil)
            SessionManager.save_phonenumber(phoneNumber:phoneNo)
            //self.navigateVerifyOTP_VC()
            self.generateOTP()
        }
    }
    
    @IBAction func didPressBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func didPress_male(_ sender: UIButton) {
        btnMale.isSelected = true
        btnFemale.isSelected = false
    }
    
    
    @IBAction func didPress_female(_ sender: UIButton) {
        btnMale.isSelected = false
        btnFemale.isSelected = true
    }
    //MARK:- OTP
    func generateOTP(){
        
        LoadingIndicatorView.show()
        FirebaseOTPManager.shared.sendVerificationOTP(phoneno: "+" + SessionManager.get_phonenumber()) { (success) in
            if success {
                LoadingIndicatorView.hide()
                self.navigateVerifyOTP_VC()
            } else {
                LoadingIndicatorView.hide()
                FTIndicator.showError(withMessage: "Something went wrong.")
            }
        }
    }
    
    func navigateVerifyOTP_VC(){
        let registerOTP_obj = self.storyboard?.instantiateViewController(withIdentifier: "RegisterOTP_VC") as? RegisterOTP_VC
        registerOTP_obj?.phnNoToShow = (self.btnCountryCode.titleLabel?.text)! + self.txtFieldPhoneNo.text!
        registerOTP_obj?.isChaperon = true
        registerOTP_obj?.userName = self.txtFieldUserName.text ?? ""
        self.navigationController?.pushViewController(registerOTP_obj!, animated: true)
    }
    
    
    //MARK:- Web Service implementation
   /* func webserviceToGenrateOTP(){
        
        LoadingIndicatorView.show()
        let webservice  = WebserviceSigleton ()
        let dictParam : NSDictionary = ["phoneNumber":SessionManager.get_phonenumber()]
        
        webservice.POSTServiceWithParameters(urlString: constantVC.WebserviceName.URL_SEND_VERIFICATION, params: dictParam as! Dictionary<String, AnyObject>, callback:{ (result,error) -> Void in
            if result != nil{
                let response = result! as Dictionary
                let resultMessage = response["msg"] as! String
                if resultMessage == "success"{
                    if let resultOTP = response["code"] {
                        
                        if let isExistingUser = response["ExistingUser"] {
                            
                            if "\(isExistingUser)" == "true" {
                                SessionManager.save_isExistingUser(isExistingUser: true)
                            } else {
                                SessionManager.save_isExistingUser(isExistingUser: false)
                            }
                        }
                        
                        LoadingIndicatorView.hide()
                        let registerOTP_obj = self.storyboard?.instantiateViewController(withIdentifier: "RegisterOTP_VC") as? RegisterOTP_VC
                        registerOTP_obj?.otp = "\(resultOTP)"
                        registerOTP_obj?.phnNoToShow = (self.btnCountryCode.titleLabel?.text)! + self.txtFieldPhoneNo.text!
                        registerOTP_obj?.isChaperon = true
                        registerOTP_obj?.userName = self.txtFieldUserName.text ?? ""
                        self.navigationController?.pushViewController(registerOTP_obj!, animated: true)
                    }
                }
                else{
                    LoadingIndicatorView.hide()
                    FTIndicator.showError(withMessage: response["msg_details"] as! String)
                }
            }
            else{
                LoadingIndicatorView.hide()
                FTIndicator.showError(withMessage: "No internet connection")
            }
        })
    }*/
}

extension RegisterPhone_Chaperon_VC: UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let currentString: NSString = textField.text! as NSString
        let newString: NSString = currentString.replacingCharacters(in: range, with: string) as NSString
        if newString.length>=10{
            btn_Continue.isHidden = false
        }else{
            btn_Continue.isHidden = true
        }
        return newString.length <= 10
    }
}

extension RegisterPhone_Chaperon_VC: CountryPickerDelegate {
    
    func countryPhoneCodePicker(_ picker: CountryPicker, didSelectCountryWithName name: String, countryCode: String, phoneCode: String, flag: UIImage) {
        self.btnCountryCode.setTitle(phoneCode , for: .normal)
        SessionManager.saveCountryCode(code: countryCode)
    }
}

extension RegisterPhone_Chaperon_VC: MICountryPickerDelegate {
    
    func countryPicker(_ picker: MICountryPicker, didSelectCountryWithName name: String, code: String, dialCode: String) {
        self.btnCountryCode.setTitle(dialCode , for: .normal)
        self.dismiss(animated: true , completion: nil)
    }
    
    func countryPicker(_ picker: MICountryPicker, didSelectCountryWithName name: String, code: String) {
    }
}

