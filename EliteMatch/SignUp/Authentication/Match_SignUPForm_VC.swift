 //
 //  Match_SignUPForm_VC.swift
 //  Tamaas
 //
 //  Created by Kitlabs-M-0002 on 9/26/18.
 //  Copyright © 2018 Krescent Global. All rights reserved.
 //
 
 import UIKit
 import PKImagePicker
 import LinkedinSwift
 import FTIndicator
 import Alamofire
 import YYImage
 import YYCache
 import YYWebImage
 
 class Match_SignUPForm_VC: UIViewController  , UITextFieldDelegate, CropViewControllerDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    //MARK:- Outlets
    @IBOutlet weak var btn_male_me: UIButton!
    @IBOutlet weak var btn_female_me: UIButton!
    @IBOutlet weak var btn_male_you: UIButton!
    @IBOutlet weak var btn_female_you: UIButton!
    @IBOutlet weak var txtfield_name: UITextField!
    @IBOutlet weak var txtField_email: UITextField!
    @IBOutlet weak var txtField_dob: UITextField!
    @IBOutlet weak var collectionVw_img: UICollectionView!
    @IBOutlet weak var line_name: UILabel!
    @IBOutlet weak var line_email: UILabel!
    @IBOutlet weak var line_dob: UILabel!
    @IBOutlet weak var lbl_requiredName: UILabel!
    @IBOutlet weak var lbl_requiredEmail: UILabel!
    @IBOutlet weak var lbl_requiredDob: UILabel!
    @IBOutlet weak var btnBack: UIButton!
    
    //MARK:- Variables
    let imagePicker = UIImagePickerController()
    let datePicker = UIDatePicker()
    var arrToUpload : [Data] = []
    var selectedIndexPath:IndexPath?
    var arrSelectedImages:[Data] = [Data(),Data()]
    private let linkedinHelper = LinkedinSwiftHelper(configuration: LinkedinSwiftConfiguration(clientId: "81ixqjnt01t77a", clientSecret: "2aNALZCsJGF9zANd", state: "DLKDJF46ikMMZADfdfds", permissions: ["r_fullprofile", "r_emailaddress"], redirectUrl: "https://EliteMatch.com/mobileapi/"))
    var isMandatoryImage1Selected:Bool = false
    var isMandatoryImage2Selected:Bool = false
    var userIs:String = ""
    var userLookingFor:String = ""
    var objStep1:signUpStep_1?
    var navigateFromWelcomeVC = Bool()
    var instaImg = [String]()
    
    //MARK:- UIView life-cycle methods
    override func viewDidLoad() {
        super.viewDidLoad()
        self.collectionVw_img.delegate = self
        self.collectionVw_img.dataSource = self
        
        if let flowLayout = self.collectionVw_img?.collectionViewLayout as? UICollectionViewFlowLayout {
            flowLayout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
            flowLayout.itemSize = CGSize(width: self.collectionVw_img.frame.size.width/2 - 10 , height: self.collectionVw_img.frame.size.width/2 - 10)
            flowLayout.minimumInteritemSpacing = 10
            flowLayout.minimumLineSpacing = 10
        }
        self.showDatePicker()
        
        //default
        self.btn_male_me.isSelected    = false
        self.btn_female_me.isSelected  = false
        self.btn_male_you.isSelected   = false
        self.btn_female_you.isSelected = false
        
        //init data
        if self.objStep1 != nil {
            if !(self.objStep1?.fullName.trimmingCharacters(in: .whitespaces) == ""){
                 self.txtfield_name.text = self.objStep1?.fullName
            }
            if !(self.objStep1?.email.trimmingCharacters(in: .whitespaces) == ""){
                 self.txtField_email.text = self.objStep1?.email
            }
        }
        
        //pre-filled datim
        if navigateFromWelcomeVC {
            
            self.btnBack.isHidden = true
            self.btnBack.isUserInteractionEnabled = false
            self.arrSelectedImages.removeAll()
            for img in (constantVC.GlobalVariables.MyProfile?.imagesUrls)! {
                if let url = URL(string: img) {
                    downloadImage(from: url)
                }
            }
            self.txtfield_name.text = constantVC.GlobalVariables.MyProfile?.fullName
            self.txtField_email.text = constantVC.GlobalVariables.MyProfile?.email
            if constantVC.GlobalVariables.MyProfile?.userIs == "Male" {
                self.btn_male_me.isSelected = true
            } else {
                self.btn_female_me.isSelected = true
            }
            if constantVC.GlobalVariables.MyProfile?.userLookingFor == "Male" {
                self.btn_male_you.isSelected = true
            } else {
                self.btn_female_you.isSelected = true
            }
            self.txtField_dob.text = constantVC.GlobalVariables.MyProfile?.dob
            self.userIs = (constantVC.GlobalVariables.MyProfile?.userIs)!
            self.userLookingFor = (constantVC.GlobalVariables.MyProfile?.userLookingFor)!
            self.isMandatoryImage1Selected = true
            self.isMandatoryImage2Selected = true
        }
        setImagesArray()
    }
    
    func setImagesArray(){
         arrToUpload.removeAll()
        if (self.objStep1?.isInstagram)! {
            arrSelectedImages.removeAll()
            arrSelectedImages = objStep1!.instagramImg
            
            if objStep1?.instagramImg.count == 0{
                arrSelectedImages = [Data(),Data()]
            }else if objStep1?.instagramImg.count == 1{
                arrSelectedImages = objStep1!.instagramImg
                arrSelectedImages.append(Data())
                self.isMandatoryImage1Selected = true
            }
            
//            print("arrSelectedImages>>>",arrSelectedImages)
//            if objStep1?.instagramImg.count != nil {
//                self.isMandatoryImage1Selected = true
//                self.isMandatoryImage2Selected = true
//            }else{
//                arrSelectedImages = [Data(),Data()]
//            }
        }else{
            print("islinkedin")
            arrSelectedImages.removeAll()
            
            arrSelectedImages = objStep1!.inLinkedInImg
            
            if objStep1?.inLinkedInImg.count == 0{
                arrSelectedImages = [Data(),Data()]
            }else if objStep1?.inLinkedInImg.count == 1{
                arrSelectedImages.removeAll()
                
                arrSelectedImages = objStep1!.inLinkedInImg
                
                arrSelectedImages.append(Data())
                self.isMandatoryImage1Selected = true
            }
            
            print("arrSelectedImages>>>",arrSelectedImages)
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        self.lbl_requiredName.isHidden = true
        self.lbl_requiredEmail.isHidden = true
        self.lbl_requiredDob.isHidden = true
        self.txtfield_name.delegate = self
        self.txtField_email.delegate = self
        self.txtField_dob.delegate = self
    }
    
    
    //MARK:- Web service Implementations
    
    func webService_SignUpStep_one(strInstaImgs:String,user_is:String,user_looking_for:String,instagram_id:String,linkedin_id:String,name:String,email:String,dob:String,arrImages:[Data]){
        
        LoadingIndicatorView.show()
        
        Alamofire.upload(
            multipartFormData: { multipartFormData in
                
                multipartFormData.append(SessionManager.get_phonenumber().data(using: String.Encoding.utf8)!, withName: "phonenumber")
                multipartFormData.append(strInstaImgs.data(using: String.Encoding.utf8)!, withName: "insta_imgs")
                multipartFormData.append(user_is.data(using: String.Encoding.utf8)!, withName: "user_is")
                multipartFormData.append(user_looking_for.data(using: String.Encoding.utf8)!, withName: "user_looking_for")
                multipartFormData.append(instagram_id.data(using: String.Encoding.utf8)!, withName: "instagram_id")
                multipartFormData.append(linkedin_id.data(using: String.Encoding.utf8)!, withName: "linkedin_id")
                multipartFormData.append(name.data(using: String.Encoding.utf8)!, withName: "name")
                multipartFormData.append(email.data(using: String.Encoding.utf8)!, withName: "email")
                multipartFormData.append(dob.data(using: String.Encoding.utf8)!, withName: "dob")
                multipartFormData.append("step1".data(using: String.Encoding.utf8)!, withName: "lastupdated_status")
                multipartFormData.append((self.objStep1?.linkedInUrl.data(using: String.Encoding.utf8)!)!, withName: "linkedin_url")
                multipartFormData.append((self.objStep1?.instagramUrl.data(using: String.Encoding.utf8)!)!, withName: "insta_url")
                multipartFormData.append(("\(SessionManager.get_QuickBloxID() ?? 0)".data(using: String.Encoding.utf8)!), withName: "quickBloxId")
                 multipartFormData.append(("\(SessionManager.get_phonenumber()).jpeg".data(using: String.Encoding.utf8)!), withName: "newname")
                for (i,img) in arrImages.enumerated() {
                    var filename = ""
                    if i == 0 {
                        filename = SessionManager.get_phonenumber() + ".jpeg"
                    } else {
                        filename = String(i) + SessionManager.get_phonenumber() + ".jpeg"
                    }
                   /* if index == 0 {
                        multipartFormData.append(img, withName: "images[]", fileName: "\(SessionManager.get_phonenumber()).jpeg", mimeType: "image/jpeg")
                    } else {
                        multipartFormData.append(img, withName: "images[]", fileName: "\(String(NSDate().timeIntervalSince1970).replacingOccurrences(of: ".", with: "")).jpeg", mimeType: "image/jpeg")
                    }*/
                    
                     multipartFormData.append(img, withName: "images[]", fileName: filename, mimeType: "image/jpeg")
                }
        },
            to:"\(constantVC.GeneralConstants.MATCH_BASE_URL)\(constantVC.WebserviceName.URL_SIGNUP_STEP_1)",
            encodingCompletion: { encodingResult in
                
                switch encodingResult {
                case .success(let upload, _, _):
                    upload.responseJSON { response in
                        
                        guard response.result.error == nil else {
                            print("Error for POST :\(response.result.error!)")
                            LoadingIndicatorView.hide()
                            return
                        }
                        
                        if let value = response.result.value {
                            
                            if let result = value as? Dictionary<String, AnyObject> {
                                
                                print(result)
                                let resultMessage = result["msg"] as! String
                                
                                if resultMessage == "success"{
                                    
                                    if let userID = result["Userid"] {
                                        SessionManager.save_Elite_userID(id: String(describing: userID))
                                    }
                                    
                                    //create user on QB
                                    SessionManager.saveUserName(name: name)
                                    SessionManager.save_myProfileGender(value: self.userIs)
                                    NimUserManager.shared.updateUserInfo()
                                    
                                    FirebaseOTPManager.shared.callToApi_updateFCMToken(completionHandler: { (success) in
                                        LoadingIndicatorView.hide()
                                        SessionManager.saveProfileApprovalStatus(status: UserProfileStatus.pending.rawValue)
                                        SessionManager.saveProfile_lastUpdatedStatus(status: UserProfile_lastUpdated.step1.rawValue)
                                        self.openNextVC()
                                    })
                                    
                                }
                                else{
                                    LoadingIndicatorView.hide()
                                    
                                    if instagram_id != "" {
                                        FTIndicator.showError(withMessage: userMessagestext.instagramIdAlreadyUsed)
                                    } else if linkedin_id != "" {
                                        FTIndicator.showError(withMessage: userMessagestext.linkedInIdAlreadyUsed)
                                    } else {
                                        FTIndicator.showError(withMessage: result["msg_details"] as? String)
                                    }
                                }
                            }
                        }
                        }
                        .uploadProgress { progress in // main queue by default
                            print("Upload Progress: \(progress.fractionCompleted)")
                    }
                    return
                case .failure(let encodingError):
                    LoadingIndicatorView.hide()
                    debugPrint(encodingError)
                }
        })
    }
    
    //MARK:- UIButton Actions
    @IBAction func btn_gender(_ sender: UIButton) {
        if sender.tag == 1 {
            self.userIs = "Male"
            self.btn_male_me.isSelected = true
            self.btn_female_me.isSelected = false
        } else if sender.tag == 2 {
            self.userIs = "Female"
            self.btn_male_me.isSelected = false
            self.btn_female_me.isSelected = true
        } else if sender.tag == 3 {
            self.userLookingFor = "Male"
            self.btn_male_you.isSelected = true
            self.btn_female_you.isSelected = false
        }else {
            self.userLookingFor = "Female"
            self.btn_male_you.isSelected = false
            self.btn_female_you.isSelected = true
        }
    }
    
    @IBAction func btn_uploadMoreImages(_ sender: UIButton) {
        if self.selectedIndexPath != nil && self.arrSelectedImages.count >= 2  {
            let indexPath = IndexPath(row: self.arrSelectedImages.count , section: 0)
            self.selectedIndexPath = indexPath
            self.openImagePicker()
        }
    }
    
    @IBAction func btn_submit(_ sender: UIButton) {
        self.view.endEditing(true)
        
        print(self.arrToUpload.count)
        print("arrSelectedImages" , self.arrSelectedImages.count)
        
        if self.isValidate() {
            //open confirmation VC's
            self.webService_SignUpStep_one(strInstaImgs: (self.objStep1?.instagramUrl)! , user_is: self.userIs, user_looking_for: self.userLookingFor , instagram_id: (self.objStep1?.instagramID)!, linkedin_id: (self.objStep1?.linkedInID)! , name: (self.objStep1?.fullName)! , email: (self.objStep1?.email)! , dob: (self.objStep1?.dob)! , arrImages: self.arrSelectedImages)
        }
    }
    
    func openNextVC(){
        let allowPermissionVC = self.storyboard?.instantiateViewController(withIdentifier: "AllowPermission_VC") as! AllowPermission_VC
        self.navigationController?.pushViewController(allowPermissionVC, animated: true)
    }
    
    @IBAction func btn_back(_ sender: UIButton) {
        setImagesArray()
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK:- UItextField Delegates
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == self.txtfield_name {
            self.line_name.backgroundColor = UIColor(red: 235/255, green: 238/255, blue: 242/255, alpha: 1.0)
            self.lbl_requiredName.isHidden = true
        }
        if textField == self.txtField_email {
            self.line_email.backgroundColor = UIColor(red: 235/255, green: 238/255, blue: 242/255, alpha: 1.0)
            self.lbl_requiredEmail.isHidden = true
        }
        if textField == self.txtField_dob {
            self.line_dob.backgroundColor = UIColor(red: 235/255, green: 238/255, blue: 242/255, alpha: 1.0)
            self.lbl_requiredDob.isHidden = true
        }
    }
    
    //MARK:- Helpers
    func isValidate() -> Bool {
        if self.userIs == "" {
            FTIndicator.showToastMessage("Please select gender")
            return false
        }
        if self.userLookingFor == "" {
            FTIndicator.showToastMessage("Please select the gender you are looking for")
            return false
        }
        if txtField_email.text != ""{
            if !(txtField_email.text?.isValidEmail ?? false) {
                self.line_email.backgroundColor = .red
                self.lbl_requiredEmail.text = "Please enter valid email id"
                self.lbl_requiredEmail.isHidden = false
                return false
            }
        }
        if let trimmedString = self.txtfield_name.text?.trimmingCharacters(in: .whitespacesAndNewlines) {
            self.objStep1?.fullName = trimmedString
            if trimmedString.count < 1 {
                self.line_name.backgroundColor = .red
                self.lbl_requiredName.isHidden = false
                return false
            }
        }
        if let trimmedString = self.txtField_email.text?.trimmingCharacters(in: .whitespacesAndNewlines) {
            self.objStep1?.email = trimmedString
            if trimmedString.count < 1 {
                self.line_email.backgroundColor = .red
                self.lbl_requiredEmail.isHidden = false
                self.lbl_requiredEmail.text = "email id is required"
                return false
            }
        }
        if let trimmedString = self.txtField_dob.text?.trimmingCharacters(in: .whitespacesAndNewlines) {
            if trimmedString.count < 1 {
                self.line_dob.backgroundColor = .red
                self.lbl_requiredDob.isHidden = false
                return false
            }
        }
        
        if !self.isMandatoryImage1Selected {
            FTIndicator.showToastMessage("Upload your photos")
            return false
        }
        if !self.isMandatoryImage2Selected {
            FTIndicator.showToastMessage("Upload your photos")
            return false
        }
        return true
    }
    
//    func isValidEmail(testStr:String) -> Bool {
//        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
//
//        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
//        return emailTest.evaluate(with: testStr)
//    }
    
    func showDatePicker(){
        datePicker.datePickerMode = .date
        datePicker.maximumDate = Date()
        
        let toolbar = UIToolbar()
        toolbar.sizeToFit()
        
        let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.plain, target: self, action: #selector(Match_SignUPForm_VC.donedatePicker))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: UIBarButtonItemStyle.plain, target: self, action: Selector(("cancelDatePicker")))
        toolbar.setItems([cancelButton,spaceButton,doneButton], animated: false)
        self.txtField_dob.inputAccessoryView = toolbar
        self.txtField_dob.inputView = datePicker
    }
    
    @objc func donedatePicker(){
        let formatter = DateFormatter()
        formatter.dateFormat = "MM-dd-yyyy"
        self.txtField_dob.text = formatter.string(from: datePicker.date)
        //formatter.dateFormat = "MM\dd\yyyy"
        self.objStep1?.dob = formatter.string(from: datePicker.date)
        self.view.endEditing(true)
    }
    
    func cancelDatePicker(){
        self.view.endEditing(true)
    }
    
    func openImagePicker(){
        self.view.endEditing(true)
        imagePicker.sourceType = .photoLibrary
        imagePicker.allowsEditing = false
        imagePicker.delegate = self
        self.present(imagePicker, animated: true, completion: nil)
    }
        
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        guard let image = info[UIImagePickerControllerOriginalImage] as? UIImage else { return }
        let cropController = CropViewController(croppingStyle: CropViewCroppingStyle.default, image: image)
        cropController.aspectRatioPreset = .preset4x3
        cropController.cropView.aspectRatio = CGSize(width: self.view.frame.width + 200 , height: 300)
        cropController.cropView.maximumZoomScale = 2.0
        cropController.cropView.cropBoxResizeEnabled = false
        cropController.delegate = self
        picker.pushViewController(cropController, animated: true)
    }
 }
 
 //MARK:- UICollectionView data-source and delegates
 extension Match_SignUPForm_VC: UICollectionViewDelegate , UICollectionViewDataSource , UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.arrSelectedImages.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath as IndexPath) as! Match_SignUPForm_CollectionCell
        cell.imgVw_profile.image = (UIImage(data: arrSelectedImages[indexPath.row]))
        self.arrToUpload.append(arrSelectedImages[indexPath.row])
        
        if indexPath.row > 1 || self.navigateFromWelcomeVC {
            cell.imgVw_avatar.isHidden = true
        }
        
        //instagram
        print(self.objStep1?.instagramImg.count)
        if self.objStep1!.isInstagram {
            if (objStep1?.instagramImg.indices.contains(indexPath.row))! {
                cell.imgVw_avatar.isHidden = true
            } else {
                cell.imgVw_avatar.isHidden = false
            }
            if let _ = self.selectedIndexPath?.row {
                cell.imgVw_avatar.isHidden = true
            }
//            if self.arrSelectedImages.indices.contains(self.selectedIndexPath.row) {
//                cell.imgVw_avatar.isHidden = true
//            }
        }
        else {
            if (objStep1?.inLinkedInImg.indices.contains(indexPath.row))! {
                cell.imgVw_avatar.isHidden = true
            } else {
                cell.imgVw_avatar.isHidden = false
            }
        }
        
        
        
      /*  if objStep1?.instagramImg.count != nil {
            cell.imgVw_avatar.isHidden = true
        }else{
           cell.imgVw_avatar.isHidden = false
        }
        if indexPath.row == 1 {
            if objStep1?.inLinkedInImg.count == 1{
               cell.imgVw_avatar.isHidden = false
            }
            if objStep1?.instagramImg.count == 2 {
               cell.imgVw_avatar.isHidden = true
            }
            else {
                cell.imgVw_avatar.isHidden = false
            }
        }*/
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        self.selectedIndexPath = indexPath
        self.openImagePicker()
    }
    
    
    public func cropViewController(_ cropViewController: CropViewController, didCropToImage image: UIImage, withRect cropRect: CGRect, angle: Int) {
        
        if selectedIndexPath?.row == 0 {
            self.isMandatoryImage1Selected = true
        }
        if selectedIndexPath?.row == 1 {
            self.isMandatoryImage2Selected = true
        }
        
        if self.selectedIndexPath?.row ?? 0 < self.arrSelectedImages.count  {
            if let cell = self.collectionVw_img.cellForItem(at: self.selectedIndexPath ?? IndexPath()) as? Match_SignUPForm_CollectionCell {
                print(self.selectedIndexPath?.row)
                
                if let imageData = UIImageJPEGRepresentation(image, 0.5) {
                    self.arrSelectedImages[self.selectedIndexPath?.row ?? 0] = imageData
                    self.arrToUpload.append(imageData)
                }
                
                cell.imgVw_avatar.isHidden = true
                cell.imgVw_profile.image = image
            }
        } else {
           
            self.collectionVw_img?.performBatchUpdates({
                
                if let imageData = UIImageJPEGRepresentation(image, 0.5) {
                    self.arrSelectedImages.insert(imageData , at: self.selectedIndexPath?.row ?? 0)
                    self.arrToUpload.append(imageData)
                }
                
                self.collectionVw_img?.insertItems(at: [(self.selectedIndexPath ?? IndexPath())])
            }, completion: nil)
            
            self.collectionVw_img?.scrollToItem(at: self.selectedIndexPath ?? IndexPath() , at: UICollectionViewScrollPosition.right, animated: true)
        }
        imagePicker.dismiss(animated: true, completion: nil)
    }
    
    public func cropViewController(_ cropViewController: CropViewController, didCropToCircularImage image: UIImage, withRect cropRect: CGRect, angle: Int) {
        
    }
    
    /* func collectionView(_ collectionView: UICollectionView,
     layout collectionViewLayout: UICollectionViewLayout,
     sizeForItemAt indexPath: IndexPath) -> CGSize {
     
     return CGSize(width: self.collectionVw_img.frame.size.width/2 - 10.0, height: self.collectionVw_img.frame.size.height);
     }
     
     func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
     return 10.0
     }*/
 }
 
 
 
 //MARK:- UICollectionView Cell
 class Match_SignUPForm_CollectionCell:UICollectionViewCell {
    
    @IBOutlet weak var imgVw_avatar: UIImageView!
    @IBOutlet weak var imgVw_profile: UIImageView!
    
 }
 
 
 extension Match_SignUPForm_VC {
    func getData(from url: URL, completion: @escaping (Data?, URLResponse?, Error?) -> ()) {
        URLSession.shared.dataTask(with: url, completionHandler: completion).resume()
    }
    
    func downloadImage(from url: URL) {
        print("Download Started")
        getData(from: url) { data, response, error in
            guard let data = data, error == nil else { return }
            self.arrSelectedImages.append(data)
            DispatchQueue.main.async {
                self.collectionVw_img.reloadData()
            }
        }
    }
 }
 
