//
//  HowItWorks_VC.swift
//  Tamaas
//
//  Created by Kitlabs-M-0002 on 10/12/18.
//  Copyright © 2018 Krescent Global. All rights reserved.
//

import UIKit

class HowItWorks_VC: UIViewController {

    //MARK:- UIView life-cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    //MARK:- UIButton Actions
    @IBAction func btn_back(_ sender: UIButton) {
        self.dismiss(animated: true , completion: nil)
    }
    
}
