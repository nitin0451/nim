//
//  AllowPermission_VC.swift
//  EliteMatch
//
//  Created by Apple on 11/09/19.
//  Copyright © 2019 Kitlabs-M-0002. All rights reserved.
//

import UIKit
import Contacts
import FTIndicator
import CoreLocation

class AllowPermission_VC: UIViewController {
    
    //MARK:- Outlets
    @IBOutlet weak var vwLocation: UIView!
    @IBOutlet weak var vwContacts: UIView!
    @IBOutlet weak var switchLocation: UISwitch!
    @IBOutlet weak var switchConatcts: UISwitch!
    
    //MARK:- Variables
    var locationManager:CLLocationManager!
    
    
    //MARK:- UIView life-cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()

        //init
        self.configureView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.configurePermissions()
    }
    
    
    //MARK:- Custom Methods
    func configureView(){
        self.vwLocation.layer.borderColor = UIColor.lightGray.cgColor
        self.vwLocation.layer.borderWidth = 1.0
        self.vwContacts.layer.borderColor = UIColor.lightGray.cgColor
        self.vwContacts.layer.borderWidth = 1.0
    }
    
    func configurePermissions(){
        if hasLocationPermission(){
            self.switchLocation.isOn = true
        } else {
            self.switchLocation.isOn = false
        }
        
        if hasContactsPermission(){
            self.switchConatcts.isOn = true
        } else {
            self.switchConatcts.isOn = false
        }
    }
    
    func hasLocationPermission() -> Bool {
        var hasPermission = false
        if CLLocationManager.locationServicesEnabled() {
            switch CLLocationManager.authorizationStatus() {
            case .notDetermined, .restricted, .denied:
                hasPermission = false
            case .authorizedAlways, .authorizedWhenInUse:
                hasPermission = true
            }
        } else {
            hasPermission = false
        }
        
        return hasPermission
    }
    
    func hasContactsPermission() ->Bool {
        
        var hasPermission = false
        
        switch CNContactStore.authorizationStatus(for: .contacts) {
        case .authorized:
            hasPermission = true
        case .denied:
            hasPermission = false
        case .restricted, .notDetermined:
            hasPermission = false
        }
        
        return hasPermission
    }
    
    func openNextVC(){
        CommonFunctions.shared.initializeUser()
        let MatchSignUpConfirmation_VC = self.storyboard?.instantiateViewController(withIdentifier: "Match_SignUp_Confirmation_VC") as! Match_SignUp_Confirmation_VC
        self.navigationController?.pushViewController(MatchSignUpConfirmation_VC, animated: true)
    }
    
    func determineMyCurrentLocation() {
        locationManager = CLLocationManager()
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        
        let authStatus = CLLocationManager.authorizationStatus()
        if authStatus == .notDetermined {
            locationManager.requestWhenInUseAuthorization()
        }
        
        if authStatus == .denied || authStatus == .restricted {
            FTIndicator.showInfo(withMessage: "Enable location services from device settings to fetch current location.")
        }
        
        if CLLocationManager.locationServicesEnabled() {
            locationManager.startUpdatingLocation()
        }
    }
  
    //MARK:- UIButton Actions
    @IBAction func didPress_locationSwitch(_ sender: UISwitch) {
        
       /* if sender.isOn {
            let alertController = UIAlertController(title: "Location Permission Required", message: "Please enable location permissions in settings.", preferredStyle: UIAlertControllerStyle.alert)
            
            let okAction = UIAlertAction(title: "Settings", style: .default, handler: {(cAlertAction) in
                //Redirect to Settings app
                UIApplication.shared.open(URL(string:UIApplicationOpenSettingsURLString)!)
            })
            
            let cancelAction = UIAlertAction(title: "Cancel" , style: UIAlertActionStyle.cancel) { (action) in
                self.switchLocation.isOn = false
            }
            
            alertController.addAction(cancelAction)
            alertController.addAction(okAction)
            self.present(alertController, animated: true, completion: nil)
        }*/
        
       
        self.determineMyCurrentLocation()
        
      /*  if !hasLocationPermission() {
            let alertController = UIAlertController(title: "Location Permission Required", message: "Please enable location permissions in settings.", preferredStyle: UIAlertControllerStyle.alert)
            
            let okAction = UIAlertAction(title: "Settings", style: .default, handler: {(cAlertAction) in
                //Redirect to Settings app
                UIApplication.shared.open(URL(string:UIApplicationOpenSettingsURLString)!)
            })
            
            let cancelAction = UIAlertAction(title: "Cancel" , style: UIAlertActionStyle.cancel) { (action) in
                self.switchLocation.isOn = false
            }
            
            alertController.addAction(cancelAction)
            alertController.addAction(okAction)
            self.present(alertController, animated: true, completion: nil)
        }*/
        
    }
    

    @IBAction func didPress_contactsSwitch(_ sender: UISwitch) {
        
        if !hasContactsPermission() {
            /*let alertController = UIAlertController(title: "Contacts Permission Required", message: "Please enable Contacts permissions in settings.", preferredStyle: UIAlertControllerStyle.alert)
            
            let okAction = UIAlertAction(title: "Settings", style: .default, handler: {(cAlertAction) in
                //Redirect to Settings app
                UIApplication.shared.open(URL(string:UIApplicationOpenSettingsURLString)!)
            })
            
            let cancelAction = UIAlertAction(title: "Cancel" , style: UIAlertActionStyle.cancel) { (action) in
                self.switchConatcts.isOn = false
            }
            alertController.addAction(cancelAction)
            alertController.addAction(okAction)
            self.present(alertController, animated: true, completion: nil)*/
            
            let contactsStore = CNContactStore()
            contactsStore.requestAccess(for: CNEntityType.contacts, completionHandler: { (granted, error) -> Void in
                //At this point an alert is provided to the user to provide access to contacts. This will get invoked if a user responds to the alert
                DispatchQueue.main.async {
                    if (!granted ){
                        self.switchConatcts.isOn = false
                        self.showContactsAlert()
                    } else {
                        self.switchConatcts.isOn = true
                        self.showContactsAlertForSaving()
                    }
                }
                
            })
        }
    }
    
    
    func showContactsAlert() {
        let alertController = UIAlertController(title: "Contacts Permission Required", message: "Please enable Contacts permissions in settings.", preferredStyle: UIAlertControllerStyle.alert)
        
        let okAction = UIAlertAction(title: "Settings", style: .default, handler: {(cAlertAction) in
            //Redirect to Settings app
            UIApplication.shared.open(URL(string:UIApplicationOpenSettingsURLString)!)
        })
        
        let cancelAction = UIAlertAction(title: "Cancel" , style: UIAlertActionStyle.cancel) { (action) in
            self.switchConatcts.isOn = false
        }
        alertController.addAction(cancelAction)
        alertController.addAction(okAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    func showContactsAlertForSaving() {
        let alertController = UIAlertController(title: "Contact Permission!", message: "NIM needs to save your contacts to the server to give a better Match experience. We will not share or use your data for any other use.", preferredStyle: UIAlertControllerStyle.alert)
        
        let okAction = UIAlertAction(title: "Allow", style: .default, handler: {(cAlertAction) in
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            appDelegate.updateAddressBook()
            UserDefaults.standard.setValue(true, forKey: "Permission")
        })
        let cancelAction = UIAlertAction(title: "Not Now" , style: UIAlertActionStyle.cancel) { (action) in
            UserDefaults.standard.setValue(false, forKey: "Permission")
        }
        alertController.addAction(cancelAction)
        alertController.addAction(okAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    @IBAction func didPress_continue(_ sender: UIButton) {
        DispatchQueue.main.async {
            self.openNextVC()
        }
    }
}
