//
//  Link_LinkedInOrInsta_VC.swift
//  EliteMatch
//
//  Created by Kitlabs-M-0002 on 11/15/18.
//  Copyright © 2018 Kitlabs-M-0002. All rights reserved.
//

import UIKit
import LinkedinSwift
import FTIndicator
import Alamofire

class signUpStep_1: NSObject {
    var firstName:String = ""
    var lastName:String = ""
    var fullName:String = ""
    var email:String = ""
    var linkedInID:String = ""
    var instagramID:String = ""
    var linkedInUrl:String = ""
    var instagramUrl:String = ""
    var instaImages:String = ""
    var instagramImg:[Data] = []
    var inLinkedInImg:[Data] = []
    var dob:String = ""
    var isInstagram:Bool = false
    var headlineTitle:String = ""
}

class Link_LinkedInOrInsta_VC: UIViewController {
  
    //MARK:- Variables
     let linkedinHelper = LinkedinSwiftHelper(configuration: LinkedinSwiftConfiguration(clientId: "81ixqjnt01t77a", clientSecret: "2aNALZCsJGF9zANd", state: "linkedin\(Int(NSDate().timeIntervalSince1970))", permissions: ["r_liteprofile,r_emailaddress"], redirectUrl: "https://com.appcoda.linkedin.oauth/oauth"), nativeAppChecker: WebLoginOnly())
    
    var arrInstagramImagesUrl:[String] = []
    var objStep1 = signUpStep_1()
    var getinstaGramImg = [String]()
    
    //MARK:- UIView life-cycle methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if constantVC.GlobalVariables.isLinkedInAccessTokenFetched {
            constantVC.GlobalVariables.isLinkedInAccessTokenFetched = false
             // self.getLinkedInProfileData()
        }
    }
    
    //MARK:- UIButton Actions
    func requestProfile() {
   
        linkedinHelper.requestURL("https://api.linkedin.com/v2/me", requestType: LinkedinSwiftRequestGet, success: { (response) -> Void in
      
            if let result = response.jsonObject {
                if let firstName = result["localizedFirstName"] as? String{
                    self.objStep1.firstName = firstName
                }
                if let lastName = result["lastName"] as? String{
                    self.objStep1.lastName = lastName
                }
                
                self.objStep1.fullName = self.objStep1.firstName + " " + self.objStep1.lastName
                
                if let id = result["id"] as? String {
                    self.objStep1.linkedInID = id
                    self.objStep1.linkedInUrl = "www.linkedin.com/in/" + "{" + id + "}"

                }
                DispatchQueue.main.async {
                    FTIndicator.dismissProgress()
                    self.requestEmail()
                }
 
            }
            
        }) { [unowned self] (error) -> Void in
            
          
        }
    }
     func requestEmail() {
        linkedinHelper.requestURL("https://api.linkedin.com/v2/emailAddress?q=members&projection=(elements*(handle~))", requestType: LinkedinSwiftRequestGet, success: { (response) -> Void in
            
            print(response)
            
            if let result = response.jsonObject {
                
                if let elements = result["elements"] as? [[String: AnyObject]] {
                    for items in elements{
                        if let handle = items["handle~"] as? [String: Any]{
                            
                            if let emailAddress = handle["emailAddress"] as? String{
                                
                                self.objStep1.email = emailAddress
                            }
                        }
                    }
                  
                }
                DispatchQueue.main.async {
                    FTIndicator.dismissProgress()
                    self.requestImage()
                }
                
            }
            
        }) { [unowned self] (error) -> Void in
            print(error.localizedDescription)
        }
    }
    
    func requestImage(){
    linkedinHelper.requestURL("https://api.linkedin.com/v2/me?projection=(id,firstName,lastName,profilePicture(displayImage~:playableStreams))", requestType: LinkedinSwiftRequestGet, success: { (response) -> Void in
        
        if let result = response.jsonObject {
            
             self.objStep1.inLinkedInImg = []
                if let profilePicture = result["profilePicture"] as? [String: Any] {
                    print(profilePicture)
                    if let identifier = profilePicture["displayImage~"] as? [String: Any]{
                        
                        if let elements = identifier["elements"] as? [[String:Any]]{
                            
                            //for itemsss in elements{
                               
                            if let itemElement = elements[elements.count-1]["identifiers"] as? [[String:Any]]{
                              
                               // for digitalMediaItem in itemElement{
                                    if let digitalMedia = itemElement[itemElement.count-1]["identifier"] as? String{
//                                        self.objStep1.linkedInUrl = digitalMedia
                                        let imageUrl = URL(string: digitalMedia )!
                                        let imageData = try! Data(contentsOf: imageUrl)
                                        self.objStep1.inLinkedInImg.append(imageData)
                                        
                                }
//                                                    }
                         
                            }
                                
                          //  }
                            
                        }
                    }
                }
       
            DispatchQueue.main.async {
                FTIndicator.dismissProgress()
                self.openStep1_VC()
            }
            
        }
        
    }) { [unowned self] (error) -> Void in
            
            
        }
    }
    
    @IBAction func btn_linkedIn(_ sender: UIButton) {
        self.objStep1.isInstagram = false
       
        //when access token available
        linkedinHelper.authorizeSuccess({ [unowned self] (lsToken) -> Void in
            self.requestProfile()
            
            }, error: { [unowned self] (error) -> Void in
                
            }, cancel: { [unowned self] () -> Void in
        })
    }
    
    @IBAction func btn_Instagram(_ sender: UIButton) {
        FacebookSignInManager.basicInfoWithCompletionHandler(self) { (dataDictionary:Dictionary<String, AnyObject>?, error:NSError?) -> Void in
            
            if error == nil {
                
                 print(dataDictionary)
                
                //save data
                if let dicResponse = dataDictionary {
                    if let dicPicture = dicResponse["picture"] as? NSDictionary {
                        if let data = dicPicture.object(forKey: "data") as? NSDictionary {
                            if let url = data.object(forKey: "url") as? String{
                                self.objStep1.instagramUrl = "\(url)"
                                self.objStep1.instaImages = "\(url)"
                                let imageData = try! Data(contentsOf: URL.init(string: "\(url)")!)
                                self.objStep1.instagramImg.append(imageData)
                            }
                        }
                    }
                    self.objStep1.isInstagram = true
                    if let fbID = dicResponse["id"] as? String {
                        self.objStep1.instagramID = fbID
                    }
                    if let name = dicResponse["name"] as? String {
                        self.objStep1.fullName = name
                    } else {
                        var name = ""
                        if let firstName = dicResponse["first_name"] as? String {
                            name = firstName
                            if let lastName = dicResponse["last_name"] as? String {
                                name = name + " " + lastName
                            }
                        }
                        self.objStep1.fullName = name
                    }
                    if let email = dicResponse["email"] as? String {
                        self.objStep1.email = email
                    }
                    self.openStep1_VC()
                }
            }
        }
    }
    
    @IBAction func btn_back(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK:- Custom Methods
    func openStep1_VC() {
        let step1_obj = self.storyboard?.instantiateViewController(withIdentifier: "Match_SignUPForm_VC") as? Match_SignUPForm_VC
        step1_obj?.objStep1 = self.objStep1
        self.navigationController?.pushViewController(step1_obj!, animated: true)
    }
    
    func showInstallLinkedInAppAlert(){
        let alert = UIAlertController(title: "Install LinkedIn app", message: "To connect with LinkedIn you need to install LinkedIn app. Would you like to do it?", preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (action: UIAlertAction!) in
        }))
        alert.addAction(UIAlertAction(title: "Install", style: .default, handler: { (action: UIAlertAction!) in
            UIApplication.shared.open( URL(string: "https://itunes.apple.com/in/app/linkedin/id288429040?mt=8")! , options: [:] , completionHandler: nil)
        }))
        present(alert, animated: true, completion: nil)
    }
}

