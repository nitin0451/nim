//
//  RegisterPhoneNo_VC.swift
//  EliteMatch
//
//  Created by Kitlabs-M-0002 on 11/15/18.
//  Copyright © 2018 Kitlabs-M-0002. All rights reserved.
//

import UIKit
import FTIndicator
import CountryPicker


class RegisterPhoneNo_VC: UIViewController  {
    
    //MARK:- Outlets
    @IBOutlet weak var mobileNumber: UITextField!
    @IBOutlet weak var btn_countryCode: UIButton!
    @IBOutlet weak var btn_Continue: FormButton!
    @IBOutlet weak var picker: CountryPicker!
    @IBOutlet weak var vwCountryPicker: UIView!
    
    //MARK:- UIView life-cycel methods
    override func viewDidLoad() {
        super.viewDidLoad()
         btn_Continue.isHidden = true
        UserDefaults.standard.removeObject(forKey: "countryCode")
        
        //init
        self.mobileNumber.delegate = self
        self.configureCountryCodePicker()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
       
        //init
        self.navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    //MARK:- Custom Methods
    func isValid() -> Bool {
        
        if mobileNumber.text!.count == 0 || mobileNumber.text!.count < 8 {
            FTIndicator.showToastMessage("Valid Phone no. is required")
            return false
        }
        return true
    }
    
    func configureCountryCodePicker(){
        //init Picker
        picker.displayOnlyCountriesWithCodes = ["CA", "FR", "TR", "IN","GB","US","AE","SA","QA","MY","SG"]
        picker.countryPickerDelegate = self
        picker.showPhoneNumbers = true
        picker.setCountry("US")
        let theme = CountryViewTheme(countryCodeTextColor: .black, countryNameTextColor: .black, rowBackgroundColor: .clear, showFlagsBorder: false)        //optional for UIPickerView theme changes
        picker.theme = theme
        //picker.setCountry(code!)
    }
    
    func showHide_countryPicker(isHidden:Bool){
        self.vwCountryPicker.isHidden = isHidden
    }
    
    //MARK:- UIButton Actions
    
    @IBAction func didPressDone_countryPicker(_ sender: UIButton) {
        self.showHide_countryPicker(isHidden: true)
    }
    
    @IBAction func btn_back(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btn_pickCountryCode(_ sender: UIButton) {
        self.view.endEditing(true)
        self.showHide_countryPicker(isHidden: false)
    }
    
    @IBAction func btn_continue(_ sender: UIButton) {
        if self.isValid() {
            mobileNumber.resignFirstResponder()
            var phoneNo = (self.btn_countryCode.titleLabel?.text)! + self.mobileNumber.text!
            phoneNo = phoneNo.replacingOccurrences(of: "+", with: "", options: NSString.CompareOptions.literal, range: nil)
            phoneNo = phoneNo.replacingOccurrences(of: "-", with: "", options: NSString.CompareOptions.literal, range: nil)
            SessionManager.save_phonenumber(phoneNumber:phoneNo)
          //  self.navigateVerifyOTP_VC()
            self.generateOTP()
        }
    }
    
    @IBAction func didPressChaperonSignUp(_ sender: UIButton) {
        let SignUpChaperon_VC = self.storyboard?.instantiateViewController(withIdentifier: "RegisterPhone_Chaperon_VC") as! RegisterPhone_Chaperon_VC
        self.navigationController?.pushViewController(SignUpChaperon_VC, animated: false)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.navigationBar.isHidden = false
    }
    
    
    //MARK:- OTP
    func generateOTP(){
      
        LoadingIndicatorView.show()
        FirebaseOTPManager.shared.sendVerificationOTP(phoneno: "+" + SessionManager.get_phonenumber()) { (success) in
            LoadingIndicatorView.hide()
            if success {
                self.navigateVerifyOTP_VC()
            } else {
                FTIndicator.showError(withMessage: "Something went wrong.")
            }
        }
    }
    
    func navigateVerifyOTP_VC(){
        
        if let registerOTP_obj = self.storyboard?.instantiateViewController(withIdentifier: "RegisterOTP_VC") as? RegisterOTP_VC {
            registerOTP_obj.phnNoToShow = (self.btn_countryCode.titleLabel?.text)! + self.mobileNumber.text!
            self.navigationController?.pushViewController(registerOTP_obj, animated: true)
        }
        
    }
    
    
    //MARK:- Web Service implementation
   /* func webserviceToGenrateOTP(){
        
        LoadingIndicatorView.show()
        let webservice  = WebserviceSigleton ()
        let dictParam : NSDictionary = ["phoneNumber":SessionManager.get_phonenumber()]
        
        webservice.POSTServiceWithParameters(urlString: constantVC.WebserviceName.URL_SEND_VERIFICATION, params: dictParam as! Dictionary<String, AnyObject>, callback:{ (result,error) -> Void in
            if result != nil{
                let response = result! as Dictionary
                let resultMessage = response["msg"] as! String
                if resultMessage == "success"{
                    if let resultOTP = response["code"] {
                        
                        if let isExistingUser = response["ExistingUser"] {
                            
                            if "\(isExistingUser)" == "true" {
                                SessionManager.save_isExistingUser(isExistingUser: true)
                            } else {
                                SessionManager.save_isExistingUser(isExistingUser: false)
                            }
                        }
                        
                        LoadingIndicatorView.hide()
                        let registerOTP_obj = self.storyboard?.instantiateViewController(withIdentifier: "RegisterOTP_VC") as? RegisterOTP_VC
                        registerOTP_obj?.otp = "\(resultOTP)"
                        registerOTP_obj?.phnNoToShow = (self.btn_countryCode.titleLabel?.text)! + self.mobileNumber.text!
                        self.navigationController?.pushViewController(registerOTP_obj!, animated: true)
                    }
                }
                else{
                    LoadingIndicatorView.hide()
                    FTIndicator.showError(withMessage: response["msg_details"] as! String)
                }
            }
            else{
                LoadingIndicatorView.hide()
                FTIndicator.showError(withMessage: "No internet connection")
            }
        })
    }*/
}
extension RegisterPhoneNo_VC: UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let currentString: NSString = textField.text! as NSString
        let newString: NSString = currentString.replacingCharacters(in: range, with: string) as NSString
        if newString.length>=10{
            btn_Continue.isHidden = false
        }else{
           btn_Continue.isHidden = true
        }
        return newString.length <= 10
    }
}

extension RegisterPhoneNo_VC: CountryPickerDelegate {
    
    func countryPhoneCodePicker(_ picker: CountryPicker, didSelectCountryWithName name: String, countryCode: String, phoneCode: String, flag: UIImage) {
        self.btn_countryCode.setTitle(phoneCode , for: .normal)
        SessionManager.saveCountryCode(code: countryCode)
    }
}

extension RegisterPhoneNo_VC: MICountryPickerDelegate {
    
    func countryPicker(_ picker: MICountryPicker, didSelectCountryWithName name: String, code: String, dialCode: String) {
        self.btn_countryCode.setTitle(dialCode , for: .normal)
        SessionManager.saveCountryCode(code: code)
        self.dismiss(animated: true , completion: nil)
    }
    
    func countryPicker(_ picker: MICountryPicker, didSelectCountryWithName name: String, code: String) {
    }
    
}

