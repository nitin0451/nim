//
//  Match_SignUp_Confirmation_VC.swift
//  Tamaas
//
//  Created by Kitlabs-M-0002 on 10/1/18.
//  Copyright © 2018 Krescent Global. All rights reserved.
//

import UIKit
import FTIndicator

class Match_SignUp_Confirmation_VC: UIViewController {

    //MARK:- Outlets
    @IBOutlet weak var vw_navigationBar: UIView!
    @IBOutlet weak var vw_decision: UIView!
    @IBOutlet weak var vw_understand: UIView!
    @IBOutlet weak var vw_completeProfile: UIView!
    @IBOutlet weak var btn_NotiBell: UIButton!
    @IBOutlet weak var lbl_decisiontext: UILabel!
    @IBOutlet weak var lbl_understandText: UILabel!
    @IBOutlet weak var lbl_completeProfileText: UILabel!
    @IBOutlet weak var lbl_completeProfileCongratulations: UILabel!
    @IBOutlet weak var vw_doneSignUp: UIView!
    @IBOutlet weak var lbl_signUpDoneText: UILabel!
    @IBOutlet weak var lbl_signUpDone_success: UILabel!
    @IBOutlet weak var btnUnderstand: UIButton!
    
    //MARK:- Variables
    var tagBtnTapped:Int = 0
    let notificationButton = Custom_Notification_BarButton()
    var isSignUpDoneNavigate:Bool = false
    var isSignUpCompleteProfileNavigate:Bool = false
    var navigateFromWelcomeVC = Bool()
    var objStep1:signUpStep_1?
    //new
    var isProfileApproved:Bool = false
    var backBtn = UIBarButtonItem()
    
    //MARK:- UIView life-cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.configureNavBar()
        navigationController?.navigationBar.backgroundColor = UIColor.white
        //init
        self.setAttributedText(label: self.lbl_decisiontext)
        self.setAttributedText(label: self.lbl_understandText)
        self.setAttributedText(label: self.lbl_completeProfileText)
        self.lbl_completeProfileCongratulations.textColor = .black
        self.configureNotificationBell()
        self.hideAllViews()
        if self.isSignUpDoneNavigate {
            self.isSignUpDoneNavigate = false
            self.show_SignDoneView()
        }
        else if self.isProfileApproved {
            self.show_completeProfileView()
        }
//        if self.isSignUpCompleteProfileNavigate {
//            self.isSignUpCompleteProfileNavigate = false
//            self.show_completeProfileView()
//        }
        else {
            self.show_decisionView()
        }
    }
    
    func setAttributedText(label: UILabel){
        let attributedString = NSMutableAttributedString(string: label.text!)
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineSpacing = 4
        attributedString.addAttribute(NSAttributedStringKey.paragraphStyle, value:paragraphStyle, range:NSMakeRange(0, attributedString.length))
        label.attributedText = attributedString
        label.textColor = UIColor(red: 94.0/255.0, green: 94.0/255.0, blue: 94.0/255.0, alpha: 1.0)
        label.textAlignment = .center
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    
    //MARK:- Helpers
    func configureNavBar() {
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.view.backgroundColor = .clear
        self.navigationController?.navigationItem.hidesBackButton = true
        navigationController?.navigationBar.barTintColor = .gray
        
        //add back button
        backBtn = UIBarButtonItem(image: UIImage(named: "arrowleft_black"), style: .plain, target: self, action: #selector(Match_SignUp_Confirmation_VC.btn_back))
        backBtn.tintColor = .gray
        self.navigationItem.leftBarButtonItem  = backBtn
        self.navigationItem.leftBarButtonItem?.tintColor = .gray
    }
    
    func configureNotificationBell() {
        notificationButton.frame = CGRect(x: 0, y: 0, width: 44, height: 44)
        notificationButton.setImage(UIImage(named: "notification_bell")?.withRenderingMode(.alwaysTemplate), for: .normal)
        notificationButton.badgeEdgeInsets = UIEdgeInsets(top: 20, left: 0, bottom: 0, right: 15)
        notificationButton.addTarget(self, action: #selector(btn_notificationBell_action), for: .touchUpInside)
        notificationButton.tintColor = .gray
       // let profileBtn = UIBarButtonItem(image: UIImage(named: "blackBack"), style: .plain, target: self, action: #selector(Match_SignUp_Confirmation_VC.btn_profile))
       // self.navigationItem.rightBarButtonItems = [UIBarButtonItem(customView: notificationButton) , profileBtn]
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: notificationButton)
    }
    
    func hideAllViews(){
        self.vw_decision.isHidden = true
        self.vw_understand.isHidden = true
        self.vw_completeProfile.isHidden = true
        self.vw_doneSignUp.isHidden = true
    }
    
    func show_SignDoneView() {
        self.navigationController?.navigationBar.isHidden = true
        self.tagBtnTapped = 4
        self.hideAllViews()
        self.vw_doneSignUp.alpha = 0
        self.vw_doneSignUp.isHidden = false
        UIView.animate(withDuration: 0.3) {
            self.vw_doneSignUp.alpha = 1
        }
    }
    
    func show_understandView() {
        
        self.backBtn.isEnabled = true
        self.backBtn.tintColor = .gray
        
        self.tagBtnTapped = 2
        self.hideAllViews()
        self.vw_understand.alpha = 0
        self.vw_understand.isHidden = false
        UIView.animate(withDuration: 0.3) {
            self.vw_understand.alpha = 1
            self.btnUnderstand.isHidden = false
        }
    }
    
    func show_decisionView() {
        self.backBtn.isEnabled = false
        self.backBtn.tintColor = .clear
        self.tagBtnTapped = 1
        self.hideAllViews()
        self.vw_decision.alpha = 0
        self.vw_decision.isHidden = false
        UIView.animate(withDuration: 0.3) {
            self.vw_decision.alpha = 1
        }
    }
    
    func show_completeProfileView() {
        
        self.backBtn.isEnabled = false
        self.backBtn.tintColor = .clear
        
        self.tagBtnTapped = 3
        self.hideAllViews()
        self.vw_completeProfile.alpha = 0
        self.vw_completeProfile.isHidden = false
        UIView.animate(withDuration: 0.3) {
            self.vw_completeProfile.alpha = 1
        }
    }   
    
    func openMessageRootVC(){
        self.view.window?.rootViewController?.dismiss(animated: true, completion: nil)
    }
    

    //MARK:- UIButton Actions
    
    @IBAction func didPress_learnMore(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "WebView_VC") as! WebView_VC
        vc.strTitle = "About Us"
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    @objc func btn_back(){
        if self.tagBtnTapped == 1 {
            if self.navigateFromWelcomeVC {
                let step1_obj = self.storyboard?.instantiateViewController(withIdentifier: "Match_SignUPForm_VC") as? Match_SignUPForm_VC
                step1_obj?.navigateFromWelcomeVC = true
                step1_obj?.objStep1 = self.objStep1
                self.navigationController?.pushViewController(step1_obj!, animated: true)
            } else {
                let transition = CATransition()
                transition.duration = 0.25
                transition.type = kCATransitionPush
                transition.subtype = kCAOnOrderIn
                self.view.window?.layer.add(transition, forKey: kCATransition)
                self.navigationController?.popViewController(animated: false)
            }
        } else if self.tagBtnTapped == 2 {
            self.show_decisionView()
        }else if self.tagBtnTapped == 3 {
            self.show_understandView()
        }else if self.tagBtnTapped == 4 {
            let transition = CATransition()
            transition.duration = 0.25
            transition.type = kCATransitionPush
            transition.subtype = kCAOnOrderIn
            self.view.window?.layer.add(transition, forKey: kCATransition)
            self.navigationController?.popViewController(animated: false)
        }
    }
    
    @objc func btn_notificationBell_action(sender: UIButton!) {
        print("Button tapped")
    }
   
    @IBAction func btn_decision(_ sender: UIButton) {
        self.show_understandView()
    }
    
    @IBAction func btn_understand(_ sender: UIButton) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "InviteFriends_VC") as! InviteFriends_VC
        if let topVC = UIApplication.topViewController() {
            topVC.present(controller , animated: true , completion: {
                self.navigationController?.popViewController(animated: false)
            })
        }
//        if CommonFunctions.shared.isTestAccount() {
//            UserConfirmationAlertManager.instance.showAlert_testAccount { (success) in
//                SessionManager.saveProfileApprovalStatus(status: UserProfileStatus.approved.rawValue)
//                self.show_completeProfileView()
//            }
//        }
//        else {
//            self.btnUnderstand.isHidden = true
//            ProfileManager.shared.getUserProfile_phoneNo(phoneNo: SessionManager.get_phonenumber()) { (success , user) in
//                if success {
//                    if let userData = user {
//
//                        //save user approval status
//                        if let status = userData.status {
//                            if status == "1" {
//                                SessionManager.saveProfileApprovalStatus(status: UserProfileStatus.approved.rawValue)
//                                self.show_completeProfileView()
//                            } else {
//                                // FTIndicator.showInfo(withMessage: "Your application is currently on our waitlist.")
//                            }
//                        }
//                    }
//                }
//            }
//        }
    }
    
    @IBAction func btn_completeProfile(_ sender: UIButton) {
        let MatchSignUpDetail_VC = self.storyboard?.instantiateViewController(withIdentifier: "Match_SignUpDetail_VC") as! Match_SignUpDetail_VC
        if objStep1?.headlineTitle != nil {
            MatchSignUpDetail_VC.titleHeadLine = ((objStep1?.headlineTitle)!)
         }
        self.navigationController?.pushViewController(MatchSignUpDetail_VC, animated: true)
    }
    
    @IBAction func btn_signUpDone(_ sender: UIButton) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "InviteFriends_VC") as! InviteFriends_VC
        if let topVC = UIApplication.topViewController() {
            topVC.present(controller , animated: true , completion: {
                self.navigationController?.popViewController(animated: false)
            })
        }
    }
    
    
    @IBAction func didPress_illDoLater(_ sender: UIButton) {
        
        CommonFunctions.shared.initializeUser()
        
       /* if SessionManager.get_myProfileGender_isMale() {
            let initialViewController = self.storyboard?.instantiateViewController(withIdentifier: "TabBarController") as! TabBarController
            self.navigationController?.pushViewController(initialViewController, animated: true)
        }else {
            let planVc = self.storyboard?.instantiateViewController(withIdentifier: "Plan_VC") as! Plan_VC
            self.navigationController?.pushViewController(planVc, animated: true)
        }*/
        
        let planVc = self.storyboard?.instantiateViewController(withIdentifier: "Plan_VC") as! Plan_VC
        self.navigationController?.pushViewController(planVc, animated: true)
    }
    
    
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true
    }
}
