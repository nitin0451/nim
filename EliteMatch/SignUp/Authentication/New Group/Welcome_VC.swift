//
//  Welcome_VC.swift
//  EliteMatch
//
//  Created by Kitlabs-M-0002 on 11/15/18.
//  Copyright © 2018 Kitlabs-M-0002. All rights reserved.
//

import UIKit
import FTIndicator


class Welcome_VC: UIViewController {
    
    //MARk:- Outlets
    @IBOutlet weak var vw_terms: UIView!
    @IBOutlet weak var txtVw_terms: UITextView!
    
    
    //MARK:- Variables
    let termsAndConditionsURL = "https://www.nimapp.co/terms.html"
    let privacyURL            = "https://www.nimapp.co/privacy.html"
    var isSingleUser:Bool = true

    //MARK:- UIView life-cycle methods
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        //init
        self.vw_terms.isHidden = true
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        self.config_termsTextView()
        self.vw_terms.dropShadow(color: .lightGray , opacity: 1, offSet: CGSize(width: -1, height: 1), radius: 3, scale: true)
    }

    
    //MARK:- UIButton Actions
    @IBAction func btn_createAccount(_ sender: UIButton) {
        self.isSingleUser = true
        self.showTerms()
    }
    
    @IBAction func btn_logIn(_ sender: UIButton) {  //open chaperon vc
        self.isSingleUser = false
        self.showTerms()
    }
    
    @IBAction func btn_howItWorks(_ sender: UIButton) {
        let howItWorks = self.storyboard?.instantiateViewController(withIdentifier: "HowItWorks_VC") as! HowItWorks_VC
        self.present(howItWorks, animated: true , completion: nil)
    }
    
    @IBAction func btn_cancelTerms(_ sender: UIButton) {
        self.hideTerms()
    }
    
    @IBAction func btn_acceptTerms(_ sender: UIButton) {
        if self.isSingleUser {
            let registerPhone_obj = self.storyboard?.instantiateViewController(withIdentifier: "RegisterPhoneNo_VC") as? RegisterPhoneNo_VC
            self.navigationController?.pushViewController(registerPhone_obj!, animated: true)
        }
        else {
            let SignUpChaperon_VC = self.storyboard?.instantiateViewController(withIdentifier: "RegisterPhone_Chaperon_VC") as! RegisterPhone_Chaperon_VC
            self.navigationController?.pushViewController(SignUpChaperon_VC, animated: false)
        }
    }
    
    //MARK:- Custom Methods
    func config_termsTextView(){
        self.txtVw_terms.delegate = self
        self.txtVw_terms.isEditable = false
        let str = "By creating an account, you agree to the NIM Terms of use and Privacy Policy."
       
        let attributedStringColor = [NSAttributedStringKey.foregroundColor : UIColor.black];
        let attributedString = NSMutableAttributedString(string: str, attributes: attributedStringColor)
        let foundRangeTerms = attributedString.mutableString.range(of: "Terms of use")
        let foundRangePrivacy = attributedString.mutableString.range(of: "Privacy Policy")
        attributedString.addAttribute(NSAttributedStringKey.link, value: termsAndConditionsURL, range: foundRangeTerms)
        attributedString.addAttribute(NSAttributedStringKey.link, value: privacyURL, range: foundRangePrivacy)
        attributedString.addAttributes([NSAttributedStringKey.font : UIFont.init(name:"CircularStd-Book",size:18)] , range: attributedString.mutableString.range(of: str))
        let style = NSMutableParagraphStyle()
        style.alignment = NSTextAlignment.center
        attributedString.addAttribute(NSAttributedStringKey.paragraphStyle , value: style, range: attributedString.mutableString.range(of: str))
        self.txtVw_terms.attributedText = attributedString
    }
    
    func showTerms(){
        self.vw_terms.alpha = 0
        self.vw_terms.isHidden = false
        UIView.animate(withDuration: 0.8) {
            self.vw_terms.alpha = 1
        }
    }
    
    func hideTerms(){
        UIView.animate(withDuration: 0.8, animations: {
            self.vw_terms.alpha = 0
        }) { _ in
            self.vw_terms.isHidden = true
        }
    }
    
}

extension Welcome_VC: UITextViewDelegate {
    func textView(_ textView: UITextView, shouldInteractWith URL: URL, in characterRange: NSRange) -> Bool {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "WebView_VC") as! WebView_VC
        if (URL.absoluteString == termsAndConditionsURL) {
            vc.strTitle = "Terms and Conditions"
        } else if (URL.absoluteString == privacyURL) {
            vc.strTitle = "Privacy Policy"
        }
        self.navigationController?.pushViewController(vc, animated: true)
        return false
    }
}
