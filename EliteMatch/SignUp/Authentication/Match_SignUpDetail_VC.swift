//
//  Match_SignUpDetail_VC.swift
//  Tamaas
//
//  Created by Kitlabs-M-0002 on 10/3/18.
//  Copyright © 2018 Krescent Global. All rights reserved.
//

import UIKit
import FTIndicator
import CoreLocation
import Alamofire
import DropDown


class Match_language {
    var id:String?
    var language:String?
}

class Match_religion {
    var id:String?
    var name:String?
}

class Match_education {
    var id:String?
    var name:String?
}

class signUpStep_2: NSObject {
    var languages:String = ""
    var inSchool:String = "false"
    var inCareer:String = "false"
    var religion:String = ""
    var living_place:String = ""
    var travel:String = ""
    var ethnicity:String = ""
    var education:String = ""
    var religionCount:String = ""
    var careerName:String = ""
    var countyCode:String = ""
}

class Match_SignUpDetail_VC: UIViewController , CLLocationManagerDelegate , UITextFieldDelegate {
    
    //MARK:- Outlets
    @IBOutlet var mainView: UIView!
    @IBOutlet weak var lbl_lang: UILabel!
    @IBOutlet weak var lbl_religion: UILabel!
    @IBOutlet weak var lbl_religiousCount: UILabel!
    @IBOutlet weak var slider_religious: UISlider!
    @IBOutlet weak var lbl_location: UILabel!
    @IBOutlet weak var lbl_travel: UILabel!
    @IBOutlet weak var lbl_ethnicity: UILabel!
    @IBOutlet weak var lbl_education: UILabel!
    @IBOutlet weak var txtField_location: UITextField!
    @IBOutlet weak var btn_school: UIButton!
    @IBOutlet weak var btn_career: UIButton!
    @IBOutlet weak var txtField_ethnicity: UITextField!
    @IBOutlet weak var line_lang: UILabel!
    @IBOutlet weak var lbl_requiredLang: UILabel!
    @IBOutlet weak var line_religion: UILabel!
    @IBOutlet weak var lbl_requiredReligion: UILabel!
    @IBOutlet weak var line_loc: UILabel!
    @IBOutlet weak var lbl_requiredLoc: UILabel!
    @IBOutlet weak var line_travel: UILabel!
    @IBOutlet weak var lbl_requiredTravel: UILabel!
    @IBOutlet weak var line_ethnicity: UILabel!
    @IBOutlet weak var lbl_requiredEthnicity: UILabel!
    @IBOutlet weak var line_education: UILabel!
    @IBOutlet weak var lbl_requiredEducation: UILabel!
    @IBOutlet weak var vw_career: UIView!
    @IBOutlet weak var lbl_requiredCareer: UILabel!
    @IBOutlet weak var txtfld_career: UITextField!
    @IBOutlet weak var line_career: UILabel!
    @IBOutlet weak var constraint_top_btnContinue: NSLayoutConstraint!
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var scrollParentView: UIView!
    @IBOutlet weak var childView: UIView!
    @IBOutlet weak var continue_Btn: UIButton!
    
    //MARK:- Variables
    var selectedCountries = [MICountry]()
    var countrySelection = [String]()
    var countryCode = [String]()
    let objStep1 = signUpStep_1()
    
    var locationManager:CLLocationManager!
    lazy var geocoder = CLGeocoder()
    var objStep2 = signUpStep_2()
    var navigateFromWelcomeVC = Bool()
    var titleHeadLine = String()
    
    
    //MARK:- UIView life-cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        self.determineMyCurrentLocation()
        self.hideRequiredFields()
        self.hideCareerView()
        self.txtField_location.delegate = self
        self.txtField_ethnicity.delegate = self
        self.txtField_location.placeHolderColor = UIColor.lightGray
        self.txtField_ethnicity.placeHolderColor = UIColor.lightGray
        
        //prefilled data
        if self.navigateFromWelcomeVC {
            
            self.btnBack.isHidden = true
            self.btnBack.isUserInteractionEnabled = false
            self.lbl_lang.textColor = UIColor.black
            self.lbl_lang.text = objStep2.languages
            self.lbl_religion.textColor = UIColor.black
            self.lbl_religion.text = objStep2.religion
            if objStep2.religionCount != "" {
                let count = NumberFormatter().number(from: objStep2.religionCount)
                self.slider_religious.value = Float(Int(count!))
                self.lbl_religiousCount.text = objStep2.religionCount
            }
            self.txtField_location.text = objStep2.living_place
            self.lbl_travel.textColor = UIColor.black
            self.lbl_travel.text = objStep2.travel
            self.txtField_ethnicity.text = objStep2.ethnicity
            self.lbl_education.textColor = UIColor.black
            self.lbl_education.text = objStep2.education
            
        }
    }
    
    //MARK:- UItextField Delegates
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == self.txtField_location {
            self.line_loc.backgroundColor = UIColor(red: 235/255, green: 238/255, blue: 242/255, alpha: 1.0)
            self.lbl_requiredLoc.isHidden = true
        }
        
        if textField == self.txtField_ethnicity {
            self.line_ethnicity.backgroundColor = UIColor(red: 235/255, green: 238/255, blue: 242/255, alpha: 1.0)
            self.lbl_requiredEthnicity.isHidden = true
        }
        
        if textField == self.txtfld_career {
            self.line_career.backgroundColor = UIColor(red: 235/255, green: 238/255, blue: 242/255, alpha: 1.0)
            self.lbl_requiredCareer.isHidden = true
        }
    }
    
    //MARK:- UILocation Delegates
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let userLocation:CLLocation = locations[0] as CLLocation
        
        // Create Location
        let location = CLLocation(latitude: userLocation.coordinate.latitude, longitude: userLocation.coordinate.longitude)
        
        // Geocode Location
        geocoder.reverseGeocodeLocation(location) { (placemarks, error) in
            // Process Response
            self.processResponse(withPlacemarks: placemarks, error: error)
            self.locationManager.stopUpdatingLocation()
        }
    }
    
    private func processResponse(withPlacemarks placemarks: [CLPlacemark]?, error: Error?) {
        
        if let error = error {
            print("Unable to Reverse Geocode Location (\(error))")
        } else {
            if let placemarks = placemarks, let placemark = placemarks.first {
                print(placemark)
                self.txtField_location.text = placemark.compactAddress
            }
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error)
    {
        print("Error \(error)")
    }
    
    //MARK:- Custom Methods
    func determineMyCurrentLocation() {
        locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        
        let authStatus = CLLocationManager.authorizationStatus()
        if authStatus == .notDetermined {
            locationManager.requestWhenInUseAuthorization()
        }
        
        if authStatus == .denied || authStatus == .restricted {
            FTIndicator.showInfo(withMessage: "Enable location services from device settings to fetch current location.")
        }
        
        if CLLocationManager.locationServicesEnabled() {
            locationManager.startUpdatingLocation()
        }
    }
    
    func showRedLine(line: UILabel , requiredField: UILabel) {
        line.backgroundColor = .red
        requiredField.isHidden = false
    }
    
    func HideRedLine(line: UILabel , requiredField: UILabel) {
        line.backgroundColor = UIColor(red: 235/255, green: 238/255, blue: 242/255, alpha: 1.0)
        requiredField.isHidden = true
    }
    
    func isValidate() -> Bool {
        
        if (self.lbl_lang.text == "Language(s)") || (self.lbl_lang.text?.count)! < 1 {
            self.showRedLine(line: line_lang , requiredField: lbl_requiredLang)
            return false
        }
        if (self.lbl_religion.text == "Religion") || (self.lbl_religion.text?.count)! < 1 {
            self.showRedLine(line: line_religion, requiredField: lbl_requiredReligion)
            return false
        }
        if let trimmedString = self.txtField_location.text?.trimmingCharacters(in: .whitespacesAndNewlines) {
            if trimmedString.count < 1 {
                self.showRedLine(line: line_loc, requiredField: lbl_requiredLoc)
                return false
            }
        }
        if  (self.lbl_travel.text == "Where can you travel?") || (self.lbl_travel.text?.count)! < 1 {
            self.showRedLine(line: line_travel, requiredField: lbl_requiredTravel)
            return false
        }
        if let trimmedString = self.txtField_ethnicity.text?.trimmingCharacters(in: .whitespacesAndNewlines) {
            if trimmedString.count < 1 {
                self.showRedLine(line: line_ethnicity, requiredField: lbl_requiredEthnicity)
                return false
            }
        }
        if (self.lbl_education.text == "Education") || (self.lbl_education.text?.count)! < 1 {
            self.showRedLine(line: line_education, requiredField: lbl_requiredEducation)
            return false
        }
        if !(self.btn_school.isSelected) && !(self.btn_career.isSelected) {
            FTIndicator.showToastMessage("please select are you in School or Career")
            return false
        }
        if let trimmedString = self.txtfld_career.text?.trimmingCharacters(in: .whitespacesAndNewlines) {
            if trimmedString.count < 1 {
                self.showRedLine(line: line_career, requiredField: lbl_requiredCareer)
                return false
            }
        }
        return true
    }
    
    func hideRequiredFields(){
        self.lbl_requiredLang.isHidden = true
        self.lbl_requiredReligion.isHidden = true
        self.lbl_requiredLoc.isHidden = true
        self.lbl_requiredTravel.isHidden = true
        self.lbl_requiredEthnicity.isHidden = true
        self.lbl_requiredEducation.isHidden = true
    }
    
    func showCareerView() {
        self.vw_career.isHidden = false
        self.constraint_top_btnContinue.constant = 90
    }
    
    func hideCareerView() {
        self.vw_career.isHidden = true
        self.constraint_top_btnContinue.constant = 46
    }
    
    //MARK:- Web service implementation
    func webService_SignUpStep_two(param:NSDictionary){
        
        LoadingIndicatorView.show()
        
        let webservice  = WebserviceSigleton ()
        webservice.POSTServiceWithParameters_Elite(urlString: constantVC.WebserviceName.URL_SIGNUP_STEP_2, params: param as! Dictionary<String, AnyObject>, callback:{ (result,error) -> Void in
            if result != nil{
                let resultMessage = result!["msg"] as! String
                
                if resultMessage == "success"{
                    LoadingIndicatorView.hide()
                    SessionManager.saveProfile_lastUpdatedStatus(status: UserProfile_lastUpdated.step2.rawValue)
                    self.openNextVC()
                }
                else{
                    LoadingIndicatorView.hide()
                    FTIndicator.showError(withMessage: result!["msg_details"] as? String)
                }
            }else {
                LoadingIndicatorView.hide()
            }
        })
    }
   
    //MARK:- UIButton Actions
    @IBAction func btn_language(_ sender: UIButton) {
        
        self.view.endEditing(true)
        DropDown_Manager.shared.configure(title: DropDown_Title.Language.rawValue , preSelected:  self.lbl_lang.text ?? "", allowMultiSelection: true , arrItems: constantVC.arrDropDown_Match.arrLangaugeDropDown) { (strSelectedValues) in
            self.lbl_lang.textColor = UIColor.black
            self.lbl_lang.text = strSelectedValues
            self.HideRedLine(line: self.line_lang , requiredField: self.lbl_requiredLang)
        }
    }
    
    @IBAction func btn_religion(_ sender: UIButton) {
        self.view.endEditing(true)
        DropDown_Manager.shared.configure(title: DropDown_Title.Religion.rawValue , preSelected: "", allowMultiSelection: false , arrItems: constantVC.arrDropDown_Match.arrReligionDropDown) { (strSelectedValues) in
            self.lbl_religion.textColor = UIColor.black
            self.lbl_religion.text = strSelectedValues
            self.HideRedLine(line: self.line_religion , requiredField: self.lbl_requiredReligion)
        }
    }
    
    @IBAction func btn_location(_ sender: UIButton) {
        
    }
    
    @IBAction func btn_travel(_ sender: UIButton) {
        self.view.endEditing(true)
        DropDown_Manager.shared.configure(title: DropDown_Title.Travel.rawValue , preSelected: self.lbl_travel.text ?? "" , allowMultiSelection: true  , arrItems: constantVC.arrDropDown_Match.arrTravel) { (strSelectedValues) in
            self.lbl_travel.textColor = UIColor.black
            self.lbl_travel.text = strSelectedValues
            self.HideRedLine(line: self.line_travel , requiredField: self.lbl_requiredTravel)
        }
    }
    
    @IBAction func btn_ethnicity(_ sender: UIButton) {
        
    }
    
    @IBAction func btn_education(_ sender: UIButton) {
        self.view.endEditing(true)
        DropDown_Manager.shared.configure(title: DropDown_Title.Education.rawValue , preSelected: "" , allowMultiSelection: false  , arrItems: constantVC.arrDropDown_Match.arrEducationDropDown) { (strSelectedValues) in
            self.lbl_education.textColor = UIColor.black
            self.lbl_education.text = strSelectedValues
            self.HideRedLine(line: self.line_education , requiredField: self.lbl_requiredEducation)
        }
    }
    
    @IBAction func btn_famliyOrigin(_ sender: Any) {
        countrySelection.removeAll()
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let countryPickerVC = storyboard.instantiateViewController(withIdentifier: "CountryPicker_VC") as! CountryPicker_VC
        countryPickerVC.delegate = self
        countryPickerVC.selectedCountries = self.selectedCountries
        self.present(countryPickerVC , animated: true , completion: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = false
    }
    
    @IBAction func btn_school(_ sender: UIButton) {
        txtfld_career.text = ""
        self.view.endEditing(true)
        if self.btn_school.isSelected {
            self.btn_school.isSelected = false
            self.btn_career.isSelected = true
            self.hideCareerView()
        } else {
            self.btn_school.isSelected = true
            self.btn_career.isSelected = false
            self.showCareerView()
            self.txtfld_career.placeholder  = "What are you studying"
        }
    }
    
    @IBAction func btn_career(_ sender: UIButton) {
        txtfld_career.text = ""
        if titleHeadLine != ""{
            self.txtfld_career.text = titleHeadLine
        }
        self.view.endEditing(true)
        if self.btn_career.isSelected {
            self.btn_school.isSelected = true
            self.btn_career.isSelected = false
            self.hideCareerView()
        } else {
            self.btn_school.isSelected = false
            self.btn_career.isSelected = true
            self.showCareerView()
            self.txtfld_career.placeholder  = "Your title"
        }
    }
   
    
    @IBAction func btn_continue(_ sender: UIButton) {
        self.view.endEditing(true)
        if self.isValidate() {
            if btn_school.isSelected {
                objStep2.inSchool = "true"
                objStep2.inCareer = "false"
            }
            if btn_career.isSelected {
                objStep2.inSchool = "false"
                objStep2.inCareer = "true"
            }
            
            objStep2.religionCount = self.lbl_religiousCount.text!
            objStep2.languages = self.lbl_lang.text!
            objStep2.religion = self.lbl_religion.text!
            objStep2.living_place = self.txtField_location.text!
            objStep2.travel = self.lbl_travel.text!
            objStep2.ethnicity = self.txtField_ethnicity.text!
            objStep2.education = self.lbl_education.text!
            objStep2.careerName = self.txtfld_career.text!
            
            let dictParam : NSDictionary = ["languages": self.lbl_lang.text!, "inSchool": "\(objStep2.inSchool)" ,"inCareer": "\(objStep2.inCareer)","religion": objStep2.religion ,"living_place": objStep2.living_place, "travelPlaces": objStep2.travel, "ethnicity": objStep2.ethnicity,"education": objStep2.education,"Userid": SessionManager.get_Elite_userID(),"lastupdated_status": "step2","religionCount": objStep2.religionCount,"careerName": objStep2.careerName,"countyCode": objStep2.countyCode] //SessionManager.get_Elite_userID()
            
            self.webService_SignUpStep_two(param: dictParam)
        
            //self.openNextVC()
        }
    }
    
    func openNextVC() {
        let matchSignUpDetailFinal_VC = self.storyboard?.instantiateViewController(withIdentifier: "Match_SignUpDetailFinal_VC") as! Match_SignUpDetailFinal_VC
        self.navigationController?.pushViewController(matchSignUpDetailFinal_VC, animated: true)
    }
    
    @IBAction func btn_back(_ sender: UIButton) {
        let transition = CATransition()
        transition.duration = 0.25
        transition.type = kCATransitionPush
        transition.subtype = kCAOnOrderIn
        self.view.window?.layer.add(transition, forKey: kCATransition)
        self.navigationController?.popViewController(animated: false)
    }
    
    @IBAction func slider_religious_action(_ sender: UISlider) {
        let currentValue = Int(sender.value)
        self.lbl_religiousCount.text = "\(currentValue)"
    }
}

extension CLPlacemark {
    
    var compactAddress: String? {
        if let name = name {
            var result = ""
            
            /* if let street = thoroughfare {
             result += ", \(street)"
             }*/
            
            if let city = locality {
                result += "\(city)"
            }
            if let country = country {
                result += ", \(country)"
            }
            return result
        }
        
        return nil
    }
}

extension Match_SignUpDetail_VC: countryPickerSelectionDelegates {
    
    func didSelectCountries(countries: [MICountry]) {
        self.countrySelection.removeAll()
        self.countryCode.removeAll()
        self.selectedCountries = countries
        for item in countries {
            countrySelection.append(item.name)
            countryCode.append(item.code)
        }
        
        let strSelectedCountries = countrySelection.joined(separator: ", ")
        txtField_ethnicity.text = strSelectedCountries
        let strCountrycode = countryCode.joined(separator: ", ")
        objStep2.countyCode = strCountrycode
    }
    
    func didCancel() {
        
    }
    
    
}
