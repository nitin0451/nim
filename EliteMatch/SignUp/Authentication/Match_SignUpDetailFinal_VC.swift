//
//  Match_SignUpDetailFinal_VC.swift
//  Tamaas
//
//  Created by Kitlabs-M-0002 on 10/3/18.
//  Copyright © 2018 Krescent Global. All rights reserved.
//

import UIKit
import FTIndicator
import DropDown

class signUpStep_3: NSObject {
    var Lifestyle:String = ""
    var isWorkOutRoutinely:String = "false"
    var isSmoke:String = "false"
    var isDrink:String = "false"
    var isOnlyEatHalal:String = "false"
    var isMarriedBefore:String = "false"
    var isHaveChildren:String = "false"
    var isPreferred_TallerPartner:String = "false"
    var height:String = ""
    var petpeeve:String = ""
    var favHoliday:String = ""
    var favShow:String = ""
    var favDessert:String = ""
    var favMusic:String = ""
    var favFood:String = ""
    var religionCount:String = ""
    var ethnicityCount:String = ""
    var about:String = ""
}


class Match_SignUpDetailFinal_VC: UIViewController , UITextFieldDelegate , UITextViewDelegate , UIPickerViewDelegate,UIPickerViewDataSource {
    
    //MARK:- Outlets
    @IBOutlet weak var txtField_height: UITextField!
    @IBOutlet weak var txtField_favoriteShow: UITextField!
    @IBOutlet weak var txtView_detail: UITextView!
    @IBOutlet weak var lbl_ethnicityCount: UILabel!
    @IBOutlet weak var lbl_religionCount: UILabel!
    @IBOutlet weak var lbl_lifeStyle: UILabel!
    @IBOutlet weak var lbl_favMusic: UILabel!
    @IBOutlet weak var lbl_favFood: UILabel!
    @IBOutlet weak var lbl_zoadicSign: UILabel!
    @IBOutlet weak var line_lifeStyle: UILabel!
    @IBOutlet weak var lbl_requiredLifestyle: UILabel!
    @IBOutlet weak var line_height: UILabel!
    @IBOutlet weak var lbl_requiredHeight: UILabel!
    @IBOutlet weak var line_favShow: UILabel!
    @IBOutlet weak var lbl_requiredFavShow: UILabel!
    @IBOutlet weak var line_favDessert: UILabel!
    @IBOutlet weak var lbl_requiredFavDessert: UILabel!
    @IBOutlet weak var line_favMusic: UILabel!
    @IBOutlet weak var lbl_requiredFavMusiv: UILabel!
    @IBOutlet weak var line_favFood: UILabel!
    @IBOutlet weak var lbl_requiredFavFood: UILabel!
    @IBOutlet weak var line_zoadicSign: UILabel!
    @IBOutlet weak var lbl_requiredZoaSign: UILabel!
    @IBOutlet weak var lbl_requiredDetails: UILabel!
   
    @IBOutlet weak var lbl_favDessert: UILabel!
    @IBOutlet weak var lbl_characterCount: UILabel!
    @IBOutlet weak var childView: UIView!
    @IBOutlet weak var submitBtn: UIButton!
    @IBOutlet weak var mainScrollView: UIScrollView!
    @IBOutlet weak var scrollParentView: UIView!
    @IBOutlet var mainview: UIView!
    @IBOutlet weak var religionMatter_Slider: UISlider!
    @IBOutlet weak var etthnicityMatter_Slider: UISlider!
    @IBOutlet weak var preferPartner_SwitchBtn: UISwitch!
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var switchBingeWatch: UISwitch!
    
    //MARK:- Variables
    let heightPicker = UIPickerView()
    var arrHeight = [[String]]()
    var objStep2 = signUpStep_2()
    var objStep3 = signUpStep_3()
    var navigateFromWelcomeVC = Bool()
    var isRootVC:Bool = false
    
    
    //MARK:- UIView lief-cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //get pre data
        self.arrHeight.append(constantVC.GeneralConstants.arrFeet)
        self.arrHeight.append(constantVC.GeneralConstants.arrInches)
        self.txtView_detail.layer.cornerRadius = 4.0
        self.txtView_detail.layer.borderWidth = 1.0
        self.txtView_detail.layer.borderColor = UIColor.lightGray.cgColor
        self.configureHeightPicker()
        
        if self.isRootVC {
            self.btnBack.isHidden = true
            self.btnBack.isUserInteractionEnabled = false
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        self.hideRequiredFields()
        self.txtField_height.delegate = self
        self.txtField_favoriteShow.delegate = self
        self.txtView_detail.delegate = self
        self.txtField_height.placeHolderColor = UIColor.lightGray
        self.txtField_favoriteShow.placeHolderColor = UIColor.lightGray
    }
    
    //MARK:- UItextField Delegates
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == self.txtField_height {
            self.HideRedLine(line: self.line_height , requiredField: lbl_requiredHeight)
        }
        if textField == self.txtField_favoriteShow {
            self.HideRedLine(line: self.line_favShow , requiredField: lbl_requiredFavShow)
        }
    }
    
    //MARK:- UItextView Delegates
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView == self.txtView_detail {
            self.lbl_requiredDetails.isHidden = true
        }
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        let newText = (textView.text as NSString).replacingCharacters(in: range, with: text)
        let numberOfChars = newText.count
        lbl_characterCount.text = numberOfChars.description + "/250"
        return numberOfChars <= 249;
    }
    
    //MARK:- UIPicker Height
    func configureHeightPicker(){
        self.txtField_height.delegate = self
        self.heightPicker.dataSource = self
        self.heightPicker.delegate = self
        self.txtField_height.inputView = self.heightPicker
        
        let toolbar = UIToolbar()
        toolbar.sizeToFit()
        
        let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.plain, target: self, action: #selector(self.doneHeightdatePicker))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: UIBarButtonItemStyle.plain, target: self, action: "cancelHeightDatePicker")
        toolbar.setItems([cancelButton,spaceButton,doneButton], animated: false)
        self.txtField_height.inputAccessoryView = toolbar
    }
    
    @objc func doneHeightdatePicker(){
        self.txtField_height.resignFirstResponder()
    }
    
    func cancelHeightDatePicker(){
        self.view.endEditing(true)
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
    }
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 2
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return self.arrHeight[component].count
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return self.arrHeight[component][row]
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        let feet =  arrHeight[0][pickerView.selectedRow(inComponent: 0)]
        let Inch = arrHeight[1][pickerView.selectedRow(inComponent: 1)]
        
        if feet != "Feet" && Inch != "Inch" {
            self.txtField_height.text =   feet + "'" + Inch
        }
    }
    
    //MARK:- Custom Methods
    
    func showRedLine(line: UILabel , requiredField: UILabel) {
        line.backgroundColor = .red
        requiredField.isHidden = false
    }
    
    func HideRedLine(line: UILabel , requiredField: UILabel) {
        line.backgroundColor = UIColor(red: 235/255, green: 238/255, blue: 242/255, alpha: 1.0)
        requiredField.isHidden = true
    }
    
    func isValidate() -> Bool {
        
        if let trimmedString = self.lbl_lifeStyle.text?.trimmingCharacters(in: .whitespacesAndNewlines) {
            if trimmedString.count < 1 || self.lbl_lifeStyle.text == "Family Values" {
                self.showRedLine(line: self.line_lifeStyle , requiredField: self.lbl_requiredLifestyle)
                return false
            } else {
                objStep3.Lifestyle = trimmedString
            }
        }
        if let trimmedString = self.txtField_height.text?.trimmingCharacters(in: .whitespacesAndNewlines) {
            if trimmedString.count < 1 {
                self.showRedLine(line: self.line_height , requiredField: self.lbl_requiredHeight)
                return false
            }else {
                objStep3.height = trimmedString.replacingOccurrences(of: "'", with: ".")
            }
        }
        if let trimmedString = self.txtField_favoriteShow.text?.trimmingCharacters(in: .whitespacesAndNewlines) {
            if trimmedString.count < 1 {
                self.showRedLine(line: self.line_favShow , requiredField: self.lbl_requiredFavShow)
                return false
            }else {
                objStep3.favShow = trimmedString
            }
        }
        if lbl_favDessert.textColor != UIColor.black || (lbl_favDessert.text?.count)! < 1 {
            self.showRedLine(line: self.line_favDessert , requiredField: self.lbl_requiredFavDessert)
            return false
        }else{
            objStep3.favDessert = self.lbl_favDessert.text!
            
        }
        if lbl_favMusic.textColor != UIColor.black || (lbl_favMusic.text?.count)! < 1 {
            self.showRedLine(line: self.line_favMusic , requiredField: self.lbl_requiredFavMusiv)
            return false
        }else {
            objStep3.favMusic = self.lbl_favMusic.text!
        }
        if lbl_favFood.textColor != UIColor.black || (lbl_favFood.text?.count)! < 1 {
            self.showRedLine(line: self.line_favFood , requiredField: self.lbl_requiredFavFood)
            return false
        }else {
            objStep3.favFood = self.lbl_favFood.text!
        }
       
        if let trimmedString = self.txtView_detail.text?.trimmingCharacters(in: .whitespacesAndNewlines) {
            if trimmedString.count < 1  {
                self.lbl_requiredDetails.isHidden = false
                return false
            }else {
                objStep3.about = trimmedString
            }
        }
        return true
    }
    
    func hideRequiredFields(){
        self.lbl_requiredLifestyle.isHidden = true
        self.lbl_requiredHeight.isHidden = true
        self.lbl_requiredFavShow.isHidden = true
        self.lbl_requiredFavDessert.isHidden = true
        self.lbl_requiredFavMusiv.isHidden = true
        self.lbl_requiredFavFood.isHidden = true
        self.lbl_requiredZoaSign.isHidden = true
        self.lbl_requiredDetails.isHidden = true
    }
    
    //MARK:- Web service implementation
    func webService_SignUpStep_three(param:NSDictionary ){
        print(param)
        LoadingIndicatorView.show()
        let webservice  = WebserviceSigleton ()
        webservice.POSTServiceWithParameters_Elite(urlString: constantVC.WebserviceName.URL_SIGNUP_STEP_3, params: param as! Dictionary<String, AnyObject>, callback:{ (result,error) -> Void in
            if result != nil{
                let resultMessage = result!["msg"] as! String
                
                if resultMessage == "success"{
                    
                    LoadingIndicatorView.hide()
                    SessionManager.saveProfile_lastUpdatedStatus(status: UserProfile_lastUpdated.step3.rawValue)
                    self.openNextVC()
                }
                else{
                    LoadingIndicatorView.hide()
                    FTIndicator.showError(withMessage: "Something Went Wrong!")
                }
            }else {
                LoadingIndicatorView.hide()
            }
        })
    }
    
    
    //MARK:- UIButton Actions
    @IBAction func btn_back(_ sender: UIButton) {
        if self.navigateFromWelcomeVC {
            let step2_obj = self.storyboard?.instantiateViewController(withIdentifier: "Match_SignUpDetail_VC") as? Match_SignUpDetail_VC
            step2_obj?.objStep2 = self.objStep2
            step2_obj?.navigateFromWelcomeVC = true
            self.navigationController?.pushViewController(step2_obj!, animated: true)
        } else {
            let transition = CATransition()
            transition.duration = 0.25
            transition.type = kCATransitionPush
            transition.subtype = kCAOnOrderIn
            self.view.window?.layer.add(transition, forKey: kCATransition)
            self.navigationController?.popViewController(animated: false)
        }
    }
    
    @IBAction func btn_lifeStyle(_ sender: UIButton) {
        self.view.endEditing(true)
        DropDown_Manager.shared.configure(title: DropDown_Title.FamilyValues.rawValue , preSelected:  self.lbl_lifeStyle.text ?? "", allowMultiSelection: false , arrItems: constantVC.arrDropDown_Match.arrLifeStyle) { (strSelectedValues) in
            self.lbl_lifeStyle.textColor = UIColor.black
            self.lbl_lifeStyle.text = strSelectedValues
            self.HideRedLine(line: self.line_lifeStyle , requiredField: self.lbl_requiredLifestyle)
        }
    }
    
    @IBAction func btn_favouriteMusic(_ sender: UIButton) {
        self.view.endEditing(true)
        DropDown_Manager.shared.configure(title: DropDown_Title.FavMusic.rawValue , preSelected:  self.lbl_favMusic.text ?? "", allowMultiSelection: false , arrItems: constantVC.arrDropDown_Match.arrFavMusic) { (strSelectedValues) in
            self.lbl_favMusic.textColor = UIColor.black
            self.lbl_favMusic.text = strSelectedValues
            self.HideRedLine(line: self.line_favMusic , requiredField: self.lbl_requiredFavMusiv)
        }
    }
    
    @IBAction func btn_favoriteFood(_ sender: UIButton) {
        self.view.endEditing(true)
        DropDown_Manager.shared.configure(title: DropDown_Title.FavFood.rawValue , preSelected:  self.lbl_favFood.text ?? "", allowMultiSelection: false , arrItems: constantVC.arrDropDown_Match.arrFavFood) { (strSelectedValues) in
            self.lbl_favFood.textColor = UIColor.black
            self.lbl_favFood.text = strSelectedValues
            self.HideRedLine(line: self.line_favFood , requiredField: self.lbl_requiredFavFood)
        }
    }
    
    @IBAction func btn_favDessert(_ sender: UIButton) {
        self.view.endEditing(true)
        DropDown_Manager.shared.configure(title: DropDown_Title.FavCookies.rawValue , preSelected:  self.lbl_favDessert.text ?? "", allowMultiSelection: false , arrItems: constantVC.arrDropDown_Match.arrFavDessert) { (strSelectedValues) in
            self.lbl_favDessert.textColor = UIColor.black
            self.lbl_favDessert.text = strSelectedValues
            self.HideRedLine(line: self.line_favDessert , requiredField: self.lbl_requiredFavDessert)
        }
    }
    
    @IBAction func switch_work_routinely(_ sender: UISwitch) {
        self.view.endEditing(true)
        if sender.isOn {
            objStep3.isWorkOutRoutinely = "true"
        } else {
            objStep3.isWorkOutRoutinely = "false"
        }
    }
    
    @IBAction func switch_smoke(_ sender: UISwitch) {
        self.view.endEditing(true)
        if sender.isOn {
            objStep3.isSmoke = "true"
        } else {
            objStep3.isSmoke = "false"
        }
    }
    
    @IBAction func switch_drink(_ sender: UISwitch) {
        self.view.endEditing(true)
        if sender.isOn {
            objStep3.isDrink = "true"
        } else {
            objStep3.isDrink = "false"
        }
    }
    
    @IBAction func switch_watchToEat(_ sender: UISwitch) {
        if sender.isOn {
            objStep3.isOnlyEatHalal = "true"
        } else {
            objStep3.isOnlyEatHalal = "false"
        }
    }
    
    @IBAction func switch_partnerTaller(_ sender: UISwitch) {
        self.view.endEditing(true)
        if sender.isOn {
            objStep3.isPreferred_TallerPartner = "true"
        } else {
            objStep3.isPreferred_TallerPartner = "false"
        }
    }
    
    @IBAction func switch_haveChildren(_ sender: UISwitch) {
        self.view.endEditing(true)
        if sender.isOn {
            objStep3.isHaveChildren = "true"
        } else {
            objStep3.isHaveChildren = "false"
        }
    }
    
    @IBAction func slider_religion(_ sender: UISlider) {
        let currentValue = Int(sender.value)
        self.lbl_religionCount.text = "\(currentValue)"
    }
    
    @IBAction func slider_ethnicity(_ sender: UISlider) {
        let currentValue = Int(sender.value)
        self.lbl_ethnicityCount.text = "\(currentValue)"
    }
    
    @IBAction func switch_marriedBefore(_ sender: UISwitch) {
        self.view.endEditing(true)
        if sender.isOn {
            objStep3.isMarriedBefore = "true"
        } else {
            objStep3.isMarriedBefore = "false"
        }
    }
    
    @IBAction func btn_submit(_ sender: UIButton) {
        self.view.endEditing(true)
        print(self.isValidate())
        if self.isValidate() {
            
            objStep3.ethnicityCount = self.lbl_ethnicityCount.text!
            objStep3.religionCount = self.lbl_religionCount.text!
            
            let dictParam : NSDictionary = ["lifestyle": objStep3.Lifestyle , "isWorkOutRoutinely": "\(objStep3.isWorkOutRoutinely)" ,"isSmoke": "\(objStep3.isSmoke)", "isDrink": "\(objStep3.isDrink)" , "isOnlyEatHalal": "\(objStep3.isOnlyEatHalal)","isMarriedBefore": "\(objStep3.isMarriedBefore)", "isHaveChildren": "\(objStep3.isHaveChildren)", "height": objStep3.height ,"petpeeve": objStep3.petpeeve, "favHoliday": objStep3.favHoliday , "fav_shows": objStep3.favShow,"fav_dessert": objStep3.favDessert, "fav_music": objStep3.favMusic, "fav_food": objStep3.favFood ,"zoadicSign": "", "isPreferred_TallerPartner": "\(objStep3.isPreferred_TallerPartner)" , "religionCount": objStep3.religionCount ,"ethnicityCount": objStep3.ethnicityCount, "about_me": objStep3.about , "Userid": SessionManager.get_Elite_userID(),"lastupdated_status": "step3" , "isBingeWatchShows": "\(self.switchBingeWatch.isOn)" , "religionMatterCount": self.lbl_religionCount.text!] //SessionManager.get_Elite_userID()
            self.webService_SignUpStep_three(param: dictParam)
        }
    }
    
    func openNextVC(){
        let matchSignUp = self.storyboard?.instantiateViewController(withIdentifier: "Match_SignUp_Confirmation_VC") as? Match_SignUp_Confirmation_VC
        matchSignUp?.isSignUpDoneNavigate = true
        self.navigationController?.pushViewController(matchSignUp!, animated: true)
    }
    
}

