//
//  RegisterOTP_VC.swift
//  EliteMatch
//
//  Created by Kitlabs-M-0002 on 11/15/18.
//  Copyright © 2018 Kitlabs-M-0002. All rights reserved.
//

import UIKit
import FTIndicator

class RegisterOTP_VC: UIViewController {
    
    //MARK:- Outlets
    @IBOutlet weak var lbl_enterCodeTitle: UILabel!
    @IBOutlet weak var txtFld_enterOTP: UITextField!
    
    //MARK:- Variables
    var phnNoToShow:String = ""
    var isChaperon = Bool()
    var userName:String = ""
    
    
    //MARK:- UIView life-cycle methods
    override func viewDidLoad() {
        super.viewDidLoad()
 
        //pre-fill OTP
        self.txtFld_enterOTP.delegate = self
        self.setPhnNoToShow()
    }
    
    func setPhnNoToShow(){
        let text = "Please enter the verification code sent to your number"
        //TODO
//        let formattedString = NSMutableAttributedString()
//        formattedString.normaltext(text + " ").bold(self.phnNoToShow)
//        self.lbl_enterCodeTitle.attributedText = formattedString
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        //init
        self.navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.txtFld_enterOTP.text = ""
    }
    
    //MARK:- Custom Methods
    func isValid() -> Bool {
        
        if self.txtFld_enterOTP.text!.count == 0 || self.txtFld_enterOTP.text!.count < 4 {
            FTIndicator.showToastMessage("Enter valid OTP")
            return false
        }
        return true
    }

    //MARK:- UIButton Actions
    @IBAction func btn_back(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btn_verfiyAndContinue(_ sender: UIButton) {
        if self.isValid() {
            txtFld_enterOTP.resignFirstResponder()
            SessionManager.saveUserName(name: self.userName)
            
        /* let user = QBUUser()
            user.login = SessionManager.get_phonenumber()
            user.password = SessionManager.get_phonenumber()
            user.phone = SessionManager.get_phonenumber()
            user.fullName = SessionManager.getUserName()
            
            NimUserManager.shared.logIn(user: user) { (success , msg) in
                if self.isChaperon {
                    self.webserviceToSignUpChaperon()
                } else {
                    LoadingIndicatorView.hide()
                    self.openNextVC()
                }
            }*/
            
            LoadingIndicatorView.show()
            self.verifyOTP()
        }
    }
    
    @IBAction func btn_ResendCode(_ sender: UIButton) {
        self.generateOTP()
    }
    
    //MARK:- OTP
    func verifyOTP(){
        
        FirebaseOTPManager.shared.verfiyOTP(verificationCode: txtFld_enterOTP.text ?? "") { (success) in
            
            if success {
                
                let user = QBUUser()
                user.login = SessionManager.get_phonenumber()
                user.password = SessionManager.get_phonenumber()
                user.phone = SessionManager.get_phonenumber()
                user.fullName = SessionManager.getUserName()
                
                NimUserManager.shared.logIn(user: user) { (success , msg) in
                    if self.isChaperon {
                        self.webserviceToSignUpChaperon()
                    } else {
                        LoadingIndicatorView.hide()
                        self.openNextVC()
                    }
                    LoadingIndicatorView.hide()
                }
            }
            else {
                LoadingIndicatorView.hide()
                FTIndicator.showError(withMessage: "Something went wrong.")
            }
        }
    }
    
    
    func generateOTP(){
        LoadingIndicatorView.show()
        FirebaseOTPManager.shared.sendVerificationOTP(phoneno: "+" + SessionManager.get_phonenumber()) { (success) in
            if success {
                self.txtFld_enterOTP.text = ""
            } else {
                FTIndicator.showError(withMessage: "Something went wrong.")
            }
            LoadingIndicatorView.hide()
        }
    }
    
    
    
    //MARK:- Web Service implementation
    /*func webserviceToVerifyOTP(){
        LoadingIndicatorView.show()
        let webservice  = WebserviceSigleton ()
        let dictParam : NSDictionary = ["phoneNumber":SessionManager.get_phonenumber(), "code": txtFld_enterOTP.text ?? ""]
        webservice.POSTServiceWithParameters(urlString: constantVC.WebserviceName.URL_VERIFY_SMS, params: dictParam as! Dictionary<String, AnyObject>, callback:{ (result,error) -> Void in
            if result != nil{
                let response = result! as Dictionary
                let resultMessage = response["msg"] as! String
                
                if resultMessage == "success"{
                    
                    if let dic = response["user_details"] as? NSDictionary {
                        if let id = dic.object(forKey: "id") {
                            SessionManager.save_Elite_userID(id: "\(id)")
                        }
                        
                        if let lastupdated_status = dic.object(forKey: "lastupdated_status") {
                            SessionManager.save_lastupdated_status(lastupdated_status: "\(lastupdated_status)")
                        }
                    }
                    
                    //create/login user on QB
                    let user = QBUUser()
                    user.login = SessionManager.get_phonenumber()
                    user.password = SessionManager.get_phonenumber()
                    user.phone = SessionManager.get_phonenumber()
                    
                    print(user.phone!)
                    
                    NimUserManager.shared.logIn(user: user) { (success , msg) in
                        if self.isChaperon {
                            self.webserviceToSignUpChaperon()
                        } else {
                            LoadingIndicatorView.hide()
                            self.openNextVC()
                        }
                    }
                }
                else{
                    LoadingIndicatorView.hide()
                    FTIndicator.showError(withMessage: "Something went wrong.")
                }
            }
            else{
                LoadingIndicatorView.hide()
                FTIndicator.showError(withMessage: "No internet connection")
            }
        })
     
     parameters: {
     user =     {
     "full_name" = "Test Chaperon ";
     phone = 919041626154;
     };
     }
    }*/
   
    
    func webserviceToSignUpChaperon(){
        let webservice  = WebserviceSigleton ()
        let dictParam : NSDictionary = ["phonenumber":SessionManager.get_phonenumber(), "name": self.userName , "quickBloxId": "\(SessionManager.get_QuickBloxID() ?? 0)" , "status": "1"]
        webservice.POSTServiceWithParameters(urlString: constantVC.WebserviceName.URL_CHAPERON_SIGNUP, params: dictParam as! Dictionary<String, AnyObject>, callback:{ (result,error) -> Void in
            if result != nil{
                let response = result! as Dictionary
                let resultMessage = response["msg"] as! String
                
                if resultMessage == "success"{
                    SessionManager.saveChaperonStatus(isActive: true)
                    
                    if let userID = result!["Userid"] {
                        SessionManager.save_Elite_userID(id: String(describing: userID))
                    }
                    
                    NimUserManager.shared.updateUserInfo()
                    LoadingIndicatorView.hide()
                    self.openNextVC()
                }
                else{
                    LoadingIndicatorView.hide()
                    FTIndicator.showError(withMessage: "Something went wrong.")
                }
            }
            else{
                LoadingIndicatorView.hide()
                FTIndicator.showError(withMessage: "No internet connection")
            }
        })
    }
    
    func openNextVC() {
        
        if self.isChaperon {
            CommonFunctions.shared.initializeUser()
            let initialViewController = self.storyboard?.instantiateViewController(withIdentifier: "TabBarController") as! TabBarController
            initialViewController.selectedIndex = 1
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            appDelegate.window?.rootViewController = initialViewController
        } else {
            
           ProfileManager.shared.getUserProfile_phoneNo(phoneNo: SessionManager.get_phonenumber(), completionHandler: { (success , userProfile) in
            
                 if let user = userProfile {
                    //user found
                    
                    //update defaults
                    SessionManager.save_Elite_userID(id: user.id ?? "")
                    SessionManager.saveProfile_lastUpdatedStatus(status: user.lastupdated_status ?? "")
                    
                    FirebaseOTPManager.shared.callToApi_updateFCMToken(completionHandler: { (success) in
                        if user.status == "0" {  //pending profile
                            SessionManager.saveProfileApprovalStatus(status: UserProfileStatus.pending.rawValue)
                            let MatchSignUpConfirmation_VC = self.storyboard?.instantiateViewController(withIdentifier: "Match_SignUp_Confirmation_VC") as! Match_SignUp_Confirmation_VC
                            self.navigationController?.pushViewController(MatchSignUpConfirmation_VC, animated: true)
                        } else { //approved profile
                            SessionManager.saveProfileApprovalStatus(status: UserProfileStatus.approved.rawValue)
                            if user.lastupdated_status == UserProfile_lastUpdated.step1.rawValue {
                                let MatchSignUpConfirmation_VC = self.storyboard?.instantiateViewController(withIdentifier: "Match_SignUp_Confirmation_VC") as! Match_SignUp_Confirmation_VC
                                MatchSignUpConfirmation_VC.isProfileApproved = true
                                self.navigationController?.pushViewController(MatchSignUpConfirmation_VC, animated: true)
                            }
                            else if user.lastupdated_status == UserProfile_lastUpdated.step2.rawValue {
                                let MatchSignUpDetail = self.storyboard?.instantiateViewController(withIdentifier: "Match_SignUpDetailFinal_VC") as! Match_SignUpDetailFinal_VC
                                self.navigationController?.pushViewController(MatchSignUpDetail, animated: true)
                            }
                            else if user.lastupdated_status == UserProfile_lastUpdated.step3.rawValue {
                                CommonFunctions.shared.initializeUser()
                                /*if SessionManager.get_myProfileGender_isMale() {
                                    let initialViewController = self.storyboard?.instantiateViewController(withIdentifier: "TabBarController") as! TabBarController
                                    self.navigationController?.pushViewController(initialViewController, animated: true)
                                } else {
                                    let plan_VC = self.storyboard?.instantiateViewController(withIdentifier: "Plan_VC") as! Plan_VC
                                    self.navigationController?.pushViewController(plan_VC, animated: true)
                                }*/
                                
                                if let strEndDate = user.plan_end_date {
                                    if strEndDate.count > 1 {
                                        if let endDate = PlanManager.shared.stringToDate(strDate: strEndDate) {
                                            if Date() < endDate {
                                                SessionManager.saveProfile_lastUpdatedStatus(status: UserProfile_lastUpdated.paymentDone.rawValue)
                                                let initialViewController = self.storyboard?.instantiateViewController(withIdentifier: "TabBarController") as! TabBarController
                                                self.navigationController?.pushViewController(initialViewController, animated: true)
                                            }
                                            else {
                                                let plan_VC = self.storyboard?.instantiateViewController(withIdentifier: "Plan_VC") as! Plan_VC
                                                self.navigationController?.pushViewController(plan_VC, animated: true)
                                            }
                                        }
                                        else {
                                            let plan_VC = self.storyboard?.instantiateViewController(withIdentifier: "Plan_VC") as! Plan_VC
                                            self.navigationController?.pushViewController(plan_VC, animated: true)
                                        }
                                    }
                                    else {
                                        let plan_VC = self.storyboard?.instantiateViewController(withIdentifier: "Plan_VC") as! Plan_VC
                                        self.navigationController?.pushViewController(plan_VC, animated: true)
                                    }
                                }
                                
                                
                                
                                
//                                if let endDate = PlanManager.shared.stringToDate(strDate: user.plan_end_date ?? "") {
//                                    if Date() < endDate {
//                                        let initialViewController = self.storyboard?.instantiateViewController(withIdentifier: "TabBarController") as! TabBarController
//                                        self.navigationController?.pushViewController(initialViewController, animated: true)
//                                    }
//                                    else {
//                                        let plan_VC = self.storyboard?.instantiateViewController(withIdentifier: "Plan_VC") as! Plan_VC
//                                        self.navigationController?.pushViewController(plan_VC, animated: true)
//                                    }
//                                }
                                
                            }
                            else if user.lastupdated_status == UserProfile_lastUpdated.paymentDone.rawValue {
                                CommonFunctions.shared.initializeUser()
                                let initialViewController = self.storyboard?.instantiateViewController(withIdentifier: "TabBarController") as! TabBarController
                                self.navigationController?.pushViewController(initialViewController, animated: true)
                            }
                        }
                    })
                   
                 } else {
                    //no user found
                    //Nitin Skip
//                    let step1_obj = self.storyboard?.instantiateViewController(withIdentifier: "Match_SignUPForm_VC") as? Match_SignUPForm_VC
//                    let objStep1 = signUpStep_1()
//                    objStep1.fullName = "Nitin"
//                    step1_obj?.objStep1 = objStep1
//                    self.navigationController?.pushViewController(step1_obj!, animated: true)
                    
                    let link_obj = self.storyboard?.instantiateViewController(withIdentifier: "Link_LinkedInOrInsta_VC") as? Link_LinkedInOrInsta_VC
                    self.navigationController?.pushViewController(link_obj!, animated: true)
                }
            
            })
            
            
           /* if SessionManager.get_lastupdated_status() == "step1" || SessionManager.get_lastupdated_status() == "step2" {
                self.webserviceToGetUserProfile()
            }
            else if SessionManager.get_lastupdated_status() == "" {
                let link_obj = self.storyboard?.instantiateViewController(withIdentifier: "Link_LinkedInOrInsta_VC") as? Link_LinkedInOrInsta_VC
                self.navigationController?.pushViewController(link_obj!, animated: true)
            }
            else if SessionManager.get_lastupdated_status() == "step3" {
                let initialViewController = self.storyboard?.instantiateViewController(withIdentifier: "TabBarController") as! TabBarController
                self.navigationController?.pushViewController(initialViewController, animated: true)
            }*/
        }
    }
}

extension RegisterOTP_VC: UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let currentString: NSString = textField.text! as NSString
        let newString: NSString = currentString.replacingCharacters(in: range, with: string) as NSString
        return newString.length <= 6
    }
}
