//
//  CountryPicker_VC.swift
//  EliteMatch
//
//  Created by Apple on 28/08/19.
//  Copyright © 2019 Kitlabs-M-0002. All rights reserved.
//

import UIKit

protocol countryPickerSelectionDelegates {
    
    func didSelectCountries(countries: [MICountry])
    func didCancel()
}

class MICountry: NSObject {
    @objc let name: String
    let code: String
    var section: Int?
    let dialCode: String!
    
    init(name: String, code: String, dialCode: String = " - ") {
        self.name = name
        self.code = code
        self.dialCode = dialCode
    }
}

struct Section {
    var countries: [MICountry] = []
    
    mutating func addCountry(_ country: MICountry) {
        countries.append(country)
    }
}

class CountryPicker_VC: UIViewController {
    
    open var customCountriesCode: [String]?
    
    fileprivate lazy var CallingCodes = { () -> [[String: String]] in
        let resourceBundle = Bundle(for: MICountryPicker.classForCoder())
        guard let path = resourceBundle.path(forResource: "CallingCodes", ofType: "plist") else { return [] }
        return NSArray(contentsOfFile: path) as! [[String: String]]
    }()
    fileprivate var searchController: UISearchController!
    fileprivate var filteredList = [MICountry]()
    fileprivate var unsourtedCountries : [MICountry] {
        let locale = Locale.current
        var unsourtedCountries = [MICountry]()
        let countriesCodes = customCountriesCode == nil ? Locale.isoRegionCodes : customCountriesCode!
        
        for countryCode in countriesCodes {
            let displayName = (locale as NSLocale).displayName(forKey: NSLocale.Key.countryCode, value: countryCode)
            let countryData = CallingCodes.filter { $0["code"] == countryCode }
            let country: MICountry
            
            if countryData.count > 0, let dialCode = countryData[0]["dial_code"] {
                country = MICountry(name: displayName!, code: countryCode, dialCode: dialCode)
            } else {
                country = MICountry(name: displayName!, code: countryCode)
            }
            unsourtedCountries.append(country)
        }
        
        return unsourtedCountries
    }
    
    fileprivate var _sections: [Section]?
    fileprivate var sections: [Section] {
        
        if _sections != nil {
            return _sections!
        }
        
        let countries: [MICountry] = unsourtedCountries.map { country in
            let country = MICountry(name: country.name, code: country.code, dialCode: country.dialCode)
            country.section = collation.section(for: country, collationStringSelector: #selector(getter: MICountry.name))
            return country
        }
        
        // create empty sections
        var sections = [Section]()
        for _ in 0..<self.collation.sectionIndexTitles.count {
            sections.append(Section())
        }
        
        // put each country in a section
        for country in countries {
            sections[country.section!].addCountry(country)
        }
        
        // sort each section
        for section in sections {
            var s = section
            s.countries = collation.sortedArray(from: section.countries, collationStringSelector: #selector(getter: MICountry.name)) as! [MICountry]
        }
        
        _sections = sections
        
        return _sections!
    }
    fileprivate let collation = UILocalizedIndexedCollation.current()
        as UILocalizedIndexedCollation
    open var didSelectCountryClosure: ((String, String) -> ())?
    open var didSelectCountryWithCallingCodeClosure: ((String, String, String) -> ())?
    var isSearching = Bool()
    
    convenience public init(completionHandler: @escaping ((String, String) -> ())) {
        self.init()
        self.didSelectCountryClosure = completionHandler
    }

    
    //MARK:- Outlets
    @IBOutlet weak var tblVw_country: UITableView!
    
    
    //MARK:- Variables
    var selectedCountries = [MICountry]()
    var delegate: countryPickerSelectionDelegates?
    
    //MARK:- UIView life-cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()

        //init
        self.tblVw_country.delegate = self
        self.tblVw_country.dataSource = self
        self.initView()
    }
    
    
    //MARK:- Custom methods
    func initView(){
        createSearchBar()
        self.selectedCountries.removeAll()
        tblVw_country.reloadData()
       // definesPresentationContext = true
    }
    
    fileprivate func createSearchBar() {
        if self.tblVw_country.tableHeaderView == nil {
            searchController = UISearchController(searchResultsController: nil)
            searchController.searchResultsUpdater = self
            searchController.dimsBackgroundDuringPresentation = false
            searchController.hidesNavigationBarDuringPresentation = false
            tblVw_country.tableHeaderView = searchController.searchBar
        }
    }
    
    fileprivate func filter(_ searchText: String) -> [MICountry] {
        filteredList.removeAll()
        isSearching = true
        sections.forEach { (section) -> () in
            section.countries.forEach({ (country) -> () in
                if country.name.characters.count >= searchText.characters.count {
                    let result = country.name.compare(searchText, options: [.caseInsensitive, .diacriticInsensitive], range: searchText.characters.startIndex ..< searchText.characters.endIndex)
                    if result == .orderedSame {
                        filteredList.append(country)
                    }
                }
            })
        }
        return filteredList
    }
    
    //MARK:- UIButton Actions
    @IBAction func didPress_cancel(_ sender: UIButton) {
        delegate?.didCancel()
        self.dismiss(animated: true , completion: nil)
    }
    
    @IBAction func didPress_done(_ sender: UIButton) {
        delegate?.didSelectCountries(countries: self.selectedCountries)
        self.dismiss(animated: true , completion: nil)
    }
    
}



extension CountryPicker_VC: UITableViewDelegate , UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if searchController.searchBar.text!.characters.count > 0 {
            return 1
        }
        return sections.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if searchController.searchBar.text!.characters.count > 0 {
            return filteredList.count
        }
        return sections[section].countries.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! countryPicker_tableCell
        
        let country: MICountry!
        let bundle = "assets.bundle/"
        
        if searchController.searchBar.text!.characters.count > 0 {
            country = filteredList[(indexPath as NSIndexPath).row]
        } else {
            country = sections[(indexPath as NSIndexPath).section].countries[(indexPath as NSIndexPath).row]
        }
        
        cell.lblTitle.text = country.name
        cell.imgVwIcon.image = UIImage(named: bundle + country.code.lowercased() + ".png", in: Bundle(for: MICountryPicker.self), compatibleWith: nil)
        
        if selectedCountries.contains(country) {
            cell.imgVwSelection.image = UIImage(named: selectionImageName.selected.rawValue)
        }
        else {
             cell.imgVwSelection.image = UIImage(named: selectionImageName.UnSelected.rawValue)
        }
       
        return cell
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if !sections[section].countries.isEmpty {
            return self.collation.sectionTitles[section] as String
        }
        return ""
    }
    
    func sectionIndexTitles(for tableView: UITableView) -> [String]? {
        return collation.sectionIndexTitles
    }
    
    func tableView(_ tableView: UITableView,
                                 sectionForSectionIndexTitle title: String,
                                 at index: Int)
        -> Int {
            return collation.section(forSectionIndexTitle: index)
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if let selectedCell = tableView.cellForRow(at: indexPath) as? countryPicker_tableCell {
            
            let country: MICountry!
            if searchController.searchBar.text!.characters.count > 0 {
                country = filteredList[(indexPath as NSIndexPath).row]
            } else {
                country = sections[(indexPath as NSIndexPath).section].countries[(indexPath as NSIndexPath).row]
            }
            
            //to Deselect
            if selectedCountries.contains(country) {
                let indexToRemove = selectedCountries.firstIndex(where: {$0 === country})
                selectedCountries.remove(at: indexToRemove ?? 0)
                selectedCell.imgVwSelection.image = UIImage(named: selectionImageName.UnSelected.rawValue)
            }
                //to select
            else {
                selectedCountries.append(country)
                selectedCell.imgVwSelection.image = UIImage(named: selectionImageName.selected.rawValue)
            }
            
            //delegates call
//            delegate?.countryPicker(self, didSelectCountryWithName: country.name, code: country.code)
//            delegate?.countryPicker?(self, didSelectCountryWithName: country.name, code: country.code, dialCode: country.dialCode)
//            didSelectCountryClosure?(country.name, country.code)
//            didSelectCountryWithCallingCodeClosure?(country.name, country.code, country.dialCode)
        }
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
}

extension CountryPicker_VC: UISearchResultsUpdating {
    
    public func updateSearchResults(for searchController: UISearchController) {
        filter(searchController.searchBar.text!)
        tblVw_country.reloadData()
    }
}



enum selectionImageName: String {
    case selected = "YBTextPicker_checked.png"
    case UnSelected = "YBTextPicker_unchecked.png"
}
