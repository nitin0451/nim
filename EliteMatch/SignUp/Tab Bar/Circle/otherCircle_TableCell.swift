//
//  otherCircle_TableCell.swift
//  EliteMatch
//
//  Created by Apple on 06/09/19.
//  Copyright © 2019 Kitlabs-M-0002. All rights reserved.
//

import UIKit
import SwipeCellKit

class otherCircle_TableCell: SwipeTableViewCell {
    
    @IBOutlet weak var userImage: UIImageView!
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var lblLastMsg: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblUnreadCount: UILabel!
    @IBOutlet weak var imgVw_sticker: UIImageView!
    
    var opponentNo:String = ""

    var dialog: QBChatDialog? {
        didSet {
            self.lblUserName.text = ""
            self.userImage.image = nil
            updateView()
        }
    }
    
    
    func updateView() {
        
        if let name = self.dialog?.name {
            let arrName = name.components(separatedBy: "-")
            if arrName.indices.contains(0){
                self.lblUserName.text = arrName[0]
            }
            
            if arrName.indices.contains(1){
                self.opponentNo = arrName[1]
                ProfileManager.shared.getRefreshImage(url: ProfileManager.shared.getOpponentProfileUrl(opponentNo: arrName[1]) , imageView: self.userImage) { (image) in
                    if let img = image {
                        self.userImage.image = img
                    }
                }
            }
        }
        
        
        if let msg = dialog?.lastMessageText {
            if MessageDialogManager.shared.isSticker(msgText: msg).isSticker {
                self.imgVw_sticker.isHidden = false
                self.lblLastMsg.text = ""
                self.imgVw_sticker.image = UIImage(named: MessageDialogManager.shared.isSticker(msgText: msg).stickerImg)
            }
            else {
                self.imgVw_sticker.isHidden = true
                MessageDialogManager.shared.setLastMsg(msgText: msg, dialog: self.dialog!) { (lastMsgText) in
                    self.lblLastMsg.text = lastMsgText
                }
            }
        } else {
            self.imgVw_sticker.isHidden = true
            self.lblLastMsg.text = "Say hi to your new friend..."
        }
        
        
        if let unreadCount = dialog?.unreadMessagesCount {
            if unreadCount > 0 {
                self.lblUnreadCount.isHidden = false
                self.lblUnreadCount.text = "\(unreadCount)"
                self.lblLastMsg.font = UIFont.init(name:"CircularStd-Medium",size:12)
            } else {
                self.lblUnreadCount.isHidden = true
                self.lblLastMsg.font = UIFont.init(name:"CircularStd-Book",size:12)
            }
        }
        if let date = dialog?.updatedAt {
            if let formattedDate = QMDateUtils.formattedShortDateString(date) {
                self.lblDate.text = formattedDate
            }
        }
    }
    
    
    func getLastMsg(msgText:String , completionHandler:@escaping (String) -> ()){
        
        //define last message
        if msgText == "Image attachment" || msgText == "Audio attachment" || msgText == "Video attachment"{
            completionHandler("Media")
        }
        else {
            completionHandler(msgText)
        }
    }
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        CommonFunctions.shared.setCornerRadius_imageView(imageView: self.userImage , radius: self.userImage.frame.size.width / 2)
        CommonFunctions.shared.setCornerRadius_label(label: self.lblUnreadCount , radius: self.lblUnreadCount.frame.size.width/2)
       
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }

}
