//
//  ChaperonContact_tableCell.swift
//  EliteMatch
//
//  Created by Apple on 30/08/19.
//  Copyright © 2019 Kitlabs-M-0002. All rights reserved.
//

import UIKit

class ChaperonContact_tableCell: UITableViewCell {
    
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var imgVwSelection: UIImageView!
    @IBOutlet weak var imgVwUser: UIImageView!
    @IBOutlet weak var lblContactStartAlphabet: UILabel!
    
    @IBOutlet weak var lblSelection: UILabel!
    

    override func awakeFromNib() {
        super.awakeFromNib()

        self.imgVwUser.layer.cornerRadius = self.imgVwUser.frame.size.width / 2.0
        self.imgVwUser.clipsToBounds = true
        self.designLblSelection()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func designLblSelection(){
        self.lblSelection.layer.cornerRadius = self.lblSelection.frame.height / 2.0
        self.lblSelection.clipsToBounds = true
        self.lblSelection.layer.borderColor = constantVC.GlobalColor.lightPurple.cgColor
        self.lblSelection.layer.borderWidth = 1.0
    }

}
