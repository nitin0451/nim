//
//  InviteFriends_VC.swift
//  EliteMatch
//
//  Created by Apple on 30/08/19.
//  Copyright © 2019 Kitlabs-M-0002. All rights reserved.
//

import UIKit
import MessageUI
import FTIndicator
import ObjectMapper

class InviteFriends_VC: UIViewController {
    
    //MARK:- Outlets
    @IBOutlet weak var tblView: UITableView! {
        didSet{
            self.tblView.delegate = self
            self.tblView.dataSource = self
        }
    }
    
    //MARK:- Variables
    var arrSectionTitle:[String] = ["Friends who are active on NIM" , "Invite Others"]
    var arrActiveUsers:[QBUUser] = []
    var arrUnRegisteredUsers:[QBAddressBookContact] = []
    var dialogID:String = ""
    var dialog:QBChatDialog!
    var isNavigateFromCircleVC:Bool = false
    
    //selected
    var arrSelectedActiveUser:[QBUUser] = []
    var arrSelectedUnRegisteredUsers:[QBAddressBookContact] = []

    var activeUsersData = [ActiveUsersModalClass]()
    var arrayActiveUserData = [ActiveUsersModalClass]()
    
    //MARK:- UIView life-cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()

        self.getApprovedUsers { (data, dataArray) in
            var arr = [ActiveUsersModalClass]()
            if data {
                if let data = dataArray as? [ActiveUsersModalClass] {
                  arr = data
                }
            }
            self.getNonRegisteredContacts(activeData: arr)
         // self.getRegisteredNIMContacts()
        }
        
    }
    
    //MARK:- Custom Methods
    func getRegisteredNIMContacts(){
        ContactManager.shared.retriveRegisteredUsersFromAddressBook(completionHandler: { (users) in
            if var users_ = users {
                
                if users_.contains(ServicesManager.instance().currentUser) {
                    if let index = users_.index(of: ServicesManager.instance().currentUser) {
                        users_.remove(at: index)
                    }
                }
                
                //check for names
                for user in users_ {
                    if user.fullName == nil {
                        if let index = users_.index(of: user) {
                            users_.remove(at: index)
                        }
                    }
                }
                
              self.arrActiveUsers = users_
            }
            DispatchQueue.main.async {
                self.tblView.reloadData()
            }
            //self.getNonRegisteredContacts()
        })
    }
    
    func getNonRegisteredContacts(activeData : [ActiveUsersModalClass]){
        
        self.arrUnRegisteredUsers.removeAll()

        for contact_ in constantVC.GlobalVariables.arrQBAddressContact {
        
//            var isUserRegistered:Bool = false
//            for user_ in self.arrActiveUsers {
//                if let phone = user_.phone {
//                    if phone == contact_.phone {
//                        isUserRegistered = true
//                        break
//                    }
//                }
//            }
        
            for data in activeData {
                if let phone = data.userData?.phoneNumber {
                    if phone == contact_.phone {
                        self.activeUsersData.append(data)
                    } else {
                        if contact_.name != "" {
                            self.arrUnRegisteredUsers.append(contact_)
                        }
                    }
                }
            }
            
//            if !isUserRegistered {
//
//            }
        }
        DispatchQueue.main.async {
            self.tblView.reloadData()
        }
    }
    
    //MARK:- UIButton Actions
    @IBAction func didPress_close(_ sender: UIButton) {
        self.dismiss(animated: true , completion: nil)
    }
    
    @IBAction func didPress_invites(_ sender: UIButton) {
        //arrSelectedActiveUser
        if self.arrayActiveUserData.count > 0 {
            
            var arrOpponentIDS:[NSNumber] = []
            for user_ in self.arrSelectedActiveUser {
                arrOpponentIDS.append(NSNumber(value: user_.id))
            }
            
            //create circle if not created
            DialogManagerNIM.instance.isMyCircleCreated { (success , chatDialog) in
                if success {
                    //join users
                    if let dialog = chatDialog {
                        
                        for user_ in self.arrSelectedActiveUser {
                            self.sendInvitationToCircle(dialog: dialog , requestTo_: "\(user_.externalUserID)")
                        }
                        
                        DialogManagerNIM.instance.addUsersTo_MyCircle(arrOpponentId: arrOpponentIDS , dialog: dialog , completionHandler: { (success) in
                        })
                    }
                } else {
                    //create circle
                    DialogManagerNIM.instance.createMyCircle(users: self.arrSelectedActiveUser , completionHandler: { (success , chatDialog) in
                        if let dialog = chatDialog {
                            for user_ in self.arrSelectedActiveUser {
                                self.sendInvitationToCircle(dialog: dialog , requestTo_: "\(user_.externalUserID)")
                            }
                        }
                    })
                }
                FTIndicator.showSuccess(withMessage: "Invitation sent successfully!")
            }
        }
        
        if self.arrSelectedUnRegisteredUsers.count > 0 {
            var arrPhoneNoToInvite:[String] = []
            
            if SessionManager.isChaperonUser() {
                
                for contact_ in self.arrSelectedUnRegisteredUsers {
                    
                    let result = self.isUserRegusteredOnNIM(phNO: contact_.phone)
                    if result.0 == true {
                        if let user = result.1 {
                            self.arrSelectedActiveUser.append(user)
                        }
                    }
                    else {
                        arrPhoneNoToInvite.append(contact_.phone)
                    }
                    
                }
                
                //send invitation to join other's circle
                for user_ in self.arrSelectedActiveUser {
                    self.sendInvitationToCircle_toAdded(requestTo_: "\(user_.externalUserID)")
                }
                
            } else {
                for item in self.arrSelectedUnRegisteredUsers {
                    arrPhoneNoToInvite.append(item.phone)
                }
            }
            
            self.sendTextForInvitation(phoneNos: arrPhoneNoToInvite)   //TODO
            FTIndicator.showSuccess(withMessage: "Invitation sent successfully!")
        }
        else {
            self.navigateToTabBar()
        }
        
        
    }
    
    
    //MARK:- Custom Methods
    
    func isUserRegusteredOnNIM(phNO:String) ->(Bool , QBUUser?){
        for user_ in self.arrActiveUsers {
            if let phone = user_.phone {
                if phone == phNO {
                    return (true , user_)
                }
            }
        }
        return (false , nil)
        
    }
    
    func sendInvitationToCircle(dialog:QBChatDialog , requestTo_:String){
        
        //send invite to users to get added in my circle
        InvitationManager.instance.sendConnect_circle_invitation(dialogID: self.getDilogID(dialog: dialog), requestTo:requestTo_ , fromName: SessionManager.getUserName()) { (success) in
        }
    }
    
    func sendInvitationToCircle_toAdded(requestTo_:String){
        //send invite to users to get added in my circle
        InvitationManager.instance.sendConnect_circle_invitation(dialogID: "" , requestTo:requestTo_ , fromName: SessionManager.getUserName()) { (success) in
        }
    }
    
    func getDilogID(dialog:QBChatDialog) ->String {
        if let id = dialog.id {
            return id
        }
        return ""
    }
   
    func navigateToTabBar(){
        CommonFunctions.shared.initializeUser()
        self.dismiss(animated: true) {
            if !self.isNavigateFromCircleVC {
                let initialViewController = self.storyboard?.instantiateViewController(withIdentifier: "TabBarController") as! TabBarController
                if let topVC = UIApplication.topViewController() {
                    topVC.navigationController?.pushViewController(initialViewController, animated: true)
                }
            }
        }
    }
    
    func set_unselectLable(label:UILabel){
        label.backgroundColor = UIColor.white
        label.textColor = constantVC.GlobalColor.lightPurple
        label.text = "+ Add"
    }
    
    func set_SelectLable(label:UILabel){
        label.backgroundColor = constantVC.GlobalColor.lightPurple
        label.textColor = UIColor.white
        label.text = "Added"
    }
    
    
    func set_unselectLable_unregistered(label:UILabel){
        label.backgroundColor = UIColor.white
        label.textColor = constantVC.GlobalColor.lightPurple
        label.text = "+ Invite"
    }
    
    func set_SelectLable_unregistered(label:UILabel){
        label.backgroundColor = constantVC.GlobalColor.lightPurple
        label.textColor = UIColor.white
        label.text = "Selected"
    }
    
    func sendTextForInvitation(phoneNos: [String]){
        if (MFMessageComposeViewController.canSendText()) {
            let controller = MFMessageComposeViewController()
            controller.body = "Hey, download this app, it lets me find you matches 🤲🏼 https://www.nimapp.co"
            controller.recipients = phoneNos
            controller.messageComposeDelegate = self
            if let topVC = UIApplication.topViewController() {
                topVC.present(controller, animated: true, completion: nil)
            }
        }
    }
    
}


extension InviteFriends_VC: UITableViewDelegate , UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView.init(frame: CGRect.init(x: 0, y: 0, width: tableView.frame.width, height: 50))
        
        headerView.backgroundColor = UIColor.white
        let label = UILabel()
        label.frame = CGRect.init(x: 5, y: 5, width: headerView.frame.width-10, height: headerView.frame.height-10)
        
        if SessionManager.isChaperonUser() {
            label.text = self.arrSectionTitle[1]
        }
        else {
            label.text = self.arrSectionTitle[section]
        }
        label.font = UIFont.init(name:"CircularStd-Medium",size:15)
        label.textColor = UIColor.black
        headerView.addSubview(label)
        return headerView
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if SessionManager.isChaperonUser() {
            return 1
        }
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if SessionManager.isChaperonUser() {
            return self.arrUnRegisteredUsers.count
        }
        else {
            if section == 0{
               // return self.arrActiveUsers.count
                return self.activeUsersData.count

            }
            return self.arrUnRegisteredUsers.count
        }
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 85.0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell" , for: indexPath) as! ChaperonContact_tableCell
        
        if SessionManager.isChaperonUser() {
                if self.arrUnRegisteredUsers.indices.contains(indexPath.row) {
                    let contact = self.arrUnRegisteredUsers[indexPath.row]
                    
                    cell.lblName.text = contact.name
                    let strprefix = contact.name.prefix(2)
                    cell.lblContactStartAlphabet.text = String(strprefix)
                    cell.imgVwUser.image = nil
                    cell.imgVwUser.backgroundColor = CommonFunctions.shared.getRandomColor(indexpath: indexPath)
                    
                    //for selection
                    if self.arrSelectedUnRegisteredUsers.contains(contact) {
                        self.set_SelectLable_unregistered(label: cell.lblSelection)
                    } else {
                        self.set_unselectLable_unregistered(label: cell.lblSelection)
                    }
                }
        }
        else {
            
            if indexPath.section == 0 {
//                if self.arrActiveUsers.indices.contains(indexPath.row) {
//                    let user = self.arrActiveUsers[indexPath.row]
//
//                    cell.lblName.text = user.fullName ?? ""
//                    cell.imgVwUser.yy_setImage(with: ProfileManager.shared.getOpponentProfileUrl(opponentNo: user.phone ?? "") , placeholder: ProfileManager.shared.getPlaceHolderUserImage())
//                    cell.lblContactStartAlphabet.text = ""
//
//                    //for selection
//                    if self.arrSelectedActiveUser.contains(user) {
//                        self.set_SelectLable(label: cell.lblSelection)
//                    } else {
//                        self.set_unselectLable(label: cell.lblSelection)
//                    }
//                }
                
                if self.activeUsersData.indices.contains(indexPath.row) {
                    let contact = self.activeUsersData[indexPath.row]
                    let contactData = contact.userData

                    cell.lblName.text = contactData?.name ?? ""
                    let strprefix = contactData?.name?.prefix(2)
                    cell.lblContactStartAlphabet.text = String(strprefix ?? "")
                    cell.imgVwUser.image = nil

                    cell.imgVwUser.backgroundColor = CommonFunctions.shared.getRandomColor(indexpath: indexPath)

                    if self.arrayActiveUserData.contains(where: {$0.userData?.id ?? "" == contactData?.id ?? ""}) {
                        self.set_SelectLable_unregistered(label: cell.lblSelection)
                    } else {
                        self.set_unselectLable_unregistered(label: cell.lblSelection)
                    }

                }
                
            }
            
            if indexPath.section == 1 {
                if self.arrUnRegisteredUsers.indices.contains(indexPath.row) {
                    let contact = self.arrUnRegisteredUsers[indexPath.row]
                    
                    cell.lblName.text = contact.name
                    let strprefix = contact.name.prefix(2)
                    cell.lblContactStartAlphabet.text = String(strprefix)
                    cell.imgVwUser.image = nil
                    cell.imgVwUser.backgroundColor = CommonFunctions.shared.getRandomColor(indexpath: indexPath)
                    
                    //for selection
                    if self.arrSelectedUnRegisteredUsers.contains(contact) {
                        self.set_SelectLable_unregistered(label: cell.lblSelection)
                    } else {
                        self.set_unselectLable_unregistered(label: cell.lblSelection)
                    }
                }
            }
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if SessionManager.isChaperonUser() {
            if let selectedCell = tableView.cellForRow(at: indexPath) as? ChaperonContact_tableCell {
                
                var selectedContact = self.arrUnRegisteredUsers[indexPath.row]
                
                if self.arrSelectedUnRegisteredUsers.contains(selectedContact) {
                    if let index = self.arrSelectedUnRegisteredUsers.index(of: selectedContact) {
                        self.arrSelectedUnRegisteredUsers.remove(at: index)
                        self.set_unselectLable_unregistered(label: selectedCell.lblSelection)
                    }
                } else {
                    self.arrSelectedUnRegisteredUsers.append(selectedContact)
                    self.set_SelectLable_unregistered(label: selectedCell.lblSelection)
                }
            }
        }
        else {
            if indexPath.section == 0 {
                if let selectedCell = tableView.cellForRow(at: indexPath) as? ChaperonContact_tableCell {
                    
                    var modalObj : ActiveUsersModalClass?
                                      
                    modalObj = self.activeUsersData[indexPath.row]

                    if self.arrayActiveUserData.contains(where: {$0.userData?.id ?? "" == modalObj?.userData?.id ?? ""}) {
                        if let index = self.arrayActiveUserData.firstIndex(where: {$0.userData?.id ?? "" == modalObj?.userData?.id ?? ""}) {
                            self.arrayActiveUserData.remove(at: index)
                            self.set_unselectLable_unregistered(label: selectedCell.lblSelection)
                        }
                    } else {
                        if let obj = modalObj {
                            self.arrayActiveUserData.append(obj)
                            self.set_SelectLable_unregistered(label: selectedCell.lblSelection)
                        }
                    }
//
//
//                    var selectedUser = self.arrActiveUsers[indexPath.row]
//
//                    if self.arrSelectedActiveUser.contains(selectedUser) {
//                        if let index = self.arrSelectedActiveUser.index(of: selectedUser) {
//                            self.arrSelectedActiveUser.remove(at: index)
//                            self.set_unselectLable(label: selectedCell.lblSelection)
//                        }
//                    } else {
//                        self.arrSelectedActiveUser.append(selectedUser)
//                        self.set_SelectLable(label: selectedCell.lblSelection)
//                    }
                }
            }
            
            if indexPath.section == 1 {
                if let selectedCell = tableView.cellForRow(at: indexPath) as? ChaperonContact_tableCell {
                    
                    var selectedContact = self.arrUnRegisteredUsers[indexPath.row]
                    
                    if self.arrSelectedUnRegisteredUsers.contains(selectedContact) {
                        if let index = self.arrSelectedUnRegisteredUsers.index(of: selectedContact) {
                            self.arrSelectedUnRegisteredUsers.remove(at: index)
                            self.set_unselectLable_unregistered(label: selectedCell.lblSelection)
                        }
                    } else {
                        self.arrSelectedUnRegisteredUsers.append(selectedContact)
                        self.set_SelectLable_unregistered(label: selectedCell.lblSelection)
                    }
                }
            }
        }
    }
}


extension InviteFriends_VC: MFMessageComposeViewControllerDelegate{
    
    func messageComposeViewController(_ controller: MFMessageComposeViewController, didFinishWith result: MessageComposeResult) {
        switch (result) {
        case .cancelled:
            print("Message was cancelled")
        case .failed:
            print("Message failed")
        case .sent:
            print("Message was sent")
            FTIndicator.showSuccess(withMessage: "Invitation sent successfully!")
            dismiss(animated: true, completion: nil)
        default:
            break
        }
        self.dismiss(animated: true, completion: nil)
    }
}

extension InviteFriends_VC {
    
    func getApprovedUsers(completionHandler:@escaping (Bool , NSArray) -> ()){
        
        WebserviceSigleton().GETService(urlString: constantVC.WebserviceName.URL_MATCHED_USER + "\(SessionManager.get_Elite_userID())" ) { (result , error) in
            if let dicResponse = result as NSDictionary? {
                if let msg = dicResponse["msg"] as? String {
                    if msg == responseStatus.success.rawValue {
                        if let data = dicResponse["msg_details"] as? [Dictionary<String,Any>] {
                            var arrayData = [ActiveUsersModalClass]()
                            for obj in data {
                                if let modalObj = Mapper<ActiveUsersModalClass>().map(JSON: obj) {
                                    arrayData.append(modalObj)
                                }
                            }
                            completionHandler(true , arrayData as NSArray)
                        }
                    }
                    else {
                        completionHandler(false , [])
                    }
                }
            } else {
                completionHandler(false , [])
            }
        }
    }
    
}
