//
//  MyCircle_TableCell.swift
//  EliteMatch
//
//  Created by Apple on 06/09/19.
//  Copyright © 2019 Kitlabs-M-0002. All rights reserved.
//

import UIKit
import SwipeCellKit

class MyCircle_TableCell: SwipeTableViewCell {
    
    @IBOutlet weak var userImage1: UIImageView!
    @IBOutlet weak var userImage2: UIImageView!
    @IBOutlet weak var userImage3: UIImageView!
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var lblLastMsg: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblUnreadCount: UILabel!
    @IBOutlet weak var imgVw_sticker: UIImageView!
    
    var opponentNo:String = ""
    
    var dialog: QBChatDialog? {
        didSet {
            self.lblUserName.text = ""
            updateView()
        }
    }
    
    
    func updateView() {
        
        self.lblUserName.text = "Your Circle"
        
        if let customData = dialog?.data {
            if let strDialogData = customData["dialogData"] as? String {
                if let dic = CommonFunctions.shared.jsonToDic(jsonString: strDialogData) {
                    if let dicChaperon = dic["CircleRequestAccepted"] as? NSDictionary {
                        if let phoneNos = dicChaperon.object(forKey: "phoneNumber") as? String {
                            
                            let arrPhoneNos = phoneNos.components(separatedBy: ",")
                            
                            ProfileManager.shared.getRefreshImage(url: ProfileManager.shared.getOpponentProfileUrl(opponentNo: SessionManager.get_phonenumber()) , imageView: self.userImage1) { (image) in
                                if let img = image {
                                    self.userImage1.image = img
                                }
                            }
                          
                            if arrPhoneNos.indices.contains(1) {
                                ProfileManager.shared.getRefreshImage(url: ProfileManager.shared.getOpponentProfileUrl(opponentNo: arrPhoneNos[1]) , imageView: self.userImage2) { (image) in
                                    if let img = image {
                                        self.userImage2.image = img
                                    }
                                }
                            }
                            
                            if arrPhoneNos.indices.contains(2) {
                                ProfileManager.shared.getRefreshImage(url: ProfileManager.shared.getOpponentProfileUrl(opponentNo: arrPhoneNos[2]) , imageView: self.userImage3) { (image) in
                                    if let img = image {
                                        self.userImage3.image = img
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        
        if let msg = dialog?.lastMessageText {
            if MessageDialogManager.shared.isSticker(msgText: msg).isSticker {
                self.imgVw_sticker.isHidden = false
                self.lblLastMsg.text = ""
                self.imgVw_sticker.image = UIImage(named: MessageDialogManager.shared.isSticker(msgText: msg).stickerImg)
            }
            else {
                self.imgVw_sticker.isHidden = true
                MessageDialogManager.shared.setLastMsg(msgText: msg, dialog: self.dialog!) { (lastMsgText) in
                    self.lblLastMsg.text = lastMsgText
                }
            }
        } else {
            self.imgVw_sticker.isHidden = true
            self.lblLastMsg.text = "Say hi to your new friend..."
        }
        
        if let unreadCount = dialog?.unreadMessagesCount {
            if unreadCount > 0 {
                self.lblUnreadCount.isHidden = false
                self.lblUnreadCount.text = "\(unreadCount)"
                self.lblLastMsg.font = UIFont.init(name:"CircularStd-Medium",size:12)
            } else {
                self.lblUnreadCount.isHidden = true
                self.lblLastMsg.font = UIFont.init(name:"CircularStd-Book",size:12)
            }
        }
        if let date = dialog?.updatedAt {
            if let formattedDate = QMDateUtils.formattedShortDateString(date) {
                self.lblDate.text = formattedDate
            }
        }
    }
    
    
    func getLastMsg(msgText:String , completionHandler:@escaping (String) -> ()){
        
        //define last message
        if msgText == "Image attachment" || msgText == "Audio attachment" || msgText == "Video attachment"{
            completionHandler("Media")
        }
        else {
            completionHandler(msgText)
        }
    }
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        CommonFunctions.shared.setCornerRadius_imageView(imageView: self.userImage1 , radius: self.userImage1.frame.size.width / 2)
        CommonFunctions.shared.setCornerRadius_imageView(imageView: self.userImage2 , radius: self.userImage2.frame.size.width / 2)
        CommonFunctions.shared.setCornerRadius_imageView(imageView: self.userImage3 , radius: self.userImage3.frame.size.width / 2)
        CommonFunctions.shared.setCornerRadius_label(label: self.lblUnreadCount , radius: self.lblUnreadCount.frame.size.width/2)
        addBorder_imageView(imageView: self.userImage1 , borderColour_: .white , borderWidth: 1.0)
        addBorder_imageView(imageView: self.userImage2 , borderColour_: .white , borderWidth: 1.0)
        addBorder_imageView(imageView: self.userImage3 , borderColour_: .white , borderWidth: 1.0)
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }

    
    func addBorder_imageView(imageView:UIImageView , borderColour_: UIColor , borderWidth: CGFloat){
        imageView.layer.borderColor = borderColour_.cgColor
        imageView.layer.borderWidth = borderWidth
    }
}
