//
//  Circle_VC.swift
//  EliteMatch
//
//  Created by Apple on 06/09/19.
//  Copyright © 2019 Kitlabs-M-0002. All rights reserved.
//

import UIKit
import SwipeCellKit
import FTIndicator
import PushKit
import CallKit
import QuickbloxWebRTC

class Circle_VC: UIViewController {
    
    //MARK:- Outlets
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var btnInviteFriends: UIButton!
    @IBOutlet weak var vwNoCircle: UIView!
    
    //MARK:- Variables
    var tabBadgeCount:Int = 0
    var refreshControl = UIRefreshControl()
    var CallByNo:String = ""
    var isGroup:String = ""
    let core = Core.instance
    
    lazy private var dataSource: UsersDataSource = {
        let dataSource = UsersDataSource(currentUser: Core.instance.currentUser)
        return dataSource
    }()
    lazy private var voipRegistry: PKPushRegistry = {
        let voipRegistry = PKPushRegistry(queue: DispatchQueue.main)
        return voipRegistry
    }()
    lazy private var backgroundTask: UIBackgroundTaskIdentifier = {
        let backgroundTask: UIBackgroundTaskIdentifier = UIBackgroundTaskInvalid
        return backgroundTask
    }()
    var completionBlock: Completion?
    
    
    //MARK:- UIView life-cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //init
        self.congigureQBChat()
        self.initView()
        self.configurePullToRefresh()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.configureNavigationBar()
        self.gettotalUnreadChats()
        self.getMessageDialogs()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.isNavigationBarHidden = true
    }
    
    
    //MARK:- Helpers
    func gettotalUnreadChats() {
        
        self.tabBadgeCount = 0
        if let dialogs = self.dialogs() {
            for dialog in dialogs {
                if let unreadCount = dialog.unreadMessagesCount as? UInt{
                    if unreadCount > 0 {
                        self.tabBadgeCount = self.tabBadgeCount + 1
                    }
                }
            }
        }
        SessionManager.save_unreadMsgCount(count: self.tabBadgeCount)
        self.setTabBar_BadgeCount()
        self.btnInviteFriends.isHidden = false
    }
    
    func setTabBar_BadgeCount(){
        if SessionManager.get_unreadMsgCount() > 0  {
            tabBarController?.tabBar.items?[2].badgeColor = UIColor(red: 62.0/255, green: 208.0/255, blue: 11.0/255, alpha: 1.0)
            tabBarController?.tabBar.items?[2].badgeValue = "\(SessionManager.get_unreadMsgCount())"
        }
        else {
            tabBarController?.tabBar.items?[2].badgeColor = UIColor.clear
        }
    }
    
    func configurePullToRefresh(){
        // Add Refresh Control to Table View
        if #available(iOS 10.0, *) {
            tableView.refreshControl = refreshControl
        } else {
            tableView.addSubview(refreshControl)
        }
        refreshControl.addTarget(self, action: #selector(self.refreshView_refreshControl), for: .valueChanged)
        refreshControl.tintColor = UIColor.lightGray
    }
    
    func configureNavigationBar(){
        self.navigationController?.isNavigationBarHidden = false
        navigationController?.navigationBar.backgroundColor = UIColor.white
        navigationController?.navigationBar.titleTextAttributes = [.foregroundColor: UIColor(red: 54/255.0, green: 54/255.0, blue: 54/255.0, alpha: 1.0)]
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.font: UIFont(name: "CircularStd-Medium", size: 18)!]
        self.navigationItem.rightBarButtonItem?.tintColor = .gray
        tableView.tableFooterView = UIView()
        CommonFunctions.shared.setNavigationBackButton(navController: self.navigationController , isHidden: true)
    }
    
    func initView(){
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.font: UIFont(name: "CircularStd-Medium", size: 18)!]
        tableView.delegate = self
        tableView.dataSource = self
    }
    
    func congigureQBChat(){
        QBRTCClient.instance().add(self)
        QBChat.instance.addDelegate(self)
        voipRegistry.delegate = self
        voipRegistry.desiredPushTypes = Set<PKPushType>([.voIP])
        dataSource = UsersDataSource(currentUser: Core.instance.currentUser)
        CallKitManager.instance.usersDatasource = dataSource
    }
    
    @objc func didPressImagePreview(_ sender:AnyObject){
        
        if sender.view.tag != 0 {
            let indexPath = IndexPath(row: sender.view.tag , section: 0)
            if let cell = self.tableView.cellForRow(at: indexPath) as? otherCircle_TableCell {
                QMImagePreview.previewImage(with: ProfileManager.shared.getOpponentProfileUrl(opponentNo: cell.opponentNo) , in: self)
            }
        }
    }
    
    //MARK:- Dialogs Related Methods
    func getMessageDialogs(){
        
        ServicesManager.instance().chatService.addDelegate(self)
        ServicesManager.instance().authService.add(self)
        
        if let dialogs = self.dialogs() {
            if dialogs.count > 0 {
                self.vwNoCircle.isHidden = true
                self.tableView.reloadData()
            } else {
                self.vwNoCircle.isHidden = false
            }
        }
        self.gettotalUnreadChats()
    }
    
    func dialogs() -> [QBChatDialog]? {
        
        
        if SessionManager.isChaperonUser() {
            var arrDialogs = ServicesManager.instance().chatService.dialogsMemoryStorage.dialogsSortByUpdatedAt(withAscending: false)
            for item in arrDialogs {
               if (item.name?.contains("Circle"))! {
                if !self.isRequestAcceptedByUser(dialog: item) {
                        if let index = arrDialogs.index(of: item) {
                            arrDialogs.remove(at: index)
                        }
                    }
               } else {
                if let index = arrDialogs.index(of: item) {
                    arrDialogs.remove(at: index)
                }
            }
            }
            return arrDialogs
        }
        else {
            var arrDialogs = ServicesManager.instance().chatService.dialogsMemoryStorage.dialogsSortByUpdatedAt(withAscending: false)
            for item in arrDialogs {
                
                if (item.name?.contains("Circle"))! {
                    if item.name == DialogManagerNIM.instance.getMyCircleName() {
                        if let index = arrDialogs.index(of: item) {
                            arrDialogs.remove(at: index)
                        }
                        arrDialogs.insert(item , at: 0)
                    }
                    else if !self.isRequestAcceptedByUser(dialog: item) {
                        if let index = arrDialogs.index(of: item) {
                            arrDialogs.remove(at: index)
                        }
                    }
                }else {
                    if let index = arrDialogs.index(of: item) {
                        arrDialogs.remove(at: index)
                    }
                }
            }
            return arrDialogs
        }
       
    }
    
    func isRequestAcceptedByUser(dialog: QBChatDialog) ->Bool{
        if let customData = dialog.data {
            if let strDialogData = customData["dialogData"] as? String {
                if let dic = CommonFunctions.shared.jsonToDic(jsonString: strDialogData) {
                    if let dicChaperon = dic["CircleRequestAccepted"] as? NSDictionary {
                        if let phoneNos = dicChaperon.object(forKey: "phoneNumber") as? String {
                            if phoneNos.contains(SessionManager.get_phonenumber()) {
                                return true
                            }
                        }
                    }
                }
            }
        }
        return false
    }
    
    
    //MARK:- UIButton Actions
    
    @IBAction func didPress_notificationBell(_ sender: UIBarButtonItem) {
        CommonFunctions.shared.navigateNotificationVC()
    }
    
    @objc func refreshView_refreshControl() {
        
        if Connectivity.isConnectedToInternet() {
            self.reloadTableViewIfNeeded()
            self.refreshControl.endRefreshing()
        }
        else {
            refreshControl.endRefreshing()
        }
    }
   
    @IBAction func didPress_inviteFriends(_ sender: UIButton) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "InviteFriends_VC") as! InviteFriends_VC
        controller.isNavigateFromCircleVC = true
        if let topVC = UIApplication.topViewController() {
            topVC.present(controller , animated: true , completion: {
                self.navigationController?.popViewController(animated: false)
            })
        }
    }
    
    //MARK:- UINavigation
    
    func navigate_ChatVC(_indexPath : IndexPath){
        if let dialogs = self.dialogs() {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let controller = storyboard.instantiateViewController(withIdentifier: "ChatViewController") as! ChatViewController
            controller.dialog = dialogs[_indexPath.row]
            if let selectedCell = self.tableView.cellForRow(at: _indexPath) as? otherCircle_TableCell {
                controller.opponentNo = selectedCell.opponentNo
            }
            if let selectedCell = self.tableView.cellForRow(at: _indexPath) as? MyCircle_TableCell {
                controller.opponentNo = SessionManager.get_phonenumber()
            }
            controller.chatType = .circle
            self.navigationController?.pushViewController(controller , animated: false)
        }
    }
    
    
}


extension Circle_VC : UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 85
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let dialogs = self.dialogs() {
            return dialogs.count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell()
        
        guard let chatDialog = self.dialogs()?[indexPath.row] else {
            return cell
        }
        
        //My Circle
        if indexPath.row == 0 && !SessionManager.isChaperonUser(){
            let myCircleCell = tableView.dequeueReusableCell(withIdentifier: "myCircle", for: indexPath as IndexPath) as! MyCircle_TableCell
            myCircleCell.delegate = self
            myCircleCell.layoutIfNeeded()
            myCircleCell.dialog = chatDialog
            return myCircleCell
        }
        
        let otherCircleCell = tableView.dequeueReusableCell(withIdentifier: "otherCell", for: indexPath as IndexPath) as! otherCircle_TableCell
        otherCircleCell.delegate = self
        otherCircleCell.layoutIfNeeded()
        otherCircleCell.dialog = chatDialog
        
        //add tap gesture
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.didPressImagePreview(_:)))
        otherCircleCell.userImage.isUserInteractionEnabled = true
        otherCircleCell.userImage.tag = indexPath.row
        otherCircleCell.userImage.addGestureRecognizer(tapGestureRecognizer)
        return otherCircleCell
        
    }
}

extension Circle_VC : UITableViewDelegate{
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        DispatchQueue.main.async {
            self.navigate_ChatVC(_indexPath: indexPath)
            tableView.deselectRow(at: indexPath, animated: true)
        }
    }
}


extension Circle_VC: QMChatServiceDelegate , QMChatConnectionDelegate , QMAuthServiceDelegate , QBChatDelegate {
    
    // MARK: - QMChatServiceDelegate
    func chatService(_ chatService: QMChatService, didUpdateChatDialogInMemoryStorage chatDialog: QBChatDialog) {
        self.reloadTableViewIfNeeded()
    }
    
    func chatService(_ chatService: QMChatService,didUpdateChatDialogsInMemoryStorage dialogs: [QBChatDialog]){
        // self.reloadTableViewIfNeeded()
    }
    
    func chatService(_ chatService: QMChatService, didAddChatDialogsToMemoryStorage chatDialogs: [QBChatDialog]) {
        self.reloadTableViewIfNeeded()
    }
    
    func chatService(_ chatService: QMChatService, didAddChatDialogToMemoryStorage chatDialog: QBChatDialog) {
        self.reloadTableViewIfNeeded()
    }
    
    func chatService(_ chatService: QMChatService, didDeleteChatDialogWithIDFromMemoryStorage chatDialogID: String) {
        self.reloadTableViewIfNeeded()
    }
    
    func chatService(_ chatService: QMChatService, didAddMessagesToMemoryStorage messages: [QBChatMessage], forDialogID dialogID: String) {
        //self.reloadTableViewIfNeeded()
    }
    
    func chatService(_ chatService: QMChatService, didAddMessageToMemoryStorage message: QBChatMessage, forDialogID dialogID: String){
        
        if let dialog_ = ServicesManager.instance().chatService.dialogsMemoryStorage.chatDialog(withID: dialogID) as? QBChatDialog {
            
            //delete clear chat object from custom data
            if NimUserManager.shared.isClearChat(dialog: dialog_) {
                NimUserManager.shared.UnClearChat_updateDialog(dialog: dialog_) { (success) in
                }
            }
        }
        // self.reloadTableViewIfNeeded()
    }
    
    
    // MARK: QMChatConnectionDelegate
    func chatServiceChatDidFail(withStreamError error: Error) {
        // SVProgressHUD.showError(withStatus: error.localizedDescription)
    }
    
    func chatServiceChatDidAccidentallyDisconnect(_ chatService: QMChatService) {
        //SVProgressHUD.showError(withStatus: "SA_STR_DISCONNECTED".localized)
    }
    
    func chatServiceChatDidConnect(_ chatService: QMChatService) {
        // SVProgressHUD.showSuccess(withStatus: "SA_STR_CONNECTED".localized, maskType:.clear)
        // if !ServicesManager.instance().isProcessingLogOut! {
        NimUserManager.shared.getDialogs()
        // }
    }
    
    func chatService(_ chatService: QMChatService,chatDidNotConnectWithError error: Error){
        //SVProgressHUD.showError(withStatus: error.localizedDescription)
    }
    
    
    func chatServiceChatDidReconnect(_ chatService: QMChatService) {
        // SVProgressHUD.showSuccess(withStatus: "SA_STR_CONNECTED".localized, maskType: .clear)
        // if !ServicesManager.instance().isProcessingLogOut! {
        NimUserManager.shared.getDialogs()
        //  }
    }
    
    
    
    // MARK: - Helpers
    func reloadTableViewIfNeeded() {
        if !ServicesManager.instance().isProcessingLogOut! {
            self.gettotalUnreadChats()
            self.tableView.reloadData()
        }
    }
    
}


extension Circle_VC: SwipeTableViewCellDelegate {
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath, for orientation: SwipeActionsOrientation) -> [SwipeAction]? {
        if orientation == .right {
            
            let str_deletebtnTitle = "Delete"
            let str_confirmationMsg = "By deleting you are leaving this match conversation."
            
            //Delete Button
            let deleteAction = SwipeAction(style: .default , title: str_deletebtnTitle ) { action, indexPath in
                //show delete channel confirmation alert
                let alertController = UIAlertController(title: "Are you sure you want to delete this chat?", message: str_confirmationMsg, preferredStyle: .alert)
                
                // Create the actions
                let okAction = UIAlertAction(title: "Yes", style: UIAlertActionStyle.default) {
                    UIAlertAction in
                    
                    if Connectivity.isConnectedToInternet() {
                        //delete code
                        guard let dialog = self.dialogs()?[indexPath.row] else {
                            return
                        }
                        
                        let deleteDialogBlock = { (dialog: QBChatDialog!) -> Void in
                            
                            // Deletes dialog from server and cache.
                            ServicesManager.instance().chatService.deleteDialog(withID: dialog.id!, completion: { (response) -> Void in
                                
                                guard response.isSuccess else {
                                    SVProgressHUD.showError(withStatus: UserMsg.Error)
                                    return
                                }
                                SVProgressHUD.showSuccess(withStatus: UserMsg.chatDeleted)
                                self.getMessageDialogs()
                                
                            })
                        }
                        
                        if dialog.type == QBChatDialogType.private || dialog.type == QBChatDialogType.group {
                            deleteDialogBlock(dialog)
                        }
                    }
                    else {
                        //OFFLINE
                        FTIndicator.showToastMessage(UserMsg.noInternetConnection)
                    }
                }
                let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel) {
                    UIAlertAction in
                    NSLog("Cancel Pressed")
                }
                
                // Add the actions
                alertController.addAction(okAction)
                alertController.addAction(cancelAction)
                
                // Present the controller
                self.present(alertController, animated: true, completion: nil)
            }
            
            deleteAction.backgroundColor = UIColor.red
            deleteAction.font = UIFont.boldSystemFont(ofSize: 11)
            deleteAction.image = UIImage(named: "delete")
            
            //More Button
            let moreAction = SwipeAction(style: .default, title: "More") { action, indexPath in
                FTIndicator.showToastMessage("Under Development")
                action.fulfill(with: .delete)
            }
            moreAction.font = UIFont.boldSystemFont(ofSize: 11)
            moreAction.textColor = UIColor.darkGray
            moreAction.image = UIImage(named: "more_option")
            return [deleteAction,moreAction]
        }
        return []
    }
    
    func tableView(_ tableView: UITableView, editActionsOptionsForRowAt indexPath: IndexPath, for orientation: SwipeActionsOrientation) -> SwipeTableOptions {
        var options = SwipeTableOptions()
        options.expansionStyle = .none
        options.transitionStyle = .border
        return options
    }
    
}


extension Circle_VC: QBRTCClientDelegate {
    
    // MARK: - QBRTCClientDelegate
    func didReceiveNewSession(_ session: QBRTCSession, userInfo: [String : String]? = nil) {
        
        if constantVC.ActiveSession.session != nil {
            if constantVC.ActiveSession.session?.id != session.id {
                session.rejectCall(["reject": "User is busy on another call"])
                return
            }
        }
        
        if let userInfo = userInfo {
            if userInfo["callBy"] != nil {
                if let no = userInfo["callBy"] {
                    CallByNo = no as! String
                }
            }
            if userInfo["dialogId"] != nil {
                if let id = userInfo["dialogId"] {
                    constantVC.ActiveCall.dialogID = id as! String
                }
            }
            if userInfo["callByName"] != nil {
                if let id = userInfo["callByName"] {
                    constantVC.ActiveCall.senderName = id as! String
                }
            }
        }
        
        constantVC.ActiveSession.session = session
        constantVC.ActiveSession.activeCallUUID = UUID()
        var opponentIDs = [session.initiatorID]
        for userID in session.opponentsIDs {
            if userID.uintValue != core.currentUser?.id {
                opponentIDs.append(userID)
            }
        }
        
        CallKitManager.instance.reportIncomingCall(withUserIDs: opponentIDs,
                                                   session: session,
                                                   uuid: constantVC.ActiveSession.activeCallUUID , userInfo: userInfo ,
                                                   callBy: constantVC.ActiveCall.senderName ,
                                                   isGroup: self.isGroup,
                                                   onAcceptAction: { [weak self] in
                                                    guard let `self` = self else {
                                                        return
                                                    }
                                                    
                                                    
                                                    if session.conferenceType == QBRTCConferenceType.audio {
                                                        
                                                        if let topController = UIApplication.topViewController() {
                                                            if let callViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "TamAudioCall_VC") as? TamAudioCall_VC {
                                                                callViewController.session = session
                                                                callViewController.callUUID = constantVC.ActiveSession.activeCallUUID
                                                                callViewController.opponentNo = self.CallByNo
                                                                callViewController.userPhoto = self.CallByNo
                                                                callViewController.opponentName = constantVC.ActiveCall.senderName
                                                                topController.present(callViewController , animated: true , completion: nil)
                                                            }
                                                        }
                                                    }
                                                    else {
                                                        
                                                        if let topController = UIApplication.topViewController() {
                                                            if let callViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "TamVideoCall_VC") as? TamVideoCall_VC {
                                                                callViewController.session = session
                                                                callViewController.callUUID = constantVC.ActiveSession.activeCallUUID
                                                                callViewController.opponentNo = self.CallByNo
                                                                callViewController.isGroup = self.isGroup
                                                                callViewController.opponentName = constantVC.ActiveCall.senderName
                                                                topController.present(callViewController , animated: true , completion: nil)
                                                            }
                                                        }
                                                    }
                                                    
            }, completion: { (end) in
                debugPrint("end")
        })
        
    }
    
    
    func session(_ session: QBRTCSession, hungUpByUser userID: NSNumber, userInfo: [String : String]? = nil) {
        print("hungUpByUser")
        
        if constantVC.ActiveSession.session == session {
            
            if isGroup == "true" && !(constantVC.ActiveSession.isCallActive) && userID == session.initiatorID {
                
                if let topController = UIApplication.topViewController() {
                    constantVC.ActiveSession.session = nil
                    constantVC.ActiveSession.isCallActive = false
                    constantVC().cleanActiveCall()
                    topController.dismiss(animated: true , completion: nil)
                }
                
                if CallKitManager.isCallKitAvailable() == true {
                    CallKitManager.instance.endCall(with: constantVC.ActiveSession.activeCallUUID) {
                        debugPrint("endCall")
                    }
                    constantVC.ActiveSession.activeCallUUID = nil
                    constantVC.ActiveSession.session = nil
                }
            }
            
        }
    }
    
    func session(_ session: QBRTCSession, rejectedByUser userID: NSNumber, userInfo: [String : String]? = nil) {
        print("rejectedByUser")
        print(userInfo)
        if let info = userInfo as? [String : String] {
            if let rejectMsg = info["reject"] as? String {
                if let topController = UIApplication.topViewController() {
                    topController.dismiss(animated: true , completion: {
                        FTIndicator.showInfo(withMessage: rejectMsg)
                    })
                }
            }
        }
    }
    
    
    func sessionDidClose(_ session: QBRTCSession) {
        
        print("sessionDidClose")
        
        constantVC.ActiveSession.isCallActive = false
        
        if constantVC.ActiveSession.session == session {
            if backgroundTask != UIBackgroundTaskInvalid {
                UIApplication.shared.endBackgroundTask(backgroundTask)
                backgroundTask = UIBackgroundTaskInvalid
            }
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + Double(Int64(1.0 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC), execute: {
                if UIApplication.shared.applicationState == .background && self.backgroundTask == UIBackgroundTaskInvalid {
                    // dispatching chat disconnect in 1 second so message about call end
                    // from webrtc does not cut mid sending
                    // checking for background task being invalid though, to avoid disconnecting
                    // from chat when another call has already being received in background
                    QBChat.instance.disconnect(completionBlock: nil)
                }
            })
            
            //add call log
            if session.initiatorID == SessionManager.get_QuickBloxID() {
                
                if session.conferenceType == QBRTCConferenceType.audio {
                    
                    if session.initiatorID == SessionManager.get_QuickBloxID() {
                        //sender
                        if let senderID = SessionManager.get_QuickBloxID() {
                            
                            if constantVC.ActiveSession.isCallActive {
                                NimUserManager.shared.sendCallNotificationMessage(with: .hangUp , duration: NimUserManager.shared.parseDuration(constantVC.ActiveCall.duration) , opponentNo: constantVC.ActiveCall.receiverNo)
                                WebserviceSigleton().addCallLog(senderID: constantVC.ActiveCall.senderID , senderNo: constantVC.ActiveCall.senderNo , receiverNo: constantVC.ActiveCall.receiverNo , sessionID: session.id , membersID: constantVC.ActiveCall.membersID , isGroup: constantVC.ActiveCall.isGroup , isAudio: constantVC.ActiveCall.isAudio , dialogID: constantVC.ActiveCall.dialogID , GroupName: constantVC.ActiveCall.groupName , duration:  constantVC.ActiveCall.duration , callState: constantVC.callState.hangup.rawValue, groupPhoto: constantVC.ActiveCall.groupPhoto)
                            }
                            else {
                                NimUserManager.shared.sendCallNotificationMessage(with: .missedNoAnswer , duration: NimUserManager.shared.parseDuration("00:00:00") , opponentNo: constantVC.ActiveCall.receiverNo)
                                
                                WebserviceSigleton().addCallLog(senderID: constantVC.ActiveCall.senderID , senderNo: constantVC.ActiveCall.senderNo , receiverNo: constantVC.ActiveCall.receiverNo , sessionID: session.id , membersID: constantVC.ActiveCall.membersID , isGroup: constantVC.ActiveCall.isGroup, isAudio: constantVC.ActiveCall.isAudio, dialogID: constantVC.ActiveCall.dialogID , GroupName: constantVC.ActiveCall.groupName , duration: "00:00:00" , callState: constantVC.callState.missedNoAnswer.rawValue, groupPhoto: constantVC.ActiveCall.groupPhoto)
                            }
                            
                        }
                        
                    }
                }
                
                if session.conferenceType == QBRTCConferenceType.video {
                    
                    if session.initiatorID == SessionManager.get_QuickBloxID() {
                        //sender
                        if constantVC.ActiveSession.isCallActive {
                            NimUserManager.shared.sendCallNotificationMessage(with: .hangUp , duration: NimUserManager.shared.parseDuration(constantVC.ActiveCall.duration), opponentNo: constantVC.ActiveCall.receiverNo)
                            
                            WebserviceSigleton().addCallLog(senderID: constantVC.ActiveCall.senderID , senderNo: constantVC.ActiveCall.senderNo , receiverNo: constantVC.ActiveCall.receiverNo , sessionID: session.id , membersID: constantVC.ActiveCall.membersID , isGroup: constantVC.ActiveCall.isGroup, isAudio: constantVC.ActiveCall.isAudio , dialogID: constantVC.ActiveCall.dialogID , GroupName: constantVC.ActiveCall.groupName , duration: constantVC.ActiveCall.duration , callState: constantVC.callState.hangup.rawValue , groupPhoto: constantVC.ActiveCall.groupPhoto)
                        }
                        else {
                            NimUserManager.shared.sendCallNotificationMessage(with: .missedNoAnswer , duration: NimUserManager.shared.parseDuration("00:00:00"), opponentNo: constantVC.ActiveCall.receiverNo)
                            
                            WebserviceSigleton().addCallLog(senderID: constantVC.ActiveCall.senderID , senderNo: constantVC.ActiveCall.senderNo , receiverNo: constantVC.ActiveCall.receiverNo , sessionID: session.id , membersID: constantVC.ActiveCall.membersID , isGroup: constantVC.ActiveCall.isGroup, isAudio: constantVC.ActiveCall.isAudio , dialogID: constantVC.ActiveCall.dialogID , GroupName: constantVC.ActiveCall.groupName , duration: "00:00:00" , callState: constantVC.callState.missedNoAnswer.rawValue , groupPhoto: constantVC.ActiveCall.groupPhoto)
                        }
                        
                    }
                }
            }
            
            if let topController = UIApplication.topViewController() {
                
                if session.conferenceType == QBRTCConferenceType.video {     //VIDEO
                    
                    //maintain view for active and background
                    if CallActiveInBackgroundManager.shared.isActiveVideoView() {
                        topController.dismiss(animated: true , completion: nil)
                    }else {
                        CallActiveInBackgroundManager.shared.hideActiveVideoCallView()
                    }
                    CallActiveInBackgroundManager.shared.closeCall()
                    
                } else {                                                     //AUDIO
                    
                    //maintain view for active and background
                    if CallActiveInBackgroundManager.shared.isActiveAudioView() {
                        topController.dismiss(animated: true , completion: nil)
                    }else {
                        CallActiveInBackgroundManager.shared.hideActiveAudioCallView()
                    }
                    CallActiveInBackgroundManager.shared.closeCallAudio()
                }
            }
            
            if CallKitManager.isCallKitAvailable() == true {
                
                CallKitManager.instance.endCall(with: constantVC.ActiveSession.activeCallUUID) {
                    debugPrint("endCall")
                }
                constantVC().cleanActiveSession()
                constantVC().cleanActiveCall()
            }
        }
    }
}

extension Circle_VC: PKPushRegistryDelegate {
    // MARK: - PKPushRegistryDelegate
    func pushRegistry(_ registry: PKPushRegistry, didUpdate pushCredentials: PKPushCredentials, for type: PKPushType) {
        //  New way, only for updated backend
        guard let deviceIdentifier = UIDevice.current.identifierForVendor?.uuidString else {
            return
        }
        let subscription: QBMSubscription! = QBMSubscription()
        subscription.notificationChannel = QBMNotificationChannel.APNSVOIP
        subscription.deviceUDID = deviceIdentifier
        subscription.deviceToken = voipRegistry.pushToken(for: .voIP)
        subscription.devicePlatform = "iOS"
        
        QBRequest.createSubscription(subscription, successBlock: { (response: QBResponse!, objects: [QBMSubscription]?) -> Void in
            //
            print(response)
        }) { (response: QBResponse!) -> Void in
            //
            print(response.error)
        }
    }
    
    func pushRegistry(_ registry: PKPushRegistry, didInvalidatePushTokenFor type: PKPushType) {
        guard let deviceIdentifier = UIDevice.current.identifierForVendor?.uuidString else {
            return
        }
        QBRequest.unregisterSubscription(forUniqueDeviceIdentifier: deviceIdentifier, successBlock: { response in
        }, errorBlock: { error in
        })
    }
    
    func pushRegistry(_ registry: PKPushRegistry, didReceiveIncomingPushWith payload: PKPushPayload, for type: PKPushType) {
        if CallKitManager.isCallKitAvailable() == true {
            if payload.dictionaryPayload[UsersConstant.voipEvent] != nil {
                let application = UIApplication.shared
                if application.applicationState == .background && backgroundTask == UIBackgroundTaskInvalid {
                    backgroundTask = application.beginBackgroundTask(expirationHandler: {
                        application.endBackgroundTask(self.backgroundTask)
                        self.backgroundTask = UIBackgroundTaskInvalid
                    })
                }
                if QBChat.instance.isConnected == false {
                    core.loginWithCurrentUser()
                }
            }
            
            /*  if payload.dictionaryPayload["callBy"] != nil {
             if let no = payload.dictionaryPayload["callBy"] {
             CallByNo = no as! String
             }
             }
             
             if payload.dictionaryPayload["isGroup"] != nil {
             if let group = payload.dictionaryPayload["isGroup"] {
             isGroup = group as! String
             }
             }*/
        }
    }
}
