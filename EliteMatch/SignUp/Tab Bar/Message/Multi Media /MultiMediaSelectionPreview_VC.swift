//
//  MultiMediaSelectionPreview_VC.swift
//  Tamaas
//
//  Created by Kitlabs-M-0002 on 6/18/19.
//  Copyright © 2019 Krescent Global. All rights reserved.
//

import UIKit
import Photos

protocol MultiMediaSelectionDelegate:class {
    func didPickedMedia(assets: [PHAsset] , arrMediaPreview: [MediaPreview])
}

class MultiMediaSelectionPreview_VC: UIViewController , ACTabScrollViewDelegate , ACTabScrollViewDataSource , UITextViewDelegate {
    
    //MARK:- Outlets
    @IBOutlet weak var tabScrollView: ACTabScrollView!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var inputBar: UIView!
    @IBOutlet weak var txtView: UITextView!
    @IBOutlet weak var inputBar_bottom: NSLayoutConstraint!
    
    
    //MARK:- Variables
    weak var Mediadelegate: MultiMediaSelectionDelegate?
    var assets:[PHAsset] = []
    var contentViews: [UIView] = []
    var currentIndex:Int = 0
    var arrMediaToSend:[MediaPreview] = []
    
    
    //MARK:- UIView life cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //init
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
        self.configureScrollTab()
        self.configInputBar()
    }
    
    //MARK:- UIkeyboard handling methods
    @objc private func keyboardWillChangeFrame(_ notification: Notification) {
        if let endFrame = (notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            var keyboardHeight = view.bounds.height - endFrame.origin.y
            if #available(iOS 11, *) {
                if keyboardHeight > 0 {
                    // if !self.isIphoneX() {
                    keyboardHeight = keyboardHeight - view.safeAreaInsets.bottom
                    // }
                }
            }
            self.inputBar_bottom?.constant = keyboardHeight
            view.layoutIfNeeded()
        }
    }
    
    @objc func dismissKeyboard()
    {
        self.view.endEditing(true)
        inputBar_bottom?.constant = 0
        view.layoutIfNeeded()
    }
    
    //MARK:- Custom Methods
    
    func configInputBar(){
        
        self.txtView.delegate = self
        self.txtView.text = "Add Caption..."
        self.txtView.textColor = UIColor.lightGray
        self.txtView.font = UIFont(name: "CircularStd-Book", size: 16)
        self.txtView.textAlignment = .left
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self,action: #selector(self.dismissKeyboard))
        self.tabScrollView.addGestureRecognizer(tap)
        
        // *** Listen to keyboard show / hide ***
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillChangeFrame), name: NSNotification.Name.UIKeyboardWillChangeFrame, object: nil)
    }
    
    func configureScrollTab(){
        
        //sticker view
        self.tabScrollView?.defaultPage = 0
        self.tabScrollView?.arrowIndicator = false
        self.tabScrollView?.tabSectionHeight = 0
        self.tabScrollView?.delegate = self
        self.tabScrollView?.dataSource = self
        
        // create content views from storyboard
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        
        for var i in 0..<self.assets.count {
            let vc = storyboard.instantiateViewController(withIdentifier: "MultiImageSelectionTabContainer_VC") as! MultiImageSelectionTabContainer_VC
            
            let objPreview = MediaPreview()
            
            if let img = self.getAssetThumbnail(asset: self.assets[i] , size: CGSize(width: self.view.frame.width , height: self.view.frame.height)) as? UIImage {
                vc.imageToShow = img
                objPreview.image = img
            }
            
            //for video
            if self.assets[i].mediaType == PHAssetMediaType.image {
                vc.isVideo = false
                objPreview.MediaType = "image"
            } else {
                vc.isVideo = true
                objPreview.MediaType = "video"
                self.getAssetVideoUrl(asset: self.assets[i]) { (url) in
                    objPreview.videoUrl = url
                    vc.videoUrl = url
                }
            }
            
            self.arrMediaToSend.append(objPreview)
            addChildViewController(vc)
            contentViews.append(vc.view)
        }
    }
    
    func getAssetThumbnail(asset: PHAsset , size: CGSize) -> UIImage {
        let manager = PHImageManager.default()
        let option = PHImageRequestOptions()
        var thumbnail = UIImage()
        option.isSynchronous = true
        manager.requestImage(for: asset, targetSize: size, contentMode: .aspectFit, options: option, resultHandler: {(result, info)->Void in
            thumbnail = result!
        })
        return thumbnail
    }
    
    
    func getAssetVideoUrl(asset: PHAsset , completionHandlerUrl:@escaping (String) -> () ){
        asset.requestContentEditingInput(with: PHContentEditingInputRequestOptions(), completionHandler: { (contentEditingInput, dictInfo) in
            
                if let strURL = (contentEditingInput!.avAsset as? AVURLAsset)?.url.absoluteString
                {
                    print("VIDEO URL: ", strURL)
                    completionHandlerUrl(strURL)
                }
        })
    }
    
    func defineCaption(){
        if let item = self.arrMediaToSend[self.currentIndex] as? MediaPreview {
            self.txtView.text = item.caption
        }
    }
  
    //MARK:- UITextViewDelegates
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.lightGray {
            textView.text = nil
            textView.textColor = UIColor.black
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = "Add Caption..."
            textView.textColor = UIColor.lightGray
        }
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        let oldText: NSString = textView.text! as NSString
        let newText: NSString = oldText.replacingCharacters(in: range , with: text) as NSString
        if let item = self.arrMediaToSend[self.currentIndex] as? MediaPreview {
            var newObj = item
            newObj.caption = newText as String
            self.arrMediaToSend[self.currentIndex] = newObj
        }
        
        for item in self.arrMediaToSend {
            print(item.caption)
        }
        return true
    }
    
   
    // MARK: ACTabScrollViewDelegate
    func tabScrollView(_ tabScrollView: ACTabScrollView, didChangePageTo index: Int) {
        self.currentIndex = index
        let current_indexPath_ = IndexPath(row: self.currentIndex , section: 0)
        self.collectionView.reloadData()
        self.collectionView.scrollToItem(at: current_indexPath_ , at: .centeredHorizontally , animated: false)
        self.defineCaption()
    }
    
    func tabScrollView(_ tabScrollView: ACTabScrollView, didScrollPageTo index: Int) {
    }
    
    func numberOfPagesInTabScrollView(_ tabScrollView: ACTabScrollView) -> Int {
        return self.assets.count
    }
    
    func tabScrollView(_ tabScrollView: ACTabScrollView, tabViewForPageAtIndex index: Int) -> UIView {
        // create a label
        let label = UILabel()
        label.textColor = UIColor(red: 77.0 / 255, green: 79.0 / 255, blue: 84.0 / 255, alpha: 1)
        label.textAlignment = .center
        
        // if the size of your tab is not fixed, you can adjust the size by the following way.
        label.sizeToFit() // resize the label to the size of content
        label.frame.size = CGSize(width: label.frame.size.width + 28, height: label.frame.size.height + 36) // add some paddings
        
        return label
    }
    
    func tabScrollView(_ tabScrollView: ACTabScrollView, contentViewForPageAtIndex index: Int) -> UIView {
        return contentViews[index]
    }
    
    //MARK:- UIButton Actions
    @IBAction func didPress_cancel(_ sender: UIButton) {
        self.dismiss(animated: true , completion: nil)
    }
    
    @IBAction func didPress_Send(_ sender: UIButton) {
        self.Mediadelegate?.didPickedMedia(assets: self.assets , arrMediaPreview: self.arrMediaToSend)
        self.presentingViewController?.presentingViewController?.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func didPress_remove(_ sender: UIButton) {
        
        self.assets.remove(at: self.currentIndex)
        self.contentViews.remove(at: self.currentIndex)
        self.arrMediaToSend.remove(at: self.currentIndex)
        
        if self.currentIndex > 0 {
           self.currentIndex = self.currentIndex - 1
           self.tabScrollView?.reloadData()
           self.collectionView.reloadData()
           self.tabScrollView?.changePageToIndex(self.currentIndex , animated: true)
        }
        else if self.currentIndex == 0 && self.assets.count > 0 {
            self.tabScrollView?.reloadData()
            self.collectionView.reloadData()
        }
        else {
            self.dismiss(animated: true , completion: nil)
        }
    }
    
    @IBAction func didPress_send(_ sender: UIButton) {
    }
    

}


extension MultiMediaSelectionPreview_VC: UICollectionViewDelegate , UICollectionViewDataSource , UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.assets.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! MultiMediaSelectionPreview_CollectionCell
        
        if let img = self.getAssetThumbnail(asset: self.assets[indexPath.row] , size: CGSize(width: self.view.frame.width , height: self.view.frame.height)) as? UIImage {
            cell.imageView.image = img
        }
        
        //set border
        if indexPath.row == self.currentIndex {
            cell.imageView.layer.borderColor = UIColor.red.cgColor
            cell.imageView.layer.borderWidth = 3.0
            cell.imageView.layer.masksToBounds = true
        } else {
            cell.imageView.layer.borderColor = UIColor.clear.cgColor
            cell.imageView.layer.borderWidth = 0.0
        }
        
        //for video
        if self.assets[indexPath.row].mediaType == PHAssetMediaType.image {
            cell.btnPlay.isHidden = true
        } else {
            cell.btnPlay.isHidden = false
        }
        
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 80.0, height: 80.0)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.tabScrollView?.changePageToIndex(indexPath.row, animated: true)
    }
    
}


class MultiMediaSelectionPreview_CollectionCell: UICollectionViewCell {
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var btnPlay: UIButton!
    
}
