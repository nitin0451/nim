//
//  ChaperonContacts_VC.swift
//  EliteMatch
//
//  Created by Kitlabs-M-0002 on 8/13/19.
//  Copyright © 2019 Kitlabs-M-0002. All rights reserved.
//

import UIKit
import MessageUI
import FTIndicator


class ChaperonContacts_VC: UIViewController {
    
    //MARK:- Outlets
    @IBOutlet weak var tblView: UITableView!
    
    //MARK:- Variables
    var arrSectionTitle:[String] = ["Friends who are active on NIM" , "Invite Others"]
    var arrActiveUsers:[QBUUser] = []
    var arrUnRegisteredUsers:[QBAddressBookContact] = []
    var dialogID:String = ""
    var dialog:QBChatDialog!
    
    //selected
    var arrSelectedActiveUser:[QBUUser] = []
    var arrSelectedUnRegisteredUsers:[QBAddressBookContact] = []
    
    //MARK:- UIView life-cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //init
        self.tblView.delegate = self
        self.tblView.dataSource = self
        self.getRegisteredNIMContacts()
    }
    
    //MARK:- Custom Methods
    func getRegisteredNIMContacts(){
        ContactManager.shared.retriveRegisteredUsersFromAddressBook(completionHandler: { (users) in
            
            if var users_ = users {
                
                if users_.contains(ServicesManager.instance().currentUser) {
                    if let index = users_.index(of: ServicesManager.instance().currentUser) {
                        users_.remove(at: index)
                    }
                }
                
                //check for names
                for user in users_ {
                    if user.fullName == nil {
                        if let index = users_.index(of: user) {
                            users_.remove(at: index)
                        }
                    }
                }
                
                self.arrActiveUsers = users_
            }
            self.getNonRegisteredContacts()
        })
    }
    
    func getNonRegisteredContacts(){
        
        self.arrUnRegisteredUsers.removeAll()
        
        for contact_ in constantVC.GlobalVariables.arrQBAddressContact {
            
            var isUserRegistered:Bool = false
            for user_ in self.arrActiveUsers {
                if let phone = user_.phone {
                    if phone == contact_.phone {
                        isUserRegistered = true
                        break
                    }
                }
            }
            if !isUserRegistered {
                self.arrUnRegisteredUsers.append(contact_)
            }
        }
        DispatchQueue.main.async {
            self.tblView.reloadData()
        }
    }
    
    
    //MARK:- UIButton Actions
    @IBAction func didPress_close(_ sender: UIButton) {
        self.dismiss(animated: true , completion: nil)
    }
    
    @IBAction func didPress_invites(_ sender: UIButton) {
        
        
        if self.arrSelectedActiveUser.count > 0 {
            var arrOpponentIDS:[NSNumber] = []
            for user_ in self.arrSelectedActiveUser {
                
                arrOpponentIDS.append(NSNumber(value: user_.id))
                
                //send invitation to chaperon
                InvitationManager.instance.sendConnect_chaperon_invitation(dialogID: self.dialogID , requestTo: "\(user_.externalUserID)" , fromName: user_.fullName ?? "" , completionHandler: { (success) in
                })
            }
            
            //add users to group
            DialogManagerNIM.instance.joinChaperonToPrivateGroupChat(arrOpponentId: arrOpponentIDS , dialog: self.dialog) { (success) in
            }
        }
        
        if self.arrSelectedUnRegisteredUsers.count > 0 {
            var arrPhoneNoToInvite:[String] = []
            for item in self.arrSelectedUnRegisteredUsers {
                arrPhoneNoToInvite.append(item.phone)
            }
            self.sendTextForInvitation(phoneNos: arrPhoneNoToInvite)
        } else {
            self.dismiss(animated: true , completion: nil)
        }
    }
    
    //MARK:- Custom Methods
    func set_unselectLable(label:UILabel){
        label.backgroundColor = UIColor.white
        label.textColor = constantVC.GlobalColor.greenColor
        label.text = "+ Add"
    }
    
    func set_SelectLable(label:UILabel){
        label.backgroundColor = constantVC.GlobalColor.greenColor
        label.textColor = UIColor.white
        label.text = "Added"
    }
    
    
    func set_unselectLable_unregistered(label:UILabel){
        label.backgroundColor = UIColor.white
        label.textColor = constantVC.GlobalColor.greenColor
        label.text = "+ Invite"
    }
    
    func set_SelectLable_unregistered(label:UILabel){
        label.backgroundColor = constantVC.GlobalColor.greenColor
        label.textColor = UIColor.white
        label.text = "Selected"
    }
    
    func sendTextForInvitation(phoneNos: [String]){
        if (MFMessageComposeViewController.canSendText()) {
            let controller = MFMessageComposeViewController()
            controller.body = "Hey, download this app, it lets me find you matches 🤲🏼 https://www.nimapp.co"
            controller.recipients = phoneNos
            controller.messageComposeDelegate = self
            if let topVC = UIApplication.topViewController() {
                topVC.present(controller, animated: true, completion: nil)
            }
        }
    }
    
}

extension ChaperonContacts_VC: UITableViewDelegate , UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView.init(frame: CGRect.init(x: 0, y: 0, width: tableView.frame.width, height: 50))
        
        headerView.backgroundColor = UIColor.white
        let label = UILabel()
        label.frame = CGRect.init(x: 5, y: 5, width: headerView.frame.width-10, height: headerView.frame.height-10)
        label.text = self.arrSectionTitle[section]
        label.font = UIFont.init(name:"CircularStd-Medium",size:15)
        label.textColor = UIColor.black
        headerView.addSubview(label)
        return headerView
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0{
            return self.arrActiveUsers.count
        }
        return self.arrUnRegisteredUsers.count
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 85.0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell" , for: indexPath) as! ChaperonContact_tableCell
        
        if indexPath.section == 0 {
            if self.arrActiveUsers.indices.contains(indexPath.row) {
                let user = self.arrActiveUsers[indexPath.row]
                
                print("external id" , user.externalUserID)
                
                cell.lblName.text = user.fullName ?? ""
                cell.imgVwUser.yy_setImage(with: ProfileManager.shared.getOpponentProfileUrl(opponentNo: user.phone ?? "") , placeholder: ProfileManager.shared.getPlaceHolderUserImage())
                cell.lblContactStartAlphabet.text = ""
                
                //for selection
                if self.arrSelectedActiveUser.contains(user) {
                    self.set_SelectLable(label: cell.lblSelection)
                } else {
                    self.set_unselectLable(label: cell.lblSelection)
                }
            }
        }
        
        if indexPath.section == 1 {
            if self.arrUnRegisteredUsers.indices.contains(indexPath.row) {
                let contact = self.arrUnRegisteredUsers[indexPath.row]
                
                cell.lblName.text = contact.name
                let strprefix = contact.name.prefix(2)
                cell.lblContactStartAlphabet.text = String(strprefix)
                
                cell.imgVwUser.backgroundColor = CommonFunctions.shared.getRandomColor(indexpath: indexPath)
                
                //for selection
                if self.arrSelectedUnRegisteredUsers.contains(contact) {
                    self.set_SelectLable_unregistered(label: cell.lblSelection)
                } else {
                    self.set_unselectLable_unregistered(label: cell.lblSelection)
                }
            }
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 0 {
            if let selectedCell = tableView.cellForRow(at: indexPath) as? ChaperonContact_tableCell {
                let selectedUser = self.arrActiveUsers[indexPath.row]
                
                if self.arrSelectedActiveUser.contains(selectedUser) {
                    if let index = self.arrSelectedActiveUser.index(of: selectedUser) {
                        self.arrSelectedActiveUser.remove(at: index)
                        self.set_unselectLable(label: selectedCell.lblSelection)
                    }
                } else {
                    self.arrSelectedActiveUser.append(selectedUser)
                    self.set_SelectLable(label: selectedCell.lblSelection)
                }
            }
        }
        
        if indexPath.section == 1 {
            if let selectedCell = tableView.cellForRow(at: indexPath) as? ChaperonContact_tableCell {
                let selectedContact = self.arrUnRegisteredUsers[indexPath.row]
                
                if self.arrSelectedUnRegisteredUsers.contains(selectedContact) {
                    if let index = self.arrSelectedUnRegisteredUsers.index(of: selectedContact) {
                        self.arrSelectedUnRegisteredUsers.remove(at: index)
                        self.set_unselectLable_unregistered(label: selectedCell.lblSelection)
                    }
                } else {
                    self.arrSelectedUnRegisteredUsers.append(selectedContact)
                    self.set_SelectLable_unregistered(label: selectedCell.lblSelection)
                }
            }
        }
    }
}


extension ChaperonContacts_VC: MFMessageComposeViewControllerDelegate{
    
    func messageComposeViewController(_ controller: MFMessageComposeViewController, didFinishWith result: MessageComposeResult) {
        switch (result) {
        case .cancelled:
            print("Message was cancelled")
        case .failed:
            print("Message failed")
        case .sent:
            print("Message was sent")
            FTIndicator.showSuccess(withMessage: "Invitation sent successfully!")
            dismiss(animated: true, completion: nil)
        default:
            break
        }
        self.dismiss(animated: true, completion: nil)
    }
}
