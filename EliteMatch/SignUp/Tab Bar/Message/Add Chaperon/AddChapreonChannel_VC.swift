//
//  AddChapreonChannel_VC.swift
//  EliteMatch
//
//  Created by osvinuser on 14/03/19.
//  Copyright © 2019 Kitlabs-M-0002. All rights reserved.
//

import UIKit
import FTIndicator
import MessageUI

class AddChapreonChannel_VC: UIViewController,EPPickerDelegate {

    //MARK:- Outlets
    @IBOutlet weak var barView: UIView!
    @IBOutlet weak var skip_Btn: UIButton!
    @IBOutlet weak var chooseContacts_Btn: UIButton!
    @IBOutlet weak var addChaperon_Lbl: UILabel!
    @IBOutlet weak var shariaCompliant_Lbl: UILabel!
    @IBOutlet weak var chaperonDescription_Lbl: UILabel!
    
    //MARK:- Variables
    var dialogID:String = ""
    var dialog:QBChatDialog!
    
    
    //MARK:- UIView life-cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
    chaperonDescription_Lbl.textColor = UIColor(red: 52/255.0, green: 52/255.0, blue: 52/255.0, alpha: 1.0)
    shariaCompliant_Lbl.textColor = UIColor(red: 52/255.0, green: 52/255.0, blue: 52/255.0, alpha: 1.0)
    chooseContacts_Btn.backgroundColor = UIColor(red: 26/255.0, green: 26/255.0, blue: 26/255.0, alpha: 1.0)
    barView.borderWidth = 0.3
    barView.borderColor = UIColor(red: 52/255.0, green: 52/255.0, blue: 52/255.0, alpha: 0.2)
        
        //TODO
//    let formattedString = NSMutableAttributedString()
//        formattedString
//            .normaltext("Chaperon can ")
//            .bold("only")
//            .normal(" view the conversation and get all notification the users get")
//    chaperonDescription_Lbl.attributedText = formattedString
    }

    override func viewDidDisappear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = false
    }
    
    
    //MARK:- UIButton Actions
    @IBAction func action_backBtn(_ sender: Any) {
        self.dismiss(animated: true , completion: nil)
    }
    
    @IBAction func action_ChooseContacts(_ sender: Any) {
        
        self.dismiss(animated: true) {
            self.navigateInviteVC()
        }
        
        
//        let contactPickerScene = EPContactsPicker(delegate: self, multiSelection:false, subtitleCellType: SubtitleCellValue.phoneNumber)
//        let navigationController = UINavigationController(rootViewController: contactPickerScene)
//        self.present(navigationController, animated: true, completion: nil)
    }
    
    @IBAction func actionUserName(_ sender: Any) {
//        let profileDetails_VC = self.storyboard?.instantiateViewController(withIdentifier: "ProfileDetails_VC") as! ProfileDetails_VC
//        self.present(profileDetails_VC , animated: true , completion: nil)
    }

    @IBAction func didPress_skip(_ sender: UIButton) {
        self.dismiss(animated: true , completion: nil)
    }
    
    
    //MARK:- Custom Methods
    func navigateInviteVC(){
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "ChaperonContacts_VC") as! ChaperonContacts_VC
        controller.dialogID = self.dialogID
        controller.dialog = self.dialog
        if let topVC = UIApplication.topViewController() {
            topVC.present(controller , animated: true , completion: nil)
        }
    }
   
//MARK: EPContactsPicker delegates
func epContactPicker(_: EPContactsPicker, didContactFetchFailed error : NSError)
{
    print("Failed with error \(error.description)")
}

func epContactPicker(_: EPContactsPicker, didSelectContact contact : EPContact)
{
    
    LoadingIndicatorView.show()
    
    //check user exists or Not
    if let phoneNo = contact.phoneNumbers[0].phoneNumber as? String {
        ProfileManager.shared.getUserProfile_phoneNo(phoneNo: phoneNo) { (success , user) in
            
            LoadingIndicatorView.hide()
            
            if success {
                //send invitation to chaperon
              /*  if let userId = user?.id {
                    InvitationManager.instance.sendConnect_chaperon_invitation(requestTo: userId, fromName: user?.name ?? "" , completionHandler: { (success) in
                        if success {
                            FTIndicator.showInfo(withMessage: userMessagestext.sendChaperonInvitation_success)
                        }
                    })
                }*/
            } else {
                self.sendAppInvitation_Text(phoneNo: phoneNo)
            }
            
        }
    }
}
    
    func sendAppInvitation_Text(phoneNo: String){
        let composeVC = MFMessageComposeViewController()
        composeVC.messageComposeDelegate = self
        composeVC.recipients = [phoneNo]
        composeVC.body = "Check out this New Secure Match Match Making App with free video chat, calls, stickers and more... Download NIM NOW!"
        if MFMessageComposeViewController.canSendText() {
            self.present(composeVC, animated: true, completion: nil)
        } else {
            print("Can't send messages.")
        }
    }

func epContactPicker(_: EPContactsPicker, didCancel error : NSError)
{
    print("User canceled the selection");
}

func epContactPicker(_: EPContactsPicker, didSelectMultipleContacts contacts: [EPContact]) {
    print("The following contacts are selected")
    for contact in contacts {
        print("\(contact.displayName())")
    }
 }
}



extension AddChapreonChannel_VC: MFMessageComposeViewControllerDelegate {
    
    func messageComposeViewController(_ controller: MFMessageComposeViewController, didFinishWith result: MessageComposeResult) {
        
        self.dismiss(animated: true , completion: nil)
    }
    
    
}

extension NSMutableAttributedString {
    @discardableResult func boldtext(_ text: String) -> NSMutableAttributedString {
        let attrs: [NSAttributedStringKey: Any] = [.font: UIFont(name: "CircularStd-Book", size: 16)!]
        let boldString = NSMutableAttributedString(string:text, attributes: attrs)
        append(boldString)
        
        return self
    }
    
    @discardableResult func normaltext(_ text: String) -> NSMutableAttributedString {
        let normal = NSAttributedString(string: text)
        append(normal)
        return self
    }
}
