//
//  ForwardList_TableViewController.swift
//  Tamaas
//
//  Created by Kitlabs-M-0002 on 5/23/19.
//  Copyright © 2019 Krescent Global. All rights reserved.
//

import UIKit
import FTIndicator

class ForwardList_TableViewController: UIViewController {
    
    //MARK:- Outlets
    @IBOutlet weak var tableView: UITableView!
    
    
    //MARK:- Variables
    var arrSlectedMsgs:[QBChatMessage] = []
    var arrSelectedDialogs:[QBChatDialog] = []
    
    
    //MARK:- UIView life-cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //init
        self.tableView.delegate = self
        self.tableView.dataSource = self

    }
    
    // MARK: - DataSource Action
    func dialogs() -> [QBChatDialog]? {
        return ServicesManager.instance().chatService.dialogsMemoryStorage.dialogsSortByUpdatedAt(withAscending: false)
    }
    
    
    //MARK:- UIButton Actions
    @IBAction func didPress_cancel(_ sender: UIButton) {
        self.dismiss(animated: true , completion: nil)
    }
    
    @IBAction func didPress_done(_ sender: UIButton) {
        self.sendMsgsToForward()
    }
    
    @objc func didPressImagePreview(_ sender:AnyObject){
        
        let indexPath = IndexPath(row: sender.view.tag , section: 0)
        if let cell = self.tableView.cellForRow(at: indexPath) as? ForwardList_TableViewCell {
            let url = URL.init(string: "\(constantVC.GeneralConstants.GET_IMAGE)\(cell.opponentNo).jpeg")
            QMImagePreview.previewImage(with: url , in: self)
        }
    }
    
    
    //MARK:- Custom Methods
    func setSelectionImage(image: String , imageView: UIImageView){
        imageView.image = UIImage(named: image)
    }
    
    //MARK:- Forward Message Methods
    func sendMsgsToForward(){
        
        for (index , dialogTo) in self.arrSelectedDialogs.enumerated() {
            for msgToForward in self.arrSlectedMsgs {
                let newMsgToForward = QBChatMessage()
                newMsgToForward.dateSent = Date()
                if let id = SessionManager.get_QuickBloxID() {
                    newMsgToForward.senderID = UInt(truncating: id)
                    newMsgToForward.deliveredIDs = [id]
                    newMsgToForward.readIDs = [id]
                }
                newMsgToForward.attachments = msgToForward.attachments
                if let text = msgToForward.text {
                    newMsgToForward.text = text
                }
                
                newMsgToForward.markable = true
                newMsgToForward.saveToHistory = "true"
                
                ForwardMessageManager.shared.sendMessage(message: newMsgToForward , dialog: dialogTo)
            }
            
            if index == self.arrSelectedDialogs.count - 1 {
                FTIndicator.showToastMessage("Message Sent!")
                self.dismiss(animated: true , completion: nil)
            }
        }
    }
    
    
}

extension ForwardList_TableViewController: UITableViewDelegate , UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70.0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let dialogs = self.dialogs() {
            return dialogs.count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if let menuCell = tableView.dequeueReusableCell(withIdentifier: "forwardCell", for: indexPath as IndexPath) as? ForwardList_TableViewCell {
            
            guard let chatDialog = self.dialogs()?[indexPath.row] else {
                return menuCell
            }
            
            print(chatDialog)
            menuCell.dialog = chatDialog
            
            //add tap gesture
            let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.didPressImagePreview(_:)))
            menuCell.userImage.isUserInteractionEnabled = true
            menuCell.userImage.tag = indexPath.row
            menuCell.userImage.addGestureRecognizer(tapGestureRecognizer)
            
            //for dialog selection
            if self.arrSelectedDialogs.contains(chatDialog) {
                self.setSelectionImage(image: DialogImg.selected.rawValue , imageView: menuCell.imgSelection)
            }else {
                self.setSelectionImage(image: DialogImg.UnSelected.rawValue , imageView: menuCell.imgSelection)
            }
            
            return menuCell
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let menuCell = tableView.cellForRow(at: indexPath) as? ForwardList_TableViewCell {
            guard let chatDialog = self.dialogs()?[indexPath.row] else {
                return
            }
            if self.arrSelectedDialogs.contains(chatDialog) {
                if let indexOfDialog = self.arrSelectedDialogs.firstIndex(of: chatDialog) {
                   self.arrSelectedDialogs.remove(at: indexOfDialog)
                   self.setSelectionImage(image: DialogImg.UnSelected.rawValue , imageView: menuCell.imgSelection)
                }
            } else {
                self.arrSelectedDialogs.append(chatDialog)
                self.setSelectionImage(image: DialogImg.selected.rawValue , imageView: menuCell.imgSelection)
            }
        }
    }
    
}

class ForwardList_TableViewCell: UITableViewCell {
    
    @IBOutlet weak var imgSelection: UIImageView!
    @IBOutlet weak var userImage: UIImageView!
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var vwGroup: UIView!
    @IBOutlet weak var groupUserImage1: UIImageView!
    @IBOutlet weak var groupUserImage2: UIImageView!
    var opponentNo:String = ""
    
    var dialog: QBChatDialog? {
        didSet {
            self.userName.text = ""
            self.userImage.image = nil
            updateView()
        }
    }
    
    func updateView() {
        self.showUserName()
        if dialog?.type == QBChatDialogType.group && dialog?.photo == nil {
            self.userImage.isHidden = true
            self.vwGroup.isHidden = false
            self.showGroupUsersImages()
        } else {
            self.userImage.isHidden = false
            self.vwGroup.isHidden = true
            self.showUserImage()
        }
    }
    
    //MARK:- DataSource Methods
    func showUserName(){
        if self.dialog?.type == QBChatDialogType.group {
            if let name = dialog?.name {
                self.userName.text = name
            }
        }
        else {
            if let data = dialog?.data as? [String: Any] {
                if let number = data["number"] as? String {
                    
                    if let dicCustomData = NimUserManager.shared.convertJsonStringToDictionary(text: number) {
                        if let dicNo = dicCustomData["number"] as? String {
                            let arrNo = dicNo.components(separatedBy: "-")
                            for item in arrNo {
                                if item != SessionManager.get_phonenumber() {
                                    opponentNo = item
                                    self.userName.text = SessionManager.getNameFromMyAddressBook(number:  opponentNo)
                                }
                            }
                        }
                    } else {
                        let arrNo = number.components(separatedBy: "-")
                        for item in arrNo {
                            if item != SessionManager.get_phonenumber() {
                                opponentNo = item
                                self.userName.text = SessionManager.getNameFromMyAddressBook(number:  opponentNo)
                            }
                        }
                    }
                }
            }
        }
    }
    
    func showUserImage() {
        if self.dialog?.type == QBChatDialogType.group {
            if let photo = self.dialog?.photo {
                self.userImage.yy_setImage(with: URL.init(string: "\(constantVC.GeneralConstants.GET_IMAGE)\(photo).jpeg") , placeholder: UIImage(named: "contactCard") , options: currentYYImgotions.currentYYImg , completion: nil)
            }
        } else {
            self.userImage.yy_setImage(with: URL.init(string: "\(constantVC.GeneralConstants.GET_IMAGE)\(self.opponentNo).jpeg") , placeholder: UIImage(named: "contactCard") , options: currentYYImgotions.currentYYImg , completion: nil)
        }
    }
    
    func showGroupUsersImages(){
        if let groupOccupantsID = self.dialog?.occupantIDs {
            let IDS = groupOccupantsID.filter { $0 != SessionManager.get_QuickBloxID() }
            
            if IDS.indices.contains(0) {
                
                //user1
                ServicesManager.instance().usersService.getUserWithID( IDS[0] as! UInt , forceLoad: true).continueOnSuccessWith(block: { (task) -> Any? in
                    if let no = task.result?.phone {
                        self.groupUserImage1.yy_setImage(with: URL.init(string: "\(constantVC.GeneralConstants.GET_IMAGE)\(no).jpeg") , placeholder: UIImage(named: "contactCard") , options: currentYYImgotions.currentYYImg , completion: { (image , url , type , stage , error) in
                            if error == nil && image != nil {
                                CommonFunctions.shared.addBorder(imageView: self.groupUserImage1 , colour: UIColor.white)
                            } else {
                                CommonFunctions.shared.addBorder(imageView: self.groupUserImage1 , colour: UIColor(red: 145.0/255.0, green: 159.0/255.0, blue: 185.0/255.0, alpha: 1.0))
                            }
                        })
                    }
                    return nil
                })
            }
            
            if IDS.indices.contains(1) {
                //user2
                ServicesManager.instance().usersService.getUserWithID( IDS[1] as! UInt , forceLoad: true).continueOnSuccessWith(block: { (task) -> Any? in
                    
                    if let no = task.result?.phone {
                        self.groupUserImage2.yy_setImage(with: URL.init(string: "\(constantVC.GeneralConstants.GET_IMAGE)\(no).jpeg") , placeholder: UIImage(named: "contactCard") , options: currentYYImgotions.currentYYImg , completion: { (image , url , type , stage , error) in
                            if error == nil && image != nil {
                                CommonFunctions.shared.addBorder(imageView: self.groupUserImage2 , colour: UIColor.white)
                            }else {
                                CommonFunctions.shared.addBorder(imageView: self.groupUserImage2 , colour: UIColor(red: 145.0/255.0, green: 159.0/255.0, blue: 185.0/255.0, alpha: 1.0))
                            }
                        })
                    }
                    return nil
                })
            }
        }
    }
  
}


enum DialogImg: String {
    case selected = "selectedBtn"
    case UnSelected = "unselectedBtn"
}


class ForwardMessageManager: NSObject {
    
    static let shared = ForwardMessageManager()
    
    func dialogID(dialog: QBChatDialog) ->String{
        if let id = dialog.id {
            return id
        }
        return ""
    }
    
    func sendMessage(message: QBChatMessage , dialog: QBChatDialog) {
        
        // Sending message.
        ServicesManager.instance().chatService.send(message, toDialogID: self.dialogID(dialog: dialog), saveToHistory: true, saveToStorage: true) { (error) ->
            Void in
            
            if error != nil {
                
                QMMessageNotificationManager.showNotification(withTitle: "SA_STR_ERROR", subtitle: "Message failed to send. Check your connection" , type: QMMessageNotificationType.warning)
                
                // Logging in to chat.
                ServicesManager.instance().chatService.connect { (error) in
                    if error == nil {
                        
                    } else {
                    }
                }
            }
            
            //Send Push Notification
            if message.text != nil {
                self.sendPushNotification(msg: message.text! , dialog: dialog )
            }
            
        }
    }
    
    func getOpponentNumber(dialog: QBChatDialog) ->String{
        if let data = dialog.data as? [String: Any] {
            if let number = data["number"] as? String {
                
                if let dicCustomData = NimUserManager.shared.convertJsonStringToDictionary(text: number) {
                    if let dicNo = dicCustomData["number"] as? String {
                        let arrNo = dicNo.components(separatedBy: "-")
                        for item in arrNo {
                            if item != SessionManager.get_phonenumber() {
                                return item
                            }
                        }
                    }
                } else {
                    let arrNo = number.components(separatedBy: "-")
                    for item in arrNo {
                        if item != SessionManager.get_phonenumber() {
                            return item
                        }
                    }
                }
            }
        }
        return ""
    }
    
    func sendPushNotification(msg: String , dialog: QBChatDialog){
        
          /*  let document = self.getOpponentNumber(dialog: dialog) + "-" + SessionManager.getPhone()
            TamFirebaseManager.shared.getDocument(collection: firebaseCollection.Mute.rawValue , document: document) { (success) in
                if !(success) {
                    //Not blocked - send custom notification
                    if let opponentID = QuickBloxManager().getOpponentID(dialog:dialog) {
                        
                        QuickBloxManager().sendPushNotification_chat(msg: msg , users: "\(opponentID)" , title: "New Message!", fromNo: SessionManager.getPhone() , toNo: self.getOpponentNumber(dialog: dialog) , dialogID: self.dialogID(dialog: dialog))
                    }
                }
            }*/
    }
    
}
