//
//  ChatInfo_VC.swift
//  Tamaas
//
//  Created by Kitlabs-M-0002 on 3/4/19.
//  Copyright © 2019 Krescent Global. All rights reserved.
//

import UIKit
import FTIndicator
import Contacts
import ContactsUI
import PhoneNumberKit
import AVKit

protocol ChatInfoDelegate {
    func clearChatDelegateMethod()
    func doVoiceCall()
    func doVideoCall()
    func doBlockUnblock(state: String , completionHandler:@escaping (Bool , String) -> ())
    func updateTitleAfterContactUpdate()
}

class ChatInfo_VC: UIViewController {
    
    //MARK:- Outlets
    @IBOutlet weak var userImage: UIImageView!
    @IBOutlet weak var vwRounded: UIView!
    @IBOutlet weak var phoneNo: UILabel!
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var collectionVw_Media: UICollectionView!
    @IBOutlet weak var switchMute: UISwitch!
    @IBOutlet weak var btnShowMore: UIButton!
    @IBOutlet weak var lblBlockContact: UILabel!
    @IBOutlet weak var lblNoMedia: UILabel!
    @IBOutlet weak var lblMute: UILabel!
    @IBOutlet weak var constraint_mediaHeight: NSLayoutConstraint!
    @IBOutlet weak var lblMedia: UILabel!
    @IBOutlet weak var lblChatSearch: UILabel!
    @IBOutlet weak var lblClearChat: UILabel!
    @IBOutlet weak var lblDeleteChat: UILabel!
    @IBOutlet weak var btnVoiceCall: UIButton!
    @IBOutlet weak var btnVideoCall: UIButton!
    @IBOutlet weak var buttonProfile: UIButton!
    
    //MARK:- Variables
    var dialog: QBChatDialog!
    var mediaMsgs:[QBChatMessage] = []
    var delegate: ChatInfoDelegate?
    var opponentNo:String = ""
    let phoneNumberKit = PhoneNumberKit()
    var videoPlayer: AVPlayer?
    var isNewNav:Bool = false
    
    //MARK:- Chaperon Methods
    func SetUp_ChaperonChatWindow(){
        self.btnVideoCall?.isHidden = true
        self.btnVoiceCall?.isHidden = true
    }
        
    //MARK:- UIView life-cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()

        //init
        if SessionManager.isChaperonUser() {
            self.SetUp_ChaperonChatWindow()
        }
        self.localise_language()
        self.initView()
        self.showUserInfo()
        self.configureNavigationBar()
        self.getMediaMessages()
    }
    
    //MARK:- Custom Methods
    func localise_language(){
        self.lblMedia.text = "Media"
        self.lblNoMedia.text = "No media found.."
        self.lblChatSearch.text = "Chat Search"
        self.lblClearChat.text = "Clear Chat"
        self.lblDeleteChat.text = "Delete Chat"
        self.lblMute.text = "Mute"
    }
    
    
    func initView(){
        self.configureBlockUnblockView()
        self.vwRounded.cornerRadius = 20.0
        self.vwRounded.clipsToBounds = true
        self.collectionVw_Media.delegate = self
        self.collectionVw_Media.dataSource = self
        self.collectionVw_Media.isHidden = true
        self.setNoMediaView(isHidden: true)
        self.btnShowMore.addTarget(self , action: #selector(self.didPressShowMore(_:)), for: .touchDown)
    }
    
    func configureBlockUnblockView(){
        if let opponentID = NimUserManager.shared.getOpponentID(dialog: self.dialog) {
            if let isBlocked = SessionManager.isChatUserBlocked(userID: "\(opponentID)") as? Bool {
                if isBlocked {
                    self.lblBlockContact.text = "UnBlock Contact"
                } else {
                    self.lblBlockContact.text = "Block Contact"
                    self.configureChatMute()
                }
            }
        }
    }
    
    func configureNavigationBar(){
        self.title = "Chat Info"
        let backImg : UIImage? = UIImage.init(named: "blackBack")!.withRenderingMode(.alwaysOriginal)
        let backBtn   = UIBarButtonItem(image: backImg,  style: .plain, target: self, action: #selector(didPressBack(sender:)))
        navigationItem.leftBarButtonItem = backBtn
    }
    
    
    func isChatMuted(completionHandler:@escaping (Bool) -> ()) {
        
//        let document = SessionManager.get_phonenumber() + "-" + self.opponentNo
//        TamFirebaseManager.shared.getDocument(collection: firebaseCollection.Mute.rawValue , document: document) { (success) in
//            if success {
//                completionHandler(true)
//            } else {
//                completionHandler(false)
//            }
//        }
    }
    
    
    func configureChatMute(){
        
        self.isChatMuted(completionHandler: { (success) in
            if success {
                self.lblMute.text = "UnMute"
                self.switchMute.setOn(true , animated: false)
                self.switchMute.tag = 2
            } else {
                self.lblMute.text = "Mute"
                self.switchMute.setOn(false , animated: false)
                self.switchMute.tag = 1
            }
        })
    }
    
    
    //MARK:- Media
    func getDialogId() ->String{
        if let id = self.dialog.id {
            return id
        }
        return ""
    }
    
    
    func getMediaMessages(){
        
        let extendedRequest = ["sort_desc" : "date_sent" , "attachments.type" : "image"]
        let resPage = QBResponsePage(limit:5, skip: 0)
        
        QBRequest.messages(withDialogID: self.getDialogId() , extendedRequest: extendedRequest , for: resPage , successBlock: { (response , msgs , page ) in
            self.mediaMsgs.append(contentsOf: msgs)
            self.getVideoMessages()
        }) { (error) in
            self.getVideoMessages()
        }
    }
    
    func getVideoMessages(){
        let extendedRequest = ["sort_desc" : "date_sent" , "attachments.type" : "video"]
        let resPage = QBResponsePage(limit:5, skip: 0)
        
        QBRequest.messages(withDialogID: self.getDialogId() , extendedRequest: extendedRequest , for: resPage , successBlock: { (response , msgs , page ) in
            self.mediaMsgs.append(contentsOf: msgs)
            self.sortMessages()
        }) { (error) in
        }
    }
    
    func sortMessages(){
        
        self.mediaMsgs = self.mediaMsgs.sorted(by: {
            $0.dateSent!.compare($1.dateSent!) == .orderedDescending
        })
        self.reloadMediaCollection()
    }
    
    func showUserInfo(){
        
        DialogManagerNIM.instance.getUserName(dailog: self.dialog) { (name) in
            self.userName.text = name
        }
        
        if let opponentNo = DialogManagerNIM.instance.getOpponentNo(dialog: self.dialog) {
            self.opponentNo = opponentNo
            self.showUserImage()
        } else if let oppn = DialogManagerNIM.instance.getOpponentNo_fromGroupName(dialog: self.dialog) {
            self.opponentNo = oppn
            self.showUserImage()
        }
        self.phoneNo.text = ""
    }
    
    func showUserImage() {
        
        let targetSize: CGSize? = self.userImage.bounds.size
        let transform = QMImageTransform(size: targetSize! , isCircle: false)
        
        let url = URL.init(string: "\(constantVC.GeneralConstants.GET_IMAGE)\(self.opponentNo).jpeg")
        
        self.userImage.yy_setImage(with: url , placeholder: nil , options: [.progressiveBlur , .setImageWithFadeAnimation ] , completion: nil)
        
       /* if url != nil {
            
            let cachedImage: UIImage? = QMImageLoader.instance.imageCache?.imageFromCache(forKey: transform.key(with: url!))
            if cachedImage != nil {
                transform.apply(for: (cachedImage)!) { (img) in
                    self.userImage.image = img
                }
            } else {
                
                QMImageLoader.instance.downloadImage(with: url! , transform: transform , options: [.highPriority , .continueInBackground , .allowInvalidSSLCertificates ], progress: { (receivedSize , expectedSize , targetURL) in
                    
                }) { (image, transfomedImage , error , cacheType , finished , imageURL) in
                    if transfomedImage != nil {
                        self.userImage.image = transfomedImage
                    }
                    if error != nil {
                        FTIndicator.showToastMessage("Error in downloading image")
                    }
                }
            }
        }*/
    }
    
    //MARK:- Chat related methods
    func deleteChat(){
        
        let alertController = UIAlertController(title: "Are you sure?", message: "You want to delete chat" , preferredStyle: .alert)
        
        // Create the actions
        let okAction = UIAlertAction(title: "Yes", style: UIAlertActionStyle.default) {
            UIAlertAction in
            
            if Connectivity.isConnectedToInternet() {
                
                //delete code
                guard let dialog = self.dialog else {
                    return
                }
                
                let deleteDialogBlock = { (dialog: QBChatDialog!) -> Void in
                    
                    // Deletes dialog from server and cache.
                    ServicesManager.instance().chatService.deleteDialog(withID: dialog.id!, completion: { (response) -> Void in
                        
                        guard response.isSuccess else {
                            SVProgressHUD.showError(withStatus: "SA_STR_ERROR_DELETING")
                            return
                        }
                        SVProgressHUD.showSuccess(withStatus: "SA_STR_DELETED")
                    })
                }
                deleteDialogBlock(dialog)
                
                //Nitin
                //navigate
                let initialViewController = self.storyboard?.instantiateViewController(withIdentifier: "TabBarController") as! TabBarController
                 let appDelegate = UIApplication.shared.delegate as! AppDelegate
                 appDelegate.window?.rootViewController = initialViewController
                
            }
            else {
                //OFFLINE
                FTIndicator.showToastMessage("No Internet Connection!")
            }
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel) {
            UIAlertAction in
            NSLog("Cancel Pressed")
        }
        
        // Add the actions
        alertController.addAction(okAction)
        alertController.addAction(cancelAction)
        
        // Present the controller
        self.present(alertController, animated: true, completion: nil)
    }
    
    //MARK:- Custom Methods
    func reloadMediaCollection(){
        if self.mediaMsgs.count > 0 {
            self.collectionVw_Media.isHidden = false
            self.collectionVw_Media.reloadData()
        } else {
            self.setNoMediaView(isHidden: false)
        }
    }
    
    @IBAction func buttonProfileAction(_ sender: UIButton) {

        let profileDetails_VC = self.storyboard?.instantiateViewController(withIdentifier: "ProfileDetails_VC") as! ProfileDetails_VC
        profileDetails_VC.matchPhoneNo = self.opponentNo
        self.present(profileDetails_VC , animated: true , completion: nil)
    }
    
    func isShowMoreHidden() ->Bool {
        if self.mediaMsgs.count < 6 {
            return true
        }
        return false
    }
   
    
    func setNoMediaView(isHidden: Bool) {
        self.lblNoMedia.isHidden = isHidden
    }
    
    
    //MARK:- UIButton Actions
    @objc func didPressBack(sender: UIBarButtonItem) {
        if !(self.isNewNav) {
            self.navigationController?.popViewController(animated: false)
        } else {
            self.dismiss(animated: false , completion: nil)
        }
        
    }
    
    @objc func didPressShowMore(_ button: UIButton) {
        if self.mediaMsgs.count > 0 {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let controller = storyboard.instantiateViewController(withIdentifier: "ChatMedia_VC") as! ChatMedia_VC
            controller.dialog = self.dialog
            self.navigationController?.pushViewController(controller , animated: true)
        }
    }
    
    @objc func didPressPlayVideo(_ button: UIButton) {
        let indexPath = IndexPath(row: button.tag , section: 0)
        if let attachment = self.mediaMsgs[indexPath.row].attachments?.first {
            self.playVideo(attachment: attachment , message: self.mediaMsgs[indexPath.row])
        }
    }
    
    @IBAction func didPressVideoCall(_ sender: UIButton) {
        self.delegate?.doVideoCall()
    }
    
    @IBAction func didPressVoiceCall(_ sender: UIButton) {
        self.delegate?.doVoiceCall()
    }
    
    @IBAction func didPressMute(_ sender: UISwitch) {
        
        /*if self.switchMute.tag == 2 {
            //unmute
            let document = SessionManager.get_phonenumber() + "-" + self.opponentNo
            TamFirebaseManager.shared.deleteDocument(collection: firebaseCollection.Mute.rawValue, document: document) { (success) in
                if success {
                    self.lblMute.text = "Mute".localized
                    self.switchMute.tag = 1
                    self.switchMute.setOn(false , animated: false)
                }
            }
        } else {
            //mute
            let document = SessionManager.get_phonenumber() + "-" + self.opponentNo
            TamFirebaseManager.shared.saveDocument(collection: firebaseCollection.Mute.rawValue , document: document) { (success) in
                if success {
                    self.lblMute.text = "UnMute".localized
                    self.switchMute.tag = 2
                    self.switchMute.setOn(true , animated: false)
                }
            }
        }*/
    }
    
    @IBAction func didPressDeleteChat(_ sender: UIButton) {
       self.deleteChat()
    }
    
    @IBAction func didPressClearChat(_ sender: UIButton) {
        self.delegate?.clearChatDelegateMethod()
    }
    @IBAction func didPressBlock(_ sender: UIButton) {
        if (self.lblBlockContact.text == "Block Contact"){
            self.showBlockAlert()
        } else {
            self.showUnBlockAlert()
        }
    }
    
    @IBAction func didPressBlockContact(_ sender: UIButton) {
        if (self.lblBlockContact.text == "Block Contact"){
            self.showBlockAlert()
        } else {
            self.showUnBlockAlert()
        }
    }
    
    //MARK:- Alerts
    func showBlockAlert(){
        let alertController = UIAlertController(title: "Are you sure?" , message: "You want to block this user", preferredStyle: .alert)
        let okAction = UIAlertAction(title: "Yes", style: UIAlertActionStyle.default) {
            UIAlertAction in
            self.delegate?.doBlockUnblock(state: "Block", completionHandler: { (success , status) in
                if success {
                    self.lblBlockContact.text = status
                }
            })
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel) {
            UIAlertAction in
        }
        alertController.addAction(okAction)
        alertController.addAction(cancelAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    
    func showUnBlockAlert(){
        let alertController = UIAlertController(title: "Are you sure?" , message: "You want to Unblock this user", preferredStyle: .alert)
        let okAction = UIAlertAction(title: "Yes", style: UIAlertActionStyle.default) {
            UIAlertAction in
            
            self.delegate?.doBlockUnblock(state: "Unblock", completionHandler: { (success , status) in
                if success {
                    self.lblBlockContact.text = status
                }
            })
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel) {
            UIAlertAction in
        }
        alertController.addAction(okAction)
        alertController.addAction(cancelAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    
    //MARK:- Contact
    func saveOrEditContact(number:String , isEdit: Bool) {
        let store = CNContactStore()
        let contact = CNMutableContact()
        let homePhone = CNLabeledValue(label: CNLabelHome, value: CNPhoneNumber(stringValue :number))
        contact.phoneNumbers = [homePhone]
        let controller = CNContactViewController(forNewContact: contact)
        controller.contactStore = store
        controller.delegate = self
        
        if isEdit {
            controller.title = "Edit Contact"
            contact.givenName = self.userName.text ?? ""
        } else {
            controller.title = "Create New Contact"
        }
        let pNavController = UINavigationController(rootViewController: controller)
        self.present(pNavController , animated: true , completion: nil)
    }
    
    
    
}

extension ChatInfo_VC: CNContactViewControllerDelegate {
    
     func contactViewController(_ viewController: CNContactViewController, didCompleteWith contact: CNContact?) {
        self.dismiss(animated: true , completion: {
            
            if contact != nil {
                var userName = ""
                
                if let name = contact?.givenName as? String{
                    userName = name
                }
                
                var addressBookQuick: NSMutableOrderedSet? = []
                constantVC.GlobalVariables.dictMyContacts = [:]
                
                if SessionManager.get_dictMyContacts() != nil {
                    constantVC.GlobalVariables.dictMyContacts = SessionManager.get_dictMyContacts()
                }
                
                var phone:String = self.opponentNo
                //parse and format for country code
                do {
                    let parsedPhNo = try self.phoneNumberKit.parse(phone, withRegion: SessionManager.getCountryCode() , ignoreType: true)
                    if let formatPhNo = self.phoneNumberKit.format(parsedPhNo, toType: .e164) as? String{
                        phone = formatPhNo
                    }
                } catch {
                    print("no country code")
                }
                
                phone = phone.components(separatedBy: CharacterSet.decimalDigits.inverted).joined()
                
                var contactQuick = QBAddressBookContact()
                contactQuick.name = userName
                contactQuick.phone = phone
                addressBookQuick?.add(contactQuick)
                
                constantVC.GlobalVariables.dictMyContacts![phone] = userName
                SessionManager.save_dictMyContacts(dict: constantVC.GlobalVariables.dictMyContacts)
                NimUserManager.shared.AddNewContactToAddressBook(addressBookQuick: addressBookQuick! , completionHandler: { (success) in
                    if success {
                        constantVC.ActiveDataSource.isActiveRefresh = true
                        self.delegate?.updateTitleAfterContactUpdate()
                        self.showUserInfo()
                        
                        ContactManager().getAddressBookAndUpdate { (success) in
                            if !(success) {
                            }
                        }
                    } else {
                        FTIndicator.showError(withMessage: "Error in saving contact")
                    }
                })
            }
        })
      }
}


extension ChatInfo_VC: UICollectionViewDelegate , UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if self.isShowMoreHidden() {
            return self.mediaMsgs.count
        }
        return self.mediaMsgs.count + 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if indexPath.row == self.mediaMsgs.count {
             let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "chatInfoMoreCell", for: indexPath) as? chatInfoShowMore_collectionCell
             cell?.btnShowMore.addTarget(self , action: #selector(self.didPressShowMore(_:)), for: .touchDown)
             return cell!
        }
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "chatInfoCell", for: indexPath) as? chatInfo_collectionViewCell
        
        cell?.btnVideoPlay.tag = indexPath.row
        cell?.btnVideoPlay.addTarget(self , action: #selector(self.didPressPlayVideo(_:)), for: .touchDown)
        
        if let msg_ = self.mediaMsgs[indexPath.row] as? QBChatMessage {
            cell?.msg = msg_
        }
        
        if let attachment = self.mediaMsgs[indexPath.row].attachments?.first {
            cell?.attachment = attachment
        }
        return cell!
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if let attachment = self.mediaMsgs[indexPath.row].attachments?.first {
            if attachment.attachmentType == QMAttachmentType.contentTypeImage {
                let fileUrl = attachment.remoteURL(withToken: false)
                QMImagePreview.previewImage(with: fileUrl, in: self)
            }
            if attachment.attachmentType == QMAttachmentType.contentTypeVideo {
                self.playVideo(attachment: attachment , message: self.mediaMsgs[indexPath.row])
            }
            
        }
    }
    
    
    func playVideo(attachment: QBChatAttachment , message: QBChatMessage){
        
        var url = attachment.remoteURL(withToken: false)
        
        var playerItem = AVPlayerItem(url: attachment.remoteURL())
        
        if videoPlayer != nil {
            videoPlayer!.replaceCurrentItem(with: nil)
            videoPlayer!.replaceCurrentItem(with: playerItem)
        } else {
            videoPlayer = AVPlayer(playerItem: playerItem)
        }
        
        var playerVC = AVPlayerViewController()
        playerVC.player = videoPlayer
        
        self.present(playerVC , animated: true) {
            
            do {
                try AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryPlayback)
            } catch {
                print(error)
            }
            
            self.videoPlayer!.play()
        }
    }
}




class chatInfo_collectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var vwSuper: UIView!
    @IBOutlet weak var imgVw: UIImageView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var btnVideoPlay: UIButton!
    
    var msg:QBChatMessage?
    var attachment: QBChatAttachment? {
        didSet {
            updateView()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        setPlayView(isHidden: true)
        stopLoading()
        vwSuper.layer.borderColor = UIColor.lightGray.cgColor
        vwSuper.layer.borderWidth = 1.0
    }
    
    func updateView(){
        
        var targetSize: CGSize? = self.bounds.size
        var transform = QMImageTransform(size: targetSize! , isCircle: false)
        
        if attachment?.attachmentType == QMAttachmentType.contentTypeImage {
            
            var url = attachment?.remoteURL(withToken: false)
            
            if url == nil {
                
                if ((attachment?.image) != nil) {
                    
                    transform.apply(for: (attachment?.image)!) { (img) in
                        self.imgVw.image = img
                    }
                }
            } else {
                var cachedImage: UIImage? = QMImageLoader.instance.imageCache?.imageFromCache(forKey: transform.key(with: url!))
                if cachedImage != nil {
                    transform.apply(for: (cachedImage)!) { (img) in
                        self.imgVw.image = img
                    }
                } else {
                    
                    self.startLoading()
                    
                    QMImageLoader.instance.downloadImage(with: url! , transform: transform , options: [.highPriority , .continueInBackground , .allowInvalidSSLCertificates ], progress: { (receivedSize , expectedSize , targetURL) in
                        
                    }) { (image, transfomedImage , error , cacheType , finished , imageURL) in
                        self.stopLoading()
                        if transfomedImage != nil {
                            self.imgVw.image = transfomedImage
                        }
                        if error != nil {
                            FTIndicator.showToastMessage("Error in downloading image")
                        }
                    }
                }
            }
        }
        
        //video
        if attachment?.attachmentType == QMAttachmentType.contentTypeVideo {
            
            var msgID:String = ""
            
            if let id = self.msg?.id {
                msgID = id
            }
            
            let image = QMImageLoader.instance.imageCache?.imageFromCache(forKey: msgID)
            if image != nil {
                
                self.setPlayView(isHidden: false)
                
                transform.apply(for: image!) { (img) in
                    self.imgVw.image = img
                }
            }
            else {
                
                self.startLoading()
                ServicesManager.instance().chatService.chatAttachmentService.prepare(attachment! , message: self.msg!) { (thumbnailImage , durationSeconds , size , error , cancelled) in
                    
                    self.stopLoading()
                    if cancelled {
                        return
                    } else if error != nil {
                        FTIndicator.showToastMessage("Error in downloading video")
                    }else {
                        if let attchmnt = self.attachment {
                            attchmnt.image = thumbnailImage
                            attchmnt.duration = lround(durationSeconds)
                            attchmnt.width = lround(Double(size.width))
                            attchmnt.height = lround(Double(size.height))
                            self.msg!.attachments = [attchmnt]
                            
                            ServicesManager.instance().chatService.messagesMemoryStorage.update(self.msg!)
                            
                            if thumbnailImage != nil {
                                transform.apply(for: thumbnailImage!) { (img) in
                                    self.imgVw.image = img
                                }
                                QMImageLoader.instance.imageCache!.store(thumbnailImage!, forKey: msgID, completion: nil)
                            }
                        }
                        self.setPlayView(isHidden: false)
                    }
                }
            }}
    }
    
    
    func startLoading(){
        self.activityIndicator.isHidden = false
        self.activityIndicator.startAnimating()
    }
    
    func stopLoading(){
        self.activityIndicator.isHidden = true
        self.activityIndicator.stopAnimating()
    }
    
    func setPlayView(isHidden: Bool) {
        btnVideoPlay.isHidden = isHidden
    }
    
}


class chatInfoShowMore_collectionCell: UICollectionViewCell {
    @IBOutlet weak var btnShowMore: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        btnShowMore.layer.cornerRadius = btnShowMore.frame.size.width/2.0
        btnShowMore.clipsToBounds = true
    }
}
