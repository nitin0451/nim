//
//  NewContactCell.swift
//  Tamaas
//
//  Created by Krescent Global on 22/02/18.
//  Copyright © 2018 Krescent Global. All rights reserved.
//

import UIKit

class NewContactCell: UITableViewCell {

    @IBOutlet weak var contactNAme: UILabel!
    @IBOutlet weak var contactImage: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

}
