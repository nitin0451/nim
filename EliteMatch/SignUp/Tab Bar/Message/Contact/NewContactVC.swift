//
//  NewContactVC.swift
//  Tamaas
//
//  Created by Krescent Global on 22/02/18.
//  Copyright © 2018 Krescent Global. All rights reserved.
//

import UIKit
import Contacts
import FTIndicator
import VisualEffectView

class NewContactVC: UIViewController , UITableViewDelegate, UITableViewDataSource {

    //MARK:- Outlets
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var userContactNo: UILabel!
    @IBOutlet weak var userImage: UIImageView!
    @IBOutlet weak var popupContact: UIView!
    @IBOutlet weak var naviBar: UINavigationBar!
    
    //MARK:- Variables
    var arrContacts = [CNContact]()
    var contactToSave:CNMutableContact!
    var contactToRemove = Int()
    var store: CNContactStore!
    
    //MARK:- UIView life-cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        store = CNContactStore()
        popupContact.cornerRadius = 5;
        tableView.delegate = self
        tableView.dataSource = self
    }
    
    override  func viewWillAppear(_ animated: Bool) {
        self.naviBar.titleTextAttributes = [NSAttributedStringKey.font: UIFont.init(name:"Lato-Regular",size:18)!]
    }
    
    //MARK:-UIButton Actions
    @IBAction func backAc(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func btn_closeContactPopUp_action(_ sender: UIButton) {
        self.removeAnimate(popupView: popupContact)
    }
    @IBAction func addToContact(_ sender: Any) {
        
        self.removeAnimate(popupView: popupContact)
        
        do{
            let saveRequest = CNSaveRequest()
            saveRequest.add(contactToSave, toContainerWithIdentifier:nil)
            try store.execute(saveRequest)
            
            FTIndicator.showSuccess(withMessage: "Contact Saved!")
            arrContacts.remove(at: contactToRemove)
            if arrContacts.count > 0{
                tableView.reloadData()
            }
            else{
                //self.navigationController?.popViewController(animated: true)
                if let topController = UIApplication.topViewController() {
                    topController.dismiss(animated: true, completion: nil)
                }
            }
        }
        catch{
            FTIndicator.showError(withMessage: "Failed, please try again")
        }
    }
    
    //MARK:- UITableViewDelegates and UITableDataSource
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 64.0
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.01
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
       return 0.01
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: NSInteger) -> Int {
        return arrContacts.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "myCell") as! NewContactCell
                let contact = arrContacts[indexPath.row]
        
                if let imageData = contact.imageData {
                    cell.contactImage.image = UIImage(data:imageData)
                }
                cell.contactNAme.text = contact.givenName
        
                return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        contactToSave = CNMutableContact()
        let contact = arrContacts[indexPath.row]
        
        contactToRemove = indexPath.row
        if let imageData = contact.imageData {
            userImage.image = UIImage(data:imageData)
        }
        userName.text = contact.familyName
        
        if contact.phoneNumbers.count > 0 {
            userContactNo.text = "\(contact.phoneNumbers[0].value.stringValue)"
        } else {
            userContactNo.text = contact.givenName
        }
        
        contactToSave.familyName = contact.familyName
        contactToSave.phoneNumbers = contact.phoneNumbers
        self.showAnimate(popupView: popupContact)
    }
    
    var visualEffectView : VisualEffectView!
    var popupViewOut = UIView()
    
    func showAnimate(popupView : UIView)
    {
        popupViewOut = popupView
        visualEffectView = VisualEffectView(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height))
        visualEffectView.colorTint = .black
        visualEffectView.colorTintAlpha = 0.4
        visualEffectView.blurRadius = 3
        visualEffectView.scale = 1
        
        self.view.addSubview(visualEffectView)
        
        self.view.addSubview(popupView)
        
        popupView.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
        popupView.alpha = 0.0
        popupView.isHidden = false
        UIView.animate(withDuration: 0.25, animations: {
            popupView.alpha = 1.0
            popupView.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
        })
    }
    
    func removeAnimate(popupView : UIView)
    {
        UIView.animate(withDuration: 0.25, animations: {
            popupView.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
            popupView.alpha = 0.0
        }, completion: {(finished : Bool) in
            if(finished)
            {
                for subview in self.view.subviews {
                    if subview is UIVisualEffectView {
                        subview.removeFromSuperview()
                    }
                }
                
                popupView.isHidden = true
            }
        })
    }

}
