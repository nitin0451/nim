//
//  GroupInfo_VC.swift
//  Tamaas
//
//  Created by Kitlabs-M-0002 on 3/4/19.
//  Copyright © 2019 Krescent Global. All rights reserved.
//

import UIKit
import FTIndicator
import AVKit

class GroupInfo_VC: UIViewController {

    //MARK:- Outlets
    @IBOutlet weak var userImage: UIImageView!
    @IBOutlet weak var groupName: UILabel!
    @IBOutlet weak var collectionView_media: UICollectionView!
    @IBOutlet weak var btnShowMore: UIButton!
    @IBOutlet weak var switchMute: UISwitch!
    @IBOutlet weak var lblparticipantsCount: UILabel!
    @IBOutlet weak var lblNoMedia: UILabel!
    @IBOutlet weak var vwRounded: UIView!
    @IBOutlet weak var vwGroupImg: UIView!
    @IBOutlet weak var groupMember1img: UIImageView!
    @IBOutlet weak var groupMember2img: UIImageView!
    @IBOutlet weak var lblMute: UILabel!
    
    //report view
    @IBOutlet weak var vwBlur: UIView!
    @IBOutlet weak var vwReport: UIView!
    @IBOutlet weak var collectionViewReport: UICollectionView!
    @IBOutlet weak var flowLayout: FlowLayout!
    @IBOutlet weak var constraint_mediaHeight: NSLayoutConstraint!
    @IBOutlet weak var imgVwNoImg: UIImageView!
    @IBOutlet weak var lblMedia: UILabel!
    @IBOutlet weak var lblClearChat: UILabel!
    @IBOutlet weak var lblReportGroup: UILabel!
    @IBOutlet weak var lblExitGroup: UILabel!
    @IBOutlet weak var lblParticipants: UILabel!
    
    
    
    //MARK:- Variables
    var dialog: QBChatDialog!
    var mediaMsgs:[QBChatMessage] = []
    var delegate: ChatInfoDelegate?
    var strGroupName:String = ""
    var videoPlayer: AVPlayer?
    var sizingCell: TagCell?
    var tags = [Tag]()
    var isNewNav:Bool = false
    
    
    //MARK:- UIView life-cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        //init
        ServicesManager.instance().chatService.addDelegate(self)
        self.localise_language()
        self.initView()
        self.showGroupName()
        self.showGroupImg()
        self.configureNavigationBar()
        self.getMediaMessages()
        self.hideReportView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
    }
    
    
    //MARK:- Custom Methods
    func localise_language(){
        self.lblMedia.text = "Media"
        self.lblNoMedia.text = "No media found.."
        self.lblParticipants.text = "Participants"
        self.lblClearChat.text = "Clear Chat"
        self.lblReportGroup.text = "Report Group"
        self.lblExitGroup.text = "Exit Group"
        self.lblMute.text = "Mute"
    }
    
    func setUserImage(isHidden: Bool){
        self.userImage.isHidden = isHidden
    }
    
    func setNoImage(isHidden: Bool){
        self.imgVwNoImg.isHidden = isHidden
    }
    
    func setGroupView(isHidden:Bool){
        self.vwGroupImg.isHidden = isHidden
    }
    func showGroupName(){
        if let name = self.dialog.name {
            
            //my circle
            if name.contains(SessionManager.get_phonenumber()) {
                self.groupName.text = "Your Circle"
            }
            else {
                let arrName = name.components(separatedBy: "-")
                if arrName.indices.contains(0){
                    self.groupName.text = arrName[0]
                }
            }
        }
    }
    
    func setParticiipantCount(){
        if let count = self.dialog.occupantIDs?.count{
            self.lblparticipantsCount.text = "\(count)"
        }
    }
    
    func showImage(url:URL) {
        let targetSize: CGSize? = self.userImage?.bounds.size
        let transform = QMImageTransform(size: targetSize! , isCircle: false)
        
        let cachedImage: UIImage? = QMImageLoader.instance.imageCache?.imageFromCache(forKey: transform.key(with: url))
        
        if cachedImage != nil {
            transform.apply(for: (cachedImage)!) { (img) in
                self.userImage.image = img
            }
        } else {
            
            QMImageLoader.instance.downloadImage(with: url , transform: transform , options: [.highPriority , .continueInBackground , .allowInvalidSSLCertificates ], progress: { (receivedSize , expectedSize , targetURL) in
            }) { (image, transfomedImage , error , cacheType , finished , imageURL) in
                if transfomedImage != nil {
                    self.userImage.image = transfomedImage
                }
                if error != nil {
                    //FTIndicator.showToastMessage("Error in downloading image")
                }
            }
        }
    }
    
    func showGroupImg(){
        
        if let name = self.dialog?.name {
            let arrName = name.components(separatedBy: "-")
            if arrName.indices.contains(1){
                self.setNoImage(isHidden: true)
                self.setUserImage(isHidden: false)
                self.showImage(url: ProfileManager.shared.getOpponentProfileUrl(opponentNo: arrName[1]))
            }
        }
        
        
        
       /* if let photo = self.dialog?.photo {
            
            self.setNoImage(isHidden: true)
            self.setUserImage(isHidden: false)
            self.userImage.yy_setImage(with: URL.init(string: "\(constantVC.GeneralConstants.GET_IMAGE)\(photo).jpeg") , placeholder: nil , options: [.progressiveBlur , .setImageWithFadeAnimation ] ) { (image , url , type , stage , error) in
            }
        } else {
            self.setUserImage(isHidden: true)
            self.setNoImage(isHidden: false)
        }*/
        
        
       /* else {
            if let groupOccupantsID = self.dialog?.occupantIDs {
                
                self.setGroupView(isHidden: false)
                let IDS = groupOccupantsID.filter { $0 != SessionManager.get_QuickBloxID() }
                
                if IDS.count > 0 && IDS.count >= 2  {
                    
                    //user1
                    ServicesManager.instance().usersService.getUserWithID( IDS[0] as! UInt , forceLoad: true).continueOnSuccessWith(block: { (task) -> Any? in
                        
                        if let no = task.result?.phone {
                            self.groupMember1img.yy_setImage(with: URL.init(string: "\(constantVC.GeneralConstants.ImageUrl)\(no).jpeg") , placeholder: UIImage(named: "contactCard") , options: [.progressiveBlur , .setImageWithFadeAnimation ] , completion: nil)
                        }
                        return nil
                    })
                    
                    //user2
                    ServicesManager.instance().usersService.getUserWithID( IDS[1] as! UInt , forceLoad: true).continueOnSuccessWith(block: { (task) -> Any? in
                        
                        if let no = task.result?.phone {
                            self.groupMember2img.yy_setImage(with: URL.init(string: "\(constantVC.GeneralConstants.ImageUrl)\(no).jpeg") , placeholder: UIImage(named: "contactCard") , options: [.progressiveBlur , .setImageWithFadeAnimation ] , completion: nil)
                        }
                        return nil
                    })
                }
            }
        }*/
    }
    
    func initView(){
        self.vwGroupImg.isHidden = true
        //self.configureChatMute()
        self.vwRounded.cornerRadius = 20.0
        self.vwRounded.clipsToBounds = true
        /*groupMember1img.layer.cornerRadius = groupMember1img.frame.size.width/2.0
        groupMember1img.clipsToBounds = true
        groupMember2img.layer.cornerRadius = groupMember2img.frame.size.width/2.0
        groupMember2img.clipsToBounds = true*/
        self.collectionView_media.delegate = self
        self.collectionView_media.dataSource = self
        self.collectionView_media.isHidden = true
        //view report
        let cellNib = UINib(nibName: "TagCell", bundle: nil)
        self.collectionViewReport.register(cellNib, forCellWithReuseIdentifier: "TagCell")
        self.collectionViewReport.backgroundColor = UIColor.clear
        self.collectionViewReport.delegate = self
        self.collectionViewReport.dataSource = self
        self.sizingCell = (cellNib.instantiate(withOwner: nil, options: nil) as NSArray).firstObject as! TagCell?
        self.flowLayout.sectionInset = UIEdgeInsetsMake(8, 8, 8, 8)
        for name in constantVC.GeneralConstants.ReportTags {
            let tag = Tag()
            tag.name = name
            self.tags.append(tag)
        }
        self.setUserImage(isHidden: true)
        self.setGroupView(isHidden: true)
        self.setNoImage(isHidden: true)
        self.setNoMediaView(isHidden: true)
        self.lblparticipantsCount.text = "\(self.getParticipantCount())"
        self.btnShowMore.addTarget(self , action: #selector(self.didPressShowMore(_:)), for: .touchDown)
    }
    
    func configureNavigationBar(){
        self.title = "Group Info"
        let backImg : UIImage? = UIImage.init(named: "blackBack")!.withRenderingMode(.alwaysOriginal)
        let backBtn   = UIBarButtonItem(image: backImg,  style: .plain, target: self, action: #selector(didPressBack(sender:)))
        navigationItem.leftBarButtonItem = backBtn
        
       // let editBtn = UIBarButtonItem(title: "Edit" , style: .plain, target: self, action: #selector(didPressEdit(sender:)))
       // navigationItem.rightBarButtonItem = editBtn
    }
    
    func getParticipantCount() ->Int{
        if let count = self.dialog.occupantIDs?.count{
            return count
        }
        return 0
    }
    
    /*func isChatMuted(completionHandler:@escaping (Bool) -> ()) {
        
        if let id = SessionManager.get_QuickBloxID() {
            TamFirebaseManager.shared.isGroupChatMuted(collection: firebaseCollection.MuteGroup.rawValue , doc: self.getDialogId() , id: id.stringValue) { (success) in
                if success {
                    completionHandler(true)
                } else {
                    completionHandler(false)
                }
            }
        }
    }
    
    
    func configureChatMute(){
        self.isChatMuted(completionHandler: { (success) in
            if success {
                self.lblMute.text = "UnMute".localized
                self.switchMute.setOn(true , animated: false)
                self.switchMute.tag = 2
            } else {
                self.lblMute.text = "Mute".localized
                self.switchMute.setOn(false , animated: false)
                self.switchMute.tag = 1
            }
        })
    }*/
    
    //MARK:- Custom Methods
    func reloadMediaCollection(){
        if self.mediaMsgs.count > 0 {
            self.collectionView_media.isHidden = false
            self.collectionView_media.reloadData()
        } else {
            self.setNoMediaView(isHidden: false)
        }
    }
    
    func setNoMediaView(isHidden: Bool) {
        self.lblNoMedia.isHidden = isHidden
    }
    
    func showParticipants(){
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "GroupParticipant_VC") as! GroupParticipant_VC
        controller.dialog = self.dialog
        self.navigationController?.pushViewController(controller , animated: false)
    }
    
    func isShowMoreHidden() ->Bool {
        if self.mediaMsgs.count < 6 {
            return true
        }
        return false
    }
    
    
    //MARK:- Media
    func getDialogId() ->String{
        if let id = self.dialog.id {
            return id
        }
        return ""
    }
    
    
    func getMediaMessages(){
        
        let extendedRequest = ["sort_desc" : "date_sent" , "attachments.type" : "image"]
        let resPage = QBResponsePage(limit:5, skip: 0)
        
        QBRequest.messages(withDialogID: self.getDialogId() , extendedRequest: extendedRequest , for: resPage , successBlock: { (response , msgs , page ) in
            self.mediaMsgs.append(contentsOf: msgs)
            self.getVideoMessages()
        }) { (error) in
            self.getVideoMessages()
        }
    }
    
    func getVideoMessages(){
        let extendedRequest = ["sort_desc" : "date_sent" , "attachments.type" : "video"]
        let resPage = QBResponsePage(limit:5, skip: 0)
        
        QBRequest.messages(withDialogID: self.getDialogId() , extendedRequest: extendedRequest , for: resPage , successBlock: { (response , msgs , page ) in
            self.mediaMsgs.append(contentsOf: msgs)
            self.sortMessages()
        }) { (error) in
        }
    }
    
    func sortMessages(){
        
        self.mediaMsgs = self.mediaMsgs.sorted(by: {
            $0.dateSent!.compare($1.dateSent!) == .orderedDescending
        })
        self.reloadMediaCollection()
    }
    
    //MARK:- Chat related methods
    func deleteChat(){
        
        let alertController = UIAlertController(title: "Are you sure?", message: "You want to exit group", preferredStyle: .alert)
        
        // Create the actions
        let okAction = UIAlertAction(title: "Yes", style: UIAlertActionStyle.default) {
            UIAlertAction in
            
            if Connectivity.isConnectedToInternet() {
                
                //delete code
                guard let dialog = self.dialog else {
                    return
                }
               
                let deleteDialogBlock = { (dialog: QBChatDialog!) -> Void in
                    // Deletes dialog from server and cache.
                    ServicesManager.instance().chatService.deleteDialog(withID: dialog.id!, completion: { (response) -> Void in
                        
                        guard response.isSuccess else {
                            SVProgressHUD.showError(withStatus:  UserMsg.Error)
                            return
                        }
                        SVProgressHUD.showSuccess(withStatus: UserMsg.groupLeft)
                    })
                }
                
                // group
                let occupantIDs = dialog.occupantIDs!.filter({ (number) -> Bool in
                    
                    return number.uintValue != ServicesManager.instance().currentUser.id
                })
                
                dialog.occupantIDs = occupantIDs
                let userLogin = ServicesManager.instance().currentUser.login ?? ""
                let notificationMessage = "\(userLogin) " + UserMsg.groupLeftMsg
                // Notifies occupants that user left the dialog.
                ServicesManager.instance().chatService.sendNotificationMessageAboutLeaving(dialog, withNotificationText: notificationMessage, completion: { (error) -> Void in
                    deleteDialogBlock(dialog)
                })
              //Nitin
                //navigate
                let initialViewController = self.storyboard?.instantiateViewController(withIdentifier: "TabBarController") as! TabBarController
                let appDelegate = UIApplication.shared.delegate as! AppDelegate
                appDelegate.window?.rootViewController = initialViewController
                
            }
            else {
                //OFFLINE
                FTIndicator.showToastMessage("No Internet Connection!")
            }
        }
        let cancelAction = UIAlertAction(title: UserMsg.cancel , style: UIAlertActionStyle.cancel) {
            UIAlertAction in
            NSLog("Cancel Pressed")
        }
        
        // Add the actions
        alertController.addAction(okAction)
        alertController.addAction(cancelAction)
        
        // Present the controller
        self.present(alertController, animated: true, completion: nil)
    }
    
    
    //MARK:- UIButton Actions
    @objc func didPressBack(sender: UIBarButtonItem) {
        if !(self.isNewNav) {
            self.navigationController?.popViewController(animated: false)
        } else {
            self.dismiss(animated: false , completion: nil)
        }
    }
    
    @objc func didPressEdit(sender: UIBarButtonItem) {
        //navigate to group info VC
        /*let storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "AddGroupInfo") as! AddGroupInfo
        vc.isEditGroup = true
        vc.dialog = self.dialog
        self.present(vc, animated: true, completion: nil)*/
    }
    
    @objc func didPressShowMore(_ button: UIButton) {
        if self.mediaMsgs.count > 0 {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let controller = storyboard.instantiateViewController(withIdentifier: "ChatMedia_VC") as! ChatMedia_VC
            controller.dialog = self.dialog
            self.navigationController?.pushViewController(controller , animated: true)
        }
    }
    
    @IBAction func didPressMute(_ sender: UISwitch) {
        
        /*if self.switchMute.tag == 2 {
            //unmute
            if let id = SessionManager.get_QuickBloxID() {
                TamFirebaseManager.shared.deleteDocument_groupMute(collection: firebaseCollection.MuteGroup.rawValue , doc: self.getDialogId() , Id: id.stringValue) { (success) in
                    if success {
                        self.lblMute.text = "Mute".localized
                        self.switchMute.tag = 1
                        self.switchMute.setOn(false , animated: false)
                    }
                }
            }
        } else {
            //mute
            if let id = SessionManager.get_QuickBloxID() {
                TamFirebaseManager.shared.saveDocument_groupMute(collection: firebaseCollection.MuteGroup.rawValue , doc: self.getDialogId() , newId: id.stringValue) { (success) in
                    if success {
                        self.lblMute.text = "UnMute".localized
                        self.switchMute.tag = 2
                        self.switchMute.setOn(true , animated: false)
                    }
                }
            }
        }*/
    }
    
    
    @IBAction func didPressShowParticipants(_ sender: UIButton) {
        self.showParticipants()
    }
    
    @IBAction func didPressClearChat(_ sender: UIButton) {
        self.delegate?.clearChatDelegateMethod()
    }
    
    @IBAction func didPressReportGroup(_ sender: UIButton) {
        self.showReportView()
    }
    
    @IBAction func didPressExitGroup(_ sender: UIButton) {
        self.deleteChat()
    }
    
    @objc func didPressPlayVideo(_ button: UIButton) {
        let indexPath = IndexPath(row: button.tag , section: 0)
        if let attachment = self.mediaMsgs[indexPath.row].attachments?.first {
            self.playVideo(attachment: attachment , message: self.mediaMsgs[indexPath.row])
        }
    }
    
    //MARK:- Report Group Related Methods
    @IBAction func didPressCloseReportView(_ sender: UIButton) {
        self.hideReportView()
    }
    
    @IBAction func didPressReport(_ sender: UIButton) {
        var str_type:String = ""
        for var i in (0..<self.tags.count) {
            if self.tags[i].selected {
                
                if str_type == "" {
                    str_type = self.tags[i].name!
                }
                else {
                    str_type = str_type + "," + self.tags[i].name!
                }
            }
        }
        self.callTo_reportgroup(type: str_type)
    }
    
    func showReportView(){
        UIView.animate(withDuration: 0.25, animations: {
            self.vwBlur.isHidden = false
            self.vwReport.isHidden = false
        })
    }
    
    func hideReportView(){
        UIView.animate(withDuration: 0.25, animations: {
            self.vwBlur.isHidden = true
            self.vwReport.isHidden = true
        })
    }
    
    func configureReportCell(_ cell: TagCell, forIndexPath indexPath: IndexPath) {
        let tag = tags[indexPath.row]
        cell.tagName.text = tag.name
        cell.backgroundColor = tag.selected ? UIColor(red: 255.0/255.0, green: 211.0/255.0, blue: 108.0/255.0, alpha: 1) : UIColor(red: 227.0/255, green: 227.0/255.0, blue: 227.0/255.0, alpha: 1)
    }
 
    
    func callTo_reportgroup(type: String) {
        
        self.view.endEditing(true)
        let webservice  = WebserviceSigleton ()
        
        let dictParam : NSDictionary = ["group_id": self.getDialogId() ,"user_id": SessionManager.get_phonenumber(), "type": type]
        FTIndicator.showProgress(withMessage: "")
        
        webservice.POSTServiceWithParameters(urlString: constantVC.WebserviceName.URL_SUBMIT_GROUP_REPORT, params: dictParam as! Dictionary<String, AnyObject>, callback:{ (result,error) -> Void in
            if result != nil{
                let response = result! as Dictionary
                let resultMessage = response["msg"] as! String
                if resultMessage == "success"{
                    FTIndicator.dismissProgress()
                    self.hideReportView()
                    UserConfirmationAlertManager.instance.showAlert_reportSubmitted(completionHandler: { (success) in
                        
                    })
                }
                else{
                    FTIndicator.dismissProgress()
                }
            }
            else{
                FTIndicator.dismissProgress()
            }
        })
    }
}


extension GroupInfo_VC: QMChatServiceDelegate , QMChatConnectionDelegate {
    
    func chatService(_ chatService: QMChatService, didUpdateChatDialogInMemoryStorage chatDialog: QBChatDialog) {
        
        if self.dialog.type != QBChatDialogType.private && self.dialog.id == chatDialog.id {
            self.dialog = chatDialog
            
            //if group dialog update title
            if chatDialog.type == QBChatDialogType.group {
                self.showGroupName()
                self.showGroupImg()
            }
        }
    }
}


extension GroupInfo_VC: UICollectionViewDelegate , UICollectionViewDataSource , UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if collectionView == self.collectionViewReport {
            return tags.count
        }
        
        if self.isShowMoreHidden() {
            return self.mediaMsgs.count
        }
        return self.mediaMsgs.count + 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if collectionView == self.collectionViewReport {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "TagCell", for: indexPath) as! TagCell
            self.configureReportCell(cell , forIndexPath: indexPath)
            return cell
        }
        
        if indexPath.row == self.mediaMsgs.count {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "chatInfoMoreCell", for: indexPath) as? chatInfoShowMore_collectionCell
            cell?.btnShowMore.addTarget(self , action: #selector(self.didPressShowMore(_:)), for: .touchDown)
            return cell!
        }
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "chatInfoCell", for: indexPath) as? chatInfo_collectionViewCell
        
        cell?.btnVideoPlay.tag = indexPath.row
        cell?.btnVideoPlay.addTarget(self , action: #selector(self.didPressPlayVideo(_:)), for: .touchDown)
        
        if let msg_ = self.mediaMsgs[indexPath.row] as? QBChatMessage {
            cell?.msg = msg_
        }
        
        if let attachment = self.mediaMsgs[indexPath.row].attachments?.first {
            cell?.attachment = attachment
        }
        return cell!
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if collectionView == self.collectionViewReport {
            self.configureReportCell(self.sizingCell! , forIndexPath: indexPath)
            return self.sizingCell!.systemLayoutSizeFitting(UILayoutFittingCompressedSize)
        }
        
        return CGSize(width: 100.0 , height: 128.0)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if collectionView == self.collectionViewReport {
            tags[indexPath.row].selected = !tags[indexPath.row].selected
            self.collectionViewReport.reloadData()
        } else {
            if let attachment = self.mediaMsgs[indexPath.row].attachments?.first {
                if attachment.attachmentType == QMAttachmentType.contentTypeImage {
                    let fileUrl = attachment.remoteURL(withToken: false)
                    QMImagePreview.previewImage(with: fileUrl, in: self)
                }
                if attachment.attachmentType == QMAttachmentType.contentTypeVideo {
                    self.playVideo(attachment: attachment , message: self.mediaMsgs[indexPath.row])
                }
            }
        }
        collectionView.deselectItem(at: indexPath, animated: false)
    }
    
    
    func playVideo(attachment: QBChatAttachment , message: QBChatMessage){
        
        var url = attachment.remoteURL(withToken: false)
        
        var playerItem = AVPlayerItem(url: attachment.remoteURL())
        
        if videoPlayer != nil {
            videoPlayer!.replaceCurrentItem(with: nil)
            videoPlayer!.replaceCurrentItem(with: playerItem)
        } else {
            videoPlayer = AVPlayer(playerItem: playerItem)
        }
        
        var playerVC = AVPlayerViewController()
        playerVC.player = videoPlayer
        
        self.present(playerVC , animated: true) {
            
            do {
                try AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryPlayback)
            } catch {
                print(error)
            }
            
            self.videoPlayer!.play()
        }
    }
}
