//
//  GroupParticipant_VC.swift
//  Tamaas
//
//  Created by Kitlabs-M-0002 on 3/7/19.
//  Copyright © 2019 Krescent Global. All rights reserved.
//

import UIKit

class GroupParticipant_VC: UIViewController {
    
    //MARK:- Outlets
    @IBOutlet weak var tblVwParticipants: UITableView!
    
    
    //MARK:- Variables
    var dialog: QBChatDialog!
    var groupParticipants:[QBUUser] = []
    
    
    //MARK:- UIView life-cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()

        //init
        self.initView()
        self.configureNavigationBar()
        self.GetGroupInfo()
    }
    
    
    //MARK:- Custom methods
    func configureNavigationBar(){
        self.title = "Participants(\(self.getParticipantCount()))"
        let backImg : UIImage? = UIImage.init(named: "blackBack")!.withRenderingMode(.alwaysOriginal)
        let backBtn   = UIBarButtonItem(image: backImg,  style: .plain, target: self, action: #selector(didPressBack(sender:)))
        navigationItem.leftBarButtonItem = backBtn
    }
    
    func initView(){
        self.tblVwParticipants.delegate = self
        self.tblVwParticipants.dataSource = self
    }
    
    func getParticipantCount() ->Int{
        if let count = self.dialog.occupantIDs?.count{
           return count
        }
        return 0
    }
    
    func GetGroupInfo(){
        
        if let occupantIDS = self.dialog.occupantIDs {
            for id in occupantIDS {
                if let user = ServicesManager.instance().usersService.usersMemoryStorage.user(withID: id as! UInt) {
                    self.groupParticipants.append(user)
                }
                else {
                    ServicesManager.instance().usersService.getUserWithID(id as! UInt , forceLoad: true).continueOnSuccessWith(block: { (task) -> Any? in
                        if let user = task.result {
                            self.groupParticipants.append(user)
                        }
                        return nil
                    })
                }
            }
            self.reloadTable()
        }
    }
    
    func reloadTable(){
        self.tblVwParticipants.reloadData()
    }
    
    //MARK:- UIButton Actions
    @objc func didPressBack(sender: UIBarButtonItem) {
        self.navigationController?.popViewController(animated: true)
    }
}

extension GroupParticipant_VC: UITableViewDelegate , UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.groupParticipants.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as? GroupParticipant_TableViewCell
        cell?.user = self.groupParticipants[indexPath.row]
        return cell!
    }
}


class GroupParticipant_TableViewCell: UITableViewCell {
    
    @IBOutlet weak var userImage: UIImageView!
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var lblUserRegisteredName: UILabel!
    
    var user: QBUUser? {
        didSet {
            updateView()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        userImage.layer.cornerRadius = userImage.frame.size.width/2.0
        userImage.clipsToBounds = true
    }
    
    func updateView(){
        
        if let no = user?.phone {
            self.userImage.yy_setImage(with: URL.init(string: "\(constantVC.GeneralConstants.GET_IMAGE)\(no).jpeg") , placeholder: UIImage(named: "contactCard") , options: [.progressiveBlur , .setImageWithFadeAnimation ] , completion: nil)
            lblUserName.text = user?.fullName //SessionManager.getNameFromMyAddressBook(number: no)
        }
        if let name = user?.fullName {
            lblUserRegisteredName.text = name
        }
    }
}
