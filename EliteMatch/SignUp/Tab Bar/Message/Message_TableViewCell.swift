//
//  Message_TableViewCell.swift
//  EliteMatch
//
//  Created by osvinuser on 07/02/19.
//  Copyright © 2019 Kitlabs-M-0002. All rights reserved.
//

import UIKit
import SwipeCellKit

class Message_TableViewCell: SwipeTableViewCell {

    //MARK:- IBOutlet
    @IBOutlet weak var messageCount: UILabel!
    @IBOutlet weak var messgeDeliveryTime: UILabel!
    @IBOutlet weak var lastMessage: UILabel!
    @IBOutlet weak var UserName: UILabel!
    @IBOutlet weak var UserProfileImge: UIImageView!
    @IBOutlet weak var imgVw_sticker: UIImageView!
    
    //MARK:- Variables
    var opponentNo:String = ""
    var dialog: QBChatDialog? {
        didSet {
            self.UserName.text = ""
            self.UserProfileImge.image = nil
            updateView()
        }
    }
  
    
    func updateView() {
        
        if let opponentNo = DialogManagerNIM.instance.getOpponentNo_fromGroupName(dialog: self.dialog!) {
            self.opponentNo = opponentNo
            
            ProfileManager.shared.getRefreshImage(url: ProfileManager.shared.getOpponentProfileUrl(opponentNo: opponentNo) , imageView: UserProfileImge) { (image) in
                if let img = image {
                    self.UserProfileImge.image = img
                }
            }
            
        } else if let opponentNo = DialogManagerNIM.instance.getOpponentNo(dialog: self.dialog!) {
            self.opponentNo = opponentNo
            
            ProfileManager.shared.getRefreshImage(url: ProfileManager.shared.getOpponentProfileUrl(opponentNo: opponentNo) , imageView: UserProfileImge) { (image) in
                if let img = image {
                    self.UserProfileImge.image = img
                }
            }
        }
        
        if let msg = dialog?.lastMessageText {
            if MessageDialogManager.shared.isSticker(msgText: msg).isSticker {
                self.imgVw_sticker.isHidden = false
                self.lastMessage.text = ""
                self.imgVw_sticker.image = UIImage(named: MessageDialogManager.shared.isSticker(msgText: msg).stickerImg)
            }
            else {
                self.imgVw_sticker.isHidden = true
                MessageDialogManager.shared.setLastMsg(msgText: msg, dialog: self.dialog!) { (lastMsgText) in
                    self.lastMessage.text = lastMsgText
                }
            }
        } else {
            self.imgVw_sticker.isHidden = true
            self.lastMessage.text = "Say hi to your new friend..."
        }
        
        
        DialogManagerNIM.instance.getUserName(dailog: self.dialog!) { (name) in
            self.UserName.text = name
        }
        
        if let unreadCount = dialog?.unreadMessagesCount {
            if unreadCount > 0 {
                self.messageCount.isHidden = false
                self.messageCount.text = "\(unreadCount)"
                self.lastMessage.font = UIFont.init(name:"CircularStd-Medium",size:12)
            } else {
                self.messageCount.isHidden = true
                self.lastMessage.font = UIFont.init(name:"CircularStd-Book",size:12)
            }
        }
        if let date = dialog?.updatedAt {
            if let formattedDate = QMDateUtils.formattedShortDateString(date) {
                self.messgeDeliveryTime.text = formattedDate
            }
        }
    }
    
    
    func getLastMsg(msgText:String , completionHandler:@escaping (String) -> ()){
        
        //define last message
        if msgText == "Image attachment" || msgText == "Audio attachment" || msgText == "Video attachment"{
            completionHandler("Media")
        }
        else {
            completionHandler(msgText)
        }
    }
   
    
    override func awakeFromNib() {
        super.awakeFromNib()
        UserProfileImge.layer.cornerRadius = UserProfileImge.frame.size.width/2
        messageCount.layer.cornerRadius = messageCount.frame.size.width/2
        messageCount.clipsToBounds = true
        UserProfileImge.clipsToBounds = true
        UserName.textColor = UIColor(red: 54/255, green: 54/255, blue: 54/255, alpha: 1.0)
        lastMessage.textColor = UIColor(red: 28/255, green: 27/255, blue: 27/255, alpha: 1.0)
        //messgeDeliveryTime.textColor = UIColor(red: 0/255, green: 161/255, blue: 98/255, alpha: 1.0)
        messageCount.backgroundColor = UIColor(red: 0/255, green: 161/255, blue: 98/255, alpha: 1.0)
        messageCount.textColor = UIColor(red: 255/255, green: 255/255, blue: 255/255, alpha: 1.0)
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
}





class MessageDialogManager: NSObject {
    
    static let shared = MessageDialogManager()
    
    func isSticker(msgText:String) ->(isSticker:Bool , stickerImg: String){
        if msgText.contains("TAMSTICKER") {
            let msgTextArr = msgText.components(separatedBy: "*")
            if msgTextArr.count == 2 {
                if let stickerName = msgTextArr[1] as? String {
                    return (true , stickerName)
                }
            }
        }
        return (false , "")
    }
    
    func setLastMsg(msgText:String , dialog: QBChatDialog , completionHandler:@escaping (String) -> ()){
        
        //define last message
        if msgText == "Image attachment" || msgText == "Audio attachment" || msgText == "Video attachment"{
            completionHandler("Media")
        }
        else {
            completionHandler(msgText)
        }
    }
}
