//
//  MatchingFor_tableCell.swift
//  EliteMatch
//
//  Created by Apple on 10/09/19.
//  Copyright © 2019 Kitlabs-M-0002. All rights reserved.
//

import UIKit

class MatchingFor_tableCell: UITableViewCell {
    
    
    @IBOutlet weak var borderView: UIView!
    @IBOutlet weak var userImage: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var imgSelection: UIImageView!
    @IBOutlet weak var vwContent: UIView!
    
    @IBOutlet weak var vwInvite: UIView!
    @IBOutlet weak var inviteUserImage: UIImageView!
    @IBOutlet weak var lblTitle_invite: UILabel!
    @IBOutlet weak var lblInviteSelection: UILabel!
    
    var opponentName:String?
    var opponentNo:String?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        self.setUserImageRound()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    func setUserImageRound(){
        self.userImage.layer.cornerRadius = self.userImage.frame.size.width / 2.0
        self.userImage.clipsToBounds = true
    }
    
    func setBorderView(){
        self.borderView.layer.cornerRadius = self.borderView.frame.size.width / 2.0
        self.borderView.clipsToBounds = true
        self.borderView.layer.borderWidth = 1.0
        self.borderView.layer.borderColor = constantVC.GlobalColor.lightPurple.cgColor
    }
    
    func set_unselectLable(){
        self.lblInviteSelection.layer.cornerRadius = self.lblInviteSelection.frame.size.height / 2.0
        self.lblInviteSelection.layer.borderColor = constantVC.GlobalColor.lightPurple.cgColor
        self.lblInviteSelection.layer.borderWidth = 1.0
        self.lblInviteSelection.textColor = constantVC.GlobalColor.lightPurple
    }

}
