//
//  FreindRequest_Cell.swift
//  EliteMatch
//
//  Created by osvinuser on 12/04/19.
//  Copyright © 2019 Kitlabs-M-0002. All rights reserved.
//

import UIKit

class FreindRequest_Cell: UITableViewCell {
    
    @IBOutlet weak var userImageView: UIImageView!
    @IBOutlet weak var notification_lbl: UILabel!
    @IBOutlet weak var time_lbl: UILabel!
    @IBOutlet weak var btnAccept: UIButton!
    @IBOutlet weak var btnReject: UIButton!
    
    
    
    
    var invite: Invitation? {
        didSet {
            self.updateView()
        }
    }
    
    func updateView(){
        if let name = self.invite?.name {
            self.notification_lbl.attributedText = self.getTitle(name: name + " ")
        }
        if let date = self.invite?.createdAt {
            self.time_lbl.text = self.getTime(strDate: date)
        }
        if let image = invite?.image {
            if let url = URL.init(string: constantVC.GeneralConstants.GET_IMAGE + image) {
                self.showImage(url: url)
            }
        }
    }
    
    
    func showImage(url:URL) {
        let targetSize: CGSize? = self.userImageView?.bounds.size
        let transform = QMImageTransform(size: targetSize! , isCircle: false)
        
        let cachedImage: UIImage? = QMImageLoader.instance.imageCache?.imageFromCache(forKey: transform.key(with: url))
        
        if cachedImage != nil {
            transform.apply(for: (cachedImage)!) { (img) in
                self.userImageView.image = img
            }
        } else {
            
            QMImageLoader.instance.downloadImage(with: url , transform: transform , options: [.highPriority , .continueInBackground , .allowInvalidSSLCertificates ], progress: { (receivedSize , expectedSize , targetURL) in
            }) { (image, transfomedImage , error , cacheType , finished , imageURL) in
                if transfomedImage != nil {
                    self.userImageView.image = transfomedImage
                }
                if error != nil {
                    //FTIndicator.showToastMessage("Error in downloading image")
                }
            }
        }
    }
    
    func getTime(strDate:String) ->String{
        if let date = CommonFunctions.shared.getDateFromString(dateFormat: "yyyy-MM-dd HH:mm:ss" , dateStr: strDate) {
            let dateFormatterPrint = DateFormatter()
            dateFormatterPrint.dateFormat = "MMM dd yyyy h:mm a"
            return dateFormatterPrint.string(from: date)
        }
        return ""
    }
    
    
    func getTitle(name:String) ->NSAttributedString{
        
        let firstAttributes: [NSAttributedStringKey: Any] = [.foregroundColor: UIColor(red: 70/255.0, green: 147/255.0, blue: 208/255.0, alpha: 1.0), NSAttributedStringKey.kern: 0]
        let secondAttributes = [NSAttributedStringKey.foregroundColor: UIColor.black]
        
        let firstString = NSMutableAttributedString(string: name , attributes: firstAttributes)
        let secondString = NSAttributedString(string: "sent you an invite to connect.", attributes: secondAttributes)
        firstString.append(secondString)
        
        return firstString
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        userImageView.layer.cornerRadius = userImageView.frame.width/2
        userImageView.clipsToBounds = true
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    
}
