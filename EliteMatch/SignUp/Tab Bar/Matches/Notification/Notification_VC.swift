//
//  Notification_VC.swift
//  EliteMatch
//
//  Created by osvinuser on 07/03/19.
//  Copyright © 2019 Kitlabs-M-0002. All rights reserved.
//

import UIKit
import FTIndicator

class Notification_VC: UIViewController {

    //MARK:- Outlets
    @IBOutlet weak var bluePoint_lbl: UILabel!
    @IBOutlet weak var notification_TblView: UITableView!
    @IBOutlet weak var friendRequestView: UIView!
    @IBOutlet weak var lblNoData: UILabel!
    @IBOutlet weak var lblChaperonRequestCount: UILabel!
    
    @IBOutlet weak var lblCircleRequestCount: UILabel!
    @IBOutlet weak var lblBluePoint_circle: UILabel!
    @IBOutlet weak var vwCircleRequest: UIView!
    
    //MARK:- Variables
    var arrInvitations:[Invitation] = []
    var arrChaperonInvitations:[Invitation] = []
    var arrCIRCLEInvitations:[Invitation] = []
    
    
    //MARK:- UIView life-cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.initView()
        self.getConnectNotifications()
        self.get_chaperon_Notifications()
        self.get_CIRCLE_Notifications()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationItem.leftBarButtonItem?.tintColor = .gray
    }
    
    
    //MARK:- Custom Methods
    func initView(){
        notification_TblView.register(UINib(nibName: "Notification_TC", bundle: nil), forCellReuseIdentifier: "Notification_TC")
        friendRequestView.layer.borderWidth = 0.5
        friendRequestView.layer.borderColor = UIColor.lightGray.cgColor
        vwCircleRequest.layer.borderWidth = 0.5
        vwCircleRequest.layer.borderColor = UIColor.lightGray.cgColor
        bluePoint_lbl.layer.cornerRadius = bluePoint_lbl.frame.size.width/2
        bluePoint_lbl.clipsToBounds = true
        lblBluePoint_circle.layer.cornerRadius = lblBluePoint_circle.frame.size.width/2
        lblBluePoint_circle.clipsToBounds = true
    }
    
    
    func getConnectNotifications(){
        InvitationManager.instance.getConnect_invitation(status: invitationStatus.pending.rawValue) { (success , arrData) in
            if success {
                if arrData.count > 0 {
                    self.lblNoData.isHidden = true
                    if let arr = DataParserManager.shared.getConnectInvitation(arrData: arrData) {
                        self.arrInvitations = arr
                        self.notification_TblView.reloadData()
                    }
                } else {
                    self.lblNoData.isHidden = false
                }
            } else {
                self.lblNoData.isHidden = false
            }
        }
    }
    
    
    func get_chaperon_Notifications(){
        InvitationManager.instance.getConnect_chaperon_invitation(status: invitationStatus.pending.rawValue) { (success , arrData) in
            if success {
                if arrData.count > 0 {
                    self.lblNoData.isHidden = true
                    if let arr = DataParserManager.shared.getConnectInvitation(arrData: arrData) {
                        if arr.count > 0 {
                            self.arrChaperonInvitations = arr
                            self.lblChaperonRequestCount.text = "\(arr.count)"
                        }
                    }
                }
            }
        }
    }
    
    
    func get_CIRCLE_Notifications(){
        InvitationManager.instance.getConnect_circle_invitation(status: invitationStatus.pending.rawValue) { (success , arrData) in
            if success {
                if arrData.count > 0 {
                    self.lblNoData.isHidden = true
                    if let arr = DataParserManager.shared.getConnectInvitation(arrData: arrData) {
                        if arr.count > 0 {
                            self.arrCIRCLEInvitations = arr
                            self.lblCircleRequestCount.text = "\(arr.count)"
                        }
                    }
                }
            }
        }
    }
    
    //MARK:- UIButton Actions
    @IBAction func didPress_back(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func didPress_circlerequest(_ sender: Any) {
        CommonFunctions.shared.navigateNotificationVC_circleRequest()
    }
    
    @IBAction func actionCharpeonRequests(_ sender: Any) {
        CommonFunctions.shared.navigateNotificationVC_chaperonRequest()
    }
    
    @objc func didPressChangeRequestStatus_accept(sender:UIButton) {
        UserConfirmationAlertManager.instance.showAlert(title: userMessagestext.areYouSure , subTitle: userMessagestext.acceptInvitationConfirmation, okTitle: "Yes", cancelTitle: "No") { (success) in
            if success {
                if let invite = self.arrInvitations[sender.tag] as? Invitation{
                    InvitationManager.instance.connectStatusChange_invitation(id: invite.id, fromName: SessionManager.getUserName(), invitationByID: invite.from_id , status: invitationStatus.active.rawValue) { (success) in
                        if success {
                            
                            FTIndicator.showProgress(withMessage: userMessagestext.invitationAccept + "\nConnecting you to chat....")
                            
                            self.arrInvitations.remove(at: sender.tag)
                            self.setInvitationData()
                           
                            //get user
                        ServicesManager.instance().usersService.getUsersWithLogins([invite.from_phone_number]).continueOnSuccessWith(block: { (task) -> Any? in
                                 if task.isCompleted {
                                    if let users = task.result as? [QBUUser] {
                                        if users.indices.contains(0) {
                                            self.createChat(user: users[0])
                                        }
                                    }
                                 }else {
                                    FTIndicator.dismissProgress()
                                 }
                            return nil
                            })
                        }
                    }
                }
            }
        }
    }
    
    func setInvitationData(){
        if self.arrInvitations.count > 0 {
            self.lblNoData.isHidden = true
            self.notification_TblView.reloadData()
        } else {
            self.lblNoData.isHidden = false
            self.notification_TblView.reloadData()
        }
    }
    
    func createChat(user:QBUUser){
        
        DialogManagerNIM.instance.CreatePrivateGroupChat(user: user) { (success, dialog) in
            if success {
                FTIndicator.dismissProgress()
                if let dialog_ = dialog {
                    self.navigate_ChatVC(dailog: dialog_)
                }
            } else {
                FTIndicator.dismissProgress()
                FTIndicator.showError(withMessage: UserMsg.errorConnectingChat)
            }
        }
    }
    
    func navigate_ChatVC(dailog:QBChatDialog){
        
        self.dismiss(animated: true) {
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let controller = storyboard.instantiateViewController(withIdentifier: "ChatViewController") as! ChatViewController
            controller.dialog = dailog
            if let opponentNo_ = DialogManagerNIM.instance.getOpponentNo(dialog: dailog) {
                controller.opponentNo = opponentNo_
            }
            controller.isNewChatCreated = true
            if let topVC = UIApplication.topViewController() {
                topVC.navigationController?.pushViewController(controller , animated: false)
            }
        }
        
    }
    
    @objc func didPressChangeRequestStatus_reject(sender:UIButton) {
        UserConfirmationAlertManager.instance.showAlert(title: userMessagestext.areYouSure , subTitle: userMessagestext.rejectInvitationConfirmation , okTitle: "Yes", cancelTitle: "No") { (success) in
            if let invite = self.arrInvitations[sender.tag] as? Invitation{
                InvitationManager.instance.connectStatusChange_invitation(id: invite.id, fromName: "", invitationByID: invite.from_id , status: invitationStatus.rejected.rawValue) { (success) in
                    if success {
                        FTIndicator.showSuccess(withMessage: userMessagestext.invitationReject)
                        self.arrInvitations.remove(at: sender.tag)
                        self.setInvitationData()
                    }
                }
            }
        }
    }
}

extension Notification_VC : UITableViewDataSource{
    
     func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView.init(frame: CGRect.init(x: 0, y: 0, width: tableView.frame.width, height: 30))
        // headerView.backgroundColor = UIColor.red
        let label = UILabel()
        label.frame = CGRect.init(x: 22, y: 0, width: headerView.frame.width-10, height: headerView.frame.height)
        label.textColor = UIColor.black
        label.font = UIFont(name: "CircularStd-Book", size: 15.0)
        label.text = "This week"
        headerView.addSubview(label)
        return headerView
    }
    
     func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
   
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrInvitations.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
       let cell = tableView.dequeueReusableCell(withIdentifier: "Notification_TC", for: indexPath) as! Notification_TC
        
        if self.arrInvitations.indices.contains(indexPath.row) {
            cell.invite = self.arrInvitations[indexPath.row]
        }
        
        //add action
        cell.notificationAccept_Btn.addTarget(self, action: #selector(didPressChangeRequestStatus_accept(sender:)), for: .touchUpInside)
        cell.notificationAccept_Btn.tag = indexPath.row
        cell.notificationReject_Btn.addTarget(self, action: #selector(didPressChangeRequestStatus_reject(sender:)), for: .touchUpInside)
        cell.notificationReject_Btn.tag = indexPath.row
        
        return cell
    }

}

extension Notification_VC : UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
      
    }
    
}
