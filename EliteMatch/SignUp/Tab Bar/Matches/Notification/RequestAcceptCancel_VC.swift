//
//  RequestAcceptCancel_VC.swift
//  EliteMatch
//
//  Created by osvinuser on 11/04/19.
//  Copyright © 2019 Kitlabs-M-0002. All rights reserved.
//

import UIKit
import FTIndicator

class RequestAcceptCancel_VC: UIViewController {
    
    //MARK:- Outlets
    @IBOutlet weak var frindRequestTblView: UITableView!
    @IBOutlet weak var lblNoRequest: UILabel!
    @IBOutlet weak var lblTitle: UILabel!
    
    //MARK:- Variables
    var strTitle:String = ""
    var arrChaperonInvitations:[Invitation] = []
    var arrCIRCLEInvitations:[Invitation] = []
    
    //MARK:- UIView life-cycyle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.initView()
        if self.isCircleRequests(){
            self.get_CIRCLE_Notifications()
        } else {
            self.get_chaperon_Notifications()
        }
    }

    override func viewWillDisappear(_ animated: Bool) {
       self.navigationController?.isNavigationBarHidden = false
       self.navigationController?.navigationBar.isHidden = false
    }
    
    //MARK:- Web Services
    func get_chaperon_Notifications(){
        InvitationManager.instance.getConnect_chaperon_invitation(status: invitationStatus.pending.rawValue) { (success , arrData) in
            if success {
                if arrData.count > 0 {
                    if let arr = DataParserManager.shared.getConnectInvitation(arrData: arrData) {
                        if arr.count > 0 {
                            self.arrChaperonInvitations = arr
                            self.setRequestData(arrData: self.arrChaperonInvitations)
                        }
                    }
                }
            }
        }
    }
    
    
    func get_CIRCLE_Notifications(){
        InvitationManager.instance.getConnect_circle_invitation(status: invitationStatus.pending.rawValue) { (success , arrData) in
            if success {
                if arrData.count > 0 {
                    if let arr = DataParserManager.shared.getConnectInvitation(arrData: arrData) {
                        if arr.count > 0 {
                            self.arrCIRCLEInvitations = arr
                            self.setRequestData(arrData: self.arrCIRCLEInvitations)
                        }
                    }
                }
            }
        }
    }
    
    func setRequestData(arrData:[Invitation]){
        if arrData.count > 0 {
            self.lblNoRequest.isHidden = true
        } else {
            self.lblNoRequest.isHidden = false
        }
        self.frindRequestTblView.reloadData()
    }
    
    
    //MARK:- Custom Methods
    
    func isCircleRequests() ->Bool{
        if self.strTitle == "Chaperon Requests" {
          return false
        }
        return true
    }
    
    func initView(){
        frindRequestTblView.dataSource = self
        frindRequestTblView.register(UINib(nibName: "FreindRequest_Cell", bundle: nil), forCellReuseIdentifier: "FreindRequest_Cell")
    }
    
    //MARK:- UIButton Actions
    @IBAction func didPress_back(_ sender: UIButton) {
        self.dismiss(animated: true , completion: nil)
    }
   
    @objc func didPressChangeRequestStatus_accept(sender:UIButton) {
        UserConfirmationAlertManager.instance.showAlert(title: userMessagestext.areYouSure , subTitle: userMessagestext.acceptInvitationConfirmation , okTitle: "Yes", cancelTitle: "No") { (success) in
            if success {
                
                //CIRCLE
                if self.isCircleRequests() {
                    if let invite = self.arrCIRCLEInvitations[sender.tag] as? Invitation{
                        InvitationManager.instance.connectStatusChange_circle_invitation(id: invite.id, fromName: SessionManager.getUserName(), invitationByID: invite.from_id , status: invitationStatus.active.rawValue , completionHandler: { (success) in
                            if success {
                                
                                FTIndicator.showProgress(withMessage: userMessagestext.invitationAccept + "\nConnecting you to chat....")
                                
                                self.arrCIRCLEInvitations.remove(at: sender.tag)
                                self.setRequestData(arrData: self.arrCIRCLEInvitations)
                                
                                
                                //join user to your circle
                                if invite.dialogID == "" {
                                    
                                    //get user
                                    ServicesManager.instance().usersService.getUsersWithLogins([invite.from_phone_number]).continueOnSuccessWith(block: { (task) -> Any? in
                                        
                                        if task.isCompleted {
                                            
                                            if let users = task.result as? [QBUUser] {
                                                if users.indices.contains(0) {
                                                    
                                                    //create circle if not created
                                                    DialogManagerNIM.instance.isMyCircleCreated { (success , chatDialog) in
                                                        if success {
                                                            //join users
                                                            if let dialog = chatDialog {
                                                                
                                                                if let id = users[0].id as? UInt {
                                                            DialogManagerNIM.instance.addUsersTo_MyCircle(arrOpponentId: [NSNumber(value:id)] , dialog: dialog , completionHandler: { (success) in
                                                                FTIndicator.dismissProgress()
                                                               self.navigate_ChatVC(dailog:dialog)
                                                                    })
                                                                }
                                                            }
                                                        } else {
                                                            //create circle
                                                            DialogManagerNIM.instance.createMyCircle(users: [users[0]] , completionHandler: { (success , chatDialog) in
                                                                if let dialog = chatDialog {
                                                                    FTIndicator.dismissProgress()
                                                                    self.navigate_ChatVC(dailog: dialog)
                                                                }
                                                            })
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        return nil
                                    })
                                 
                                }
                                else {
                                    let extendedRequest = ["_id" : invite.dialogID]
                                    let page = QBResponsePage(limit: 1, skip: 0)
                                    
                                    QBRequest.dialogs(for: page , extendedRequest: extendedRequest , successBlock: { (response , arrDialogs , nmber , page ) in
                                        
                                        if arrDialogs.indices.contains(0) {
                                            
                                            DialogManagerNIM.instance.UpdateDialogStatus_CIRCLEAcceptRequest(dialog: arrDialogs[0], completionHandler: { (success) in
                                                if success {
                                                    FTIndicator.dismissProgress()
                                                    self.navigate_ChatVC(dailog: arrDialogs[0])
                                                } else {
                                                    FTIndicator.dismissProgress()
                                                    FTIndicator.showError(withMessage: UserMsg.errorConnectingChat)
                                                }
                                            })
                                        } else {
                                            FTIndicator.dismissProgress()
                                            FTIndicator.showError(withMessage: UserMsg.errorConnectingChat)
                                        }
                                    }, errorBlock: { (error) in
                                        print(error.description)
                                        FTIndicator.dismissProgress()
                                        FTIndicator.showError(withMessage: UserMsg.errorConnectingChat)
                                    })
                                }
                               
                            }
                        })
                    }
                }
                //CHAPERON
                else {
                    if let invite = self.arrChaperonInvitations[sender.tag] as? Invitation{
                        InvitationManager.instance.connectStatusChange_chaperon_invitation(id: invite.id, fromName: SessionManager.getUserName(), invitationByID: invite.from_id , status: invitationStatus.active.rawValue , completionHandler: { (success) in
                            if success {
                                
                                FTIndicator.showProgress(withMessage: userMessagestext.invitationAccept + "\nConnecting you to chat....")
                                
                                self.arrChaperonInvitations.remove(at: sender.tag)
                                self.setRequestData(arrData: self.arrChaperonInvitations)
                                
                                let extendedRequest = ["_id" : invite.dialogID]
                                let page = QBResponsePage(limit: 1, skip: 0)
                                
                                QBRequest.dialogs(for: page , extendedRequest: extendedRequest , successBlock: { (response , arrDialogs , nmber , page ) in
                                    
                                    if arrDialogs.indices.contains(0) {
                                        
                                        DialogManagerNIM.instance.UpdateDialogStatus_chaperonAcceptRequest(dialog: arrDialogs[0], completionHandler: { (success) in
                                            if success {
                                                FTIndicator.dismissProgress()
                                                self.navigate_ChatVC(dailog: arrDialogs[0])
                                            } else {
                                                FTIndicator.dismissProgress()
                                                FTIndicator.showError(withMessage: UserMsg.errorConnectingChat)
                                            }
                                        })
                                    } else {
                                        FTIndicator.dismissProgress()
                                        FTIndicator.showError(withMessage: UserMsg.errorConnectingChat)
                                    }
                                }, errorBlock: { (error) in
                                    print(error.description)
                                    FTIndicator.dismissProgress()
                                    FTIndicator.showError(withMessage: UserMsg.errorConnectingChat)
                                })
                            }
                        })
                    }
                }
            }
        }
    }
    
    @objc func didPressChangeRequestStatus_reject(sender:UIButton) {
        UserConfirmationAlertManager.instance.showAlert(title: userMessagestext.areYouSure , subTitle: userMessagestext.rejectInvitationConfirmation , okTitle: "Yes", cancelTitle: "No") { (success) in
            if self.isCircleRequests() {
                if let invite = self.arrCIRCLEInvitations[sender.tag] as? Invitation{
                    InvitationManager.instance.connectStatusChange_circle_invitation(id: invite.id, fromName: "", invitationByID: invite.from_id , status: invitationStatus.rejected.rawValue , completionHandler: { (success) in
                        if success {
                            FTIndicator.showSuccess(withMessage: userMessagestext.invitationReject)
                            self.arrCIRCLEInvitations.remove(at: sender.tag)
                            self.setRequestData(arrData: self.arrCIRCLEInvitations)
                            
                            let extendedRequest = ["_id" : invite.dialogID]
                            let page = QBResponsePage(limit: 1, skip: 0)
                            
                            QBRequest.dialogs(for: page , extendedRequest: extendedRequest , successBlock: { (response , arrDialogs , nmber , page ) in
                                
                                if arrDialogs.indices.contains(0) {
                                    self.deleteDialogBlock(arrDialogs[0])
                                }
                            }, errorBlock: { (error) in
                                
                                print(error.description)
                                
                            })
                        }
                    })
                }
            }
            else {
                if let invite = self.arrChaperonInvitations[sender.tag] as? Invitation{
                    InvitationManager.instance.connectStatusChange_chaperon_invitation(id: invite.id, fromName: "", invitationByID: invite.from_id , status: invitationStatus.rejected.rawValue , completionHandler: { (success) in
                        if success {
                            FTIndicator.showSuccess(withMessage: userMessagestext.invitationReject)
                            self.arrChaperonInvitations.remove(at: sender.tag)
                            self.setRequestData(arrData: self.arrChaperonInvitations)
                            
                            let extendedRequest = ["_id" : invite.dialogID]
                            let page = QBResponsePage(limit: 1, skip: 0)
                            
                            QBRequest.dialogs(for: page , extendedRequest: extendedRequest , successBlock: { (response , arrDialogs , nmber , page ) in
                                
                                if arrDialogs.indices.contains(0) {
                                    self.deleteDialogBlock(arrDialogs[0])
                                }
                            }, errorBlock: { (error) in
                                
                                print(error.description)
                                
                            })
                        }
                    })
                }
            }
        }
    }
    
    let deleteDialogBlock = { (dialog: QBChatDialog!) -> Void in
        
        // Deletes dialog from server and cache.
        ServicesManager.instance().chatService.deleteDialog(withID: dialog.id!, completion: { (response) -> Void in
            
            guard response.isSuccess else {
               // SVProgressHUD.showError(withStatus: "SA_STR_ERROR_DELETING".localized)
                return
            }
            
            if dialog.type == QBChatDialogType.private {
               // SVProgressHUD.showSuccess(withStatus: "SA_STR_DELETED".localized)
            } else {
                //SVProgressHUD.showSuccess(withStatus: "SA_STR_LEFT".localized)
            }
            
            
        })
    }
  
  
    func navigate_ChatVC(dailog:QBChatDialog){
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "ChatViewController") as! ChatViewController
        controller.dialog = dailog
        if let opponentNo_ = DialogManagerNIM.instance.getOpponentNo_fromGroupName(dialog: dailog){
            controller.opponentNo = opponentNo_
        }
        controller.chatType = .circle
        self.presentingViewController?.presentingViewController?.dismiss(animated: true, completion: {
            if let topVC = UIApplication.topViewController() {
                print(topVC)
                topVC.navigationController?.pushViewController(controller , animated: false)
            }
        })
       
    }
    
}

extension RequestAcceptCancel_VC : UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView.init(frame: CGRect.init(x: 0, y: 0, width: tableView.frame.width, height: 30))
        // headerView.backgroundColor = UIColor.red
        let label = UILabel()
        label.frame = CGRect.init(x: 22, y: 5, width: headerView.frame.width-10, height: headerView.frame.height)
        label.textColor = UIColor.black
        label.font = UIFont(name: "CircularStd-Book", size: 15.0)
        label.text = "This week"
        headerView.addSubview(label)
        return headerView
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 85
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.isCircleRequests() {
            return self.arrCIRCLEInvitations.count
        }
        return self.arrChaperonInvitations.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "FreindRequest_Cell", for: indexPath) as! FreindRequest_Cell
        
        
        if self.isCircleRequests() {
            if self.arrCIRCLEInvitations.indices.contains(indexPath.row) {
                cell.invite = self.arrCIRCLEInvitations[indexPath.row]
            }
        } else {
            if self.arrChaperonInvitations.indices.contains(indexPath.row) {
                cell.invite = self.arrChaperonInvitations[indexPath.row]
            }
        }
        
        //add action
        cell.btnAccept.addTarget(self, action: #selector(didPressChangeRequestStatus_accept(sender:)), for: .touchUpInside)
        cell.btnAccept.tag = indexPath.row
        cell.btnReject.addTarget(self, action: #selector(didPressChangeRequestStatus_reject(sender:)), for: .touchUpInside)
        cell.btnReject.tag = indexPath.row
        return cell
    }
}

extension RequestAcceptCancel_VC : UITableViewDelegate{
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    }
}
