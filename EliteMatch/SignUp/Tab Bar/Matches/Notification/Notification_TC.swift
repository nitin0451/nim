//
//  Notification_TC.swift
//  EliteMatch
//
//  Created by osvinuser on 07/03/19.
//  Copyright © 2019 Kitlabs-M-0002. All rights reserved.
//

import UIKit

class Notification_TC: UITableViewCell {

    @IBOutlet weak var user_imgView_btn: UIButton!
    @IBOutlet weak var notification_Timelbl: UILabel!
    @IBOutlet weak var notificationAccept_Btn: UIButton!
    @IBOutlet weak var notification_Lbl: UILabel!
    @IBOutlet weak var notificationReject_Btn: UIButton!
    @IBOutlet weak var imagVW_user: UIImageView!
    
    
    var invite: Invitation? {
        didSet {
            self.updateView()
        }
    }
    
    
    func updateView(){
        if let name = self.invite?.name {
            self.notification_Lbl.attributedText = self.getTitle(name: name + " ")
        }
        if let date = self.invite?.createdAt {
            self.notification_Timelbl.text = self.getTime(strDate: date)
        }
        if let image = invite?.image {
            if let url = URL.init(string: constantVC.GeneralConstants.GET_IMAGE + image) {
                print("dsfrfreertertrtreerytry" , url)
                self.showImage(url: url)
            }
        }
    }
    
    
    func showImage(url:URL) {
        let targetSize: CGSize? = self.imagVW_user?.bounds.size
        let transform = QMImageTransform(size: targetSize! , isCircle: false)
        
        let cachedImage: UIImage? = QMImageLoader.instance.imageCache?.imageFromCache(forKey: transform.key(with: url))
        
        if cachedImage != nil {
            transform.apply(for: (cachedImage)!) { (img) in
                self.imagVW_user.image = img
            }
        } else {
            
            QMImageLoader.instance.downloadImage(with: url , transform: transform , options: [.highPriority , .continueInBackground , .allowInvalidSSLCertificates ], progress: { (receivedSize , expectedSize , targetURL) in
            }) { (image, transfomedImage , error , cacheType , finished , imageURL) in
                if transfomedImage != nil {
                    self.imagVW_user.image = transfomedImage
                }
                if error != nil {
                    //FTIndicator.showToastMessage("Error in downloading image")
                }
            }
        }
    }
    
    func getTime(strDate:String) ->String{
        if let date = CommonFunctions.shared.getDateFromString(dateFormat: "yyyy-MM-dd HH:mm:ss" , dateStr: strDate) {
            let dateFormatterPrint = DateFormatter()
            dateFormatterPrint.dateFormat = "MMM dd yyyy h:mm a"
            return dateFormatterPrint.string(from: date)
        }
        return ""
    }
    
    
    func getTitle(name:String) ->NSAttributedString{
        
        let firstAttributes: [NSAttributedStringKey: Any] = [.foregroundColor: UIColor(red: 70/255.0, green: 147/255.0, blue: 208/255.0, alpha: 1.0), NSAttributedStringKey.kern: 0]
        let secondAttributes = [NSAttributedStringKey.foregroundColor: UIColor.black]
        
        let firstString = NSMutableAttributedString(string: name , attributes: firstAttributes)
        let secondString = NSAttributedString(string: "sent you an invite to connect.", attributes: secondAttributes)
        firstString.append(secondString)
        
        return firstString
    }
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        imagVW_user.cornerRadius = imagVW_user.frame.width/2
        imagVW_user.clipsToBounds = true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}
