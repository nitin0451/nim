//
//  Matches_VC.swift
//  EliteMatch
//
//  Created by Kitlabs-M-0002 on 11/20/18.
//  Copyright © 2018 Kitlabs-M-0002. All rights reserved.
//

import UIKit
import CountdownLabel
import SwiftGifOrigin
import FTIndicator
import Firebase
import PushKit
import AVFoundation
import MediaPlayer
import MessageUI

class Matches_VC: UIViewController {
    
    //MARK:- Outlets
    @IBOutlet weak var constraint_vwNavigation_top: NSLayoutConstraint!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var timleCount_Lbl: CountdownLabel!
    @IBOutlet weak var expireHourLbl: UILabel!
    @IBOutlet weak var yourMachesLbl: UILabel!
    @IBOutlet weak var vwChaperon: UIView!
    @IBOutlet var mainView: UIView!
    @IBOutlet weak var backgroundBlack_icon: UIImageView!
    @IBOutlet weak var top_view: UIView!
    @IBOutlet weak var lblNoData: UILabel!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var lblNoInternet: UILabel!
    @IBOutlet weak var btnJoinCircle: UIButton!
    @IBOutlet weak var vwGifLoader: UIView!
    @IBOutlet weak var imgVwGifLoader: UIImageView!
    @IBOutlet weak var vwTopMatchingFor: UIView!
    @IBOutlet weak var imgVwUser_matchingFor: UIImageView!
    @IBOutlet weak var lblMatchingFor_title: UILabel!
    @IBOutlet weak var lblMatchingFor_subTitle: UILabel!
    @IBOutlet weak var constraint_topViewHeight: NSLayoutConstraint!
    
    //view matching for
    @IBOutlet weak var vwMatchingFor: UIView!
    @IBOutlet weak var vwSuper_matchingFor: UIView!
    @IBOutlet weak var constraint_vwSuperMatchingFor_height: NSLayoutConstraint!
    @IBOutlet weak var tableVw_matchingFor: UITableView!
    
    //view match popup
    @IBOutlet var vwSuper_MatchPopUp: UIView!
    @IBOutlet weak var vwMatchPopUp: UIView!
    @IBOutlet weak var userImage_matchPopUp: UIImageView!
    @IBOutlet weak var txtField_msgMatchPopUp: UITextField!
    @IBOutlet weak var lbl_userName_matchPopUp: UILabel!
    
    //view circle
    @IBOutlet var vwSuper_circle: UIView!
    @IBOutlet weak var vwContainer_circle: UIView!
    @IBOutlet weak var tablevw_circle: UITableView!
    @IBOutlet weak var constraint_vwSuperCircle_height: NSLayoutConstraint!
    
    //MARK:- Variables
    private var timer: Timer?
    var arrDailyMatch:[DailyMatch] = []
    var selectedIndex:Int?
    var selectedCircles:[QBChatDialog] = []
    var selectedMatchIndex:Int?
    
    lazy private var voipRegistry: PKPushRegistry = {
        let voipRegistry = PKPushRegistry(queue: DispatchQueue.main)
        return voipRegistry
    }()
    lazy private var backgroundTask: UIBackgroundTaskIdentifier = {
        let backgroundTask: UIBackgroundTaskIdentifier = UIBackgroundTaskInvalid
        return backgroundTask
    }()
    var completionBlock: Completion?
    
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    var bombSoundEffect: AVAudioPlayer?
    
    func play() {
        let path = Bundle.main.path(forResource: "ringtone.wav", ofType:nil)!
        let url = URL(fileURLWithPath: path)
        
        do {
            bombSoundEffect = try AVAudioPlayer(contentsOf: url)
            bombSoundEffect?.play()
        } catch {
            // couldn't load file :(
            print("dfghthryuyujyu")
        }
    }
        
    var matchTimer: Timer?
    var intervalSeconds: Int?
    var tempCount:Int = 0
    
    //MARK:- UIView life-cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //self.play()
        voipRegistry.delegate = self
        voipRegistry.desiredPushTypes = Set<PKPushType>([.voIP])
        
        self.configureGifLoader()   
        self.navigationController?.isNavigationBarHidden = true
        //UIApplication.shared.statusBarView?.backgroundColor = .clear
        if SessionManager.isChaperonUser() {
            self.setChaperonView(isHidden: false)
        } else {
            mainView.backgroundColor = UIColor(red: 248/256, green: 248/256, blue: 248/256, alpha: 1.0)
            expireHourLbl.textColor = UIColor(red: 255/255, green: 255/255, blue: 255/255, alpha: 1.0)
            self.callToGetMatches(userID: SessionManager.get_Elite_userID())
        }
        
        let nib = UINib(nibName: "Match_CollectionViewCell", bundle: nil)
        collectionView?.register(nib, forCellWithReuseIdentifier: "Match_CollectionViewCell")
        collectionView.delegate = self
        collectionView.dataSource = self
        NimUserManager.shared.updateUserInfo()  //update user info over QB
        self.configureJoinACircleButton()
  
        let notificationCenter = NotificationCenter.default
        notificationCenter.addObserver(self, selector: #selector(self.appMovedToBackground), name: Notification.Name.UIApplicationDidEnterBackground, object: nil)

        notificationCenter.addObserver(self, selector: #selector(self.appCameToForeground), name: Notification.Name.UIApplicationWillEnterForeground, object: nil)

        self.setUpTimer()
//        self.matchesViewObj = InvitationVIew(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height))
//        self.view.addSubview(self.matchesViewObj!)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        CommonFunctions.shared.enableQBKeyboard()
        self.navigationController?.isNavigationBarHidden = true
        self.configureNotificationObservers()
        
        if Connectivity.isConnectedToInternet() {
            self.lblNoInternet.isHidden = true
        } else {
            self.lblNoInternet.isHidden = false
            self.constraint_vwNavigation_top.constant = 30
        }
       
        //init
        self.configureMatchingForView()
        CommonFunctions.shared.setNavigationBackButton(navController: self.navigationController , isHidden: true)
        if SessionManager.isChaperonUser() {
            CommonFunctions.shared.setNavigationWhite(navController: self.navigationController)
        } else {
            CommonFunctions.shared.setNavigationBlack(navController: self.navigationController)
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.navigationBar.barTintColor  = UIColor.white
        self.navigationController?.navigationBar.tintColor = UIColor.black
        self.navigationController?.isNavigationBarHidden = true
    }
    
    func setUpTimer() {
        
        let no = SessionManager.get_phonenumber()
        ProfileManager.shared.getUserProfile_phoneNo(phoneNo: no) { (success , userProfile) in
            var profile : UserProfile?
            profile = userProfile
            if let approvalDate = profile?.approvalDate {
                let formatter = DateFormatter()
                formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
                formatter.locale = .current
                formatter.timeZone = TimeZone(abbreviation: "UTC")
                guard let newApprovalDate = formatter.date(from: approvalDate) else {return}
                
                var timerDate : Date?
                timerDate = newApprovalDate
                
                let diff = Calendar.current.dateComponents([.second,.minute,.hour], from: newApprovalDate, to: Date())
                guard let minutes = diff.minute else {return}
                guard let seconds = diff.second else {return}
                
                if let hour = diff.hour,hour >= 24 {
                    let value = hour.quotientAndRemainder(dividingBy: 24)
                    let extraHours = value.remainder
                                        
                    let totalSeconds = (extraHours*60*60) + (minutes*60) + seconds
                    let remainingMins = 24*60 - ((extraHours*60) + minutes)
                    print(remainingMins)
                    let newTimerTime = Date().addingTimeInterval(TimeInterval(-totalSeconds))
                        timerDate = newTimerTime
                        SessionManager.instance.timerValue = totalSeconds
                    self.configureMatchTimer(minsLeft: remainingMins)

                } else if let hour = diff.hour,hour < 24{
                    let totalSeconds = (hour*60*60) + (minutes*60) + seconds
                    let minsLeft = (24*60) - (hour*60)
                    self.configureMatchTimer(minsLeft: minsLeft)
                    SessionManager.instance.timerValue = totalSeconds
                }
                
                guard let fireDate = timerDate else {return}

                self.matchTimer = Timer(fireAt: fireDate, interval: 1.0, target: self, selector: #selector(self.matchTimerFunc(timerValue:)), userInfo: nil, repeats: true)
                
            }
        }
        
    }
    
    @objc func matchTimerFunc(timerValue: Timer?) {
        if let _ = timerValue?.fireDate {
            self.tempCount += 1
            print(self.tempCount)
            SessionManager.instance.timerValue = self.tempCount
            
            if SessionManager.instance.timerValue == 24*60*60 {
                SessionManager.instance.timerValue = 0
                SessionManager.instance.dailyApiCount = nil
            }
        }
    }
    
    @objc func appMovedToBackground() {
        SessionManager.instance.oldTime = Date()

    }
    
    @objc func appCameToForeground() {
        if let timerValue = SessionManager.instance.timerValue {
           print(timerValue)
            let oneDaySec = 24*60*60
            if timerValue >= oneDaySec {
                let diff = timerValue - oneDaySec
                SessionManager.instance.timerValue = diff
                SessionManager.instance.dailyApiCount = nil
                let mins = (24*60) - (diff/60)
                self.configureMatchTimer(minsLeft: mins)
            } else {
                let mins = (24*60)-(timerValue/60)
                self.configureMatchTimer(minsLeft: mins)
            }


//           if let diffInDays = Calendar.current.dateComponents([.second], from: timerTime, to: Date()).second {
//               self.intervalSeconds = diffInDays
//               print(diffInDays)
//               let value = secondsToHoursMinutesSeconds(seconds: diffInDays)
//               print(value)
//               if value.0 >= 24 && value.2 > 0 {
//                   SessionManager.instance.timerValue = nil
//                   SessionManager.instance.dailyApiCount = 0
//               }
//           }

       }
               
    }
    
    //MARK:- NSNotification Observers
    @objc func handleNotification_updateView_InternetDisconnected(_ notificationObj: NSNotification) {
        self.lblNoInternet.isHidden = false
        self.constraint_vwNavigation_top.constant = 30
    }
    
    @objc func handleNotification_updateView_InternetConnected(_ notificationObj: NSNotification) {
        self.lblNoInternet.isHidden = true
        self.constraint_vwNavigation_top.constant = 0
        NotificationCenter.default.post(name: Notification.Name.UIApplicationWillEnterForeground, object: nil)
    }
    
    
    //MARK:- Time Conversion Function
    func secondsToHoursMinutesSeconds (seconds : Int) -> (Int, Int, Int) {
      return (seconds / 3600, (seconds % 3600) / 60, (seconds % 3600) % 60)
    }
    
    func timeString(time: TimeInterval) -> String {
        let hour = Int(time) / 3600
        let minute = Int(time) / 60 % 60
        let second = Int(time) % 60

        // return formated string
        return String(format: "%02i:%02i:%02i", hour, minute, second)
    }
    
    //MARK:- Custom Methods
    func configureNotificationObservers(){
        NotificationCenter.default.addObserver(self, selector: #selector(self.handleNotification_updateView_InternetDisconnected), name: constantVC.NSNotification_name.NSNotification_InternetDisconnected , object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.handleNotification_updateView_InternetConnected), name: constantVC.NSNotification_name.NSNotification_InternetConnected , object: nil)
    }
    
    func configureMatchingForView(){
        self.vwTopMatchingFor.layer.borderColor = UIColor.white.cgColor
        self.vwTopMatchingFor.layer.borderWidth = 1.0
        self.imgVwUser_matchingFor.layer.cornerRadius = self.imgVwUser_matchingFor.frame.size.width / 2.0
        self.imgVwUser_matchingFor.layer.borderColor = UIColor.white.cgColor
        self.imgVwUser_matchingFor.layer.borderWidth = 1.0
        self.imgVwUser_matchingFor.clipsToBounds = true
        
        if SessionManager.isChaperonUser() {
            self.lblMatchingFor_subTitle.text = "Match your friends"
        }
        else {
            
            ProfileManager.shared.getRefreshImage(url: ProfileManager.shared.getMyProfileUrl() , imageView: self.imgVwUser_matchingFor) { (image) in
                if let img = image {
                    self.imgVwUser_matchingFor.image = img
                }
            }
            
            
           // self.imgVwUser_matchingFor.yy_setImage(with: ProfileManager.shared.getMyProfileUrl() , placeholder: ProfileManager.shared.getPlaceHolderUserImage())
            self.lblMatchingFor_subTitle.text = "Yourself"
        }
    }
    
    func configureJoinACircleButton(){
        self.btnJoinCircle.layer.borderWidth = 1.0
        self.btnJoinCircle.layer.borderColor = constantVC.GlobalColor.lightPurple.cgColor
    }
    
    func configureGifLoader(){
        if !SessionManager.isChaperonUser() {
            self.add_subView(subView: self.vwGifLoader)
            self.imgVwGifLoader.image = UIImage.gif(name: "loader")
            timer = Timer.scheduledTimer(withTimeInterval: 3, repeats: false, block: { (timer) in
                self.hide()
            })
        }
    }
    func hide() {
        self.remove_subView(subView: self.vwGifLoader)
        timer?.invalidate()
    }
    
    func setChaperonView(isHidden:Bool) {
        self.vwChaperon.isHidden = false
        self.constraint_topViewHeight.constant = 0
        yourMachesLbl.isHidden = true
        expireHourLbl.isHidden = true
        timleCount_Lbl.isHidden = true
        top_view.isHidden = true
        self.lblNoData.isHidden = true
        self.lblNoInternet.isHidden = true
        self.activityIndicator.isHidden = true
    }
   
    func configureMatchTimer(minsLeft:Int){
        
        timleCount_Lbl.setCountDownTime(fromDate: NSDate() , minutes: TimeInterval(minsLeft) * 60)
        timleCount_Lbl.start()
    }
    
    func showViewMatchingFor(){
        self.add_subView(subView: self.vwSuper_matchingFor)
        self.vwSuper_matchingFor.alpha = 0
        UIView.animate(withDuration: 0.5) {
            self.vwSuper_matchingFor.alpha = 1
        }
        self.constraint_vwSuperMatchingFor_height.constant = MatchingForManager.shared.superMatctchingForViewHeight()
        self.tableVw_matchingFor.delegate = self
        self.tableVw_matchingFor.dataSource = self
        self.tableVw_matchingFor.reloadData()
    }
    
    func hideViewMatchingFor(){
        UIView.animate(withDuration: 0.5, animations: {
            self.vwSuper_matchingFor.alpha = 0
        }) { _ in
            self.remove_subView(subView: self.vwSuper_matchingFor)
        }
    }
    
    func showView_MatchPopUp(){
        self.add_subView(subView: self.vwSuper_MatchPopUp)
        self.vwMatchPopUp.layer.cornerRadius = 6.0
        self.vwSuper_MatchPopUp.alpha = 0
        UIView.animate(withDuration: 0.7) {
            self.vwSuper_MatchPopUp.alpha = 1
        }
    }
    
    func hideView_MatchPopUp(){
        UIView.animate(withDuration: 0.5, animations: {
            self.vwSuper_MatchPopUp.alpha = 0
        }) { _ in
            self.remove_subView(subView: self.vwSuper_MatchPopUp)
        }
    }
    
    func getUserAge(dob:String) ->String {
        if let date = CommonFunctions.shared.getDateFromString(dateFormat: "mm-dd-yyyy" , dateStr: dob) {
            if let age_ = CommonFunctions.shared.calculateAge(dob: date) {
                return ", \(age_)"
            }
        }
        return ""
    }
    
    
    func showView_circlePopUP(){
        self.add_subView(subView: self.vwSuper_circle)
        self.vwContainer_circle.layer.cornerRadius = 6.0
        self.vwContainer_circle.clipsToBounds = true
        self.constraint_vwSuperCircle_height.constant = MatchingForManager.shared.superCircleViewHeight()
        self.vwSuper_circle.alpha = 0
        self.tablevw_circle.delegate = self
        self.tablevw_circle.dataSource = self
        self.tablevw_circle.reloadData()
        UIView.animate(withDuration: 0.7) {
            self.vwSuper_circle.alpha = 1
        }
    }
    
    func hideView_circlePopUP(){
        UIView.animate(withDuration: 0.5, animations: {
            self.vwSuper_circle.alpha = 0
        }) { _ in
            self.remove_subView(subView: self.vwSuper_circle)
        }
    }
    
    //MARK:- UINavigation
    func navigateToInviteVC(){
        
        let controller:InviteFriends_popUp_VC = self.storyboard!.instantiateViewController(withIdentifier: "InviteFriends_popUp_VC") as! InviteFriends_popUp_VC
        controller.handler = { [weak self] data in
            guard let self = self else {return}
            if let value = data as? [String] {
                if (MFMessageComposeViewController.canSendText()) {
                    let messageController = MFMessageComposeViewController()
                    messageController.body = "Hey, download this app, it lets me find you matches 🤲🏼 https://www.nimapp.co"
                    messageController.recipients = value
                    messageController.messageComposeDelegate = self
                    self.present(messageController, animated: true, completion: nil)
                }
            }
        }
        self.present(controller, animated: true, completion: nil)
        
//        let appDelegate = UIApplication.shared.delegate as! AppDelegate
//        let controller:InviteFriends_popUp_VC = self.storyboard!.instantiateViewController(withIdentifier: "InviteFriends_popUp_VC") as! InviteFriends_popUp_VC
//        appDelegate.window?.addSubview((controller.view)!)
//        self.addChildViewController(controller)
//        controller.didMove(toParentViewController: self)
    }
    
    //MARK:- UIButton Actions - Matching For
    @IBAction func didPress_matchingFor(_ sender: UIButton) {
        self.showViewMatchingFor()
    }
    
    @IBAction func didPress_closeVwSuperMatchingFor(_ sender: UIButton) {
        self.hideViewMatchingFor()
    }
    
    //MARK:- UIButton Actions - circle
    
    @IBAction func didPress_closeCircleVw(_ sender: UIButton) {
        self.hideView_circlePopUP()
    }
    
    @IBAction func didPress_sendMsg_circleVw(_ sender: UIButton) {
        //share to your circle
        if let selectedIndex = self.selectedMatchIndex {
            if let selectedCell = self.collectionView.cellForItem(at: IndexPath(item: selectedIndex , section: 0)) as? Match_CollectionViewCell {
                
                if self.arrDailyMatch.indices.contains(selectedIndex) {
                    for (index,circleDialog) in self.selectedCircles.enumerated() {
                        if let dialogID = circleDialog.id , let MatchImage = selectedCell.MatchImage {
                            MatchingForManager.shared.sendMessage_Match(with: MatchImage , caption: self.txtField_msgMatchPopUp.text ?? "" , dialogID: dialogID , dialog: circleDialog, match: self.arrDailyMatch[selectedIndex] , completionHandler: { (success) in
                            })
                            
                            //at last circle
                            if index == self.selectedCircles.count - 1 {
                                self.navigate_ChatVC(dailog: circleDialog)
                            }
                        }
                    }
                }
            }
        }
    }
    
    //MARK:- UIButton Actions - share match
    @objc func didPress_share(sender:UIButton) {
        
        self.showView_MatchPopUp()
        let indentView = UIView(frame: CGRect(x: 0, y: 0, width: 10, height: 20))
        self.txtField_msgMatchPopUp.leftView = indentView
        self.txtField_msgMatchPopUp.leftViewMode = .always
        self.txtField_msgMatchPopUp.text = ""
        
        if self.arrDailyMatch.indices.contains(sender.tag) {
            let match = self.arrDailyMatch[sender.tag]
            
            self.selectedMatchIndex = sender.tag
            self.lbl_userName_matchPopUp.text = (match.name) + self.getUserAge(dob: match.dob)
            self.txtField_msgMatchPopUp.layer.cornerRadius = self.txtField_msgMatchPopUp.frame.size.height/2.0
            self.txtField_msgMatchPopUp.layer.borderColor = UIColor.lightGray.cgColor
            self.txtField_msgMatchPopUp.layer.borderWidth = 1.0
            
            let arrImages = match.userImages
            if arrImages.count > 0 {
                if arrImages.indices.contains(0) {
                    if let url = URL.init(string: constantVC.GeneralConstants.GET_IMAGE + arrImages[0].filename) {
                        
                        ProfileManager.shared.getRefreshImage(url: url , imageView: self.userImage_matchPopUp) { (image) in
                            if let img = image {
                                self.userImage_matchPopUp.image = img
                            }
                        }
                    }
                }
            }
        }
    }
    
    @IBAction func didPress_shareWithGroupChat(_ sender: UIButton) {
        //share to your circle
        if let selectedIndex = self.selectedMatchIndex {
            if let selectedCell = self.collectionView.cellForItem(at: IndexPath(item: selectedIndex , section: 0)) as? Match_CollectionViewCell {
                
                if let circles = MatchingForManager.shared.dialogs_circlePopUP() {
                    if circles.indices.contains(0) {
                        if let myCircleDialog = MatchingForManager.shared.dialogs_circlePopUP()?[0] {
                            
                            print(myCircleDialog.id)
                            print(selectedCell.MatchImage)
                            
                            if let dialogID = myCircleDialog.id , let MatchImage = selectedCell.MatchImage {
                                
                                if self.arrDailyMatch.indices.contains(selectedIndex) {
                                    MatchingForManager.shared.sendMessage_Match(with: MatchImage , caption: self.txtField_msgMatchPopUp.text ?? "" , dialogID: dialogID , dialog: myCircleDialog, match: self.arrDailyMatch[selectedIndex] , completionHandler: { (success) in
                                    })
                                    self.navigate_ChatVC(dailog: myCircleDialog)
                                }
                            }
                        }
                    }
                    else {
                        FTIndicator.showInfo(withMessage: "Please create your circle first!")
                    }
                }
              
            }
        }
    }
    
    func navigate_ChatVC(dailog:QBChatDialog){
        self.hideView_circlePopUP()
        self.hideView_MatchPopUp()
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "ChatViewController") as! ChatViewController
        controller.dialog = dailog
        if let opponentNo_ = DialogManagerNIM.instance.getOpponentNo(dialog: dailog) {
            controller.opponentNo = opponentNo_
        }
        if let topVC = UIApplication.topViewController() {
            topVC.navigationController?.pushViewController(controller , animated: false)
        }
    }
    
    
    @IBAction func didPress_circleDropDown(_ sender: UIButton) {
        self.showView_circlePopUP()
    }
    
    //MARK:- UIButton Actions
    @objc func didPressViewProfile(sender:UIButton) {
        
     /*   if SessionManager.getProfile_lastUpdatedStatus() == UserProfile_lastUpdated.step3 && SessionManager.get_myProfileGender_isMale() {
            let plan_VC = self.storyboard?.instantiateViewController(withIdentifier: "Plan_VC") as! Plan_VC
            self.navigationController?.pushViewController(plan_VC, animated: true)
        } else {
            let profileDetails_VC = self.storyboard?.instantiateViewController(withIdentifier: "ProfileDetails_VC") as! ProfileDetails_VC
            profileDetails_VC.dailyMatch = self.arrDailyMatch[sender.tag]
            self.present(profileDetails_VC , animated: true , completion: nil)
        }*/
        
        let profileDetails_VC = self.storyboard?.instantiateViewController(withIdentifier: "ProfileDetails_VC") as! ProfileDetails_VC
        profileDetails_VC.modalPresentationStyle = .fullScreen
        profileDetails_VC.matchPhoneNo = self.arrDailyMatch[sender.tag].phoneNumber
        self.present(profileDetails_VC , animated: true , completion: nil)
    }
    
    @objc func didPressSendInvitation(sender:UIButton) {
     /*   if SessionManager.getProfile_lastUpdatedStatus() == UserProfile_lastUpdated.step3 && SessionManager.get_myProfileGender_isMale() {
            let plan_VC = self.storyboard?.instantiateViewController(withIdentifier: "Plan_VC") as! Plan_VC
            self.navigationController?.pushViewController(plan_VC, animated: true)
        } else {
            UserConfirmationAlertManager.instance.showAlert(title: userMessagestext.areYouSure , subTitle: userMessagestext.sendInvitationConfirmation , okTitle: "Yes", cancelTitle: "No") { (success) in
                if success {
                    if self.arrDailyMatch.indices.contains(sender.tag) {
                        let user = self.arrDailyMatch[sender.tag]
                        LoadingIndicatorView.show()
                        
                        InvitationManager.instance.sendConnect_invitation(requestTo: user.id, fromName: SessionManager.getUserName()) { (success) in
                            if success {
                                FTIndicator.showSuccess(withMessage: "Request Sent!")
                                
                                //refresh view
                                self.callToGetMatches(userID: SessionManager.get_Elite_userID())
                                
                            } else {
                                FTIndicator.showError(withMessage: "Something went wrong!")
                            }
                            LoadingIndicatorView.hide()
                        }
                    }
                }
            }
        }*/
        
     

        if SessionManager.instance.canHitApi {
            
            if self.arrDailyMatch.indices.contains(sender.tag) {
                     let user = self.arrDailyMatch[sender.tag]
                     LoadingIndicatorView.show()

                     InvitationManager.instance.sendConnect_invitation(requestTo: user.id, fromName: SessionManager.getUserName()) { (success) in
                         if success {
                             FTIndicator.showSuccess(withMessage: "Request Sent!")

                            if let index = self.arrDailyMatch.firstIndex(where: {$0.id == user.id}) {
                                self.arrDailyMatch.remove(at: index)
                                self.reloadCollectionData()
                            }
                             //refresh view
                            // self.callToGetMatches(userID: SessionManager.get_Elite_userID())

                         } else {
                             FTIndicator.showError(withMessage: "Something went wrong!")
                         }
                         LoadingIndicatorView.hide()
                     }
                }
           
        } else {
            guard let interval = self.intervalSeconds else {return}
            let timeValue = timeString(time: TimeInterval(interval))
            FTIndicator.showSuccess(withMessage: "Your daily quota is finished, please try after \(timeValue) ")

        }

//        UserConfirmationAlertManager.instance.showAlert(title: userMessagestext.areYouSure , subTitle: userMessagestext.sendInvitationConfirmation , okTitle: "Yes", cancelTitle: "No") { (success) in
//            if success {
//            }
//        }
        
      
       /* cheersItsMatchView.isHidden = false
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.font: UIFont(name: "CircularStd-Medium", size: 18)!]
        self.navigationController?.navigationBar.barTintColor  = UIColor.white
        let nav = self.navigationController?.navigationBar
        // nav?.barStyle = UIBarStyle.black
        nav?.tintColor = UIColor.black
        nav?.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.black]
        self.navigationItem.rightBarButtonItem?.tintColor = UIColor.gray
        
        UIView.animate(withDuration: 0.6, animations: {
            sender.alpha = 0.0
        }) { (completed) in
            if completed
            {
                UIView.animate(withDuration: 0.6, animations: {
                    sender.alpha = 1.0
                })
            }
        }*/
    }
  
    
    @IBAction func didPressSignUpRegularUser(_ sender: UIButton) {
        
        UserConfirmationAlertManager.instance.showAlert(title: "NIM" , subTitle: userMessagestext.signUpAsMemberConfirmation , okTitle: "Ok", cancelTitle: "Cancel") { (success) in
            if success {
                SessionManager.save_Elite_userID(id: "")
                SessionManager.save_phonenumber(phoneNumber: "")
                SessionManager.saveChaperonStatus(isActive: false)
                
                let Welcome = self.storyboard?.instantiateViewController(withIdentifier: "nav_Welcome_VC")
                if let topVC = UIApplication.topViewController() {
                    topVC.present(Welcome! , animated: true, completion: nil)
                }
            }
        }
    }
  
    @IBAction func didPress_joinCircle_chaperon(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "InviteFriends_VC") as! InviteFriends_VC
        if let topVC = UIApplication.topViewController() {
            topVC.present(controller , animated: true , completion: nil)
        }
    }
   
    //Match Popup
    @IBAction func didPress_closeMatchPopUp(_ sender: UIButton) {
        self.hideView_MatchPopUp()
    }
   
    //MARK:- Web services
    func callToGetMatches(userID:String){
        
        self.showGetMatchLoader()
        
        WebserviceSigleton.shared.GETService(urlString: constantVC.WebserviceName.URL_DAILY_MATCHES + userID) { (response , error) in
            
            if let dicResponse = response as NSDictionary? {
                SessionManager.instance.dailyApiCount = SessionManager.instance.dailyApiCount ?? 0 + 1
                if let msg = dicResponse["msg"] as? String {
                    
                    if msg == responseStatus.success.rawValue {
                        
//                        if let last_Matched_time = dicResponse["last_Matched_time"] as? String {
//                               let minutesLeft = MatchTimer.shared.getTimeLeft_minutes(lastUpdatedTime: last_Matched_time)
//                             self.configureMatchTimer(minsLeft: minutesLeft)
//                            
//                        }
                        
                        if let arrData = dicResponse["msg_details"] as? NSArray {
                            
                            if let arr = DataParserManager.shared.getDailyMatch(arrData: arrData) {
                                self.arrDailyMatch = arr
                                self.reloadCollectionData()
                            }
                        }
                    }
                    else {
                        self.arrDailyMatch.removeAll()
                        self.reloadCollectionData()
                    }
                }
            } else {
                self.reloadCollectionData()
                FTIndicator.showError(withMessage: "Something went wrong!")
            }
        }
    }
    
    func reloadCollectionData(){
        DispatchQueue.main.async {
            self.hideGetMatchLoader()
            if self.arrDailyMatch.count < 1 {
               self.lblNoData.isHidden = false
               self.collectionView.reloadData()
            } else {
               self.top_view.isHidden = false
               self.top_view.backgroundColor = UIColor.black
               self.constraint_topViewHeight.constant = 140
               self.lblNoData.isHidden = true
               self.collectionView.isHidden = false
               self.collectionView.reloadData()
           }
        }
    }
    
    func showGetMatchLoader(){
        self.arrDailyMatch.removeAll()
        self.collectionView.reloadData()
        self.activityIndicator.isHidden = false
        self.activityIndicator.startAnimating()
    }
    
    func hideGetMatchLoader(){
        self.activityIndicator.stopAnimating()
        self.activityIndicator.isHidden = true
    }
    
    func updateViewAfterSelectionMatchingFor(name:String, phNo: String){
        self.hideViewMatchingFor()
        self.vwChaperon.isHidden = true
        self.lblMatchingFor_subTitle.text = name
        
        ProfileManager.shared.getRefreshImage(url: ProfileManager.shared.getOpponentProfileUrl(opponentNo: phNo) , imageView: self.imgVwUser_matchingFor) { (image) in
            if let img = image {
                self.imgVwUser_matchingFor.image = img
            }
        }
        
    }
    
    func showMatchingFor_Selection(imageView:UIImageView) {
        imageView.image = UIImage(named: "circlePurple")
    }
    
    func showMatchingFor_UnSelection(imageView:UIImageView) {
        imageView.image = UIImage(named: "circleGray")
    }
    
}

extension Matches_VC : UICollectionViewDelegate {
    
}

extension Matches_VC : UICollectionViewDataSource , UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: self.view.frame.size.width-90, height: self.collectionView.frame.size.height)
    }
    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1     //return number of sections in collection view
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.arrDailyMatch.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Match_CollectionViewCell", for: indexPath) as! Match_CollectionViewCell
        
        if self.arrDailyMatch.indices.contains(indexPath.row) {
            cell.dailyMatch = self.arrDailyMatch[indexPath.row]
        }
       
        cell.btnViewProfile.addTarget(self, action: #selector(didPressViewProfile), for: .touchUpInside)
        cell.btnViewProfile.tag = indexPath.row
        cell.btnSendInvitation.addTarget(self, action: #selector(didPressSendInvitation(sender:)), for: .touchUpInside)
        cell.btnSendInvitation.tag = indexPath.row
        cell.btnShare.addTarget(self, action: #selector(didPress_share(sender:)), for: .touchUpInside)
        cell.btnShare.tag = indexPath.row
        return cell     
    }
    
    
    func configureCell(cell:
        UICollectionViewCell, forItemAtIndexPath: NSIndexPath) {
        //  cell.backgroundColor = UIColor.black
        
        //Customise your cell
    }
}



extension Matches_VC: UITableViewDelegate , UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if tableView == self.tablevw_circle {
            return MatchingForManager.shared.getNumberOfRows_circlePopUp()
        }
        return MatchingForManager.shared.getNumberOfRows()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell" , for: indexPath) as! MatchingFor_tableCell
        
        if tableView == self.tablevw_circle {
            
            guard let chatDialog = MatchingForManager.shared.dialogs_circlePopUP()?[indexPath.row] else {
                return cell
            }
            
            if let name = chatDialog.name {
                let arrName = name.components(separatedBy: "-")
                if arrName.indices.contains(0){
                    if DialogManagerNIM.instance.getMyCircleName().contains(arrName[0]) {
                        cell.lblTitle.text = "Your Circle"
                    } else {
                        cell.lblTitle.text = arrName[0]
                    }
                }
                
                if arrName.indices.contains(1){
                    ProfileManager.shared.getRefreshImage(url: ProfileManager.shared.getOpponentProfileUrl(opponentNo: arrName[1]) , imageView: cell.userImage) { (image) in
                        if let img = image {
                            cell.userImage.image = img
                        }
                    }
                }
            }
            
            if self.selectedCircles.contains(chatDialog) {
                cell.imgSelection.image = UIImage(named: "circlePurple")
            } else {
                cell.imgSelection.image = UIImage(named: "circleGray")
            }
        }
        else {
            if SessionManager.isChaperonUser() {
                if indexPath.row == MatchingForManager.shared.getNumberOfRows() - 1 {
                    cell.vwContent.isHidden = true
                    cell.vwInvite.isHidden = false
                    
                    //TODO image
                    cell.lblTitle_invite.text = "Match your Friends"
                    cell.set_unselectLable()
                }
                else {
                    cell.vwContent.isHidden = false
                    cell.vwInvite.isHidden = true
                    
                    guard let chatDialog = MatchingForManager.shared.dialogs()?[indexPath.row] else {
                        return cell
                    }
                    
                    if let name = chatDialog.name {
                        let arrName = name.components(separatedBy: "-")
                        if arrName.indices.contains(0){
                            cell.opponentName = arrName[0].replacingOccurrences(of: "Circle", with: "")
                            cell.lblTitle.text = arrName[0].replacingOccurrences(of: "Circle", with: "")
                        }
                        
                        if arrName.indices.contains(1){
                            cell.opponentNo = arrName[1]
                            
                            ProfileManager.shared.getRefreshImage(url: ProfileManager.shared.getOpponentProfileUrl(opponentNo: arrName[1]) , imageView: cell.userImage) { (image) in
                                if let img = image {
                                    cell.userImage.image = img
                                }
                            }
                        }
                    }
                    
                    if self.selectedIndex == nil {
                        self.showMatchingFor_UnSelection(imageView: cell.imgSelection)
                    }
                    else if self.selectedIndex == indexPath.row {
                        self.showMatchingFor_Selection(imageView: cell.imgSelection)
                    }
                    else {
                        self.showMatchingFor_UnSelection(imageView: cell.imgSelection)
                    }
                }
            }
            else {
                if indexPath.row == 0 {
                    cell.vwContent.isHidden = false
                    cell.vwInvite.isHidden = true
                    
                    cell.lblTitle.text = "Yourself"
                    cell.setBorderView()
                    cell.vwContent.backgroundColor = constantVC.GlobalColor.lightPurpleMyCircle
                    
                    ProfileManager.shared.getRefreshImage(url: ProfileManager.shared.getMyProfileUrl() , imageView: cell.userImage) { (image) in
                        if let img = image {
                            cell.userImage.image = img
                        }
                    }
                    
                    if self.selectedIndex == nil || self.selectedIndex == 0 {
                        self.showMatchingFor_Selection(imageView: cell.imgSelection)
                    }
                    else {
                        self.showMatchingFor_UnSelection(imageView: cell.imgSelection)
                    }
                    
                }
                else if indexPath.row == MatchingForManager.shared.getNumberOfRows() - 1 {
                    cell.vwContent.isHidden = true
                    cell.vwInvite.isHidden = false
                    
                    //TODO image
                    ProfileManager.shared.getRefreshImage(url: ProfileManager.shared.getMyProfileUrl() , imageView: cell.userImage) { (image) in
                        if let img = image {
                            cell.userImage.image = img
                        }
                    }
                   
                    cell.lblTitle_invite.text = "Match your Friends."
                    cell.set_unselectLable()
                }
                else {
                    cell.vwContent.isHidden = false
                    cell.vwInvite.isHidden = true
                    
                    guard let chatDialog = MatchingForManager.shared.dialogs()?[indexPath.row - 1] else {
                        return cell
                    }
                    
                    if let name = chatDialog.name {
                        let arrName = name.components(separatedBy: "-")
                        if arrName.indices.contains(0){
                            cell.opponentName = arrName[0].replacingOccurrences(of: "Circle", with: "")
                            cell.lblTitle.text = arrName[0].replacingOccurrences(of: "Circle", with: "")
                        }
                        
                        if arrName.indices.contains(1){
                            cell.opponentNo = arrName[1]
                            ProfileManager.shared.getRefreshImage(url: ProfileManager.shared.getOpponentProfileUrl(opponentNo: arrName[1]) , imageView: cell.userImage) { (image) in
                                if let img = image {
                                    cell.userImage.image = img
                                }
                            }
                        }
                    }
                    
                    if self.selectedIndex == nil {
                        self.showMatchingFor_UnSelection(imageView: cell.imgSelection)
                    }
                    else if self.selectedIndex == indexPath.row {
                        self.showMatchingFor_Selection(imageView: cell.imgSelection)
                    }
                    else {
                        self.showMatchingFor_UnSelection(imageView: cell.imgSelection)
                    }
                }
            }
        }
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if tableView == self.tablevw_circle {
            if let selectedCell = tableView.cellForRow(at: indexPath) as? MatchingFor_tableCell {
                guard let chatDialog = MatchingForManager.shared.dialogs_circlePopUP()?[indexPath.row] else {
                    return
                }
                
                if self.selectedCircles.contains(chatDialog) {
                    
                    if let index = self.selectedCircles.index(of: chatDialog) {
                        self.selectedCircles.remove(at: index)
                        selectedCell.imgSelection.image = UIImage(named: "circleGray")
                    }
                } else {
                    self.selectedCircles.append(chatDialog)
                    selectedCell.imgSelection.image = UIImage(named: "circlePurple")
                }
            }
        }
        else {
            self.selectedIndex = indexPath.row
            if SessionManager.isChaperonUser() {
                
                if indexPath.row == MatchingForManager.shared.getNumberOfRows() - 1 {
                    self.navigateToInviteVC()
                }
                else {
                    if let selectedCell = tableView.cellForRow(at: indexPath) as? MatchingFor_tableCell {
                        selectedCell.imgSelection.image = UIImage(named: "circlePurple")
                        
                        if let opponentNO = selectedCell.opponentNo {
                            ServicesManager.instance().usersService.getUsersWithLogins([opponentNO]).continueOnSuccessWith { (task) -> Any? in
                                if task.isCompleted {
                                    if let users = task.result as? [QBUUser]{
                                        if users.indices.contains(0) {
                                            
                                            self.updateViewAfterSelectionMatchingFor(name: selectedCell.opponentName ?? "", phNo: opponentNO)
                                            self.callToGetMatches(userID: "\(users[0].externalUserID)")
                                            
                                        }
                                    }
                                }
                                return nil
                            }
                        }
                        
                    }
                }
            }
            else {
                
                //case1
                if indexPath.row == 0 {
                    self.updateViewAfterSelectionMatchingFor(name: "Yourself", phNo: SessionManager.get_phonenumber())
                    self.callToGetMatches(userID: SessionManager.get_Elite_userID())
                }
                    //case 2
                else if indexPath.row == MatchingForManager.shared.getNumberOfRows() - 1 {
                    self.navigateToInviteVC()
                }
                    //case 3
                else {
                    if let selectedCell = tableView.cellForRow(at: indexPath) as? MatchingFor_tableCell {
                        selectedCell.imgSelection.image = UIImage(named: "circlePurple")
                        
                        if let opponentNO = selectedCell.opponentNo {
                            
                            ServicesManager.instance().usersService.getUsersWithLogins([opponentNO]).continueOnSuccessWith { (task) -> Any? in
                                if task.isCompleted {
                                    if let users = task.result as? [QBUUser]{
                                        if users.indices.contains(0) {
                                            
                                            self.updateViewAfterSelectionMatchingFor(name: selectedCell.opponentName ?? "", phNo: opponentNO)
                                            self.callToGetMatches(userID: "\(users[0].externalUserID)")
                                            
                                        }
                                    }
                                }
                                return nil
                            }
                        }
                        
                    }
                }
            }
        }
    }
    
}


//MARK:- Handle subviews

extension Matches_VC {
    
    func add_subView(subView: UIView){
        subView.frame = CGRect(x: 0 , y: 0 , width: self.view.frame.width , height: self.view.frame.height)
        self.view.addSubview(subView)
    }
    
    func remove_subView(subView: UIView){
        subView.removeFromSuperview()
    }
    
}


extension Matches_VC: PKPushRegistryDelegate {
    
    // MARK: - PKPushRegistryDelegate
    func pushRegistry(_ registry: PKPushRegistry, didUpdate pushCredentials: PKPushCredentials, for type: PKPushType) {
        //  New way, only for updated backend
        guard let deviceIdentifier = UIDevice.current.identifierForVendor?.uuidString else {
            return
        }
        let subscription: QBMSubscription! = QBMSubscription()
        subscription.notificationChannel = QBMNotificationChannel.APNSVOIP
        subscription.deviceUDID = deviceIdentifier
        subscription.deviceToken = voipRegistry.pushToken(for: .voIP)
        subscription.devicePlatform = "iOS"
        
        QBRequest.createSubscription(subscription, successBlock: { (response: QBResponse!, objects: [QBMSubscription]?) -> Void in
            //
            print(response)
        }) { (response: QBResponse!) -> Void in
            //
            print(response.error)
        }
    }
    
    func pushRegistry(_ registry: PKPushRegistry, didInvalidatePushTokenFor type: PKPushType) {
        guard let deviceIdentifier = UIDevice.current.identifierForVendor?.uuidString else {
            return
        }
        QBRequest.unregisterSubscription(forUniqueDeviceIdentifier: deviceIdentifier, successBlock: { response in
        }, errorBlock: { error in
        })
    }
    
    func pushRegistry(_ registry: PKPushRegistry, didReceiveIncomingPushWith payload: PKPushPayload, for type: PKPushType) {
        
        print(payload.dictionaryPayload)
        
        self.play()
       
        //to clear cache
        if let imgUrl = payload.dictionaryPayload["imgUrl"] as? String {
            ProfileManager.shared.clearMyProfileCache_onrequest(strUrl: imgUrl)
        }
        
    }
}

extension Matches_VC : MFMessageComposeViewControllerDelegate{
    
    func messageComposeViewController(_ controller: MFMessageComposeViewController, didFinishWith result: MessageComposeResult) {
          switch (result) {
            case .cancelled:
                print("Message was cancelled")
                controller.dismiss(animated: true, completion: nil)
            case .failed:
                print("Message failed")
            case .sent:
                print("Message was sent")
                FTIndicator.showSuccess(withMessage: "Invitation sent successfully!")
                controller.dismiss(animated: true, completion: nil)
            default:
                controller.dismiss(animated: true, completion: nil)
                break
            }
        
    }
    
}
