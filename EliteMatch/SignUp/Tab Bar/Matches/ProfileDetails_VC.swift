//
//  ProfileDetails_VC.swift
//  EliteMatch
//
//  Created by Kitlabs-M-0002 on 2/11/19.
//  Copyright © 2019 Kitlabs-M-0002. All rights reserved.
//

import UIKit
import Serrata

class ProfileDetails_VC: UIViewController  {
    
    //MARK:- Outlets
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var collectionVw_userImage: UICollectionView!
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var lblLocation: UILabel!
    @IBOutlet weak var lblBirthday: UILabel!
    @IBOutlet weak var lblZordiac: UILabel!
    @IBOutlet weak var lblHeight: UILabel!
    @IBOutlet weak var lblReligion: UILabel!
    @IBOutlet weak var lblEthnicity: UILabel!
    @IBOutlet weak var lblFood: UILabel!
    @IBOutlet weak var lblMusic: UILabel!
    @IBOutlet weak var lblDessert: UILabel!
    @IBOutlet weak var lblShow: UILabel!
    @IBOutlet weak var lblSmoke: UILabel!
    @IBOutlet weak var lblDrink: UILabel!
    @IBOutlet weak var lblWorkout: UILabel!
    @IBOutlet weak var lblLifeStyle: UILabel!
    @IBOutlet weak var pageController: UIPageControl!
    @IBOutlet weak var famlyOriginCollectionView: UICollectionView!
    @IBOutlet weak var vwContent_info: UIView!
    @IBOutlet weak var constraint_collectionVW_top: NSLayoutConstraint!
    
    //MARK:- Varaibles
    var countryCode = [String]()
    var userProfile:UserProfile?
    var matchPhoneNo:String = ""
    var fromAppdelegate = false
    var block : CompletionHandler?
    var arrSlideLeaf = [SlideLeaf]()
    
    override var prefersStatusBarHidden : Bool {
        return true
    }
    
    //MARK:- UIView life-cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //init
        self.configureSafeAreaLayout()
        self.setVwContentCorner()
        self.collectionVw_userImage.dataSource = self
        self.collectionVw_userImage.delegate = self
       
        let nib = UINib(nibName: "FamilyOrigin_CC", bundle: nil)
        famlyOriginCollectionView?.register(nib, forCellWithReuseIdentifier: "FamilyOrigin_CC")
        famlyOriginCollectionView.delegate = self
        famlyOriginCollectionView.dataSource = self
        famlyOriginCollectionView.backgroundColor = UIColor.clear
        lblLocation.textColor = UIColor(red: 0/255, green: 0/255, blue: 0/255, alpha: 0.6)
        
        ProfileManager.shared.getUserProfile_phoneNo(phoneNo: self.matchPhoneNo) { (success , userProfile) in
            self.userProfile = userProfile
            self.showProfileDetails()
        }
    }
    
    //MARK:- Custom Methods
    func setVwContentCorner(){
        self.vwContent_info.layer.cornerRadius = 20.0
        self.vwContent_info.clipsToBounds = true
    }
    
    func configureSafeAreaLayout(){
        if UIDevice().type == .iPhoneX || UIDevice().type == .iPhoneXS || UIDevice().type == .iPhoneXSMax || UIDevice().type == .iPhoneXR || UIDevice().type == .iPhone11 || UIDevice().type == .iPhone11Pro || UIDevice().type == .iPhone11ProMax {
            self.constraint_collectionVW_top.constant = 0
        }
        else {
            self.constraint_collectionVW_top.constant = 60
        }
    }
    
    //MARK:- Helpers
    func getUserAge(dateStr:String) ->String {
        if let date = CommonFunctions.shared.getDateFromString(dateFormat: "mm-dd-yyyy" , dateStr: dateStr) {
            if let age_ = CommonFunctions.shared.calculateAge(dob: date) {
                return ", \(age_)"
            }
        }
        return ""
    }
    
    func getUserDOB(dateStr:String) ->String {
        if let date = CommonFunctions.shared.getDateFromString(dateFormat: "mm-dd-yyyy" , dateStr: dateStr) {
            let dateFormatterPrint = DateFormatter()
            dateFormatterPrint.dateFormat = "MMM d, yyyy"
            return dateFormatterPrint.string(from: date)
        }
        return ""
    }
    
    func getUserHeight(strHeight:String) ->String{
        let heightInces = strHeight
        if var heightCm = Double(heightInces) {
            heightCm = heightCm * 2.54 * 12
            let height_ = String(format: "%.2f", heightCm)
            let str = "\(heightInces) (\(height_)cm)"
            let aString = str
            let newString = aString.replacingOccurrences(of: ".", with: "'", options: .literal, range: nil)
            return newString
        }
        return strHeight
    }
    
    func showProfileDetails(){
        
        if let arrImages = self.userProfile?.images {
            for userImageObj in arrImages {
                if let filename = userImageObj.filename {
                    let leaf : SlideLeaf = SlideLeaf(imageUrlString: constantVC.GeneralConstants.GET_IMAGE + filename)
                    self.arrSlideLeaf.append(leaf)
                }
            }
        }
        
        self.collectionVw_userImage.reloadData()
        pageController.numberOfPages = self.userProfile?.images?.count ?? 0
        
        if let name = self.userProfile?.name , let dob = self.userProfile?.dob {
            self.lblUserName.text = name + self.getUserAge(dateStr: dob)
        }
        
        if let livingPlace = self.userProfile?.living_place {
            var cName = ""
            if let carrer = self.userProfile?.inCareer,carrer == "true" {
                if let careerName = self.userProfile?.careerName {
                   cName = careerName
                }
            }
            self.lblLocation.text = cName + "\n" + livingPlace
        }
        if let dob = self.userProfile?.dob {
            self.lblBirthday.text = self.getUserDOB(dateStr: dob)
        }
        if let height = self.userProfile?.height {
            self.lblHeight.text = self.getUserHeight(strHeight: height)
        }
        if let religion = self.userProfile?.religion {
            self.lblReligion.text = religion
        }
        if let countryCode = self.userProfile?.countyCode {
            let countryCodeArr = countryCode.components(separatedBy: ",")
            self.countryCode = countryCodeArr
            self.famlyOriginCollectionView.reloadData()
        }
        
        if let enthicity = self.userProfile?.ethnicity {
            self.lblEthnicity.text = enthicity
        }
        if let food = self.userProfile?.fav_food {
            self.lblFood.text = food
        }
        if let fav_music = self.userProfile?.fav_music {
            self.lblMusic.text = fav_music
        }
        if let fav_dessert = self.userProfile?.fav_dessert {
            self.lblDessert.text = fav_dessert
        }
        if let fav_shows = self.userProfile?.fav_shows {
            self.lblShow.text = fav_shows
        }
        if let lifestyle = self.userProfile?.lifestyle {
            self.lblLifeStyle.text = lifestyle
        }
        if let smoke = self.userProfile?.isSmoke {
            self.lblSmoke.text = self.getBoolFromString(str: smoke) == true ? "Yes" : "No"
        }
        if let drink = self.userProfile?.isDrink {
            self.lblDrink.text = self.getBoolFromString(str: drink) == true ? "Yes" : "No"
        }
        if let workout = self.userProfile?.isWorkOutRoutinely {
            self.lblWorkout.text = self.getBoolFromString(str: workout) == true ? "Yes" : "No"
        }
        
        if let dob = self.userProfile?.dob {
            if let date = CommonFunctions.shared.getDateFromString(dateFormat: "mm-dd-yyyy" , dateStr: dob) {
                
                let formatterDate = DateFormatter()
                formatterDate.dateFormat = "dd"
                let formatterMonth = DateFormatter()
                formatterMonth.dateFormat = "MM"
                
                let date_: Int = Int(formatterDate.string(from: date))!
                let month_: Int = Int(formatterMonth.string(from: date))!
                
                if month_ == 1 && date_ <= 19||(month_ == 12 && date_ >= 22){
                    self.lblZordiac.text = "Capricorn"
                }
                else if (month_ == 2 && date_ <= 18)||(month_ == 1 && date_ >= 20){
                    self.lblZordiac.text = "Aquarius"
                }
                else if (month_ == 3 && date_ <= 20)||(month_ == 2 && date_ >= 19){
                    self.lblZordiac.text = "Pisces"
                }
                else if (month_ == 4 && date_ <= 19)||(month_ == 3 && date_ >= 21){
                    self.lblZordiac.text = "Aries"
                }
                else if (month_ == 5 && date_ <= 20)||(month_ == 4 && date_ >= 20){
                    self.lblZordiac.text = "Taurus"
                }
                else if (month_ == 6 && date_ <= 20)||(month_ == 5 && date_ >= 21){
                    self.lblZordiac.text = "Gemini"
                }
                else if (month_ == 7 && date_ <= 22)||(month_ == 6 && date_ >= 21){
                    self.lblZordiac.text = "Cancer"
                }
                else if (month_ == 8 && date_ <= 22)||(month_ == 7 && date_ >= 23){
                    self.lblZordiac.text = "Leo"
                }
                else if (month_ == 9 && date_ <= 22)||(month_ == 8 && date_ >= 23){
                    self.lblZordiac.text = "Virgo"
                }
                else if (month_ == 10 && date_ <= 22)||(month_ == 9 && date_ >= 23){
                    self.lblZordiac.text = "Libra"
                }
                else if (month_ == 11 && date_ <= 21)||(month_ == 10 && date_ >= 23){
                    self.lblZordiac.text = "Scorpio"
                }
                else if (month_ == 12 && date_ <= 21)||(month_ == 11 && date_ >= 22){
                    self.lblZordiac.text = "Sagittarius"
                }
            }
        }
        
    }
    
    func getBoolFromString(str:String) ->Bool{
        if str == "0" {
            return false
        }
        return true
    }
    
    
    //MARK:- UIButton Actions
    @objc func didPressBack(sender:UIButton) {
        
        if fromAppdelegate{
            guard let block = self.block else {return}
            self.view.window?.rootViewController?.dismiss(animated: true, completion: nil)
            block(true)
        } else {
            self.dismiss(animated: true , completion: nil)
        }
    }
    
}

extension ProfileDetails_VC: UICollectionViewDelegate , UICollectionViewDataSource , UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if(collectionView == collectionVw_userImage) {
            return CGSize(width: self.view.frame.size.width , height: collectionView.frame.size.height)
        }else{
            return CGSize(width:32 , height: 32)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if(collectionView == collectionVw_userImage) {
            if let arrImages = self.userProfile?.images {
                return arrImages.count
            }
            return 1
        }else{
            return countryCode.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if(collectionView == collectionVw_userImage) {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ProfileDetail_CollectionCell", for: indexPath as IndexPath) as! ProfileDetail_CollectionCell
            
            if let arrImages = self.userProfile?.images {
                if arrImages.count > 0 {
                    cell.ImgVwUser.isHidden = false
                    cell.imgVwAvatar.isHidden = true
                    cell.userImage = arrImages[indexPath.row]
                } else {
                    cell.ImgVwUser.isHidden = true
                    cell.imgVwAvatar.isHidden = false
                    cell.btnBack.setImage(UIImage(named: "arrowleft_black"), for: .normal)
                }
            }
            
        //add action
        cell.btnBack.addTarget(self, action: #selector(self.didPressBack(sender:)), for: .touchUpInside)
        return cell
        } else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "FamilyOrigin_CC", for: indexPath as IndexPath) as! FamilyOrigin_CC
            
            let bundle = "assets.bundle/"
                print(bundle + countryCode[indexPath.item].lowercased() + ".png")
            cell.flagImgView.image = UIImage(named: bundle + countryCode[indexPath.item].lowercased().trimmingCharacters(in: .whitespaces) + ".png", in: nil, compatibleWith: nil)
           return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if(collectionView == collectionVw_userImage) {

            guard let selectedCell = collectionView.cellForItem(at: indexPath) as? ProfileDetail_CollectionCell else {
                return
            }
            let slideView = SlideLeafViewController.make(leafs:  self.arrSlideLeaf, startIndex: indexPath.item, fromImageView: selectedCell.ImgVwUser)
            slideView.delegate = self
            slideView.modalPresentationStyle = .fullScreen
            present(slideView, animated: true, completion: nil)
        }
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        var visibleRect = CGRect()
        
        visibleRect.origin = collectionVw_userImage.contentOffset
        visibleRect.size = collectionVw_userImage.bounds.size
        
        let visiblePoint = CGPoint(x: visibleRect.midX, y: visibleRect.midY)
        
        guard let indexPath = collectionVw_userImage.indexPathForItem(at: visiblePoint) else { return }
        print(indexPath)
        pageController.currentPage = indexPath.row
    }
    
}

class ProfileDetail_CollectionCell: UICollectionViewCell {
    
    @IBOutlet weak var ImgVwUser: UIImageView!
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var imgVwAvatar: UIImageView!
    
    
    var userImage: UserImage? {
        didSet {
            self.imgVwAvatar.isHidden = false
            self.ImgVwUser.isHidden = true
            self.setUserImageRoundedCorner()
            self.updateView()
        }
    }
    
    func setUserImageRoundedCorner(){
        self.ImgVwUser.layer.cornerRadius = 20.0
        self.ImgVwUser.clipsToBounds = true
    }
    
    func updateView(){
        if let filename = userImage?.filename {
            if let url = URL.init(string: constantVC.GeneralConstants.GET_IMAGE + filename) {
                ProfileManager.shared.getRefreshImage(url: url , imageView: self.ImgVwUser) { (image) in
                    if let img = image {
                        self.imgVwAvatar.isHidden = true
                        self.ImgVwUser.isHidden = false
                        self.ImgVwUser.image = img
                    }
                }
            }
        }
    }
}

extension ProfileDetails_VC: SlideLeafViewControllerDelegate {

    func tapImageDetailView(slideLeaf: SlideLeaf, pageIndex: Int) {
        // code...
    }

    func longPressImageView(slideLeafViewController: SlideLeafViewController, slideLeaf: SlideLeaf, pageIndex: Int) {
        // code...
    }

    func slideLeafViewControllerDismissed(slideLeaf: SlideLeaf, pageIndex: Int) {
        // code...
        let indexPath = IndexPath(row: pageIndex, section: 0)
        collectionVw_userImage.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
        
    }
}
