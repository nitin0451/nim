//
//  Match_CollectionViewCell.swift
//  EliteMatch
//
//  Created by Kitlabs-M-0002 on 2/11/19.
//  Copyright © 2019 Kitlabs-M-0002. All rights reserved.
//

import Foundation
import UIKit
import YYWebImage
import YYImage

class Match_CollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var profileImgView: UIImageView!
    @IBOutlet weak var rightArrow: UIImageView!
    @IBOutlet weak var viewProfile: UILabel!
    @IBOutlet weak var ProfileDescription: UILabel!
    @IBOutlet weak var profileName: UILabel!
    @IBOutlet weak var btnSendInvitation: UIButton!
    @IBOutlet weak var btnViewProfile: UIButton!
    @IBOutlet weak var superVw: UIView!
    
    @IBOutlet weak var imgVwAvatar: UIImageView!
    @IBOutlet weak var userDetailsView: UIView!
    @IBOutlet weak var detailView: UIView!
    @IBOutlet weak var btnShare: UIButton!
    
    var MatchImage:UIImage?
    
    var dailyMatch: DailyMatch? {
        didSet {
            self.profileImgView.image = nil
            self.updateView()
        }
    }
    
    func updateView() {
        self.profileName.text = (dailyMatch?.name)! + self.getUserAge()
        self.ProfileDescription!.text = self.getUserLivingPlace() + self.getCareerName()
        self.btnSendInvitation.setTitle(self.getInvitationStatus() , for: .normal)
        if let arrImages = self.dailyMatch?.userImages {
            if arrImages.count > 0 {
                self.profileImgView.isHidden = false
                self.imgVwAvatar.isHidden = true
                if arrImages.indices.contains(0) {
                    if let url = URL.init(string: constantVC.GeneralConstants.GET_IMAGE + arrImages[0].filename) {
                        ProfileManager.shared.getRefreshImage(url: url , imageView: self.profileImgView) { (image) in
                            if let img = image {
                                self.MatchImage = img
                                self.profileImgView.image = img
                            }
                        }
                    }
                }
            }else {
                self.imgVwAvatar.isHidden = false
                self.profileImgView.isHidden = true
            }
        }
    }
    
    func getUserAge() ->String {
        if let date = CommonFunctions.shared.getDateFromString(dateFormat: "mm-dd-yyyy" , dateStr: (self.dailyMatch?.dob)!) {
            if let age_ = CommonFunctions.shared.calculateAge(dob: date) {
                return ", \(age_)"
            }
        }
        return ""
    }
    
    func getUserLivingPlace() ->String{
        if let place = self.dailyMatch?.living_place {
            return place
        }
        return ""
    }
    
    func getCareerName() ->String{
        if let career = self.dailyMatch?.careerName , self.dailyMatch?.careerName != "" {
            return ", " + career
        }
        return ""
    }
    
    func getInvitationStatus() ->String{
        self.btnSendInvitation.isUserInteractionEnabled = false
        if let arrInvitations = self.dailyMatch?.invitations {
            for invites in arrInvitations {
                
                if invites.from_id == SessionManager.get_Elite_userID() {
                    if invites.status == invitationStatus.active.rawValue {
                        return invitationButtonText.startConversation.rawValue
                    }
                    else if invites.status == invitationStatus.pending.rawValue {
                        return invitationButtonText.pending.rawValue
                    }
                    else if invites.status == invitationStatus.rejected.rawValue {
                        return invitationButtonText.rejected.rawValue
                    }
                    else {
                        self.btnSendInvitation.isUserInteractionEnabled = true
                        return invitationButtonText.sendInvitation.rawValue
                    }
                }
            }
        }
        self.btnSendInvitation.isUserInteractionEnabled = true
        return invitationButtonText.sendInvitation.rawValue
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        superVw.isUserInteractionEnabled = true
        self.isUserInteractionEnabled = true
        profileName.textColor = UIColor(red: 0/255, green: 0/255, blue: 0/255, alpha: 1.0)
        ProfileDescription.textColor = UIColor(red: 52/255, green: 52/255, blue: 52/255, alpha: 1.0)
        btnSendInvitation.backgroundColor = UIColor(red: 26/255, green: 26/255, blue: 26/255, alpha: 1.0)
        profileName.textColor = UIColor(red: 28/255, green: 27/255, blue: 27/255, alpha: 1.0)
        viewProfile.textColor = UIColor(red: 28/255, green: 27/255, blue: 27/255, alpha: 1.0)
    }
    
    
    
}
