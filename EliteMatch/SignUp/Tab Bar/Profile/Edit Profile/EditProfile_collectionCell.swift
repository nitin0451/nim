//
//  EditProfile_collectionCell.swift
//  EliteMatch
//
//  Created by Apple on 24/09/19.
//  Copyright © 2019 Kitlabs-M-0002. All rights reserved.
//

import UIKit

class EditProfile_collectionCell: UICollectionViewCell {
    
    
    @IBOutlet weak var userImage: UIImageView!
    @IBOutlet weak var btnCross: UIButton!
    @IBOutlet weak var btnNoImage: UIButton!
    @IBOutlet weak var imgLoader: UIActivityIndicatorView!
    
    
    var isImageUpdated:Bool = false
    var isAnimate: Bool! = true
    
    
    var userImage2: UserImage? {
        didSet {
            self.stopLoader()
            self.userImage.image = nil
            self.updateView2()
        }
    }
    
    
    func updateView2(){
        if let filename = userImage2?.filename {
            
            if filename.count < 1 {
                
                let targetSize: CGSize? = userImage.bounds.size
                let transform = QMImageTransform(size: targetSize! , isCircle: false)
                
                if let img_ = userImage2?.image {
                    transform.apply(for: (img_)) { (img) in
                        self.userImage.image = img
                    }
                }
            }
            else {
                if let url = URL.init(string: constantVC.GeneralConstants.GET_IMAGE + filename) {
                    self.startLoader()
                    self.btnNoImage.isHidden = true
                    
                    ProfileManager.shared.getRefreshImage(url: url , imageView: self.userImage) { (image) in
                        if let img = image {
                            self.stopLoader()
                            self.userImage.image = img
                        }
                    }
                }
            }
        }
    }
    
    
    func setCornerRadius_userImage(){
        self.userImage.isHidden = false
        self.userImage.layer.cornerRadius = self.btnCross.frame.size.width / 2
        self.userImage.clipsToBounds = true
    }
    
    func roundAndBorder_crossButton(){
        self.btnCross.isHidden = false
        self.btnCross.layer.cornerRadius = self.btnCross.frame.size.width / 2
        self.btnCross.clipsToBounds = true
        self.btnCross.layer.borderWidth = 4.0
        self.btnCross.layer.borderColor = UIColor.white.cgColor
    }
    
    
    func configureNoImageButton(){
        self.btnNoImage.isHidden = false
        self.btnNoImage.layer.cornerRadius = self.btnCross.frame.size.width / 2
        self.btnNoImage.clipsToBounds = true
    }
    
    
    //Animation
    //Animation of image
    func startAnimate() {
        let shakeAnimation = CABasicAnimation(keyPath: "transform.rotation")
        shakeAnimation.duration = 0.05
        shakeAnimation.repeatCount = 4
        shakeAnimation.autoreverses = true
        shakeAnimation.duration = 0.2
        shakeAnimation.repeatCount = 99999
        
        let startAngle: Float = (-2) * 3.14159/180
        let stopAngle = -startAngle
        
        shakeAnimation.fromValue = NSNumber(value: startAngle as Float)
        shakeAnimation.toValue = NSNumber(value: 3 * stopAngle as Float)
        shakeAnimation.autoreverses = true
        shakeAnimation.timeOffset = 290 * drand48()
        
        let layer: CALayer = self.layer
        layer.add(shakeAnimation, forKey:"animate")
        //removeBtn.isHidden = false
        isAnimate = true
    }
    
    
    func stopAnimate() {
        let layer: CALayer = self.layer
        layer.removeAnimation(forKey: "animate")
       // self.removeBtn.isHidden = true
        isAnimate = false
    }
    
    
    //Image loader
    func startLoader(){
        DispatchQueue.main.async {
            self.imgLoader.isHidden = false
            self.imgLoader.startAnimating()
        }
    }
    
    func stopLoader(){
        DispatchQueue.main.async {
            self.imgLoader.stopAnimating()
            self.imgLoader.isHidden = true
        }
    }
}
