//
//  EditProfile_VC.swift
//  EliteMatch
//
//  Created by Kitlabs-M-0002 on 8/7/19.
//  Copyright © 2019 Kitlabs-M-0002. All rights reserved.
//

import UIKit
import Alamofire
import FTIndicator

class EditProfile_VC: UIViewController , UIImagePickerControllerDelegate,UINavigationControllerDelegate {
    
    //MARK:- Outlets
    @IBOutlet weak var collectionVw_userImages: UICollectionView!
    @IBOutlet weak var txtFld_name: UITextField!
    @IBOutlet weak var txtFld_location: UITextField!
    @IBOutlet weak var txtFld_bio: UITextField!
    @IBOutlet weak var txtFld_birthday: UITextField!
    @IBOutlet weak var txtFld_email: UITextField!
    @IBOutlet weak var lbl_languages: UILabel!
    @IBOutlet weak var lbl_religion: UILabel!
    @IBOutlet weak var lbl_religiousCount: UILabel!
    @IBOutlet weak var slider_religious: UISlider!
    @IBOutlet weak var lbl_travel: UILabel!
    @IBOutlet weak var lbl_familyOrigin: UILabel!
    @IBOutlet weak var lbl_education: UILabel!
    @IBOutlet weak var btn_school: UIButton!
    @IBOutlet weak var btn_career: UIButton!
    @IBOutlet weak var txtFld_schoolCareerText: UITextField!
    @IBOutlet weak var lbl_familyValues: UILabel!
    @IBOutlet weak var switch_workRoutinely: UISwitch!
    @IBOutlet weak var switch_smoke: UISwitch!
    @IBOutlet weak var switch_drink: UISwitch!
    @IBOutlet weak var switch_eatHalal: UISwitch!
    @IBOutlet weak var switch_marriedBefore: UISwitch!
    @IBOutlet weak var switch_children: UISwitch!
    @IBOutlet weak var txtFld_height: UITextField!
    @IBOutlet weak var switch_bingeWatch: UISwitch!
    @IBOutlet weak var lbl_favCookie: UILabel!
    @IBOutlet weak var lbl_favMusic: UILabel!
    @IBOutlet weak var lbl_favFood: UILabel!
    @IBOutlet weak var switch_tallerPartner: UISwitch!
    @IBOutlet weak var lbl_religionMatter: UILabel!
    @IBOutlet weak var lbl_ethnicityMatter: UILabel!
    //@IBOutlet weak var pageController: UIPageControl!
    @IBOutlet weak var slider_religionMatter: UISlider!
    @IBOutlet weak var slider_ethnicityMatter: UISlider!
    @IBOutlet weak var txtFld_favShow: UITextField!
    @IBOutlet weak var constraint_vwProfile_height: NSLayoutConstraint!
    @IBOutlet weak var constraint_vwProfileImages_top: NSLayoutConstraint!
    @IBOutlet weak var btnDoneShaking: UIButton!
    //Nitin
    @IBOutlet weak var nameRequired_label: UILabel!
    
    //MARK:- Variables
    let datePicker = UIDatePicker()
    var arrHeight = [[String]]()
    let heightPicker = UIPickerView()
    let imagePicker = UIImagePickerController()
    var countrySelection = [String]()
    var countryCode = [String]()
    var userProfile:UserProfile!
    var arrSelectedUserImgs:[imageWithId] = []
    var arrNewImages:[UserImage] = []
    var selectedIndexPath:IndexPath?
    var longPressedEnabled:Bool = false
    var isChangesApplied:Bool = false
    
  
    //MARK:- UIView life-cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //pageController.numberOfPages = 0
        self.configureTextField()
        self.collectionVw_userImages.delegate = self
        self.collectionVw_userImages.dataSource = self
        self.addLongTabGesture_collectionItem()
        self.configure_vwProfile_height()
        showDatePicker()
        self.configureHeightPicker()
        self.arrHeight.append(constantVC.GeneralConstants.arrFeet)
        self.arrHeight.append(constantVC.GeneralConstants.arrInches)
        
        //get user profile
        ProfileManager.shared.getUserProfile_phoneNo(phoneNo: SessionManager.get_phonenumber()) { (success , user) in
            if success {
                if let user_ = user {
                    self.userProfile = user_
                    self.collectionVw_userImages.reloadData()
                    self.setProfileData()
                }
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(true, animated: true)
    }
    
    //MARK:-UIButton Actions
    @IBAction func didPress_back(_ sender: UIButton) {
        if self.isChangesApplied {
            self.showChangesDoneAlert()
        }
        else {
            if constantVC.AppUpdationsChecks.isProfileUpdated {
                ProfileManager.shared.clearMyProfileCache_onrequest(strUrl: "\(ProfileManager.shared.getMyProfileUrl())")
            }
            self.navigationController?.popViewController(animated: true)
        }
        
    }
    
    @IBAction func didPress_save(_ sender: UIButton) {
        if ReachabilityManager.isConnectedToNetwork() {
            if self.isValidate() {
                self.updateProfileImages()
            }
        } else {
            FTIndicator.showToastMessage("Please check your internet connection.")
        }
       
    }
    
    @IBAction func didPress_DropDown(_ sender: UIButton) {
        self.view.endEditing(true)
        
        configureChangesApplied()
        
        switch sender.tag {
            
        case DropDown_tag.languages.rawValue:
            DropDown_Manager.shared.configure(title: DropDown_Title.Language.rawValue , preSelected: self.lbl_languages.text ?? "" , allowMultiSelection: true , arrItems: constantVC.arrDropDown_Match.arrLangaugeDropDown) { (strSelectedValues) in
                self.lbl_languages.text = strSelectedValues
            }
            
        case DropDown_tag.religion.rawValue:
            DropDown_Manager.shared.configure(title: DropDown_Title.Religion.rawValue , preSelected: self.lbl_religion.text ?? "" , allowMultiSelection: false , arrItems: constantVC.arrDropDown_Match.arrReligionDropDown) { (strSelectedValues) in
                self.lbl_religion.text = strSelectedValues
            }
            
        case DropDown_tag.travel.rawValue:
            DropDown_Manager.shared.configure(title: DropDown_Title.Travel.rawValue , preSelected: self.lbl_travel.text ?? "" , allowMultiSelection: true , arrItems: constantVC.arrDropDown_Match.arrTravel) { (strSelectedValues) in
                self.lbl_travel.text = strSelectedValues
            }
            
        case DropDown_tag.familyOrigin.rawValue:
            countrySelection.removeAll()
            let picker = MICountryPicker()
            picker.showCallingCodes = false
            picker.delegate = self
            self.navigationController?.setNavigationBarHidden(false, animated: true)
            self.navigationController?.pushViewController(picker, animated: true)
            
        case DropDown_tag.education.rawValue:
            DropDown_Manager.shared.configure(title: DropDown_Title.Education.rawValue , preSelected: self.lbl_education.text ?? "" , allowMultiSelection: false , arrItems: constantVC.arrDropDown_Match.arrEducationDropDown) { (strSelectedValues) in
                self.lbl_education.text = strSelectedValues
            }
            
        case DropDown_tag.familyValues.rawValue:
            DropDown_Manager.shared.configure(title: DropDown_Title.FamilyValues.rawValue , preSelected: self.lbl_familyValues.text ?? "" , allowMultiSelection: false , arrItems: constantVC.arrDropDown_Match.arrLifeStyle) { (strSelectedValues) in
                self.lbl_familyValues.text = strSelectedValues
            }
            
        case DropDown_tag.favCookie.rawValue:
            DropDown_Manager.shared.configure(title: DropDown_Title.FavCookies.rawValue , preSelected: self.lbl_favCookie.text ?? "" , allowMultiSelection: false , arrItems: constantVC.arrDropDown_Match.arrFavDessert) { (strSelectedValues) in
                self.lbl_favCookie.text = strSelectedValues
            }
            
        case DropDown_tag.favFood.rawValue:
            DropDown_Manager.shared.configure(title: DropDown_Title.FavFood.rawValue , preSelected: self.lbl_favFood.text ?? "" , allowMultiSelection: false , arrItems: constantVC.arrDropDown_Match.arrFavFood) { (strSelectedValues) in
                self.lbl_favFood.text = strSelectedValues
            }
            
        case DropDown_tag.favMusic.rawValue:
            DropDown_Manager.shared.configure(title: DropDown_Title.FavMusic.rawValue , preSelected: self.lbl_favMusic.text ?? "" , allowMultiSelection: false , arrItems: constantVC.arrDropDown_Match.arrFavMusic) { (strSelectedValues) in
                self.lbl_favMusic.text = strSelectedValues
            }
            
        default:
            print("default")
        }
    }
    
    @IBAction func didPress_Slider(_ sender: UISlider) {
        self.view.endEditing(true)
        let currentValue = Int(sender.value)
        
        switch sender.tag {
        case DropDown_tag.religiousCount.rawValue:
            self.lbl_religiousCount.text = "\(currentValue)"
        case DropDown_tag.religionMatter.rawValue:
            self.lbl_religionMatter.text = "\(currentValue)"
        case DropDown_tag.ethnicityMatter.rawValue:
            self.lbl_ethnicityMatter.text = "\(currentValue)"
        default:
            print("default")
        }
    }
    
    @IBAction func didPress_school(_ sender: UIButton) {
        self.view.endEditing(true)
        if self.btn_school.isSelected {
            self.btn_school.isSelected = false
            self.btn_career.isSelected = true
        } else {
            self.btn_school.isSelected = true
            self.btn_career.isSelected = false
        }
    }
    
    @IBAction func didPress_career(_ sender: UIButton) {
        self.view.endEditing(true)
        if self.btn_career.isSelected {
            self.btn_school.isSelected = true
            self.btn_career.isSelected = false
        } else {
            self.btn_school.isSelected = false
            self.btn_career.isSelected = true
        }
    }
    
    @IBAction func didPress_Switch(_ sender: UISwitch) {
        self.view.endEditing(true)
    }
    
    @objc func didPress_cross(sender: UIButton!) {
        
        if sender.tag != 0 &&  sender.tag != 1 {
            let indexPath = IndexPath(item: sender.tag , section: 0)
            if let cell = self.collectionVw_userImages.cellForItem(at: indexPath) as? EditProfile_collectionCell{
                
                if var images = self.userProfile.images {
                    if images.indices.contains(sender.tag) {
                        let dictParam : NSDictionary = ["id": images[sender.tag].id , "image": images[sender.tag].filename]
                        self.deleteUserImage(param: dictParam)
                        images.remove(at: sender.tag)
                        self.userProfile.images = images
                        self.collectionVw_userImages.reloadData()
                    }
                }
                
                cell.userImage.isHidden = true
                cell.btnCross.isHidden = true
                cell.configureNoImageButton()
            }
        }
        else {
            FTIndicator.showToastMessage("Your two profiles are required.")
        }
    }
    
    
    @objc func didPress_addNewImage(sender: UIButton!) {
        self.selectedIndexPath = IndexPath(row: sender.tag , section: 0)
        self.openImagePicker()
    }
    
    @IBAction func didPress_doneShakingEffect(_ sender: UIButton) {
        self.btnDoneShaking.isHidden = true
        longPressedEnabled = false
        self.collectionVw_userImages.reloadData()
    }
    
    
    
    @objc func longTap(_ gesture: UIGestureRecognizer){
        
        switch(gesture.state) {
        case .began:
            guard let selectedIndexPath = collectionVw_userImages.indexPathForItem(at: gesture.location(in: collectionVw_userImages)) else {
                return
            }
            collectionVw_userImages.beginInteractiveMovementForItem(at: selectedIndexPath)
        case .changed:
          collectionVw_userImages.updateInteractiveMovementTargetPosition(gesture.location(in: gesture.view!))
        case .ended:
            collectionVw_userImages.endInteractiveMovement()
            self.configureShakingDoneButton()
            longPressedEnabled = true
            self.collectionVw_userImages.reloadData()
        default:
            collectionVw_userImages.cancelInteractiveMovement()
        }
    }
    
    //MARK:- Custom Methods
    
    func configureShakingDoneButton(){
        self.btnDoneShaking.isHidden = false
        self.btnDoneShaking.layer.cornerRadius = self.btnDoneShaking.frame.size.height / 2
       // self.btnDoneShaking.layer.borderColor = UIColor.black.cgColor
        //self.btnDoneShaking.layer.borderWidth = 1.0
    }
    
    func showDatePicker(){
        datePicker.datePickerMode = .date
        datePicker.maximumDate = Date()
        
        let toolbar = UIToolbar()
        toolbar.sizeToFit()
        
        let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.plain, target: self, action: #selector(self.donedatePicker))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: UIBarButtonItemStyle.plain, target: self, action: #selector(self.cancelDatePicker))
        toolbar.setItems([cancelButton,spaceButton,doneButton], animated: false)
        self.txtFld_birthday.inputAccessoryView = toolbar
        self.txtFld_birthday.inputView = datePicker
    }
    
    @objc func donedatePicker(){
        let formatter = DateFormatter()
        formatter.dateFormat = "MM-dd-yyyy"
        self.txtFld_birthday.text = formatter.string(from: datePicker.date)
        self.view.endEditing(true)
    }
    
    @objc func cancelDatePicker(){
        self.view.endEditing(true)
    }
    
    func showChangesDoneAlert(){
        let alert = UIAlertController(title: "NIM", message: "Do you want to save changes?" , preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Yes", style: .default, handler: { (action: UIAlertAction!) in
            if self.isValidate() {
                self.updateProfileImages()
            }
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (action: UIAlertAction!) in
            if constantVC.AppUpdationsChecks.isProfileUpdated {
                ProfileManager.shared.clearMyProfileCache_onrequest(strUrl: "\(ProfileManager.shared.getMyProfileUrl())")
            }
            self.navigationController?.popViewController(animated: true)
        }))
        present(alert, animated: true, completion: nil)
    }
    
    //MARK:- UIPicker Height
    func configureHeightPicker(){
        self.txtFld_height.delegate = self
        self.heightPicker.dataSource = self
        self.heightPicker.delegate = self
        self.txtFld_height.inputView = self.heightPicker
        
        let toolbar = UIToolbar()
        toolbar.sizeToFit()
        
        let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.plain, target: self, action: #selector(self.doneHeightdatePicker))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: UIBarButtonItemStyle.plain, target: self, action: #selector(self.cancelHeightDatePicker))
        toolbar.setItems([cancelButton,spaceButton,doneButton], animated: false)
        self.txtFld_height.inputAccessoryView = toolbar
    }
    
    @objc func doneHeightdatePicker(){
        self.txtFld_height.resignFirstResponder()
    }
    
    @objc func cancelHeightDatePicker(){
        self.view.endEditing(true)
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
    }
    
    //MARK:- Image Picker
    func openImagePicker(){
        self.view.endEditing(true)
        imagePicker.sourceType = .photoLibrary
        imagePicker.allowsEditing = false
        imagePicker.delegate = self
        self.present(imagePicker, animated: true, completion: nil)
    }
    
    
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        guard let image = info[UIImagePickerControllerOriginalImage] as? UIImage else { return }
        let cropController = CropViewController(croppingStyle: CropViewCroppingStyle.default, image: image)
        cropController.aspectRatioPreset = .preset4x3
        cropController.cropView.aspectRatio = CGSize(width: self.view.frame.width + 200 , height: 300)
        cropController.cropView.maximumZoomScale = 2.0
        cropController.cropView.cropBoxResizeEnabled = false
        cropController.delegate = self
        picker.pushViewController(cropController, animated: true)
    }
    
    //MARK:- Custom Methods
    func addLongTabGesture_collectionItem(){
       let longPressGesture = UILongPressGestureRecognizer(target: self, action: #selector(self.longTap(_:)))
       self.collectionVw_userImages.addGestureRecognizer(longPressGesture)
    }
    
    func configure_vwProfile_height(){
        let height = self.collectionVw_userImages.bounds.width/3.0
        self.constraint_vwProfile_height.constant = height * 2 + 40
    }
    
    func setProfileData(){
        //pageController.numberOfPages = self.userProfile.images?.count ?? 0
        self.txtFld_name.text = self.userProfile.name ?? ""
        self.txtFld_location.text = self.userProfile.living_place ?? ""
        self.txtFld_bio.text = self.userProfile.about_me ?? ""
        self.txtFld_birthday.text = self.userProfile.dob ?? ""
        self.txtFld_email.text = self.userProfile.email ?? ""
        self.lbl_languages.text = self.userProfile.languages ?? ""
        self.lbl_religion.text = self.userProfile.religion ?? ""
        self.lbl_religiousCount.text = self.userProfile.religionCount ?? ""
        self.slider_religious.setValue(Float(self.userProfile.religionCount ?? "0")!, animated: true)
        self.lbl_travel.text = self.userProfile.travelPlaces ?? ""
        self.lbl_familyOrigin.text = self.userProfile.ethnicity ?? ""
        self.lbl_education.text = self.userProfile.education ?? ""
        if self.userProfile.inSchool == "true" {
            self.btn_school.isSelected = true
        }
        if self.userProfile.inCareer == "true" {
            self.btn_school.isSelected = true
        }
        self.txtFld_schoolCareerText.text = self.userProfile.careerName ?? ""
        self.lbl_familyValues.text = self.userProfile.lifestyle ?? ""
        
        if self.userProfile.isWorkOutRoutinely == "true" {
            self.switch_workRoutinely.setOn(true, animated: true)
        }
        if self.userProfile.isSmoke == "true" {
            self.switch_smoke.setOn(true, animated: true)
        }
        if self.userProfile.isDrink == "true" {
            self.switch_drink.setOn(true, animated: true)
        }
        if self.userProfile.isOnlyEatHalal == "true" {
            self.switch_eatHalal.setOn(true, animated: true)
        }
        if self.userProfile.isMarriedBefore == "true" {
            self.switch_marriedBefore.setOn(true, animated: true)
        }
        if self.userProfile.isHaveChildren == "true" {
            self.switch_children.setOn(true, animated: true)
        }
        self.txtFld_height.text = self.userProfile.height ?? ""
        self.txtFld_favShow.text = self.userProfile.fav_shows ?? ""
        if self.userProfile.isBingeWatchShows == "true" {
            self.switch_bingeWatch.setOn(true , animated: true)
        }
        self.lbl_favCookie.text = self.userProfile.fav_dessert ?? ""
        self.lbl_favMusic.text = self.userProfile.fav_music ?? ""
        self.lbl_favFood.text = self.userProfile.fav_food ?? ""
        if self.userProfile.isPreferred_TallerPartner == "true" {
            self.switch_tallerPartner.setOn(true, animated: true)
        }
        self.lbl_religionMatter.text = self.userProfile.religionMatterCount ?? "0"
        self.lbl_ethnicityMatter.text = self.userProfile.ethnicityCount ?? "0"
        self.slider_religionMatter.setValue(Float(self.userProfile.religionMatterCount ?? "0")!, animated: true)
        self.slider_ethnicityMatter.setValue(Float(self.userProfile.ethnicityCount ?? "0")!, animated: true)
    }
    
    func isValidate() -> Bool {
        
        if (self.txtFld_name.text?.trimmingCharacters(in: .whitespacesAndNewlines))?.count == 0 {
            FTIndicator.showToastMessage("Please enter name")
            return false
        }
        if (self.txtFld_location.text?.trimmingCharacters(in: .whitespacesAndNewlines))?.count == 0 {
            FTIndicator.showToastMessage("Please enter location")
            return false
        }
        if (self.txtFld_bio.text?.trimmingCharacters(in: .whitespacesAndNewlines))?.count == 0 {
            FTIndicator.showToastMessage("Please enter bio")
            return false
        }
        if (self.txtFld_email.text?.trimmingCharacters(in: .whitespacesAndNewlines))?.count == 0 {
            FTIndicator.showToastMessage("Please enter email")
            return false
        }
        if !(txtFld_email.text?.isValidEmail ?? false) {
            FTIndicator.showToastMessage("Please enter a valid email")
            return false
        }
        if (self.txtFld_schoolCareerText.text?.trimmingCharacters(in: .whitespacesAndNewlines))?.count == 0 {
            if self.btn_school.isSelected {
                FTIndicator.showToastMessage("Please enter school information")
            } else {
                FTIndicator.showToastMessage("Please enter career information")
            }
            return false
        }
        if (self.txtFld_favShow.text?.trimmingCharacters(in: .whitespacesAndNewlines))?.count == 0 {
            FTIndicator.showToastMessage("Please enter favorite show")
            return false
        }
        //Nitin
        if (self.lbl_familyOrigin.text?.trimmingCharacters(in: .whitespacesAndNewlines))?.count == 0 {
                   FTIndicator.showToastMessage("Please enter your family origin")
                   return false
        }
        if (self.lbl_familyValues.text?.trimmingCharacters(in: .whitespacesAndNewlines))?.count == 0 {
                   FTIndicator.showToastMessage("Please enter your family values")
                   return false
        }
        
        return true
    }
    
    func configureChangesApplied(){
        self.isChangesApplied = true
    }
    
    func configureTextField(){
        self.txtFld_name.delegate = self
        self.txtFld_location.delegate = self
        self.txtFld_bio.delegate = self
        self.txtFld_birthday.delegate = self
        self.txtFld_email.delegate = self
        self.txtFld_schoolCareerText.delegate = self
    }
    
    
    //MARK:- Web service implementation
    func getSwitchValue(switch_: UISwitch) ->String{
        
        if switch_.isOn {
            return "true"
        }
        return "false"
    }
    
    
    func updateProfileImages(){
        LoadingIndicatorView.show()
        constantVC.AppUpdationsChecks.isProfileUpdated = true
        
        if self.arrNewImages.count > 0 {
            for (index , new) in self.arrNewImages.enumerated() {
                
                //webservice call to update image
                if let img = new.image , let order_ = new.order {
                    if let imageData = UIImageJPEGRepresentation(img  , 1) {
                        
                        ProfileManager.shared.webService_addUserImage(order: order_ , id: SessionManager.get_Elite_userID() , imgdata: imageData) { (success, msg) in
                            
                            if index == self.arrNewImages.count - 1 {
                                self.updateProfileImages_alreadyUploaded()
                            }
                        }
                        
                    }
                }
            }
        }
        else if self.arrSelectedUserImgs.count > 0 {
            for (index , img) in self.arrSelectedUserImgs.enumerated() {
                if let imageData = UIImageJPEGRepresentation(img.image! , 1) {
                    
                    if let id = img.id , let name = img.imgName , let order = img.order {
                        self.webService_updateUserProfileImage(id: id, orderID: order, imgName: name, imgdata: imageData) { (success) in
                            if !success {
                                LoadingIndicatorView.hide()
                                return
                            }
                            else {
                            NimUserManager.shared.sendPushNotification_profileUpdate_tags(strUrl: constantVC.GeneralConstants.GET_IMAGE + name)
                                
                                if order == "0" {
                                ProfileManager.shared.clearMyProfileCache_onrequest(strUrl: "\(ProfileManager.shared.getMyProfileUrl())")
                                }
                                
                                if index == self.arrSelectedUserImgs.count - 1 {
                                    self.UpdateOtherProfileParam()
                                }
                                
                            }
                        }
                    }
                }
            }
        }
        else {
            self.UpdateOtherProfileParam()
        }
       
    }
    
    
    func updateProfileImages_alreadyUploaded(){
        if self.arrSelectedUserImgs.count > 0 {
            for (index , img) in self.arrSelectedUserImgs.enumerated() {
                if let imageData = UIImageJPEGRepresentation(img.image! , 1) {
                    
                    if let id = img.id , let name = img.imgName , let order = img.order {
                        self.webService_updateUserProfileImage(id: id, orderID: order, imgName: name, imgdata: imageData) { (success) in
                            if !success {
                                LoadingIndicatorView.hide()
                                return
                            }
                            else {                                NimUserManager.shared.sendPushNotification_profileUpdate_tags(strUrl: constantVC.GeneralConstants.GET_IMAGE + name)
                                
                                if order == "0" {
                                    ProfileManager.shared.clearMyProfileCache_onrequest(strUrl: "\(ProfileManager.shared.getMyProfileUrl())")
                                }
                                
                                if index == self.arrSelectedUserImgs.count - 1 {
                                    self.UpdateOtherProfileParam()
                                }
                                
                            }
                        }
                    }
                }
            }
        }
        else {
            self.UpdateOtherProfileParam()
        }
    }
    
    
    func UpdateOtherProfileParam(){
        if self.isValidate() {
            var inSchool:String = "false"
            var inCareer:String = "false"
            
            if self.btn_school.isSelected {
                inSchool = "true"
            } else {
                inCareer = "true"
            }
            
            let dictParam : NSDictionary = ["Userid": SessionManager.get_Elite_userID(), "name": self.txtFld_name.text?.trimmingCharacters(in: .whitespacesAndNewlines) ?? "" ,"living_place": self.txtFld_location.text?.trimmingCharacters(in: .whitespacesAndNewlines) ?? "" , "about_me": self.txtFld_bio.text?.trimmingCharacters(in: .whitespacesAndNewlines) ?? "" , "dob": self.txtFld_birthday.text!,"email":self.txtFld_email.text?.trimmingCharacters(in: .whitespacesAndNewlines) ?? "","languages":self.lbl_languages.text!,"religion":self.lbl_religion.text!,"religionCount":self.lbl_religiousCount.text!,"travelPlaces":self.lbl_travel.text!,"ethnicity":self.lbl_familyOrigin.text!,"education":self.lbl_education.text!,"inSchool": inSchool,"inCareer": inCareer,"lifestyle":self.lbl_familyValues.text!,"isWorkOutRoutinely": self.getSwitchValue(switch_: self.switch_workRoutinely),"isSmoke":self.getSwitchValue(switch_: self.switch_smoke),"isDrink":self.getSwitchValue(switch_: self.switch_drink),"isOnlyEatHalal":self.getSwitchValue(switch_: self.switch_eatHalal),"isMarriedBefore":self.getSwitchValue(switch_: self.switch_marriedBefore),"isHaveChildren":self.getSwitchValue(switch_: self.switch_children),"height":self.txtFld_height.text!,"fav_shows":self.txtFld_favShow.text?.trimmingCharacters(in: .whitespacesAndNewlines) ?? "","isBingeWatchShows":self.getSwitchValue(switch_: self.switch_bingeWatch),"fav_dessert":self.lbl_favCookie.text!,"fav_music":self.lbl_favMusic.text!,"fav_food":self.lbl_favFood.text!,"isPreferred_TallerPartner":self.getSwitchValue(switch_: self.switch_tallerPartner),"religionMatterCount":self.lbl_religionMatter.text!,"ethnicityCount":self.lbl_ethnicityMatter.text!]
            
            ProfileManager.shared.UpdateProfile(param: dictParam) { (success) in
                LoadingIndicatorView.hide()
                if success {
                    SessionManager.saveUserName(name: (self.txtFld_name.text?.trimmingCharacters(in: .whitespacesAndNewlines))!)
                    FTIndicator.showSuccess(withMessage: "Profile Updated Successfully!")
                    self.navigationController?.popViewController(animated: true)
                }
            }
        }
    }
    
    
    func webService_updateUserProfileImage(id:String , orderID:String , imgName:String , imgdata:Data , completionHandler:@escaping (Bool) -> ()){
        
        Alamofire.upload(
            multipartFormData: { multipartFormData in
                
                //update profile images
                multipartFormData.append(id.data(using: String.Encoding.utf8)!, withName: "imgID")
                multipartFormData.append("\(SessionManager.get_phonenumber() + ".jpeg")".data(using: String.Encoding.utf8)!, withName: "filename")
                multipartFormData.append(id.data(using: String.Encoding.utf8)!, withName: "orderID")
                if orderID == "0" {
                    multipartFormData.append(imgdata, withName: "profileImage", fileName: "\(SessionManager.get_phonenumber() + ".jpeg")" , mimeType: "image/jpeg")
                }
                else {
                    multipartFormData.append(imgdata, withName: "profileImage", fileName: imgName , mimeType: "image/jpeg")
                }
                
        },
            to:"\(constantVC.GeneralConstants.MATCH_BASE_URL)\(constantVC.WebserviceName.URL_UPDATE_USER_PROFILE_IMAGES)",
            encodingCompletion: { encodingResult in
                
                switch encodingResult {
                case .success(let upload, _, _):
                    upload.responseJSON { response in
                        
                        guard response.result.error == nil else {
                            print("Error for POST :\(response.result.error!)")
                            completionHandler(false)
                            return
                        }
                        
                        if let value = response.result.value {
                            
                            if let result = value as? Dictionary<String, AnyObject> {
                                
                                print(result)
                                let resultMessage = result["msg"] as! String
                                
                                if resultMessage == "success"{
                                    
//                                    if let rowdata = result["rowdata"] as? NSDictionary {
//                                        if let filename = rowdata.object(forKey: "filename") as? String {
//                                            completionHandler(true , filename)
//                                        }
//                                    }
                                    
                                    completionHandler(true)
                                }
                                else{
                                    completionHandler(false)
                                }
                            }
                        }
                    }
                        .uploadProgress { progress in // main queue by default
                            print("Upload Progress: \(progress.fractionCompleted)")
                    }
                    return
                case .failure(let encodingError):
                    LoadingIndicatorView.hide()
                    debugPrint(encodingError)
                }
        })
    }
    
    
    //MARK:- Profiles relates methods
    func updateCollectionCell(indexPath_:IndexPath , image:UIImage){
        if let cell = self.collectionVw_userImages.cellForItem(at:indexPath_) as? EditProfile_collectionCell{
            
            let targetSize: CGSize? = cell.userImage.bounds.size
            let transform = QMImageTransform(size: targetSize! , isCircle: false)
            
            transform.apply(for: (image)) { (img) in
                cell.stopLoader()
                cell.userImage.image = img
                cell.isImageUpdated = true
                cell.btnNoImage.isHidden = true
                cell.roundAndBorder_crossButton()
                cell.setCornerRadius_userImage()
            }
        }
    }
  
    
    func deleteUserImage(param:NSDictionary) {
        ProfileManager.shared.delete_profilePhoto(param: param) { (success) in
        }
    }
    
    
    func updateOrderOfImages(param:NSDictionary, completionHandler:@escaping (Bool) -> ()){
        ProfileManager.shared.updateOrder_profilePhoto(param: param) { (success) in
            if success {
                constantVC.AppUpdationsChecks.isProfileUpdated = true
                completionHandler(true)
            } else {
                completionHandler(false)
            }
        }
    }
}

extension EditProfile_VC: UIPickerViewDelegate , UIPickerViewDataSource {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 2
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return self.arrHeight[component].count
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return self.arrHeight[component][row]
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        let feet =  arrHeight[0][pickerView.selectedRow(inComponent: 0)]
        let Inch = arrHeight[1][pickerView.selectedRow(inComponent: 1)]
        
        if feet != "Feet" && Inch != "Inch" {
            self.txtFld_height.text =   feet + "'" + Inch
        }
    }
}

extension EditProfile_VC: UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        configureChangesApplied()
        return true
    }
}

extension EditProfile_VC: UICollectionViewDelegate , UICollectionViewDataSource , UICollectionViewDelegateFlowLayout  {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: UIScreen.main.bounds.size.width/3 - 20, height: UIScreen.main.bounds.size.width/3 - 20)
    }
    
//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
//
//        let yourWidth = collectionView.bounds.width/3.0
//        let yourHeight = yourWidth
//        return CGSize(width: yourWidth, height: yourHeight)
//    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
       return 6
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath as IndexPath) as! EditProfile_collectionCell
        
        if self.userProfile != nil {
            if let images = self.userProfile.images {
                if images.indices.contains(indexPath.row) {
                    let img = images[indexPath.row]
                    cell.btnNoImage.isHidden = true
                    cell.userImage2 = img
                    cell.roundAndBorder_crossButton()
                    cell.setCornerRadius_userImage()
                }
                else {
                    cell.configureNoImageButton()
                }
            }
        }
        else {
            cell.configureNoImageButton()
        }
        
        //add action to cross button
        cell.btnCross.addTarget(self, action: #selector(didPress_cross), for: .touchUpInside)
        cell.btnCross.tag = indexPath.row
        
        //add action to plus button
        cell.btnNoImage.addTarget(self, action: #selector(didPress_addNewImage), for: .touchUpInside)
        cell.btnNoImage.tag = indexPath.row
        
        if longPressedEnabled   {
            cell.startAnimate()
        }else{
            cell.stopAnimate()
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.selectedIndexPath = indexPath
        self.openImagePicker()
    }
    
    func collectionView(_ collectionView: UICollectionView, canMoveItemAt indexPath: IndexPath) -> Bool {
        
        if self.userProfile != nil {
            if let images = self.userProfile.images {
                if images.indices.contains(indexPath.row){
                    return true
                }
            }
        }
        return false
    }
    
    func collectionView(_ collectionView: UICollectionView, moveItemAt sourceIndexPath: IndexPath, to destinationIndexPath: IndexPath) {
        print("Start index :- \(sourceIndexPath.item)")
        print("End index :- \(destinationIndexPath.item)")
        
       if self.userProfile != nil {
            if var images = self.userProfile.images {
                
                if images.indices.contains(sourceIndexPath.item) &&  images.indices.contains(destinationIndexPath.item) {
                    let tmp = images[sourceIndexPath.item]
                    images[sourceIndexPath.item] = images[destinationIndexPath.item]
                    images[destinationIndexPath.item] = tmp
                    self.userProfile.images = images
                    self.collectionVw_userImages.reloadData()
                    
//                    if sourceIndexPath.item == 0 || destinationIndexPath.item == 0 {
//                        ProfileManager.shared.clearMyProfileCache()
//                    }
                   // var imageArray = [Dictionary<String,Any>]()
                    //var tempDict = Dictionary<String,Any>()
                    var newName = ""
                    var newName1 = ""
                    var oldName = ""
                    var oldName1 = ""
                    var id = ""
                    var id1 = ""
                    var orderId = ""
                    var orderId1 = ""
                    
                    if sourceIndexPath.item == 0{
                        id = images[sourceIndexPath.item].id ?? ""
                        orderId = String(sourceIndexPath.item)
                        newName = SessionManager.get_phonenumber() + ".jpeg"
                        oldName = images[sourceIndexPath.item].filename ?? ""
                        
//                        tempDict["id"] = images[sourceIndexPath.item].id ?? ""
//                        tempDict["order_id"] = String(sourceIndexPath.item)
//                        tempDict["newName"] = SessionManager.get_phonenumber() + ".jpeg"
//                        tempDict["oldName"] = images[sourceIndexPath.item].filename ?? ""
//                        imageArray.append(tempDict)
//                        tempDict.removeAll()
//
                        id1 = images[destinationIndexPath.item].id ?? ""
                        orderId1 = String(destinationIndexPath.item)
                        newName1 = String(destinationIndexPath.item) + SessionManager.get_phonenumber()  + ".jpeg"
                        oldName1 = images[destinationIndexPath.item].filename ?? ""
                        
//                        tempDict["id"] = images[destinationIndexPath.item].id ?? ""
//                        tempDict["order_id"] = String(destinationIndexPath.item)
//                        tempDict["newName"] = String(destinationIndexPath.item) + SessionManager.get_phonenumber()  + ".jpeg"
//                        tempDict["oldName"] = images[destinationIndexPath.item].filename ?? ""
//                        imageArray.append(tempDict)
                        
                    } else if destinationIndexPath.item == 0 {
                        id = images[destinationIndexPath.item].id ?? ""
                        orderId = String(destinationIndexPath.item)
                        newName = SessionManager.get_phonenumber() + ".jpeg"
                        oldName = images[destinationIndexPath.item].filename ?? ""
                        
//                        tempDict["id"] = images[destinationIndexPath.item].id ?? ""
//                        tempDict["order_id"] = String(destinationIndexPath.item)
//                        tempDict["newName"] = SessionManager.get_phonenumber() + ".jpeg"
//                        tempDict["oldName"] = images[destinationIndexPath.item].filename ?? ""
//                        imageArray.append(tempDict)
//                        tempDict.removeAll()
                        
                        id1 = images[sourceIndexPath.item].id ?? ""
                        orderId1 = String(sourceIndexPath.item)
                        newName1 = String(sourceIndexPath.item) + SessionManager.get_phonenumber()  + ".jpeg"
                        oldName1 = images[sourceIndexPath.item].filename ?? ""
                        
//                        tempDict["id"] = images[sourceIndexPath.item].id ?? ""
//                        tempDict["order_id"] = String(sourceIndexPath.item)
//                        tempDict["newName"] = String(sourceIndexPath.item) + SessionManager.get_phonenumber()  + ".jpeg"
//                        tempDict["oldName"] = images[sourceIndexPath.item].filename ?? ""
//                        imageArray.append(tempDict)
                    }
             
                    let headers = [
                      "content-type": "application/x-www-form-urlencoded"
                    ]

                    let postData = NSMutableData(data: "action=imageOrder".data(using: String.Encoding.utf8)!)
                    postData.append("&userid=\(userProfile.id ?? "")".data(using: String.Encoding.utf8)!)
                    postData.append("&image[0][newName]=\(newName)".data(using: String.Encoding.utf8)!)
                    postData.append("&image[0][oldName]=\(oldName)".data(using: String.Encoding.utf8)!)
                    postData.append("&image[0][id]=\(id)".data(using: String.Encoding.utf8)!)
                    postData.append("&image[0][order_id]=\(orderId)".data(using: String.Encoding.utf8)!)
                    postData.append("&image[1][newName]=\(newName1)".data(using: String.Encoding.utf8)!)
                    postData.append("&image[1][oldName]=\(oldName1)".data(using: String.Encoding.utf8)!)
                    postData.append("&image[1][id]=\(id1)".data(using: String.Encoding.utf8)!)
                    postData.append("&image[1][order_id]=\(orderId1)".data(using: String.Encoding.utf8)!)

                    let request = NSMutableURLRequest(url: NSURL(string: "http://18.223.24.94/elitematch/api.php")! as URL,
                                                            cachePolicy: .useProtocolCachePolicy,
                                                        timeoutInterval: 60.0)
                    request.httpMethod = "POST"
                    request.allHTTPHeaderFields = headers
                    request.httpBody = postData as Data

                    let session = URLSession.shared
                    let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
                      if (error != nil) {
                        print(error ?? "")
                      } else {
                        guard let newData = data as NSData? else {return}
                        if let jsonDict = self.nsdataToJSON(data: newData) as? Dictionary<String,Any>{
                            print(jsonDict)
                            if let msg = jsonDict["msg"] as? String,msg == "success" {
                                constantVC.AppUpdationsChecks.isProfileUpdated = true
                            }
                        }
                      }
                    })

                    dataTask.resume()
                    
                    
                    
//                    var imageNew = ""
//                    if sourceIndexPath.item == 0 {
//                        imageNew = SessionManager.get_phonenumber() + ".jpeg"
//                    } else {
//                        imageNew = String(sourceIndexPath.item) + SessionManager.get_phonenumber()  + ".jpeg"
//                    }
                  //  let dictParam : NSDictionary = ["image": imageArray , "userid": userProfile.id ?? "" ]
                
//                    self.updateOrderOfImages(param: dictParam) { (success) in
//                        if success {
//
//
////                            if destinationIndexPath.item == 0 {
////                                imageNew = SessionManager.get_phonenumber() + ".jpeg"
////                            } else {
////                                imageNew = String(destinationIndexPath.item) + SessionManager.get_phonenumber()  + ".jpeg"
////                            }
////                            dictParam = ["orderID": "\(destinationIndexPath.item)" , "id": images[destinationIndexPath.item].id ?? "", "newfilename": imageNew]
////
////                            self.updateOrderOfImages(param: dictParam) { (success) in
////                                print(success)
//                           // }
//                        }
                   // }
                }
            }
        }
    }
    
    func nsdataToJSON(data: NSData) -> AnyObject? {
        do {
            return try JSONSerialization.jsonObject(with: data as Data, options: .mutableContainers) as AnyObject
        } catch let myJSONError {
            print(myJSONError)
        }
        return nil
    }
}


extension EditProfile_VC: CropViewControllerDelegate {
    
    public func cropViewController(_ cropViewController: CropViewController, didCropToImage image: UIImage, withRect cropRect: CGRect, angle: Int) {
        
        //update image
        if let index = self.selectedIndexPath {
            
            if var images = self.userProfile.images {
                
                //to update
                if images.indices.contains(index.row) {
                    let userImage = images[index.row]
                    var img = ""
                    if index.row != 0 {
                        img = String(index.row) + (userImage.filename ?? "")
                    } else {
                        img = SessionManager.get_phonenumber() + ".jpeg"//userImage.filename ?? ""
                    }
                    let obj = imageWithId()
                    obj.id = userImage.id
                    obj.image = image
                    obj.imgName = img
                    obj.order = String(index.row)//userImage.order
                    
                    if self.arrSelectedUserImgs.contains(where: { $0.id ==  userImage.id}) {
                        self.arrSelectedUserImgs.filter {$0.id == userImage.id}.first?.image = image
                    }
                    else if self.arrNewImages.contains(where: { $0.order ==  userImage.order}) {
                        self.arrNewImages.filter {$0.order == userImage.order}.first?.image = image
                    }
                    else {
                        self.arrSelectedUserImgs.append(obj)
                    }
                    
                    self.updateCollectionCell(indexPath_: index , image: image)
                    
                }
                    //to add
                else {
                    
                    let dicUser = NSMutableDictionary()
                    dicUser.setValue( "" , forKey: "id")
                    dicUser.setValue( "" , forKey: "user_id")
                    dicUser.setValue( "" , forKey: "filename")
                    dicUser.setValue( "\(images.count)" , forKey: "order")
                    
                    if NimUserManager.shared.convertDictionaryToJsonString(dict: dicUser) != "" {
                        
                        if let userImage = getMapper_UserImage(jsonStr: NimUserManager.shared.convertDictionaryToJsonString(dict: dicUser)) {
                            userImage.image = image
                            userImage.order = "\(images.count)"
                            images.append(userImage)
                            self.userProfile.images = images
                            self.updateCollectionCell(indexPath_: IndexPath(item: images.count - 1 , section: 0) , image: image)
                            self.arrNewImages.append(userImage)
                           
//                            if self.arrNewImages.contains(where: { $0.order == "\(index.row + 1)"}) {
//                                self.arrNewImages.filter {$0.order == "\(index.row + 1)"}.first?.image = image
//                            } else {
//                                self.arrNewImages.append(obj)
//                            }
                            
                        }
                    }
                    
                    
                    
                   /* let obj = imageWithId()
                    obj.image = image
                    obj.order = "\(index.row + 1)"
                    
                    if self.arrNewImages.contains(where: { $0.order == "\(index.row + 1)"}) {
                        self.arrNewImages.filter {$0.order == "\(index.row + 1)"}.first?.image = image
                    } else {
                        self.arrNewImages.append(obj)
                    }*/
                }
            }
            
            self.configureChangesApplied()
        }
        imagePicker.dismiss(animated: true, completion: nil)
    }
    
    public func cropViewController(_ cropViewController: CropViewController, didCropToCircularImage image: UIImage, withRect cropRect: CGRect, angle: Int) {
        
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        var visibleRect = CGRect()
        
        visibleRect.origin = collectionVw_userImages.contentOffset
        visibleRect.size = collectionVw_userImages.bounds.size
        
        let visiblePoint = CGPoint(x: visibleRect.midX, y: visibleRect.midY)
        
        guard let indexPath = collectionVw_userImages.indexPathForItem(at: visiblePoint) else { return }
        print(indexPath)
       // pageController.currentPage = indexPath.row
        
    }
}

extension EditProfile_VC: MICountryPickerDelegate {
    
    func countryPicker(_ picker: MICountryPicker, didSelectCountryWithName name: String, code: String, dialCode: String) {
        
        //for cancel
        if code == "cancel" {
            countrySelection.removeAll()
            self.lbl_familyOrigin.text = ""
        }
            //for selection
        else {
            if countrySelection.contains(name){
                countrySelection = countrySelection.filter {$0 != name}
            }else{
                countrySelection.append(name)
            }
            let strSelectedCountries = countrySelection.joined(separator: ", ")
            lbl_familyOrigin.text = strSelectedCountries
            let strCountrycode = countryCode.joined(separator: ", ")
        }
    }
    
    func countryPicker(_ picker: MICountryPicker, didSelectCountryWithName name: String, code: String) {
    }
}


