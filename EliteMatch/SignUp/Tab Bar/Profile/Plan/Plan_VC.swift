//
//  Plan_VC.swift
//  EliteMatch
//
//  Created by Kitlabs-M-0002 on 11/22/18.
//  Copyright © 2018 Kitlabs-M-0002. All rights reserved.
//

import UIKit
import FTIndicator
import PassKit

class Plan_VC: UIViewController {

    //MARK:- Outlets
    @IBOutlet weak var subscriptionSettings_button: UIButton!
    @IBOutlet weak var lbl_monthly: UILabel!
    @IBOutlet weak var lbl_annually: UILabel!
    @IBOutlet weak var switch_plan: UISwitch!
    @IBOutlet weak var imgVw_planFrame: UIImageView!
    @IBOutlet weak var lbl_1: UILabel!
    @IBOutlet weak var lbl_2: UILabel!
    @IBOutlet weak var lbl_3: UILabel!
    @IBOutlet weak var lbl_4: UILabel!
    @IBOutlet weak var lbl_planAmount: UILabel!
    @IBOutlet weak var btnBack: UIButton!
    
    //view membership alert
    @IBOutlet var vw_membership: UIView!
    @IBOutlet weak var vwMembershipInner: UIView!
    @IBOutlet weak var imgTick4: UIImageView!
    
    @IBOutlet weak var testerSkip_button: UIButton!
    //view membership info
    @IBOutlet weak var vwMembershipInfo: UIView!
    @IBOutlet weak var vwMembershipInfoSubView: UIView!
    @IBOutlet weak var lblTitleMembershipInfo: UILabel!
    @IBOutlet weak var lblSubTitleMembershipinfo: UILabel!
    @IBOutlet weak var btnUpgradePlan_membershipIno: UIButton!
    
    //MARK:- Variables
    var isNavigateFromProfileVC:Bool = false
    var isPaymentPending:Bool = false
    
    //MARK:- UIView life-cycle methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if CommonFunctions.shared.isTestAccount() {
            self.testerSkip_button.isHidden = false
        } else {
            self.testerSkip_button.isHidden = true
        }
        //init
       // self.setAnnualPlan()
        self.setMonthlyPlan()
        if self.isPaymentPending {
            self.showMembershipAlert()
        }
        
       if SessionManager.getProfile_lastUpdatedStatus() == UserProfile_lastUpdated.paymentDone {
            self.configureMembershipInfoView()
            ProfileManager.shared.getUserProfile_phoneNo(phoneNo: SessionManager.get_phonenumber()) { (success , user) in
                if let user_ = user {
                    if user_.plan_id == "1" {
                        self.lblTitleMembershipInfo.text = "Monthly Membership"
                        self.btnUpgradePlan_membershipIno.setTitle("Upgrade Membership & Save $24", for: .normal)
                    } else {
                        self.lblTitleMembershipInfo.text = "6-Months Membership"
                        self.btnUpgradePlan_membershipIno.setTitle("Switch to monthly plan", for: .normal)
                    }
                    
                    if let startDate = user_.plan_start_date {
                        self.lblSubTitleMembershipinfo.text = "since " + startDate
                    }
                }
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        if self.isNavigateFromProfileVC {
            self.btnBack.isHidden = false
            self.btnBack.isUserInteractionEnabled = true
        }
    }
    
    @IBAction func testerSkip_buttonAction(_ sender: Any) {
        if CommonFunctions.shared.isTestAccount() {
            UserConfirmationAlertManager.instance.showAlert_testAccount { (success) in
                SessionManager.saveProfileApprovalStatus(status: UserProfileStatus.approved.rawValue)
                self.paymentFinalize()
            }
        }
    }
    
    //MARK:- UIButton Actions
    @IBAction func btn_promoCode(_ sender: UIButton) {
   
    }
    
    @IBAction func cancelMemership_buttonAction(_ sender: Any) {
        guard let settingsUrl = URL(string: UIApplicationOpenSettingsURLString) else {
            return
        }

        if UIApplication.shared.canOpenURL(settingsUrl) {
            UIApplication.shared.open(settingsUrl, completionHandler: { (success) in
                print("Settings opened: \(success)") // Prints true
            })
        }
    }
    
    @IBAction func btn_continue(_ sender: UIButton) {
        //self.openStripeVC()
        
        if self.switch_plan.isOn {
            self.configureApplePay(amount: 5.99)
        } else {
            self.configureApplePay(amount: 9.99)
        }
//
//        if CommonFunctions.shared.isTestAccount() {
//            UserConfirmationAlertManager.instance.showAlert_testAccount { (success) in
//                SessionManager.saveProfileApprovalStatus(status: UserProfileStatus.approved.rawValue)
//                self.paymentFinalize()
//            }
//        }
//        else {
//
//        }
    }
    
    func configureApplePay(amount: NSDecimalNumber){
        let request = PKPaymentRequest()
        request.merchantIdentifier = ApplePay.mechantIdentifier
        request.supportedNetworks = [PKPaymentNetwork.visa, PKPaymentNetwork.masterCard, PKPaymentNetwork.amex]
        request.merchantCapabilities = PKMerchantCapability.capability3DS
        request.countryCode = "US"
        request.currencyCode = "USD"
        
        request.paymentSummaryItems = [
            PKPaymentSummaryItem(label: "NIM Plan", amount: amount)
        ]
        
        let applePayController = PKPaymentAuthorizationViewController(paymentRequest: request)
        applePayController?.delegate = self
        self.present(applePayController!, animated: true, completion: nil)
    }
    
    @IBAction func btn_Nothanks(_ sender: UIButton) {
        self.showMembershipAlert()
    }
    
    @IBAction func switch_plan(_ sender: UISwitch) {
        if !sender.isOn {
           self.setMonthlyPlan()
          //   self.setAnnualPlan()
        } else {
           // self.setMonthlyPlan()

            self.setAnnualPlan()
        }
    }
    
    @IBAction func didPress_back(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK:- Custom Methods
    func configureMembershipInfoView(){
        self.showMembershipInfo()
        vwMembershipInfoSubView.layer.shadowColor = UIColor.lightGray.cgColor
        vwMembershipInfoSubView.layer.shadowOpacity = 1
        vwMembershipInfoSubView.layer.shadowOffset = CGSize.zero
    }
    
    func setMonthlyPlan(){
        self.imgTick4.isHidden = false
        self.switch_plan.isOn = false
        self.switch_plan.tintColor = UIColor.black
        self.switch_plan.layer.cornerRadius = self.switch_plan.frame.height / 2
        self.switch_plan.backgroundColor = UIColor.black
        self.lbl_monthly.textColor = UIColor(red: 67/255, green:179/255, blue: 255/255, alpha: 1.0)
        self.lbl_annually.textColor = UIColor.lightGray
        self.imgVw_planFrame.image = UIImage(named: "monthlyBox")
        self.lbl_planAmount.text = "$9.99/mo"
        self.lbl_1.text = "Send invites to your daily matches"
        self.lbl_2.text = "View Match profile details"
        self.lbl_3.text = "New list of Matches everyday"
        self.lbl_4.text = "Cancel membership anytime"
    }
    
    func setAnnualPlan(){
        self.imgTick4.isHidden = false
        self.switch_plan.isOn = true
        self.switch_plan.onTintColor = UIColor.black
        self.lbl_annually.textColor = UIColor(red: 67/255, green:179/255, blue: 255/255, alpha: 1.0)
        self.lbl_monthly.textColor = UIColor.lightGray
        self.imgVw_planFrame.image = UIImage(named: "AnnuallyBox")
        self.lbl_planAmount.text = "$5.99/mo"
        self.lbl_1.text = "You will be charged $35.99 at each cycle"
        self.lbl_2.text = "Includes all monthly member offers"
        self.lbl_3.text = "Get more Match list"
        self.lbl_4.text = "Standout with Bold Listing"
    }
    
    func showMembershipAlert(){
        self.vw_membership.alpha = 0
        self.vw_membership.layer.cornerRadius = 3.0
        self.add_subView(subView: self.vw_membership)
        UIView.animate(withDuration: 0.1) {
            self.vw_membership.alpha = 1
        }
    }
    
    func hideMembershipAlert(){
        UIView.animate(withDuration: 0.4, animations: {
            self.vw_membership.alpha = 0
        }) { _ in
            self.remove_subView(subView: self.vw_membership)
        }
    }
    
    func showMembershipInfo(){
        self.vwMembershipInfo.isHidden = false
    }
    
    func hideMembershipInfo(){
        self.vwMembershipInfo.isHidden = false
    }
    
    func showTabBar_home(){
        CommonFunctions.shared.initializeUser()
        let initialViewController = self.storyboard?.instantiateViewController(withIdentifier: "TabBarController") as! TabBarController
        self.navigationController?.pushViewController(initialViewController, animated: true)
    }
    
    func paymentFinalize(){
        
        var planID:String = ""
        if self.switch_plan.isOn {
          //  planID = "1"
            planID = "2"
        } else {
          //  planID = "2"
            planID = "1"

        }
        
        PlanManager.shared.updateMembership(startDate: PlanManager.shared.PlanstartDate() , endDate: PlanManager.shared.PlanEndDate() , planID: planID) { (success) in
            
            SessionManager.saveProfile_lastUpdatedStatus(status: UserProfile_lastUpdated.paymentDone.rawValue)
            //updating user profile data
            DispatchQueue.global(qos: .background).async {
                ProfileManager.shared.getUserProfile_phoneNo(phoneNo: SessionManager.get_phonenumber(), completionHandler: { (success, user) in
                })
            }
            self.showTabBar_home()
        }
    }
    
    //MARK:- UIButton Actions - Promo Code
    @IBAction func btn_reedemNow(_ sender: UIButton) {
   
    }
    
    @IBAction func btn_dontHavePromoCode(_ sender: UIButton) {
        
    }
    
    //MARK:- UIButton Actions - Membership
    @IBAction func btn_GotIt(_ sender: UIButton) {
        self.hideMembershipAlert()
    }
    
    //MARK:- UIButton Actions - membership info
    @IBAction func didPress_upgradePlan(_ sender: UIButton) {

    }
    
    @IBAction func didPressVwPurchaseHistory(_ sender: UIButton) {

    }
    
    @IBAction func subscriptionSettings_buttonAction(_ sender: Any) {
        
        guard let settingsUrl = URL(string: UIApplicationOpenSettingsURLString) else {
          return
        }
        if UIApplication.shared.canOpenURL(settingsUrl)  {
          if #available(iOS 10.0, *) {
            UIApplication.shared.open(settingsUrl, completionHandler: { (success) in
            })
          }
          else  {
            UIApplication.shared.openURL(settingsUrl)
          }
        }

    }
}

extension Plan_VC: PKPaymentAuthorizationViewControllerDelegate {
    
    func paymentAuthorizationViewControllerDidFinish(_ controller: PKPaymentAuthorizationViewController) {
        controller.dismiss(animated: true , completion: nil)
    }
    
    func paymentAuthorizationViewController(_ controller: PKPaymentAuthorizationViewController, didAuthorizePayment payment: PKPayment, completion: @escaping (PKPaymentAuthorizationStatus) -> Void) {
        completion(PKPaymentAuthorizationStatus.success)
        self.paymentFinalize()
    }

}


extension Plan_VC {
    
    func add_subView(subView: UIView){
        subView.frame = CGRect(x: 0 , y: 0 , width: self.view.frame.width , height: self.view.frame.height)
        self.view.addSubview(subView)
    }
    
    func remove_subView(subView: UIView){
        subView.removeFromSuperview()
    }
    
}
