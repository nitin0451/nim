//
//  BillingHistory_TC.swift
//  EliteMatch
//
//  Created by osvinuser on 19/03/19.
//  Copyright © 2019 Kitlabs-M-0002. All rights reserved.
//

import UIKit

class BillingHistory_TC: UITableViewCell {

    @IBOutlet weak var billingMonthLbl: UILabel!
    @IBOutlet weak var billingDateLbl: UILabel!
    @IBOutlet weak var billingPriceLbl: UILabel!
    @IBOutlet weak var billingProductNameLbl: UILabel!
    @IBOutlet weak var billingPaymentIcon: UIImageView!
    @IBOutlet weak var billingSucessLbl: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        billingMonthLbl.textColor = UIColor(red: 0/255, green: 0/255, blue: 0/255, alpha: 0.6)
        billingProductNameLbl.textColor = UIColor(red: 0/255, green: 0/255, blue: 0/255, alpha: 0.5)
        billingSucessLbl.textColor = UIColor(red: 70/255, green: 159/255, blue: 227/255, alpha: 01.0)
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
