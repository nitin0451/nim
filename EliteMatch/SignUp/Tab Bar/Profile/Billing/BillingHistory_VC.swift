//
//  BillingHistory_VC.swift
//  EliteMatch
//
//  Created by osvinuser on 19/03/19.
//  Copyright © 2019 Kitlabs-M-0002. All rights reserved.
//

import UIKit

class BillingHistory_VC: UIViewController {
    
    @IBOutlet weak var BillingHistoryTblView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.font: UIFont(name: "CircularStd-Medium", size: 18)!]
        
        BillingHistoryTblView.register(UINib(nibName: "BillingHistory_TC", bundle: nil), forCellReuseIdentifier: "BillingHistory_TC")
        BillingHistoryTblView.delegate = self
        BillingHistoryTblView.dataSource = self
        BillingHistoryTblView.reloadData()
        navigationController?.navigationBar.barTintColor = UIColor.white
        navigationController?.navigationBar.titleTextAttributes = [.foregroundColor: UIColor(red: 54/255.0, green: 54/255.0, blue: 54/255.0, alpha: 1.0)]
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.font: UIFont(name: "CircularStd-Medium", size: 18)!]
        BillingHistoryTblView.tableFooterView = UIView()
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = false
        self.navigationController?.navigationBar.isHidden = false
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = true
    }
    
    @IBAction func actionBillingHistory(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
}

extension BillingHistory_VC : UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 100
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "BillingHistory_TC", for: indexPath) as! BillingHistory_TC
        return cell
        
    }
}

extension BillingHistory_VC : UITableViewDelegate{
    
}
