//
//  ProfileViewController.swift
//  Tamaas
//
//  Created by Krescent Global on 25/01/18.
//  Copyright © 2018 Krescent Global. All rights reserved.
//

import UIKit
import FTIndicator
import PKImagePicker
import Alamofire
import MessageUI

class ProfileViewController: UIViewController , MFMailComposeViewControllerDelegate {
    
    //MARK:- Outlets
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var reportView: UIView!
    @IBOutlet var MainView: UIView!
    @IBOutlet weak var backGroundBlur_View: UIView!
    @IBOutlet weak var writeHere_TxtView: UITextView!
    
    
    //MARK:- Variables
    let arrProfile = ["GENERAL","Billing","Invite","FAQ", "Terms of Services","Report","Terms of use","Privacy Policy","Logout"] // "Membership Plans","Others",
    let arrImages = [UIImage(named: ""),UIImage(named: "billing"),UIImage(named: "invite"),UIImage(named: "FAQ"),UIImage(named: "service"),UIImage(named: "report"),UIImage(named: "service"),UIImage(named: "privacy"),UIImage(named: "logout")] // ,UIImage(named: "membership"), UIImage(named: "report"),
    
    // Mark : - Url
    let termsAndConditionsURL = "https://www.nimapp.co/terms.html"
    let privacyURL            = "https://www.nimapp.co/privacy.html"
    let picker = PKImagePickerViewController()
    var chosenImage : UIImage!
    var isProfileImageUpdated:Bool = false
    private var blurView = UIVisualEffectView(effect: UIBlurEffect(style: .light))
    
    //MARK:- UIView life-cycle methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override  func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.initView()
        if constantVC.AppUpdationsChecks.isProfileUpdated {
            constantVC.AppUpdationsChecks.isProfileUpdated = false
            self.tableView.reloadData()
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
          super.viewDidAppear(animated)
    }
    
    //MARK:- Custom Methods
    func initView(){
        self.navigationController?.navigationBar.barTintColor = .white
        reportView.isHidden = true
        writeHere_TxtView.textContainerInset = UIEdgeInsets(top: 20, left: 20, bottom: 0, right: 0)
        writeHere_TxtView.delegate = self
        writeHere_TxtView.text = "Write Here"
        writeHere_TxtView.textColor = UIColor.lightGray
        self.navigationController?.isNavigationBarHidden = true
    }
    
    //MARK:- UIButton Actions
    @IBAction func action_CancelBtn(_ sender: Any) {
        reportView.isHidden = true
        backGroundBlur_View.removeBlurToBackground(view: self.view, blurView: blurView)
    }
    
    @IBAction func action_SubmitBtn(_ sender: Any) {
        reportView.isHidden = true
        backGroundBlur_View.removeBlurToBackground(view: self.view, blurView: blurView)
        if writeHere_TxtView.text.count > 0 {
            FTIndicator.showSuccess(withMessage: "Report submitted successfully.")
        }
    }
    
    @IBAction func action_EditProfileBtn(_ sender: UIButton) {
//        if !SessionManager.isChaperonUser() {
//            let editProfile = self.storyboard?.instantiateViewController(withIdentifier: "EditProfile_VC") as! EditProfile_VC
//            self.navigationController?.pushViewController(editProfile, animated: true)
//        }
        let editProfile = self.storyboard?.instantiateViewController(withIdentifier: "EditProfile_VC") as! EditProfile_VC
        self.navigationController?.pushViewController(editProfile, animated: true)
    }

    //MARK:- Custom Methods
    func openAlertForNonSingleUser(){
        let alertController = UIAlertController(title: "NIM", message: "Apply as a single member", preferredStyle: UIAlertControllerStyle.alert)
        
        let okAction = UIAlertAction(title: "Settings", style: .default, handler: {(cAlertAction) in
            
            UserConfirmationAlertManager.instance.showAlert(title: "NIM" , subTitle: userMessagestext.signUpAsMemberConfirmation , okTitle: "Ok", cancelTitle: "Cancel") { (success) in
                if success {
                    SessionManager.save_Elite_userID(id: "")
                    SessionManager.save_phonenumber(phoneNumber: "")
                    SessionManager.saveChaperonStatus(isActive: false)
                    
                    CommonFunctions.shared.cleanSessionManager()
                               
                               //Nitin
                    let Welcome = self.storyboard?.instantiateViewController(withIdentifier: "nav_Welcome_VC")
                    let appDelegate = UIApplication.shared.delegate as! AppDelegate
                    appDelegate.window?.rootViewController = Welcome
                    appDelegate.window?.makeKeyAndVisible()
                    
//                    
//                    let Welcome = self.storyboard?.instantiateViewController(withIdentifier: "nav_Welcome_VC")
//                    if let topVC = UIApplication.topViewController() {
//                        topVC.present(Welcome! , animated: true, completion: nil)
//                    }
                }
            }
        })
        
        let cancelAction = UIAlertAction(title: "Cancel" , style: UIAlertActionStyle.cancel) { (action) in
        }
        alertController.addAction(cancelAction)
        alertController.addAction(okAction)
        self.present(alertController, animated: true, completion: nil)
    }
   
    func showImage(url:URL , imgVw:UIImageView) {
        let targetSize: CGSize? = imgVw.bounds.size
        let transform = QMImageTransform(size: targetSize! , isCircle: false)
        
        let cachedImage: UIImage? = QMImageLoader.instance.imageCache?.imageFromCache(forKey: transform.key(with: url))
        
        if cachedImage != nil {
            transform.apply(for: (cachedImage)!) { (img) in
                imgVw.contentMode = .scaleToFill
                imgVw.image = img
            }
        } else {
            
            QMImageLoader.instance.downloadImage(with: url , transform: transform , options: [.highPriority , .continueInBackground , .allowInvalidSSLCertificates ], progress: { (receivedSize , expectedSize , targetURL) in
            }) { (image, transfomedImage , error , cacheType , finished , imageURL) in
                if transfomedImage != nil {
                    imgVw.contentMode = .scaleToFill
                    imgVw.image = transfomedImage
                }
                if error != nil {
                    //FTIndicator.showToastMessage("Error in downloading image")
                }
            }
        }
    }
    
  
}

extension ProfileViewController : UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        self.tableView.deselectRow(at: indexPath, animated: true)
        if indexPath.row == 1 {
            
            if SessionManager.isChaperonUser() {
                self.openAlertForNonSingleUser()
            }
            else {
                let planVc = self.storyboard?.instantiateViewController(withIdentifier: "Plan_VC") as! Plan_VC
                planVc.isNavigateFromProfileVC = true
                self.navigationController?.pushViewController(planVc, animated: true)
            }
        }
//        else if indexPath.row == 2 {
//            if SessionManager.isChaperonUser() {
//                self.openAlertForNonSingleUser()
//            }
//            else {
//                let billingHistory = self.storyboard?.instantiateViewController(withIdentifier: "BillingHistory_VC")
//                self.navigationController?.pushViewController(billingHistory!, animated: true)
//            }
//        }
        else if indexPath.row == 2 {
            let textToShare = "Sign up for NIM.It’s an Exclusive, Halal Matchmaking Service!"
            if let myWebsite = NSURL(string: "https://www.nimapp.co") {
                let objectsToShare = [textToShare, myWebsite] as [Any]
                let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
                
                //New Excluded Activities Code
                activityVC.excludedActivityTypes = [UIActivityType.airDrop, UIActivityType.addToReadingList]
                self.present(activityVC, animated: true, completion: nil)
            }
        }else if indexPath.row == 3 {
            
        }
        else if indexPath.row == 4{
      
            
        } else if indexPath.row == 5{
            backGroundBlur_View.addBlurToBackground(view: backGroundBlur_View, blurView: blurView)
            reportView.isHidden = false
            
        } else if indexPath.row == 6{
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "WebView_VC") as! WebView_VC
                vc.strTitle = "Terms and Conditions"
                vc.strUrl = termsAndConditionsURL
            self.navigationController?.pushViewController(vc, animated: true)
            
        } else if indexPath.row == 7 {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "WebView_VC")
                as! WebView_VC
                vc.strTitle = "Privacy Policy"
                vc.strUrl = privacyURL
            self.navigationController?.pushViewController(vc, animated: true)
            
        } else if indexPath.row == 8 {
            //show logout confirmation alert
            let alertController = UIAlertController(title: "Logout", message: "Are you sure you want to logout?", preferredStyle: .alert)
            let okAction = UIAlertAction(title: "Yes", style: UIAlertActionStyle.default) {
                UIAlertAction in
                CommonFunctions.shared.cleanSessionManager()
                
                //Nitin
                let Welcome = self.storyboard?.instantiateViewController(withIdentifier: "nav_Welcome_VC")
                let appDelegate = UIApplication.shared.delegate as! AppDelegate
                appDelegate.window?.rootViewController = Welcome
                appDelegate.window?.makeKeyAndVisible()

            }
            let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel) {
                UIAlertAction in
            }
            alertController.addAction(okAction)
            alertController.addAction(cancelAction)
            self.present(alertController, animated: true, completion: nil)
        }
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.1
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 320
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0 || indexPath.row == 5 {
            return 40.0
        }
        return 55.0
    }
}

extension ProfileViewController : UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrProfile.count
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let cell = self.tableView.dequeueReusableCell(withIdentifier: "myHeader") as! HeaderCell
        cell.userName.text = SessionManager.getUserName()
        
        if !SessionManager.isChaperonUser() {
            
            ProfileManager.shared.getRefreshImage(url: ProfileManager.shared.getMyProfileUrl() , imageView: cell.userBgImage) { (image) in
                DispatchQueue.main.async {
                    if let img = image {
                        cell.userBgImage.contentMode = .scaleToFill
                        cell.userBgImage.image = img
                    } else {
                        cell.userBgImage.contentMode = .scaleAspectFit
                    }
                }
                
            }
            
            ProfileManager.shared.getRefreshImage(url: ProfileManager.shared.getMyProfileUrl() , imageView: cell.userImage) { (image) in
                if let img = image {
                    cell.userImage.image = img
                }
            }
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0 || indexPath.row == 4 {
            let cell = self.tableView.dequeueReusableCell(withIdentifier: "headingCell", for: indexPath) as! ProfileCell
            cell.lbl_heading.text = self.arrProfile[indexPath.row]
            return cell
        }
        
        let cell = self.tableView.dequeueReusableCell(withIdentifier: "myCell", for: indexPath) as! ProfileCell
        cell.imgVw_option.image = self.arrImages[indexPath.row]
        cell.lbl_option.text = self.arrProfile[indexPath.row]
        return cell
    }
}

class HeaderCell: UITableViewCell {
    
    @IBOutlet weak var aboutUser: UILabel!
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var userBgImage: UIImageView!
    @IBOutlet weak var userImage: UIImageView!
    
    @IBOutlet weak var edit_profile: UIButton!
    @IBOutlet weak var firstContentView: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        edit_profile.setTitle("Edit Profile", for: .normal)
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}

class ProfileCell: UITableViewCell {
    
    //headingCell
    @IBOutlet weak var lbl_heading: UILabel!
    
    //myCell
    @IBOutlet weak var imgVw_option: UIImageView!
    @IBOutlet weak var lbl_option: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
    }

}

extension ProfileViewController: UITextViewDelegate {
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.lightGray {
            textView.text = nil
            textView.textColor = UIColor.black
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = "Write here"
            textView.textColor = UIColor.lightGray
        }
    }
}
