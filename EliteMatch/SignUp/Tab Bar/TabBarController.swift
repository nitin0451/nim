//
//  TabBarController.swift
//  Tamaas
//
//  Created by Krescent Global on 19/01/18.
//  Copyright © 2018 Krescent Global. All rights reserved.
//

import UIKit

class TabBarController: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()
        self.selectedIndex = 0
        //self.setTabBarItems()
        for item in self.tabBar.items!{
            item.selectedImage = item.selectedImage?.withRenderingMode(.alwaysOriginal)
            item.image = item.image?.withRenderingMode(.alwaysOriginal)
        }
        if let tabbarItems = self.tabBar.items {
            for item in tabbarItems {
                item.imageInsets = UIEdgeInsetsMake(0,-3,0,0)
            }
        }
          
        let selectedColor   = UIColor(red: 139.0/255.0, green: 149.0/255.0, blue: 245.0/255.0, alpha: 1.0)
        let unselectedColor = UIColor(red: 146.0/255.0, green: 146.0/255.0, blue: 146.0/255.0, alpha: 1.0)
        
        let attributesUnselected = [NSAttributedString.Key.font:
            UIFont(name: "CircularStd-Book", size: 12.0)!,
                                    NSAttributedString.Key.foregroundColor: unselectedColor] as [NSAttributedStringKey: Any]
        let attributesSelected = [NSAttributedString.Key.font:
            UIFont(name: "CircularStd-Book", size: 12.0)!,
                                  NSAttributedString.Key.foregroundColor: selectedColor] as [NSAttributedStringKey: Any]
        
        if #available(iOS 13, *) {
            let appearance = UITabBarAppearance()
            appearance.stackedLayoutAppearance.selected.titleTextAttributes = [NSAttributedString.Key.foregroundColor: selectedColor]
            tabBar.standardAppearance = appearance
        } else {
            UITabBarItem.appearance().setTitleTextAttributes( attributesUnselected , for: .normal)
            UITabBarItem.appearance().setTitleTextAttributes( attributesSelected , for: .selected)
        }
    }
    
    func setTabBarItems(){

         let myTabBarItem1 = (self.tabBar.items?[0])! as UITabBarItem
         myTabBarItem1.image = UIImage(named: "matchGray")?.withRenderingMode(UIImageRenderingMode.alwaysOriginal)
         myTabBarItem1.selectedImage = UIImage(named: "matchBlue ")?.withRenderingMode(UIImageRenderingMode.alwaysOriginal)
         myTabBarItem1.title = "Matches"
         myTabBarItem1.imageInsets = UIEdgeInsets(top: 0, left: 0, bottom: -3, right: 0)

         let myTabBarItem2 = (self.tabBar.items?[1])! as UITabBarItem
         myTabBarItem2.image = UIImage(named: "msgGray")?.withRenderingMode(UIImageRenderingMode.alwaysOriginal)
         myTabBarItem2.selectedImage = UIImage(named: "msgBlue")?.withRenderingMode(UIImageRenderingMode.alwaysOriginal)
         myTabBarItem2.title = "Messages"
         myTabBarItem2.imageInsets = UIEdgeInsets(top: 0, left: 0, bottom: -3, right: 0)


         let myTabBarItem3 = (self.tabBar.items?[2])! as UITabBarItem
         myTabBarItem3.image = UIImage(named: "circleGray")?.withRenderingMode(UIImageRenderingMode.alwaysOriginal)
         myTabBarItem3.selectedImage = UIImage(named: "circleBlue")?.withRenderingMode(UIImageRenderingMode.alwaysOriginal)
         myTabBarItem3.title = "Circle"
         myTabBarItem3.imageInsets = UIEdgeInsets(top: 0, left: 0, bottom: -3, right: 0)

         let myTabBarItem4 = (self.tabBar.items?[3])! as UITabBarItem
         myTabBarItem4.image = UIImage(named: "profileGray")?.withRenderingMode(UIImageRenderingMode.alwaysOriginal)
         myTabBarItem4.selectedImage = UIImage(named: "profileBlue")?.withRenderingMode(UIImageRenderingMode.alwaysOriginal)
         myTabBarItem4.title = "Profile"
         myTabBarItem4.imageInsets = UIEdgeInsets(top: 0, left: 0, bottom: -3, right: 0)

    }
    
    
}
