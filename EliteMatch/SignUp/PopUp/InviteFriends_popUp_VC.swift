//
//  InviteFriends_popUp_VC.swift
//  EliteMatch
//
//  Created by Apple on 12/09/19.
//  Copyright © 2019 Kitlabs-M-0002. All rights reserved.
//

import UIKit
import MessageUI
import FTIndicator
import ObjectMapper

typealias CompletionHandler = (_ data: Any?) -> Void
class InviteFriends_popUp_VC: UIViewController,MFMessageComposeViewControllerDelegate {

    //MARK:- Outlets
    @IBOutlet weak var vwContainer: UIView!
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var tableView: UITableView!
    
    //MARK:- Variables
    var arrSectionTitle:[String] = ["Friends who are active on NIM" , "Invite Others"]
    var arrSectionTitle_chaperon:[String] = ["Invite Others"]
    
    //selected
    var arrActiveUsers:[QBUUser] = []
    var arrSelectedActiveUser:[QBUUser] = []
    var filteredActiveUser:[QBUUser] = []

    var arrUnRegisteredUsers:[QBAddressBookContact] = []
    var filteredUnRegisteredUser:[QBAddressBookContact] = []
    var arrSelectedUnRegisteredUsers:[QBAddressBookContact] = []

    var dialogID:String = ""
    var dialog:QBChatDialog!
    //search
    var searchActive : Bool = false
    var handler: CompletionHandler?
    
    var activeUsersData = [ActiveUsersModalClass]()
    var arrayActiveUserData = [ActiveUsersModalClass]()
    var filteredActiveUserData = [ActiveUsersModalClass]()
    
    //MARK:- UIView life-cycel Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //init View
        self.addTapGesture_dismissKeyboard()
        self.configureContainerView()
    }
    
    //MARK:-Custom Methods
    func addTapGesture_dismissKeyboard(){
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
    
    func configureContainerView(){
        self.searchBar.delegate = self
        self.vwContainer.layer.cornerRadius = 40.0
        self.vwContainer.clipsToBounds = true
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.getApprovedUsers { (data, dataArray) in
            var arr = [ActiveUsersModalClass]()
            if data {
                if let data = dataArray as? [ActiveUsersModalClass] {
                    arr = data
                }
            }
            self.getNonRegisteredContacts(activeData: arr)
           // self.getRegisteredNIMContacts()
        }
    }
    
    func getRegisteredNIMContacts(){
        ContactManager.shared.retriveRegisteredUsersFromAddressBook(completionHandler: { (users) in
            if var users_ = users {
                
                if users_.contains(ServicesManager.instance().currentUser) {
                    if let index = users_.index(of: ServicesManager.instance().currentUser) {
                        users_.remove(at: index)
                    }
                }
                
                //check for names
                for user in users_ {
                    if user.fullName == nil {
                        if let index = users_.index(of: user) {
                            users_.remove(at: index)
                        }
                    }
                }
                
                self.arrActiveUsers = users_
            }
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
           // self.getNonRegisteredContacts()
        })
    }
    
    func getNonRegisteredContacts(activeData : [ActiveUsersModalClass]){
        
        self.arrUnRegisteredUsers.removeAll()
        
        if SessionManager.isChaperonUser() {
            self.arrUnRegisteredUsers = constantVC.GlobalVariables.arrQBAddressContact
            
        } else {
            for contact_ in constantVC.GlobalVariables.arrQBAddressContact {
                
//                var isUserRegistered:Bool = false
//                for user_ in self.arrActiveUsers {
//                    if let phone = user_.phone {
//                        if phone == contact_.phone {
//                            isUserRegistered = true
//                            break
//                        }
//                    }
//                }

                for data in activeData {
                    if let phone = data.userData?.phoneNumber {
                        if phone == contact_.phone {
                            self.activeUsersData.append(data)
                        } else {
                            if contact_.name != "" {
                                self.arrUnRegisteredUsers.append(contact_)
                            }
                        }
                    }
                }
                
//                if !isUserRegistered && !self.arrUnRegisteredUsers.contains(contact_){
//                    print(contact_)
//                    self.arrUnRegisteredUsers.append(contact_)
//                }
            }
        }
        
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
        
    }
    
    func sendInvitationToCircle(dialog:QBChatDialog , requestTo_:String){
        //send invite to users to get added in my circle
        InvitationManager.instance.sendConnect_circle_invitation(dialogID: self.getDilogID(dialog: dialog), requestTo:requestTo_ , fromName: SessionManager.getUserName()) { (success) in
        }
    }
    
    
    func sendInvitationToCircle_toAdded(requestTo_:String){
        //send invite to users to get added in my circle
        InvitationManager.instance.sendConnect_circle_invitation(dialogID: "" , requestTo:requestTo_ , fromName: SessionManager.getUserName()) { (success) in
        }
    }
    
    func getDilogID(dialog:QBChatDialog) ->String {
        if let id = dialog.id {
            return id
        }
        return ""
    }
    
    func closePopUp(){
        self.dismiss(animated: true, completion: nil)
        
        //Nitin
//        self.willMove(toParentViewController: nil)
//        self.view.removeFromSuperview()
//        self.removeFromParentViewController()
    }
    
    func set_unselectLable(label:UILabel){
        label.backgroundColor = UIColor.white
        label.textColor = constantVC.GlobalColor.lightPurple
        label.text = "+ Add"
    }
    
    func set_SelectLable(label:UILabel){
        label.backgroundColor = constantVC.GlobalColor.lightPurple
        label.textColor = UIColor.white
        label.text = "Added"
    }
    
    func set_unselectLable_unregistered(label:UILabel){
        label.backgroundColor = UIColor.white
        label.textColor = constantVC.GlobalColor.lightPurple
        label.text = "+ Invite"
    }
    
    func set_SelectLable_unregistered(label:UILabel){
        label.backgroundColor = constantVC.GlobalColor.lightPurple
        label.textColor = UIColor.white
        label.text = "Invited"
    }
    
    func sendTextForInvitation(phoneNos: [String]){
       // Nitin
        self.dismiss(animated: true) {
            guard let block = self.handler else {return}
            block(phoneNos)
        }
//        if (MFMessageComposeViewController.canSendText()) {
//            let controller = MFMessageComposeViewController()
//            controller.body = "Hey, download this app, it lets me find you matches 🤲🏼 https://www.nimapp.co"
//            controller.recipients = phoneNos
//            controller.messageComposeDelegate = self
//            if let mainVc = UIApplication.shared.keyWindow?.rootViewController as? TabBarController{
//                if let navVc = mainVc.viewControllers?[0] as? UINavigationController {
//                    if let vc = navVc.childViewControllers[0] as? Matches_VC {
//                        
//                    }
//                }
//            }
//        }
    }
    
    //MARK:- UIButton Actions
    @IBAction func didPress_close(_ sender: UIButton) {
        self.closePopUp()
    }
    
    
    @IBAction func didPress_invite(_ sender: UIButton) {
        
        if self.arrSelectedActiveUser.count > 0 {
            
            var arrOpponentIDS:[NSNumber] = []
            for user_ in self.arrSelectedActiveUser {
                arrOpponentIDS.append(NSNumber(value: user_.id))
            }
            
            //create circle if not created
            DialogManagerNIM.instance.isMyCircleCreated { (success , chatDialog) in
                if success {
                    //join users
                    if let dialog = chatDialog {
                        
                        for user_ in self.arrSelectedActiveUser {
                            self.sendInvitationToCircle(dialog: dialog , requestTo_: "\(user_.externalUserID)")
                        }
                        
                        DialogManagerNIM.instance.addUsersTo_MyCircle(arrOpponentId: arrOpponentIDS , dialog: dialog , completionHandler: { (success) in
                        })
                    }
                } else {
                    //create circle
                    DialogManagerNIM.instance.createMyCircle(users: self.arrSelectedActiveUser , completionHandler: { (success , chatDialog) in
                        if let dialog = chatDialog {
                            for user_ in self.arrSelectedActiveUser {
                                self.sendInvitationToCircle(dialog: dialog , requestTo_: "\(user_.externalUserID)")
                            }
                        }
                    })
                }
            }
        }
        
//        if self.arrSelectedUnRegisteredUsers.count > 0 {
//            var arrPhoneNoToInvite:[String] = []
//
//            if SessionManager.isChaperonUser() {
//
//                for contact_ in self.arrSelectedUnRegisteredUsers {
//
//                        let result = self.isUserRegusteredOnNIM(phNO: contact_.phone)
//                        if result.0 == true {
//                            if let user = result.1 {
//                                self.arrSelectedActiveUser.append(user)
//                            }
//                        }
//                        else {
//                            arrPhoneNoToInvite.append(contact_.phone)
//                        }
//
//                }
//
//                //send invitation to join other's circle
//                for user_ in self.arrSelectedActiveUser {
//                    self.sendInvitationToCircle_toAdded(requestTo_: "\(user_.externalUserID)")
//                }
//
//            } else {
//                for item in self.arrSelectedUnRegisteredUsers {
//                    arrPhoneNoToInvite.append(item.phone)
//                }
//            }
//
//            self.sendTextForInvitation(phoneNos: arrPhoneNoToInvite)   //TODO
//        }
        
        
//        if self.arrayActiveUserData.count > 0 {
//
//            var arrPhoneNoToInvite:[String] = []
//
//            if SessionManager.isChaperonUser() {
//
//                for contact_ in self.arrayActiveUserData {
//                    let contactData = contact_.userData
//                    let result = self.isUserRegusteredOnNIM(phNO: contactData?.phoneNumber ?? "")
//                    if result.0 == true {
//                        if let user = result.1 {
//                            self.arrSelectedActiveUser.append(user)
//                        }
//                    }
//                    else {
//                        arrPhoneNoToInvite.append(contactData?.phoneNumber ?? "")
//                    }
//
//                }
//
//                //send invitation to join other's circle
//                for user_ in self.arrSelectedActiveUser {
//                    self.sendInvitationToCircle_toAdded(requestTo_: "\(user_.externalUserID)")
//                }
//
//            } else {
////                for item in self.arrSelectedUnRegisteredUsers {
////                    arrPhoneNoToInvite.append(item.phone)
////                }
////
//                for item in self.arrayActiveUserData {
//                    let contactData = item.userData
//                    arrPhoneNoToInvite.append(contactData?.phoneNumber ?? "")
//                }
//            }
//
//            self.sendTextForInvitation(phoneNos: arrPhoneNoToInvite)   //TODO
//        }
        
        
        //FTIndicator.showSuccess(withMessage: "Invitation sent successfully!")
        self.closePopUp()
    }
    
    
    func isUserRegusteredOnNIM(phNO:String) ->(Bool , QBUUser?){
        for user_ in self.arrActiveUsers {
            if let phone = user_.phone {
                if phone == phNO {
                    return (true , user_)
                }
            }
        }
        return (false , nil)
    }
    
}


extension InviteFriends_popUp_VC: UITableViewDelegate , UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView.init(frame: CGRect.init(x: 0, y: 0, width: tableView.frame.width, height: 50))
        
        headerView.backgroundColor = UIColor.white
        let label = UILabel()
        label.frame = CGRect.init(x: 5, y: 5, width: headerView.frame.width-10, height: headerView.frame.height-10)
        
        if SessionManager.isChaperonUser() {
            label.text = self.arrSectionTitle[1]
        }
        else {
            label.text = self.arrSectionTitle[section]
        }
        label.font = UIFont.init(name:"CircularStd-Medium",size:15)
        label.textColor = UIColor.black
        headerView.addSubview(label)
        return headerView
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if SessionManager.isChaperonUser() {
            return 1
        }
        return  2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if SessionManager.isChaperonUser() {
            if(searchActive) {
                return self.filteredUnRegisteredUser.count
            }
            return self.arrUnRegisteredUsers.count
        }
        else {
            if(searchActive) {
                if section == 0{
                    return self.filteredActiveUser.count
                }
                 return self.filteredActiveUserData.count
               // return self.filteredUnRegisteredUser.count
            }
            
            if section == 0{
                return self.arrActiveUsers.count
            }
            return self.arrUnRegisteredUsers.count
        }
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 85.0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell" , for: indexPath) as! ChaperonContact_tableCell
        
        if SessionManager.isChaperonUser() {
             if searchActive {
//                if self.filteredActiveUserData.indices.contains(indexPath.row) {
//                    let contact = self.filteredActiveUserData[indexPath.row]
//                    let contactData = contact.userData
//
//                    cell.lblName.text = contactData?.name ?? ""
//                    let strprefix = contactData?.name?.prefix(2)
//                    cell.lblContactStartAlphabet.text = String(strprefix ?? "")
//                    cell.imgVwUser.image = nil
//
//                    cell.imgVwUser.backgroundColor = CommonFunctions.shared.getRandomColor(indexpath: indexPath)
//
//                    if self.arrayActiveUserData.contains(where: {$0.userData?.id ?? "" == contactData?.id ?? ""}) {
//                        self.set_SelectLable_unregistered(label: cell.lblSelection)
//                    } else {
//                        self.set_unselectLable_unregistered(label: cell.lblSelection)
//                    }
//
//                }
                
                if self.filteredUnRegisteredUser.indices.contains(indexPath.row) {
                    let contact = self.filteredUnRegisteredUser[indexPath.row]

                    cell.lblName.text = contact.name
                    let strprefix = contact.name.prefix(2)
                    cell.lblContactStartAlphabet.text = String(strprefix)
                    cell.imgVwUser.image = nil
                    cell.imgVwUser.backgroundColor = CommonFunctions.shared.getRandomColor(indexpath: indexPath)

                    //for selection
                    if self.arrSelectedUnRegisteredUsers.contains(contact) {
                        self.set_SelectLable_unregistered(label: cell.lblSelection)
                    } else {
                        self.set_unselectLable_unregistered(label: cell.lblSelection)
                    }
                }
            }
             else {
//                if self.activeUsersData.indices.contains(indexPath.row) {
//                    let contact = self.activeUsersData[indexPath.row]
//                    let contactData = contact.userData
//
//                    cell.lblName.text = contactData?.name ?? ""
//                    let strprefix = contactData?.name?.prefix(2)
//                    cell.lblContactStartAlphabet.text = String(strprefix ?? "")
//                    cell.imgVwUser.image = nil
//
//                    cell.imgVwUser.backgroundColor = CommonFunctions.shared.getRandomColor(indexpath: indexPath)
//
//                    if self.arrayActiveUserData.contains(where: {$0.userData?.id ?? "" == contactData?.id ?? ""}) {
//                        self.set_SelectLable_unregistered(label: cell.lblSelection)
//                    } else {
//                        self.set_unselectLable_unregistered(label: cell.lblSelection)
//                    }
//
//                }
                if self.arrUnRegisteredUsers.indices.contains(indexPath.row) {
                    let contact = self.arrUnRegisteredUsers[indexPath.row]

                    cell.lblName.text = contact.name
                    let strprefix = contact.name.prefix(2)
                    cell.lblContactStartAlphabet.text = String(strprefix)
                    cell.imgVwUser.image = nil
                    cell.imgVwUser.backgroundColor = CommonFunctions.shared.getRandomColor(indexpath: indexPath)

                    //for selection
                    if self.arrSelectedUnRegisteredUsers.contains(contact) {
                        self.set_SelectLable_unregistered(label: cell.lblSelection)
                    } else {
                        self.set_unselectLable_unregistered(label: cell.lblSelection)
                    }
                }
            }
            
        }
        else {
            if searchActive {
                
                if indexPath.section == 0 {
                    if self.filteredActiveUser.indices.contains(indexPath.row) {
                        let user = self.filteredActiveUser[indexPath.row]
                        
                        cell.lblName.text = user.fullName ?? ""
                        cell.imgVwUser.yy_setImage(with: ProfileManager.shared.getOpponentProfileUrl(opponentNo: user.phone ?? "") , placeholder: ProfileManager.shared.getPlaceHolderUserImage())
                        cell.lblContactStartAlphabet.text = ""
                        
                        //for selection
                        if self.arrSelectedActiveUser.contains(user) {
                            self.set_SelectLable(label: cell.lblSelection)
                        } else {
                            self.set_unselectLable(label: cell.lblSelection)
                        }
                    }
                }
                
                if indexPath.section == 1 {
                    
                    if self.filteredUnRegisteredUser.indices.contains(indexPath.row) {
                        let contact = self.filteredUnRegisteredUser[indexPath.row]

                        cell.lblName.text = contact.name
                        let strprefix = contact.name.prefix(2)
                        cell.lblContactStartAlphabet.text = String(strprefix)
                        cell.imgVwUser.image = nil
                        cell.imgVwUser.backgroundColor = CommonFunctions.shared.getRandomColor(indexpath: indexPath)

                        //for selection
                        if self.arrSelectedUnRegisteredUsers.contains(contact) {
                            self.set_SelectLable_unregistered(label: cell.lblSelection)
                        } else {
                            self.set_unselectLable_unregistered(label: cell.lblSelection)
                        }
                    }
                }
                
                return cell
            }
            
            if indexPath.section == 0 {
                if self.arrActiveUsers.indices.contains(indexPath.row) {
                    let user = self.arrActiveUsers[indexPath.row]
                    
                    cell.lblName.text = user.fullName ?? ""
                    cell.imgVwUser.yy_setImage(with: ProfileManager.shared.getOpponentProfileUrl(opponentNo: user.phone ?? "") , placeholder: ProfileManager.shared.getPlaceHolderUserImage())
                    cell.lblContactStartAlphabet.text = ""
                    
                    //for selection
                    if self.arrSelectedActiveUser.contains(user) {
                        self.set_SelectLable(label: cell.lblSelection)
                    } else {
                        self.set_unselectLable(label: cell.lblSelection)
                    }
                }
            }
            
            if indexPath.section == 1 {
                if self.arrUnRegisteredUsers.indices.contains(indexPath.row) {
                    let contact = self.arrUnRegisteredUsers[indexPath.row]

                    cell.lblName.text = contact.name
                    let strprefix = contact.name.prefix(2)
                    cell.lblContactStartAlphabet.text = String(strprefix)

                    cell.imgVwUser.image = nil
                    cell.imgVwUser.backgroundColor = CommonFunctions.shared.getRandomColor(indexpath: indexPath)

                    //for selection
                    if self.arrSelectedUnRegisteredUsers.contains(contact) {
                        self.set_SelectLable_unregistered(label: cell.lblSelection)
                    } else {
                        self.set_unselectLable_unregistered(label: cell.lblSelection)
                    }
                }
                
            }
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if SessionManager.isChaperonUser() {
            if let selectedCell = tableView.cellForRow(at: indexPath) as? ChaperonContact_tableCell {
                
                var modalObj : ActiveUsersModalClass?
                
                if searchActive {
                    modalObj = self.filteredActiveUserData[indexPath.row]
                } else {
                    modalObj = self.arrayActiveUserData[indexPath.row]
                }
//                var selectedContact = QBAddressBookContact()
//
//                if searchActive {
//                    selectedContact = self.filteredUnRegisteredUser[indexPath.row]
//                } else {
//                    selectedContact = self.arrUnRegisteredUsers[indexPath.row]
//                }
                
//                if self.arrSelectedUnRegisteredUsers.contains(selectedContact) {
//                    if let index = self.arrSelectedUnRegisteredUsers.index(of: selectedContact) {
//                        self.arrSelectedUnRegisteredUsers.remove(at: index)
//                        self.set_unselectLable_unregistered(label: selectedCell.lblSelection)
//                    }
//                } else {
//                    self.arrSelectedUnRegisteredUsers.append(selectedContact)
//                    self.set_SelectLable_unregistered(label: selectedCell.lblSelection)
//                }
                
                if self.arrayActiveUserData.contains(where: {$0.userData?.id ?? "" == modalObj?.userData?.id ?? ""}) {
                    if let index = self.arrayActiveUserData.firstIndex(where: {$0.userData?.id ?? "" == modalObj?.userData?.id ?? ""}) {
                        self.arrayActiveUserData.remove(at: index)
                        self.set_unselectLable_unregistered(label: selectedCell.lblSelection)
                    }
                }  else {
                    if let obj = modalObj {
                        self.arrayActiveUserData.append(obj)
                        self.set_SelectLable_unregistered(label: selectedCell.lblSelection)
                   }
               }

            }
        }
        else {
            if indexPath.section == 0 {
                if let selectedCell = tableView.cellForRow(at: indexPath) as? ChaperonContact_tableCell {
                    
                    var selectedUser = QBUUser()
                    
                    if searchActive {
                        selectedUser = self.filteredActiveUser[indexPath.row]
                    } else {
                        selectedUser = self.arrActiveUsers[indexPath.row]
                    }
                    
                    if self.arrSelectedActiveUser.contains(selectedUser) {
                        if let index = self.arrSelectedActiveUser.index(of: selectedUser) {
                            self.arrSelectedActiveUser.remove(at: index)
                            self.set_unselectLable(label: selectedCell.lblSelection)
                        }
                    } else {
                        self.showAlert()
                        self.arrSelectedActiveUser.append(selectedUser)
                        self.set_SelectLable(label: selectedCell.lblSelection)
                    }
                }
            }
            
            if indexPath.section == 1 {
                if let selectedCell = tableView.cellForRow(at: indexPath) as? ChaperonContact_tableCell {
                    
                    var modalObj : ActiveUsersModalClass?
                    
                    if searchActive {
                        modalObj = self.filteredActiveUserData[indexPath.row]
                    } else {
                        modalObj = self.activeUsersData[indexPath.row]
                    }
                    
                    if self.arrayActiveUserData.contains(where: {$0.userData?.id ?? "" == modalObj?.userData?.id ?? ""}) {
                        if let index = self.arrayActiveUserData.firstIndex(where: {$0.userData?.id ?? "" == modalObj?.userData?.id ?? ""}) {
                            self.arrayActiveUserData.remove(at: index)
                            self.set_unselectLable_unregistered(label: selectedCell.lblSelection)
                        }
                    } else {
                        if let obj = modalObj {
                            self.arrayActiveUserData.append(obj)
                             self.set_SelectLable_unregistered(label: selectedCell.lblSelection)
                        }
                    }

                    
//                    var selectedContact = QBAddressBookContact()
//
//                    if searchActive {
//                        selectedContact = self.filteredUnRegisteredUser[indexPath.row]
//                    } else {
//                        selectedContact = self.arrUnRegisteredUsers[indexPath.row]
//                    }
//
//                    if self.arrSelectedUnRegisteredUsers.contains(selectedContact) {
//                        if let index = self.arrSelectedUnRegisteredUsers.index(of: selectedContact) {
//                            self.arrSelectedUnRegisteredUsers.remove(at: index)
//                            self.set_unselectLable_unregistered(label: selectedCell.lblSelection)
//                        }
//                    } else {
//                        self.arrSelectedUnRegisteredUsers.append(selectedContact)
//                        self.set_SelectLable_unregistered(label: selectedCell.lblSelection)
//                    }
                }
            }
        }
    }
    
    
    func showAlert() {
        let alert = UIAlertController(title: "Hello", message: "Add me to your circle. I want to help ;)! ", preferredStyle: UIAlertControllerStyle.alert)
        let okAction = UIAlertAction(title: "OK", style: .destructive, handler: nil)
        alert.addAction(okAction)
        self.present(alert, animated: true, completion: nil)
    }
    
}


extension InviteFriends_popUp_VC{
    
    func messageComposeViewController(_ controller: MFMessageComposeViewController, didFinishWith result: MessageComposeResult) {
          switch (result) {
            case .cancelled:
                print("Message was cancelled")
                controller.dismiss(animated: true, completion: nil)
            case .failed:
                print("Message failed")
            case .sent:
                print("Message was sent")
             //   FTIndicator.showSuccess(withMessage: "Invitation sent successfully!")
                controller.dismiss(animated: true, completion: nil)
            default:
                controller.dismiss(animated: true, completion: nil)
                break
            }
        
    }
    
}

//MARK:- UISearchBarDelegate
extension InviteFriends_popUp_VC: UISearchBarDelegate {
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        searchActive = true
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        searchActive = false
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchActive = false
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchActive = false
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        filteredActiveUser = arrActiveUsers.filter { ($0.fullName ?? "").lowercased().contains(searchText.lowercased()) }
      //  filteredUnRegisteredUser = arrUnRegisteredUsers.filter { $0.name.lowercased().contains(searchText.lowercased()) }
        filteredActiveUserData = activeUsersData.filter{
            ($0.userData?.name?.lowercased().contains(searchText.lowercased()) ?? false)
        }
        if(filteredActiveUser.count == 0 && filteredActiveUserData.count == 0){
            searchActive = false;
        } else {
            searchActive = true;
        }
        self.tableView.reloadData()
    }
}


extension InviteFriends_popUp_VC: UIGestureRecognizerDelegate {
    
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        
        if (touch.view?.isDescendant(of: self.tableView))! {
            return false
        }
        return true
    }
    
}


extension InviteFriends_popUp_VC {
    
    func getApprovedUsers(completionHandler:@escaping (Bool , NSArray) -> ()){
        
        WebserviceSigleton().GETService(urlString: constantVC.WebserviceName.URL_MATCHED_USER + "\(SessionManager.get_Elite_userID())" ) { (result , error) in
            if let dicResponse = result as NSDictionary? {
                if let msg = dicResponse["msg"] as? String {
                    if msg == responseStatus.success.rawValue {
                        if let data = dicResponse["msg_details"] as? [Dictionary<String,Any>] {
                            var arrayData = [ActiveUsersModalClass]()
                            for obj in data {
                                if let modalObj = Mapper<ActiveUsersModalClass>().map(JSON: obj) {
                                    arrayData.append(modalObj)
                                }
                            }
                            completionHandler(true , arrayData as NSArray)
                        }
                    }
                    else {
                        completionHandler(false , [])
                    }
                }
            } else {
                completionHandler(false , [])
            }
        }
    }
    
}
