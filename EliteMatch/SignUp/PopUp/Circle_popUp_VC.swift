//
//  Circle_popUp_VC.swift
//  EliteMatch
//
//  Created by Apple on 12/09/19.
//  Copyright © 2019 Kitlabs-M-0002. All rights reserved.
//

import UIKit

class Circle_popUp_VC: UIViewController {
    
    
    //MARK:- Outlets
    @IBOutlet weak var vwContainer: UIView!
    @IBOutlet weak var tableView: UITableView!
    
    
    //MARK:- Variables
    var selectedCircles:[QBChatDialog] = []
    
    
    //MARK:- UIView life-cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()

        self.configureContainerView()
    }
    
    //MARK:-Custom Methods
    func configureContainerView(){
        self.vwContainer.layer.cornerRadius = 40.0
        self.vwContainer.clipsToBounds = true
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.getMessageDialogs()
    }
    
    func closePopUp(){
        self.willMove(toParentViewController: nil)
        self.view.removeFromSuperview()
        self.removeFromParentViewController()
    }
    
    
    //MARK:- Dialogs Related Methods
    func getMessageDialogs(){
        
        if let dialogs = self.dialogs() {
            if dialogs.count > 0 {
                self.tableView.reloadData()
            } else {
                
            }
        }
    }
    
    func dialogs() -> [QBChatDialog]? {
        
        if SessionManager.isChaperonUser() {
            var arrDialogs = ServicesManager.instance().chatService.dialogsMemoryStorage.dialogsSortByUpdatedAt(withAscending: false)
            for item in arrDialogs {
                if (item.name?.contains("Circle"))! {
                    
                    
                } else {
                    if let index = arrDialogs.index(of: item) {
                        arrDialogs.remove(at: index)
                    }
                }
            }
            //TODO
            //            if !self.isRequestAcceptedByUser(dialog: item) {
            //                if let index = arrDialogs.index(of: item) {
            //                    arrDialogs.remove(at: index)
            //                }
            //            }
            return arrDialogs
        }
        else {
            var arrDialogs = ServicesManager.instance().chatService.dialogsMemoryStorage.dialogsSortByUpdatedAt(withAscending: false)
            for item in arrDialogs {
                
                if (item.name?.contains("Circle"))! {
                    if item.name == DialogManagerNIM.instance.getMyCircleName() {
                        if let index = arrDialogs.index(of: item) {
                            arrDialogs.remove(at: index)
                        }
                        arrDialogs.insert(item , at: 0)
                    }
                    /*else if !self.isRequestAcceptedByUser(dialog: item) {  TODO
                     if let index = arrDialogs.index(of: item) {
                     arrDialogs.remove(at: index)
                     }
                     }*/
                }else {
                    if let index = arrDialogs.index(of: item) {
                        arrDialogs.remove(at: index)
                    }
                }
            }
            return arrDialogs
        }
        
    }
    
    func isRequestAcceptedByUser(dialog: QBChatDialog) ->Bool{
        if let customData = dialog.data {
            if let strDialogData = customData["dialogData"] as? String {
                if let dic = CommonFunctions.shared.jsonToDic(jsonString: strDialogData) {
                    if let dicChaperon = dic["CircleRequestAccepted"] as? NSDictionary {
                        if let phoneNos = dicChaperon.object(forKey: "phoneNumber") as? String {
                            if phoneNos.contains(SessionManager.get_phonenumber()) {
                                return true
                            }
                        }
                    }
                }
            }
        }
        return false
    }
    
    //MARK:- UIButton Actions
    @IBAction func didPress_close(_ sender: Any) {
        self.closePopUp()
    }
    

    @IBAction func didPress_send(_ sender: UIButton) {
    }
    
}




extension Circle_popUp_VC: UITableViewDelegate , UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let dialogs = self.dialogs() {
            return dialogs.count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60.0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell" , for: indexPath) as! MatchingFor_tableCell
        
        guard let chatDialog = self.dialogs()?[indexPath.row] else {
            return cell
        }
        
        if let name = chatDialog.name {
            let arrName = name.components(separatedBy: "-")
            if arrName.indices.contains(0){
                cell.lblTitle.text = arrName[0]
            }
            
            if arrName.indices.contains(1){
                cell.userImage.yy_setImage(with: ProfileManager.shared.getOpponentProfileUrl(opponentNo: arrName[1]) , placeholder: ProfileManager.shared.getPlaceHolderUserImage())
            }
        }
        
        if self.selectedCircles.contains(chatDialog) {
            cell.imgSelection.image = UIImage(named: "circlePurple")
        } else {
            cell.imgSelection.image = UIImage(named: "circleGray")
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if let selectedCell = tableView.cellForRow(at: indexPath) as? MatchingFor_tableCell {
            guard let chatDialog = self.dialogs()?[indexPath.row] else {
                return
            }
            
            if self.selectedCircles.contains(chatDialog) {
                
                if let index = self.selectedCircles.index(of: chatDialog) {
                    self.selectedCircles.remove(at: index)
                    selectedCell.imgSelection.image = UIImage(named: "circleGray")
                }
            } else {
                self.selectedCircles.append(chatDialog)
                selectedCell.imgSelection.image = UIImage(named: "circlePurple")
            }
        }
    }
}

