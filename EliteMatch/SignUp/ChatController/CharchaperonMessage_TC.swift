//
//  Message_TableViewCell.swift
//  EliteMatch
//
//  Created by osvinuser on 07/02/19.
//  Copyright © 2019 Kitlabs-M-0002. All rights reserved.
//

import UIKit
import SwipeCellKit

class CharchaperonMessage_TC: SwipeTableViewCell {
    
    @IBOutlet weak var messageCount: UILabel!
    @IBOutlet weak var messgeDeliveryTime: UILabel!
    @IBOutlet weak var lastMessage: UILabel!
    @IBOutlet weak var UserName: UILabel!
    @IBOutlet weak var UserProfileImge: UIImageView!
    @IBOutlet weak var otherUserProfileImage: UIImageView!
    @IBOutlet weak var starIcon: UIImageView!
    
    var user1No:String = ""
    var user2No:String = ""
    
    var dialog: QBChatDialog? {
        didSet {
            self.UserName.text = ""
            self.UserProfileImge.image = nil
            updateView()
        }
    }
    
    
    func updateView() {
        
        if let gname = dialog?.name {
            let arrNo = gname.components(separatedBy: "-")
            
            if arrNo.indices.contains(0) {
                self.user1No = arrNo[0]
                self.UserProfileImge.yy_setImage(with: ProfileManager.shared.getOpponentProfileUrl(opponentNo: arrNo[0]) , placeholder: ProfileManager.shared.getPlaceHolderUserImage())
            }
            
            if arrNo.indices.contains(1) {
                self.user2No = arrNo[1]
                self.otherUserProfileImage.yy_setImage(with: ProfileManager.shared.getOpponentProfileUrl(opponentNo: arrNo[1]) , placeholder: ProfileManager.shared.getPlaceHolderUserImage())
            }
        }
        
        DialogManagerNIM.instance.getUserName_fromNo(number: self.user1No) { (name1) in
            DialogManagerNIM.instance.getUserName_fromNo(number: self.user2No) { (name2) in
                self.UserName.text = name1 + " & " + name2
            }
        }
        if let lastMsg = self.dialog?.lastMessageText {
            self.getLastMsg(msgText: lastMsg , completionHandler: { (lastMsgText) in
                self.lastMessage.text = lastMsgText
            })
        }
        if let unreadCount = dialog?.unreadMessagesCount {
            if unreadCount > 0 {
                self.messageCount.isHidden = false
                self.messageCount.text = "\(unreadCount)"
                self.lastMessage.font = UIFont.init(name:"CircularStd-Medium",size:12)
            } else {
                self.messageCount.isHidden = true
                self.lastMessage.font = UIFont.init(name:"CircularStd-Book",size:12)
            }
        }
        if let date = dialog?.updatedAt {
            if let formattedDate = QMDateUtils.formattedShortDateString(date) {
                self.messgeDeliveryTime.text = formattedDate
            }
        }
    }
    
    
    func getLastMsg(msgText:String , completionHandler:@escaping (String) -> ()){
        
        //define last message
        if msgText == "Image attachment" || msgText == "Audio attachment" || msgText == "Video attachment"{
            completionHandler("Media")
        }
        else {
            completionHandler(msgText)
        }
    }
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        UserProfileImge.layer.cornerRadius = UserProfileImge.frame.size.width/2
        messageCount.layer.cornerRadius = messageCount.frame.size.width/2
        messageCount.clipsToBounds = true
        UserProfileImge.clipsToBounds = true
        otherUserProfileImage.layer.cornerRadius = otherUserProfileImage.frame.size.width/2
        otherUserProfileImage.clipsToBounds = true
        UserName.textColor = UIColor(red: 54/255, green: 54/255, blue: 54/255, alpha: 1.0)
        
        messgeDeliveryTime.textColor = UIColor(red: 70/255, green: 159/255, blue: 227/255, alpha: 1.0)
        messageCount.backgroundColor = UIColor(red: 70/255, green: 159/255, blue: 227/255, alpha: 1.0)
        messageCount.textColor = UIColor.white
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
