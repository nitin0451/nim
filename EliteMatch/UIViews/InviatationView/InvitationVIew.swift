//
//  InvitationVIew.swift
//  EliteMatch
//
//  Created by Apple on 22/12/19.
//  Copyright © 2019 Kitlabs-M-0002. All rights reserved.
//

import UIKit

class InvitationVIew: UIView {

    //MARK:- IBOutlet
    @IBOutlet weak var userName_button: UIButton!
    @IBOutlet weak var name_label: UILabel!
    @IBOutlet weak var notNow_button: UIButton!
    @IBOutlet weak var acceptNow_button: UIButton!
    @IBOutlet weak var viewProfile_button: UIButton!
    @IBOutlet weak var cross_button: UIButton!
 //   @IBOutlet weak var message_label: UILabel!
    @IBOutlet weak var chapron_imageView: UIImageView!

    var userData: UserProfile? {
        didSet {
            showData()
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        loadViewFromNib()
    }
          
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        loadViewFromNib ()
    }
      
    func loadViewFromNib() {
        let viewheader = UINib(nibName: "InvitationView", bundle: Bundle(for: type(of: self))).instantiate(withOwner: self, options: nil)[0] as! UIView
        viewheader.frame = bounds
        viewheader.autoresizingMask = [.flexibleWidth, .flexibleHeight]
           self.addSubview(viewheader)
    }
    
    func showData() {
        guard let data = userData else {return}
        if let name = data.name {
            self.name_label.text = name + " wants to connect with you"
        }
        
        if let no = data.phoneNumber {
         ProfileManager.shared.getRefreshImage(url: ProfileManager.shared.getOpponentProfileUrl(opponentNo: no) , imageView: self.chapron_imageView) { (image) in
             if let img = image {
                 self.chapron_imageView.image = img
             }
           }
        }
       
//        if let images = data.images,images.count>0 {
//            if let image = images[0].image {
//                self.chapron_imageView.image = image
//            }
//        }
    }
    
}
