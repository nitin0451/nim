//
//  MatchView.swift
//  EliteMatch
//
//  Created by Apple on 22/12/19.
//  Copyright © 2019 Kitlabs-M-0002. All rights reserved.
//

import UIKit

class MatchView: UIView {

    //MARK:- IBOutlet
    @IBOutlet weak var cross_button: UIButton!
    @IBOutlet weak var male_imageView: UIImageView!
    @IBOutlet weak var female_imageView: UIImageView!
    @IBOutlet weak var notNow_button: UIButton!
    @IBOutlet weak var startConversation_button: UIButton!
    @IBOutlet weak var viewProfile_button: UIButton!
 
    var userData: UserProfile? 
    var toData: Dictionary<String,Any>? 
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        loadViewFromNib()
    }
       
    required init?(coder aDecoder: NSCoder) {
       super.init(coder: aDecoder)
       loadViewFromNib ()
    }
   
    func loadViewFromNib() {
        let viewheader = UINib(nibName: "MatchView", bundle: Bundle(for: type(of: self))).instantiate(withOwner: self, options: nil)[0] as! UIView
        viewheader.frame = bounds
        viewheader.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        self.addSubview(viewheader)
    }
    
    func showData() {
        guard let data = userData else {return}
        
        if let no = data.phoneNumber {
         ProfileManager.shared.getRefreshImage(url: ProfileManager.shared.getOpponentProfileUrl(opponentNo: no) , imageView: self.female_imageView) { (image) in
             if let img = image {
                 self.female_imageView.image = img
             }
           }
        }
        
        guard let toData = toData else {return}
        guard let number = toData["phoneNumber"] as? String else {return}
        
         ProfileManager.shared.getRefreshImage(url: ProfileManager.shared.getOpponentProfileUrl(opponentNo: number) , imageView: self.female_imageView) { (image) in
                if let img = image {
                    self.male_imageView.image = img
                }
         }
    }
}
