//
//  AppDelegate.swift
//  EliteMatch
//
//  Created by Kitlabs-M-0002 on 11/15/18.
//  Copyright © 2018 Kitlabs-M-0002. All rights reserved.
//

import UIKit
import IQKeyboardManager
import LinkedinSwift
import DropDown
import Firebase
import FBSDKLoginKit
import ObjectMapper
import ObjectiveC
import FTIndicator

@UIApplicationMain

class AppDelegate: UIResponder, UIApplicationDelegate , UNUserNotificationCenterDelegate , NotificationServiceDelegate {

    var window: UIWindow?
    let storyboard = UIStoryboard(name: "Main", bundle: nil)
    var invitationView : InvitationVIew?
    var matchView : MatchView?
    
    private var AssociatedObjectHandle: UInt8 = 0

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        //init
        CommonFunctions.shared.enableQBKeyboard()
        DropDown.startListeningToKeyboard()
        WebserviceSigleton().webservice_getDropDownData()
        FirebaseApp.configure()
        Messaging.messaging().delegate = self
        self.configureQuickBlox()
        
        //register push notification
        self.registerPushNotifications()
        
        //set initial VC
       self.navigateToInitialView_withCheck()
        
        if !self.isForceUpdate() {
            DispatchQueue.main.asyncAfter(deadline: .now() + 1.0, execute: {
                self.addUpdateAlert()
            })
        }
        
        if let rootView = self.window?.rootViewController as? TabBarController {
            print(rootView)
        }
        
       return ApplicationDelegate.shared.application(application , didFinishLaunchingWithOptions: launchOptions)
    }
    
    
    func registerPushNotifications() {
        
        if #available(iOS 10.0, *) {
            let app = UIApplication.shared
            let center = UNUserNotificationCenter.current()
            center.delegate = self
            center.requestAuthorization(options: [.sound, .alert, .badge], completionHandler: { granted, error in
                if let error = error {
                    debugPrint("\(String(describing: error.localizedDescription))")
                    return
                }
                center.getNotificationSettings(completionHandler: { settings in
                    if settings.authorizationStatus == .authorized {
                        DispatchQueue.main.async(execute: {
                            app.registerForRemoteNotifications()
                        })
                    }
                })
            })
        }
        else {
            let userNotificationTypes: UIUserNotificationType = [.alert, .badge, .sound]
            let settings = UIUserNotificationSettings(types: userNotificationTypes, categories: nil)
            UIApplication.shared.registerUserNotificationSettings(settings)
            UIApplication.shared.registerForRemoteNotifications()
        }
    }
    
    //MARK:- Needs Update
   func isForceUpdate() -> Bool {
       
    if ReachabilityManager.isConnectedToNetwork() {
           
           guard let infoDictionary = Bundle.main.infoDictionary else { return false }
           guard let appID = infoDictionary["CFBundleIdentifier"] else {
               return false
           }
           guard let url = URL(string: "http://itunes.apple.com/lookup?bundleId=\(appID)") else {
               return false
           }
           do {
               guard let dictObject = (try Data(contentsOf: url)).getDictionaryFromData() else {
                   return false
               }
               if let resultCount = dictObject["resultCount"] as? Int, resultCount == 1 {
                   if let result = dictObject["results"] as? [Dictionary<String,Any>] {
                       if let appStoreVersion = result[0]["version"] {
                           
                           guard let currentVersion = infoDictionary["CFBundleShortVersionString"] as? NSString else {
                               return false
                           }
                           
                           guard let versionAppStore = appStoreVersion as? String else {
                               return false
                           }
                           
                           if versionAppStore.compare(currentVersion as String, options: .numeric) == .orderedDescending  {
                               print("store version is newer")
                               return false
                           } else {
                               print("store version is Older")
                               return true
                           }
                       }
                   }
               }
           } catch {
               print(error.localizedDescription)
           }
           return true
       } else {
           
//               let alert = UIAlertController(title: "Message", message: AKErrorHandler.CommonErrorMessages.NO_INTERNET_AVAILABLE, preferredStyle: .alert)
//
//               alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
//               DispatchQueue.main.async {
//                   self.window?.rootViewController?.present(alert, animated: true, completion: nil)
//               }
           return true
           
       }
   }
    
    
    //Alert to be shown  while updating the app
    func addUpdateAlert() {
        
        let alert = UIAlertController(title: constantVC.appTitle.alertTitle, message: "There is an upgraded version for this app, please upgrade your app to latest version.", preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "Update", style: UIAlertAction.Style.default, handler:{ (ACTION :UIAlertAction!)in
            
            guard let url  = URL(string: constantVC.appTitle.liveURL) else {
                return
            }
            UIApplication.shared.open(url)
            self.window?.rootViewController?.present(alert, animated: true, completion: nil)
        }))
        self.window?.rootViewController?.present(alert, animated: true, completion: nil)
    }
    
    func navigateToInitialView_withCheck(){
        
        if SessionManager.isChaperonUser() {
            
            if let value = UserDefaults.standard.value(forKey: "Permission") as? Bool,value{
                self.updateAddressBook()
            }

            let initialViewController = self.storyboard.instantiateViewController(withIdentifier: "TabBarController") as! TabBarController
            self.window?.rootViewController = initialViewController
            self.window?.makeKeyAndVisible()
        }
        else if SessionManager.get_phonenumber() == "" || SessionManager.getProfileApprovalStatus() == UserProfileStatus.notSent {
            let Welcome = self.storyboard.instantiateViewController(withIdentifier: "nav_Welcome_VC")
            self.window?.rootViewController = Welcome
            self.window?.makeKeyAndVisible()
        }
        else if SessionManager.getProfileApprovalStatus() == UserProfileStatus.pending {
            let MatchSignUpConfirmation_VC = self.storyboard.instantiateViewController(withIdentifier: "Match_SignUp_Confirmation_VC") as! Match_SignUp_Confirmation_VC
            let ObjNavi = UINavigationController(rootViewController: MatchSignUpConfirmation_VC)
            self.window?.rootViewController = ObjNavi
            self.window?.makeKeyAndVisible()
        }
        else if SessionManager.getProfileApprovalStatus() == UserProfileStatus.approved {
            
            if SessionManager.getProfile_lastUpdatedStatus() == UserProfile_lastUpdated.step1 {
                let MatchSignUpConfirmation_VC = self.storyboard.instantiateViewController(withIdentifier: "Match_SignUp_Confirmation_VC") as! Match_SignUp_Confirmation_VC
                MatchSignUpConfirmation_VC.isProfileApproved = true
                let ObjNavi = UINavigationController(rootViewController: MatchSignUpConfirmation_VC)
                self.window?.rootViewController = ObjNavi
                self.window?.makeKeyAndVisible()
            }
            else if SessionManager.getProfile_lastUpdatedStatus() == UserProfile_lastUpdated.step2 {
                let MatchSignUpDetail = self.storyboard.instantiateViewController(withIdentifier: "Match_SignUpDetailFinal_VC") as! Match_SignUpDetailFinal_VC
                MatchSignUpDetail.isRootVC = true
                let ObjNavi = UINavigationController(rootViewController: MatchSignUpDetail)
                self.window?.rootViewController = ObjNavi
                self.window?.makeKeyAndVisible()
            }
            else if SessionManager.getProfile_lastUpdatedStatus() == UserProfile_lastUpdated.step3 {
                
                /*if SessionManager.get_myProfileGender_isMale() {
                    self.updateAddressBook()
                    let initialViewController = self.storyboard.instantiateViewController(withIdentifier: "TabBarController") as! TabBarController
                    self.window?.rootViewController = initialViewController
                    self.window?.makeKeyAndVisible()
                    
                } else {
                    let plan_VC = self.storyboard.instantiateViewController(withIdentifier: "Plan_VC") as! Plan_VC
                    plan_VC.isPaymentPending = true
                    let ObjNavi = UINavigationController(rootViewController: plan_VC)
                    self.window?.rootViewController = ObjNavi
                    self.window?.makeKeyAndVisible()
                }*/
                //Nitin Check 
                self.updateAddressBook()
                let initialViewController = self.storyboard.instantiateViewController(withIdentifier: "TabBarController") as! TabBarController
                self.window?.rootViewController = initialViewController
                self.window?.makeKeyAndVisible()
//
//                let plan_VC = self.storyboard.instantiateViewController(withIdentifier: "Plan_VC") as! Plan_VC
//                plan_VC.isPaymentPending = true
//                let ObjNavi = UINavigationController(rootViewController: plan_VC)
//                self.window?.rootViewController = ObjNavi
//                self.window?.makeKeyAndVisible()
            }
            else if SessionManager.getProfile_lastUpdatedStatus() == UserProfile_lastUpdated.paymentDone {
                
                if let value = UserDefaults.standard.value(forKey: "Permission") as? Bool,value{
                            self.updateAddressBook()
                        }
                let initialViewController = self.storyboard.instantiateViewController(withIdentifier: "TabBarController") as! TabBarController
                self.window?.rootViewController = initialViewController
                self.window?.makeKeyAndVisible()
            }
        }
    }
    
    func updateAddressBook(){
        DispatchQueue.global(qos: .background).async {
            ContactManager.shared.getAddressBookAndUpdate(completionHandler: { (success) in
            })
        }
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
        AppEvents.activateApp()
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
        UIApplication.shared.applicationIconBadgeNumber = 0
        ReachabilityManager.shared.stopMonitoring()
        
        // Logging in to chat.
        ServicesManager.instance().chatService.connect { (error) in
            if error == nil {
                print("CHAT CONNECTED")
            } else {
                print(error?.localizedDescription)
                print("CHAT NOT CONNECTED")
            }
        }
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        UIApplication.shared.applicationIconBadgeNumber = 0
        ReachabilityManager.shared.startMonitoring()
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        // Logging out from chat.
        SessionManager.instance.oldTime = Date()
        ServicesManager.instance().chatService.disconnect(completionBlock: nil)
    }
    
    func application(_ app: UIApplication, open url: URL, options: [UIApplicationOpenURLOptionsKey : Any] = [:]) -> Bool {
        if LinkedinSwiftHelper.shouldHandle(url) {
            return LinkedinSwiftHelper.application(app, open: url, sourceApplication: options[UIApplicationOpenURLOptionsKey.sourceApplication] as? String, annotation: options[UIApplicationOpenURLOptionsKey.annotation])
        }
        if Auth.auth().canHandle(url) {
            return true
        }
        return ApplicationDelegate.shared.application(app , open: url , options: options)
    }
    
    //MARK:- Push Notifications
   
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        // Pass device token to auth
        
        let tokenParts = deviceToken.map { data -> String in
            return String(format: "%02.2hhx", data)
        }
        
        let deviceTokenString_ = tokenParts.joined()
        print("device token" , deviceTokenString_)
        
        Core.instance.registerForRemoteNotifications(withDeviceToken: deviceToken)
        Auth.auth().setAPNSToken(deviceToken, type: .prod)
        
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification notification: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        
        if Auth.auth().canHandleNotification(notification) {
            completionHandler(.noData)
            return
        }
    }
    
    //MARK:- UNUserNotificationCenterDelegate
    //for displaying notification when app is in foreground
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        
        if let userInfo:NSDictionary = notification.request.content.userInfo as? NSDictionary {
            print("userInfo" , userInfo)
            
            if let aps = userInfo["aps"] as? Dictionary<String,AnyObject> {
                if let alert = aps["alert"] as? Dictionary<String,AnyObject> {
                    if let title = alert["title"] as? String,title == "New Request" {
//                        if let userDataArr = userInfo["from_userdata"] as? [Dictionary<String,AnyObject>] {
//                            let userData = userDataArr[0]
//                            let data = UserProfile(JSON: userData)
//                            self.showInvitationView(fromData: data, toData: nil)
//                            return
//                        }
                    } else if let title = alert["title"] as? String, title == "Request Accepted" {
                        
                        
                        
                    }
                }
            }
            //profile approved
            if let aps = userInfo.object(forKey: "aps") as? NSDictionary {
                if let alert = aps.object(forKey: "alert") as? NSDictionary {
                    
                    if let title = alert.object(forKey: "title") as? String {
                        
                        if title == "Profile verified" {
                            SessionManager.saveProfileApprovalStatus(status: UserProfileStatus.approved.rawValue)
                            
                            let MatchSignUpConfirmation_VC = self.storyboard.instantiateViewController(withIdentifier: "Match_SignUp_Confirmation_VC") as! Match_SignUp_Confirmation_VC
                            MatchSignUpConfirmation_VC.isProfileApproved = true
                            let ObjNavi = UINavigationController(rootViewController: MatchSignUpConfirmation_VC)
                            self.window?.rootViewController = ObjNavi
                            self.window?.makeKeyAndVisible()
                        }
                    }
                }
                
                //for new user notification
                if let flag = aps.object(forKey: "flag") as? String {
                    //new user
                    if flag == "newUser" {
                        completionHandler([.alert, .badge, .sound])
                    }
                }
            }
            
            
            //clean cache
            if let flag = userInfo.object(forKey: "flag") as? String {
                 if flag == "profileUpdate" {
                    if let strUrl = userInfo.object(forKey: "imgUrl") as? String {
                        ProfileManager.shared.clearMyProfileCache_onrequest(strUrl: strUrl)
                        completionHandler([ ])
                    }
                }
            }
            
            
            
            //chat
            if let chatFlag = userInfo["flag"] as? String {
                
                guard let dialogID = userInfo["dialog_id"] as? String else {
                    return
                }
                
                guard !dialogID.isEmpty else {
                    return
                }
                
                let dialogWithIDWasEntered: String? = ServicesManager.instance().currentDialogID
                if dialogWithIDWasEntered == dialogID {
                    return
                }
            }
        }
        completionHandler([.alert, .badge, .sound])
    }
    
    

    // For handling tap and user actions
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        
        print(response.notification.request.content.userInfo)
        
        if let userInfo:NSDictionary = response.notification.request.content.userInfo as? NSDictionary {
            if let aps = userInfo["aps"] as? Dictionary<String,AnyObject> {
                if let alert = aps["alert"] as? Dictionary<String,AnyObject> {
                    if let title = alert["title"] as? String,title == "New Request" {
                        if let userDataString = userInfo["users_data"] as? String {
                            guard let userDataDict = self.convertStringIntoArray(str: userDataString) else {return }
                            guard let fromData = userDataDict["from"] as? Dictionary<String,Any> else {return}
                            guard let toData = userDataDict["to"] as? Dictionary<String,Any> else {return}
                            let data = UserProfile(JSON: fromData)
                            self.showInvitationView(fromData: data, toData: toData)
                        }
                    } else if let title = alert["title"] as? String, title == "Request Accepted" {
                       if let userDataString = userInfo["users_data"] as? String {
                            guard let userDataDict = self.convertStringIntoArray(str: userDataString) else {return }
                            guard let fromData = userDataDict["from"] as? Dictionary<String,Any> else {return}
                            guard let toData = userDataDict["to"] as? Dictionary<String,Any> else {return}
                            let data = UserProfile(JSON: fromData)
                            self.showMatchView(fromData: data, toData: toData)
                        }
                    } else if let title = alert["title"] as? String,title == "Profile verified" {
                        SessionManager.saveProfileApprovalStatus(status: UserProfileStatus.approved.rawValue)
                        
                         let MatchSignUpConfirmation_VC = self.storyboard.instantiateViewController(withIdentifier: "Match_SignUp_Confirmation_VC") as! Match_SignUp_Confirmation_VC
                         MatchSignUpConfirmation_VC.isProfileApproved = true
                         let ObjNavi = UINavigationController(rootViewController: MatchSignUpConfirmation_VC)
                         self.window?.rootViewController = ObjNavi
                         self.window?.makeKeyAndVisible()
                        
                    }
                }
            }
            
            if let aps = userInfo["aps"] as? NSDictionary {
                //for new user notification
                if let flag = aps["flag"] as? String, flag == "newUser"{
                 //new user
                    if let user_name = aps["user_name"] as? String , let userID = aps["quickbloxID"] as? String {
                        self.ConnectChatToNewUser(userID: userID , phoneNo: user_name)
                        return
                    }
                }
            }
            
            //profile notification
            if let flag = userInfo["flag"] as? String {
                
                /* if flag == "profile" {
                 let notiVC = UIStoryboard(name:"Main", bundle: nil).instantiateViewController(withIdentifier: "NavNotification_VC") as! UINavigationController
                 constantVC.ActiveDataSource.isNavigateNotification = true
                 self.window?.rootViewController = notiVC
                 self.window?.makeKeyAndVisible()
                 }*/
                
            }
            
            if let chatFlag = userInfo["flag"] as? String {
                
                guard let dialogID = userInfo["dialog_id"] as? String else {
                    return
                }
                
                guard !dialogID.isEmpty else {
                    return
                }
                
                let dialogWithIDWasEntered: String? = ServicesManager.instance().currentDialogID
                if dialogWithIDWasEntered == dialogID {
                    return
                }
                
                ServicesManager.instance().notificationService.pushDialogID = dialogID
                
                // calling dispatch async for push notification handling to have priority in main queue
                DispatchQueue.main.async {
                ServicesManager.instance().notificationService.handlePushNotificationWithDelegate(delegate: self)
                }
            }
            
        }
        completionHandler()
    }
    
    func ConnectChatToNewUser(userID: String , phoneNo:String){
        
        ServicesManager.instance().chatService.connect { (error) in
            if error == nil && userID != "" {
                
                ServicesManager.instance().chatService.createPrivateChatDialog(withOpponentID: UInt(userID) ?? 0 , data: phoneNo + "-" + SessionManager.get_phonenumber() , completion: { (response , chatDialog) in
                    
                    if response.isSuccess {
                        
                        if let dialog_ = chatDialog {
                            NimUserManager.shared.sendSystemMsg_updateDialog_toOccupants(dialog: dialog_)
                            self.moveToChatController(chatDialog: dialog_)
                        }
                    }
                    else {
                        print(response.error.debugDescription)
                    }
                })
            } else {
                print(error.debugDescription)
            }
        }
        
    }
   
    //MARK:- Quickblox
    func configureQuickBlox() {
        
        QBSettings.applicationID = QuickBloxAccount.kQBApplicationID
        QBSettings.authKey = QuickBloxAccount.kQBAuthKey
        QBSettings.authSecret = QuickBloxAccount.kQBAuthSecret
        QBSettings.accountKey = QuickBloxAccount.kQBAccountKey
        
        //endpoints
        QBSettings.apiEndpoint = QuickBloxAccount.kQBApiEndPoint
        QBSettings.chatEndpoint = QuickBloxAccount.kQBchatEndPoint
        
        //server
        let urls = ["stun:turn.quickblox.com",
                    "turn:turn.quickblox.com:3478?transport=udp",
                    "turn:turn.quickblox.com:3478?transport=tcp"]
        
        if let server = QBRTCICEServer.init(urls: urls,
                                            username: "quickblox",
                                            password: "baccb97ba2d92d71e26eb9886da5f1e0") {
            QBRTCConfig.setICEServers([server])
        }
        
        QBSettings.carbonsEnabled = true
        QBSettings.autoReconnectEnabled = true
        QBSettings.logLevel = QBLogLevel.debug
        QBSettings.enableXMPPLogging()
        QBSettings.keepAliveInterval = 10.0
        QBRTCConfig.setAnswerTimeInterval(TimeIntervalConstant.answerTimeInterval)
        QBRTCConfig.setDialingTimeInterval(TimeIntervalConstant.dialingTimeInterval)
        QBRTCConfig.setLogLevel(QBRTCLogLevel.verbose)
        
        if AppDelegateConstant.enableStatsReports == 1 {
            QBRTCConfig.setStatsReportTimeInterval(1.0)
        }
        SVProgressHUD.setDefaultMaskType(SVProgressHUDMaskType.clear)
        QBRTCClient.initializeRTC()
        
        Settings.instance.load()
        
        //login
        NimUserManager.shared.loginAndGetDialogs()
        
    }
}

extension AppDelegate: MessagingDelegate {
    
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String) {
        InstanceID.instanceID().instanceID { (result, error) in
            if let error = error {
                print("Error fetching remote instange ID: \(error)")
            } else if let result = result {
                print("Remote instance ID token: \(result.token)")
                SessionManager.saveFCMToken(token: result.token)
            }
        }
    }
}

extension UIApplication {
    class func topViewController(controller: UIViewController? = UIApplication.shared.keyWindow?.rootViewController) -> UIViewController? {
        if let navigationController = controller as? UINavigationController {
            return topViewController(controller: navigationController.visibleViewController)
        }
        if let tabController = controller as? UITabBarController {
            if let selected = tabController.selectedViewController {
                return topViewController(controller: selected)
            }
        }
        if let presented = controller?.presentedViewController {
            return topViewController(controller: presented)
        }
        return controller
    }
}

extension AppDelegate {
    
    func notificationServiceDidStartLoadingDialogFromServer() {
    }
    
    func notificationServiceDidFinishLoadingDialogFromServer() {
    }
    
    func notificationServiceDidSucceedFetchingDialog(chatDialog: QBChatDialog!) {
        self.moveToChatController(chatDialog: chatDialog)
        /*let dialogWithIDWasEntered = ServicesManager.instance().currentDialogID
         if !dialogWithIDWasEntered.isEmpty {
         // some chat already opened, return to dialogs view controller first
         navigatonController.popViewController(animated: false);
         }
         
         navigatonController.pushViewController(chatController, animated: true)*/
    }
    
    func notificationServiceDidFailFetchingDialog() {
    }
    
    func moveToChatController(chatDialog: QBChatDialog){
        let chatController: ChatViewController = UIStoryboard(name:"Main", bundle: nil).instantiateViewController(withIdentifier: "ChatViewController") as! ChatViewController
        chatController.dialog = chatDialog
        chatController.isUnreadCountAvailable = true
        constantVC.openPrivateChat.isActiveOpenPrivateChat = true
        
        let nav: UINavigationController = UIStoryboard(name:"Main", bundle: nil).instantiateViewController(withIdentifier: "navChat") as! UINavigationController
        nav.viewControllers = [chatController]
        self.window?.rootViewController = nav
        self.window?.makeKeyAndVisible()
        self.window?.endEditing(true)
    }
}

//MARK:- Notification Handle
extension AppDelegate {
        
    func showInvitationView(fromData: UserProfile?,toData: Dictionary<String,Any>?) {
        
        self.invitationView = InvitationVIew(frame: CGRect(x: 0, y: 0, width: self.window?.frame.width ?? 0.0, height: self.window?.frame.height ?? 0.0))
        guard let inviteView = self.invitationView else {return}
        let dict:[String:Any] = ["fromData": fromData!,"toData":toData!]

        inviteView.userData = fromData
        inviteView.notNow_button.addTarget(self, action: #selector(self.notInterestedAction(_:)), for: .touchUpInside)
        inviteView.cross_button.addTarget(self, action: #selector(self.crossFunction), for: .touchUpInside)
        inviteView.userName_button.addTarget(self, action: #selector(self.openProfileDetail(_:)), for: .touchUpInside)
        inviteView.acceptNow_button.addTarget(self, action: #selector(self.acceptInvite(_:)), for: .touchUpInside)
        
        inviteView.notNow_button.associatedDictValue = dict
        inviteView.acceptNow_button.associatedDictValue = dict
        inviteView.userName_button.assocaitedStringValue = fromData?.phoneNumber ?? ""
        self.window?.addSubview(inviteView)
    }
    
    @objc func crossFunction() {
        self.invitationView?.removeFromSuperview()
    }
    
    @objc func notInterestedAction(_ sender: UIButton ) {

        guard let associateData = sender.associatedDictValue else {return}
        guard let fromData = associateData["fromData"] as? UserProfile else {return}
        guard let toData = associateData["toData"] as? Dictionary<String,Any> else {return}
        guard let toId = toData["id"] as? String else {return}
        
        InvitationManager.instance.connectStatusChange_invitation(id: toId, fromName: "", invitationByID: fromData.id ?? "" , status: invitationStatus.rejected.rawValue) { (success) in
            if success {
                FTIndicator.showSuccess(withMessage: userMessagestext.invitationReject)
                self.invitationView?.removeFromSuperview()
            }
        }
        
    }
    
    @objc func openProfileDetail(_ sender: UIButton) {
        guard let phoneNo = sender.assocaitedStringValue else {return}
        self.viewProfile(number: phoneNo, isMatchView: false)
    }
    
    @objc func acceptInvite(_ sender: UIButton) {
        guard let associateData = sender.associatedDictValue else {return}
        guard let fromData = associateData["fromData"] as? UserProfile else {return}
        guard let toData = associateData["toData"] as? Dictionary<String,Any> else {return}
        guard let toId = toData["id"] as? String else {return}

        InvitationManager.instance.connectStatusChange_invitation(id: toId, fromName: fromData.name ?? "", invitationByID: fromData.id ?? "" , status: invitationStatus.active.rawValue) { (success) in
               if success {
                self.invitationView?.removeFromSuperview()

                guard let newFromData = UserProfile(JSON: toData) else {return}
                let newToData : [String:Any] = ["id" : fromData.id ?? "","phoneNumber" : fromData.phoneNumber ?? "","name": fromData.name ?? ""]
                print(newFromData, newToData)
                self.showMatchView(fromData: newFromData, toData: newToData)
                   //FTIndicator.showProgress(withMessage: userMessagestext.invitationAccept + "\nConnecting you to chat....")
//                ServicesManager.instance().usersService.getUsersWithLogins([fromData.phoneNumber ?? ""]).continueOnSuccessWith(block: { (task) -> Any? in
//                        if task.isCompleted {
//
////                           if let users = task.result as? [QBUUser] {
////                               if users.indices.contains(0) {
////                                   self.createChat(user: users[0])
////                               }
////                           }
//                        }else {
//                           FTIndicator.dismissProgress()
//                        }
//                   return nil
//                   })
               }
           }
        
    }
    
    func createChat(user:QBUUser){
         
         DialogManagerNIM.instance.CreatePrivateGroupChat(user: user) { (success, dialog) in
             if success {
                 FTIndicator.dismissProgress()
                 if let dialog_ = dialog {
                     self.navigate_ChatVC(dailog: dialog_)
                 }
             } else {
                 FTIndicator.dismissProgress()
                 FTIndicator.showError(withMessage: UserMsg.errorConnectingChat)
             }
         }
     }
     
     func navigate_ChatVC(dailog:QBChatDialog){
        self.invitationView?.removeFromSuperview()
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "ChatViewController") as! ChatViewController
        controller.dialog = dailog
        if let opponentNo_ = DialogManagerNIM.instance.getOpponentNo(dialog: dailog) {
            controller.opponentNo = opponentNo_
        }
        controller.isNewChatCreated = true
        if let topVC = UIApplication.topViewController() {
            topVC.navigationController?.pushViewController(controller , animated: false)
        }
         
     }
    
    func showMatchView(fromData: UserProfile?,toData: Dictionary<String,Any>?) { // from is me here and to is other person
        
        self.matchView = MatchView(frame: CGRect(x: 0, y: 0, width: self.window?.frame.width ?? 0.0, height: self.window?.frame.height ?? 0.0))
        guard let matchViewObj = self.matchView else {return}
        guard let toNumber = toData?["phoneNumber"] as? String else {return}
        let dict:[String:Any] = ["fromData": fromData!,"toData":toData!]
        
        matchViewObj.userData = fromData
        matchViewObj.toData = toData
        matchViewObj.showData()
        
        matchViewObj.notNow_button.addTarget(self, action: #selector(self.matchViewCrossAction), for: .touchUpInside)
        matchViewObj.cross_button.addTarget(self, action: #selector(self.matchViewCrossAction), for: .touchUpInside)
        matchViewObj.startConversation_button.addTarget(self, action: #selector(self.startConversationAction(_:)), for: .touchUpInside)
        matchViewObj.viewProfile_button.addTarget(self, action: #selector(self.viewProfileAction(_:)), for: .touchUpInside)
        
        matchViewObj.startConversation_button.associatedDictValue = dict
        matchViewObj.viewProfile_button.assocaitedStringValue = toNumber
        self.window?.addSubview(matchViewObj)
        
    }
    
    @objc func matchViewCrossAction() {
        self.matchView?.removeFromSuperview()
    }
    
    
    @objc func startConversationAction(_ sender:UIButton) {
      
        guard let associateData = sender.associatedDictValue else {return}
        guard let toData = associateData["toData"] as? Dictionary<String,Any> else {return}
        guard let toPhoneNo = toData["phoneNumber"] as? String else {return}
        
        FTIndicator.showProgress(withMessage: "Connecting you to chat....")
        ServicesManager.instance().usersService.getUsersWithLogins([toPhoneNo]).continueOnSuccessWith(block: { (task) -> Any? in
                if task.isCompleted {
                   if let users = task.result as? [QBUUser] {
                        if users.indices.contains(0) {
                            self.matchView?.removeFromSuperview()
                            self.createChat(user: users[0])
                       }
                   }
                }else {
                   FTIndicator.dismissProgress()
                }
           return nil
        })
        
    }
    
    @objc func viewProfileAction(_ sender: UIButton) {
        guard let phoneNo = sender.assocaitedStringValue else {return}
        self.viewProfile(number: phoneNo, isMatchView: true)
        
    }
 
    func viewProfile(number:String?, isMatchView: Bool?) {
        
        let profileDetails_VC = self.storyboard.instantiateViewController(withIdentifier: "ProfileDetails_VC") as! ProfileDetails_VC
        profileDetails_VC.matchPhoneNo = number ?? ""
        profileDetails_VC.fromAppdelegate = true
        if isMatchView ?? false {
            self.matchView?.isHidden = true
        } else {
            self.invitationView?.isHidden = true
        }
        profileDetails_VC.block = { [weak self] data in
            guard let self = self else {return}
            guard let _ = data as? Bool else {return}
            if isMatchView ?? false {
                self.matchView?.isHidden = false
            } else {
                self.invitationView?.isHidden = false
            }
        }
        self.window?.rootViewController?.present(profileDetails_VC, animated: true, completion: nil)
        
    }
    
    
   func convertStringIntoArray(str:String?)-> Dictionary<String,Any>? {
       
       guard let string = str else {return nil}
       guard let data = string.data(using: .utf8) else {return nil}
       do {
           if let jsonArray = try JSONSerialization.jsonObject(with: data, options : .allowFragments) as? Dictionary<String,Any> {
               print(jsonArray) // use the json here
               return jsonArray
           } else {
               print("bad json")
               return nil
           }
       } catch let error as NSError {
           print(error)
           return nil
       }
   }
    
}
