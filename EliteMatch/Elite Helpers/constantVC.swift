
import UIKit
import YYWebImage

let kChatPresenceTimeInterval:TimeInterval = 45
let kDialogsPageLimit:UInt = 100
let kMessageContainerWidthPadding:CGFloat = 40.0

typealias Completion = (() -> Void)

class constantVC: NSObject {
    
    struct appTitle {
      static let alertTitle        = "NIM"
      static let liveURL           = "https://apps.apple.com/us/app/nim-match/id1478519908?ls=1"
    }
    
    struct GlobalColor {
        static let appColor = UIColor(red: 187.0 / 255.0, green: 138.0 / 255.0, blue: 56.0 / 255.0, alpha: 1.0)
        static let titleColor = UIColor(red: 54.0/255.0, green: 54.0/255.0, blue: 54.0/255.0, alpha: 1.0)
        static let lightBlackColor = UIColor(red: 54.0, green: 54.0, blue: 54.0, alpha: 0.8)
        static let bgColor = UIColor(red: 217.0/255.0, green: 6.0/255.0, blue: 71.0/255.0, alpha: 0.4)
        static let greenColor = UIColor(red: 0/255, green: 161/255, blue: 98/255, alpha: 1.0)
        static let lightPurple = UIColor(red: 129/255, green: 136/255, blue: 239/255, alpha: 1.0)
        static let lightPurpleMyCircle = UIColor(red: 239/255, green: 240/255, blue: 254/255, alpha: 1.0)
    }
    
    struct arrDropDown_Match {
        static var arrLangaugeDropDown:[String] = []
        static var arrReligionDropDown:[String] = []
        static var arrEducationDropDown:[String] = []
        static var arrLifeStyle:[String] = []
        static var arrFavMusic:[String] = []
        static var arrFavFood:[String] = []
        static var arrZoadicSign:[String] = []
        static var arrFavHoliday:[String] = ["AHoliday" , "BHoliday" , "CHoliday" , "DHoliday"]
        static var arrFavDessert:[String] = []
        static var arrTravel:[String] = []
        static var arrCareer:[String] = ["A" , "B" , "C" , "D"]
    }
    
    struct Instagram_api{
        static let INSTAGRAM_AUTHURL = "https://api.instagram.com/oauth/authorize/"
        static let INSTAGRAM_CLIENT_ID = "b3b8fdd03a394fab89725107b5ec45f8"
        static let INSTAGRAM_CLIENTSERCRET = "ec586c92865a4dbe9fec4333acc89242"
        static let INSTAGRAM_REDIRECT_URI =  "https://www.google.com"  //"https://www.google.com" // "http://tamaahs.com/" //"http://www.tamaaas.com/mobileapi/"
        static let INSTAGRAM_ACCESS_TOKEN = ""
        static let INSTAGRAM_SCOPE = "public_content"
        static let INSTAGRAM_GET_IMAGES_URL = "https://api.instagram.com/v1/users/self/media/recent/?access_token="
    }
    
    struct LinkedIn_api{
//        static let linkedInKey = "81ixqjnt01t77a"
//        static let linkedInSecret = "2aNALZCsJGF9zANd"
//        static let authorizationEndPoint = "https://com.appcoda.linkedin.oauth/oauth"
//        static let accessTokenEndPoint = "https://www.linkedin.com/uas/oauth2/accessToken"
//        static let redirectURL = "https://EliteMatch.com/mobileapi/"
    }
    
    // MARK: - URL
    struct WebserviceName {
        static let URL_GET_PREDATA = "PreData"
        static let URL_SIGNUP_STEP_1 = "MatchMakingStep1"
        static let URL_SIGNUP_STEP_2 = "MatchMakingStep2"
        static let URL_SIGNUP_STEP_3 = "MatchMakingStep3"
        static let URL_ELITE_USER_PROFILE = "MyProfile"
        static let URL_CHAPERON_SIGNUP = "signUpChaperon"
        static let URL_EDIT_PROFILE    = "updateprofile"
       // static let URL_DAILY_MATCHES = "dailyMatch&userId="
        static let URL_DAILY_MATCHES = "Match&id="
        static let URL_MATCHED_USER = "Matchbook&id="
        static let URL_SEND_INVITATION = "send_invetation"
        static let URL_STATUS_INVITATION = "Status_invetation"
        static let URL_GET_INVITATIONS = "invetations"
        static let URL_GET_PROFILE_BY_PHONENO = "getUserDetailsByPhoneNo"
        static let URL_UPDATE_FCM_TOKEN = "fcm_token_registeration"
        static let URL_UPDATE_USER_PROFILE_IMAGES    = "update_userimages"
        
        static let URL_ADD_PROFILE_PHOTO = "uploadSingle"
        static let URL_DELETE_PROFILE_PHOTO = "deleteUserimages"
        static let URL_UPDATE_PROFILES_ORDER    = "imageOrder"//"userImageOrder"
        
        //chaperon invitation
        static let URL_SEND_CHAPERON_INVITATION    = "send_invetation_chaperon"
        static let URL_CHAPERON_INVITATION_STATUS    = "Status_invetation_chaperon"
        static let URL_GET_CHAPERON_INVITATION    = "invetation_chaperons"
        
        //tam
        static let URL_GETBLOCKED           = "GetBlockList"
        static let URL_BLOCKUSER            = "BlockUnBlockUser"
        
        //chaperon invitation
        static let URL_SEND_CIRCLE_INVITATION    = "sendCircleInvitation"
        static let URL_CIRCLE_INVITATION_STATUS    = "changeStatusCircleInvitation"
        static let URL_GET_CIRCLE_INVITATION    = "circleInvitation"
        
        //plan
        static let URL_UPDATE_PLAN_DETAILS    = "user_plan_update"
        
        //report group
        static let URL_SUBMIT_GROUP_REPORT = "SubmitGroupReport"
    }
    
    
    struct NSNotification_name {
         static let NSNotification_profileUpdate   = Notification.Name("profileUpdate")
         static let NSNotification_postLikeComment   = Notification.Name("postLikeComment")
         static let NSNotification_bellUnreadCount   = Notification.Name("bellUnreadCount")
         static let NSNotification_InternetDisconnected   = Notification.Name("InternetDisconnected")
         static let NSNotification_InternetConnected   = Notification.Name("InternetConnected")
         static let NSNotification_updateNotificationList   = Notification.Name("updateNotificationList")
         static let NSNotification_updateTimer = Notification.Name("updateCallTimer")
    }
    
    struct GeneralConstants {
        static let BASE_IP     = "http://18.223.24.94/"
        static let MATCH_BASE_URL    = "\(GeneralConstants.BASE_IP)elitematch/api.php?action="
        static let GET_IMAGE = "\(GeneralConstants.BASE_IP)elitematch/instaupload/"
        static let arrFeet:[String] = ["Feet","1","2","3","4","5","6","7","8","9","10"]
        static let arrInches:[String] = ["Inch","0","1","2","3","4","5","6","7","8","9","10","11"]
        static let contactKey   = "Contact attachment"
        static let appLiveLink = "https://itunes.apple.com/us/app/tamaaas-messenger/id1449952054?mt=8"
        static let appLiveLinkAndroid = "https://play.google.com/store/apps/details?id=com.tamaaas"
        static let chatBubbleColour = UIColor(red: 104/255, green:76/255, blue: 180/255, alpha: 1.0)
        static let ReportTags = ["Nudity", "Voilence", "Harassment", "Suicide", "False News", "Spam", "Unauthorized Sale", "Hate Speech", "Racism"]
    }
    

    // MARK: - Global Variables
    struct GlobalVariables {
        static var MyProfile:Profile?
        static var isLinkedInAccessTokenFetched:Bool = false
        static var isDialPadLaunched:Bool = false
        static var dictMyContacts : Dictionary<String, String>?
        static var arrQBAddressContact:[QBAddressBookContact] = []
    }
    
    
    struct AppUpdationsChecks {
       static var isProfileUpdated:Bool = false
    }
    
    
    // MARK: - Message
    struct CustomMessage {
        static let message = "NO RECORD FOUND"
        static let networkErrorMessage = "Something went wrong. Try again later!"
    }
    
    class var QB_USERS_ENVIROMENT: String {
        
        #if DEBUG
        return "dev"
        #elseif QA
        return "qbqa"
        #else
        assert(false, "Not supported build configuration")
        return ""
        #endif
    }
    
    //MARK:- QuickBlox
    struct ActiveDataSource {
        static var Messages:[QBChatMessage] = []
        static var isActiveRefresh:Bool = false
        static var isActiveSignUp:Bool = false
        static var isNavigatePostDetails:Bool = false
        static var isNavigateNotification:Bool = false
        static var isNeedToRefreshPost:Bool = false
    }
    
    struct ActiveCall {
        static var nameToShow:String = ""
        static var senderID:String = ""
        static var senderNo:String = ""
        static var senderName:String = ""
        static var receiverNo:String = ""
        static var membersID:String = ""
        static var isGroup:String = ""
        static var isAudio:String = ""
        static var dialogID:String = ""
        static var groupName:String = ""
        static var duration:String = ""
        static var groupPhoto:String = ""
        static var opponentsIDs:[NSNumber] = []
    }
    
    struct ActiveSession {
        static var session: QBRTCSession?
        static var isCallActive:Bool = false
        static var activeCallUUID: UUID?
        static var remoteActiveUsers:[User] = []
        static var videoViews = [UInt: UIView]()
        static var cameraCapture: QBRTCCameraCapture?
        static var isBackgroundViewActive:Bool = false
        static var callTimer: Timer?
        static var timeDuration: TimeInterval = 0.0
        static var activeDialog: QBChatDialog?
        static var isStartedConnectingCall:Bool = false
    }
    
    enum versionError: Error {
        case invalidBundleInfo
        case invalidResponse
    }
    
    func cleanActiveSession() {
        ActiveSession.session = nil
        ActiveSession.isCallActive = false
        ActiveSession.activeCallUUID = nil
        ActiveSession.remoteActiveUsers = []
        ActiveSession.videoViews.removeAll()
        ActiveSession.cameraCapture = nil
        ActiveSession.isBackgroundViewActive = false
        ActiveSession.timeDuration = 0.0
        ActiveSession.activeDialog = nil
        ActiveSession.isStartedConnectingCall = false
    }
    
    func cleanActiveCall(){
        ActiveCall.senderID = ""
        ActiveCall.senderNo = ""
        ActiveCall.receiverNo = ""
        ActiveCall.membersID = ""
        ActiveCall.isGroup = ""
        ActiveCall.isAudio = ""
        ActiveCall.dialogID = ""
        ActiveCall.groupName = ""
        ActiveCall.duration = ""
        ActiveCall.groupPhoto = ""
        ActiveCall.opponentsIDs = []
        ActiveCall.senderName = ""
        ActiveCall.nameToShow = ""
    }
    
    enum callState: String {
        case hangup = "hangup"
        case missedNoAnswer = "missedNoAnswer"
    }
    
    struct openPrivateChat{
        static var isActiveOpenPrivateChat:Bool = false
        static var Dialog: QBChatDialog!
        static var isActiveOpenChatWithActiveCall = false
    }
}

struct YYImgCacheOptions {
    static let withRefreshCache:(YYWebImageOptions) = [ .progressiveBlur, .setImageWithFadeAnimation , .refreshImageCache]
    static let withOutRefreshCache:(YYWebImageOptions) = [.progressiveBlur , .setImageWithFadeAnimation]
}

struct currentYYImgotions {
    static var currentYYImg:(YYWebImageOptions) = YYImgCacheOptions.withOutRefreshCache
}

enum responseStatus:String {
    case success = "success"
    case error = "error"
}

enum invitationStatus:String {
    case active = "ACTIVE"
    case pending = "PENDING"
    case rejected = "REJECTED"
}

enum invitationButtonText:String {
    case sendInvitation = "Send Invitation"
    case startConversation = "Start Conversation"
    case pending = "Pending"
    case rejected = "Rejected"
}

struct UsersConstant {
    static let pageSize: UInt = 50
    static let aps = "aps"
    static let alert = "alert"
    static let voipEvent = "VOIPCall"
}

struct MsgToReplyKeys {
    static let msg_id = "msg_id"
    static let senderNumber = "senderNumber"
    static let replyId = "replyId"
    static let replyNumber = "replyNumber"
    static let msgType = "msgType"
    static let TEXT = "text"
    static let replyMsg = "replyMsgDetail"
    static let msg = "actual_msg"
    static let senderID = "senderID"
    static let IMAGE = "image"
    static let VIDEO = "video"
    static let CONTACT = "contact"
    static let STICKER = "sticker"
    static let AUDIO = "audio"
    static let MEDIA = "media"
    static let thumbnail = "thumbnail"
    static let stickerName = "stickerName"
}

struct ReplyContainer {
    static let maxReplyContainerHeight:CGFloat = 110.0
    static let minReplyContainerHeight:CGFloat = 60.0
}

struct UsersAlertConstant {
    static let checkInternet = NSLocalizedString("Please check your Internet connection", comment: "")
    static let okAction = NSLocalizedString("Ok", comment: "")
    static let shouldLogin = NSLocalizedString("You should login to use chat API. Session hasn’t been created. Please try to relogin the chat.", comment: "")
    static let logout = NSLocalizedString("Logout...", comment: "")
}

enum CallViewControllerState : Int {
    case disconnected
    case connecting
    case connected
    case disconnecting
}

struct CallStateConstant {
    static let disconnected = "Disconnected"
    static let connecting = "Connecting..."
    static let connected = "Connected"
    static let disconnecting = "Disconnecting..."
}

struct CallConstant {
    static let opponentCollectionViewCellIdentifier = "OpponentCollectionViewCellIdentifier"
    static let unknownUserLabel = "Unknown user"
    static let sharingViewControllerIdentifier = "SharingViewController"
    static let refreshTimeInterval: TimeInterval = 1.0
    static let recordingTitle = "[Recording] "
    static let memoryWarning = NSLocalizedString("MEMORY WARNING: leaving out of call", comment: "")
    static let sessionDidClose = NSLocalizedString("Session did close due to time out", comment: "")
    static let savingRecord = NSLocalizedString("Saving record", comment: "")
}

struct userMessagestext {
    static let invitationAccept = "Request accepted successfully!"
    static let invitationReject = "Request rejected!"
    static let areYouSure = "Are you sure"
    static let sendInvitationConfirmation = "you want to send invitation to this user?"
    static let acceptInvitationConfirmation = "you want to accept this invitation?"
    static let rejectInvitationConfirmation = "you want to reject this invitation?"
    static let instagramIdAlreadyUsed = "This instagram id is already used"
    static let linkedInIdAlreadyUsed = "This linkedIn id is already used"
    static let sendChaperonInvitation_success = "Invitation Sent Successfully!"
    static let circleCreatedSuccessfully = "Circle created Successfully!"
    static let signUpAsMemberConfirmation = "You're an existing single member,do you want to switch?"
}

struct UserMsg {
    static let Error = "Error"
    static let msgFailedToSend = "Message failed to send. Check your connection"
    static let noInternetConnection = "No Internet Connection!"
    static let sent = "sent"
    static let seen = "seen"
    static let read = "read"
    static let delivered = "delivered"
    static let sending = "sending"
    static let notSent = "not sent"
    static let cancel = "cancel"
    static let failedToSend = "Message failed to send"
    static let tryAgain = "Try again"
    static let deleteMsg = "Delete"
    static let chatDeleted = "Chat Deleted"
    static let errorConnectingChat = "Error in Connecting Chat"
    static let groupLeft = "Group Left"
    static let groupLeftMsg = "has left the chat."
    
}

enum UserProfileStatus : String {
    case approved = "approved"
    case pending = "pending"
    case notSent = "notSent"
}

enum UserProfile_lastUpdated : String {
    case noupdate = "noupdate"
    case step1 = "step1"
    case step2 = "step2"
    case step3 = "step3"
    case paymentPending = "paymentPending"
    case paymentDone = "paymentDone"
}

enum DropDown_Title : String {
    case Zoadiac = "Select Zoadiac"
    case Religion = "Select Religion"
    case Language = "Select Languages(s)"
    case Travel = "Select Travel Places"
    case Education = "Select Education"
    case FamilyValues = "Select Family Values"
    case FavCookies = "Select Favorite Cookies"
    case FavMusic = "Select Favorite Music"
    case FavFood = "Select Favorite Food"
}

enum DropDown_tag : Int {
    case languages = 1
    case religion = 2
    case religiousCount = 3
    case travel = 4
    case familyOrigin = 5
    case education = 6
    case familyValues = 7
    case workRoutinely = 8
    case smoke = 9
    case drink = 10
    case eatHalal = 11
    case marriedBefore = 12
    case children = 13
    case favShow = 14
    case bingWatch = 15
    case favCookie = 16
    case favMusic = 17
    case favFood = 18
    case tallerPartner = 19
    case religionMatter = 20
    case ethnicityMatter = 21
}


struct TimeIntervalConstant {
    static let answerTimeInterval: TimeInterval = 60.0
    static let dialingTimeInterval: TimeInterval = 5.0
}

struct AppDelegateConstant {
    static let enableStatsReports: UInt = 1
}


class Sticker: NSObject {
    
    static let stickerKey   = "TAMSTICKER*"
    
    //stickers
    static let family = ["family_1","family_2","family_3","family_4","family_5","family_6","family_7","family_8","family_9","family_10","family_11","family_12","family_13","family_14","family_15","family_16","family_17","family_18","family_19","family_20","family_21","family_22","family_23","family_24","family_25","family_26","family_27","family_28","family_29","family_30","family_31","family_32"]
    
    static let food = ["food_1","food_2","food_3","food_4","food_5","food_6","food_7","food_8","food_9","food_10","food_11","food_12","food_13","food_14","food_15","food_16","food_23","food_17","food_18","food_19","food_20","food_21","food_22"]
    
    static let saying = ["saying_1","saying_2","saying_3","saying_4","saying_5","saying_6","saying_54","saying_36","saying_33","saying_34","saying_35","saying_40","saying_41","saying_42","saying_43","saying_45","saying_46","saying_47","saying_48","saying_49","saying_50","saying_51","saying_53","saying_11","saying_16","saying_17","saying_18","saying_19","saying_20","saying_21"]
    //"saying_52","saying_14","saying_15","saying_12","saying_13","saying_39","saying_44","saying_37","saying_38","saying_7","saying_8","saying_9","saying_10","saying_22","saying_23","saying_24","saying_25","saying_26","saying_27","saying_28","saying_29","saying_30","saying_31","saying_32"
    static let famous = ["famous_1","famous_2","famous_3","famous_5","famous_6","famous_7","famous_8","famous_9","famous_10"]
    
    static let sport = ["sport_12" , "sport_13" ,"sport_14" , "sport_15","sport_16" , "sport_17","sport_1","sport_2","sport_18","sport_3","sport_4","sport_5","sport_6","sport_7","sport_8","sport_9","sport_10" , "sport_11"]
    
    static let islam = ["islam_13","islam_14","islam_15","islam_16","islam_17","islam_18","islam_19","islam_20","islam_21","islam_22","islam_23" , "islam_24","islam_25","islam_26","islam_27","islam_28" , "islam_29","islam_30","islam_1","islam_4","islam_5","islam_6","islam_7","islam_9","islam_10","islam_11" ,"islam_12"]
    
    static let arrStickers = Sticker.family + Sticker.food + Sticker.saying + Sticker.islam
    //+ Sticker.famous + Sticker.sport
    static let stickerOptions = [UIImage(named: "english_option"),UIImage(named: "sendSmiley"),UIImage(named: "family_option"),UIImage(named: "food_option"),UIImage(named: "moreMashalla"),UIImage(named: "specialPerson"),UIImage(named: "sport_option"),UIImage(named: "islam_option")]
}



enum ChatType: String {
    
    case oneToOnePrivate = "oneToOnePrivate"
    case isChaperon = "isChaperon"
    case circle = "circle"
}


struct chatColours {
    static let gray:UIColor = UIColor(red: 166.3/255.0, green: 171.5/255.0, blue: 171.8/255.0, alpha: 1.0)
    static let red:UIColor = UIColor(red: 254.6/255.0, green: 30.3/255.0, blue: 12.5/255.0, alpha: 1.0)
    static let purple:UIColor = UIColor(red: 104/255, green:76/255, blue: 180/255, alpha: 1.0)
}


struct ApplePay {
    static let mechantIdentifier = "merchant.com.NIMMatchMaking"
}


struct StripeAccount {
    static let apiKey = "pk_test_SeCHueq1QOzniEUyBn91Ude300SgOBy3ae"    //"pk_test_II5kXpqEX8NhX41WsOgRc53J00D7WTdimc"
    static let apiSecretKey = "sk_test_wFlbdCpJyanmmM9dDULzYDe800C0PMrAW6"  //"sk_test_oBTKFiI0sJwetxO4fgRDAvhn00wYTnecxW"
}
