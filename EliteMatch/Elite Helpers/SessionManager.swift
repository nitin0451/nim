import UIKit

class SessionManager {
    
    static let defaults = UserDefaults.standard
    static let timerConstant = "timerValue"
    static let dateConstant = "OldDateValue"
    static let dailyApiCount = "dailyApiCount"
    static let instance = SessionManager()
    
    //MARK:- Print log data
    class func printLOG(data:String) {
        // print(data)
    }
    
    //MARK:- ELITE MATCH USER
    class func save_Elite_userID(id:String) {
        defaults.set(id , forKey: "eliteUserId")
    }
    
    class func get_Elite_userID() -> String {
        if let id = defaults.object(forKey: "eliteUserId") as? String {
            return id
        }
        return ""
    }
    
    //PHONE NUMBER
    class func save_phonenumber(phoneNumber:String) {
        defaults.set(phoneNumber , forKey: "phoneNumber")
    }
    
    class func get_phonenumber() -> String {
        if let phoneNumber = defaults.object(forKey: "phoneNumber") as? String {
            return phoneNumber
        }
        return ""
    }
    
    //MARk:- Chaperon
    class func saveChaperonStatus(isActive: Bool) {
        defaults.set(isActive , forKey: "isChaperon")
    }
    class func isChaperonUser() -> Bool {
        if let isActive = defaults.object(forKey: "isChaperon") as? Bool {
            return isActive
        }
        return false
    }
    
    //MARK:- QUICK BLOX - USER_ID
    class func save_QuickBloxID(id:NSNumber) {
        defaults.set(id , forKey: "QuickBloxID")
    }
    
    class func get_QuickBloxID() -> NSNumber? {
        if let id = defaults.object(forKey: "QuickBloxID") as? NSNumber {
            return id
        }
        return nil
    }
    
    //MARK:- COUNTRY CODE
    class func saveCountryCode(code:String){
        defaults.set(code , forKey: "countryCode")
    }
    
    class func getCountryCode() -> String {
        if let code = defaults.object(forKey: "countryCode") as? String {
            return code
        }
        return ""
    }
   
    //MARK:- OTP VERIFICATION CODE
    class func saveOTPVerificationID(code:String){
        defaults.set(code , forKey: "verificationID")
    }
    
    class func getOTPVerificationID() -> String {
        if let code = defaults.object(forKey: "verificationID") as? String {
            return code
        }
        return ""
    }
    
    //MARK:- Chat Blocked Users
    class func save_chatBlockedUsersID(IDS: [String]) {
        defaults.set(IDS , forKey: "chatBlockedUsers")
    }
    
    class func get_chatBlockedUsersID() -> [String]? {
        if let arr = defaults.object(forKey: "chatBlockedUsers") as? [String]? {
            return arr
        }
        return nil
    }
    
    class func isChatUserBlocked(userID: String) ->Bool {
        if let IDS = SessionManager.get_chatBlockedUsersID() {
            if IDS.contains(userID) {
                return true
            }
        }
        return false
    }
    
    //MARK:- MY CONTACTS DIC
    
    class func save_dictMyContacts(dict:Dictionary<String, String>?) {
        defaults.set(dict , forKey: "dictMyContacts")
    }
    
    class func get_dictMyContacts() -> Dictionary<String, String>? {
        if let dict = defaults.object(forKey: "dictMyContacts") as? Dictionary<String, String> {
            return dict
        }
        return nil
    }
    
    class func getNameFromMyAddressBook(number:String) ->String {
        if SessionManager.get_dictMyContacts() != nil {
            let no = number.components(separatedBy: CharacterSet.decimalDigits.inverted).joined()
            if let name = SessionManager.get_dictMyContacts()?[no]{
                return name
            }
        }
        return "+" + number
    }
    
    //MARK:- UUID
    class func getAndSave_UUID() -> String {
        defaults.set(UIDevice.current.identifierForVendor!.uuidString , forKey: "device_UUID")
        return self.get_UUID()
    }
    
    class func get_UUID() -> String {
        if let id = defaults.object(forKey: "device_UUID") as? String {
            return id
        }
        return ""
    }
    
    //MARK:- Profile Approval Status
    class func getProfileApprovalStatus() -> UserProfileStatus {
        if let status_ = defaults.object(forKey: "profileApprovalStatus") as? String {
            
            switch status_ {
            case UserProfileStatus.approved.rawValue:
                return UserProfileStatus.approved
            case UserProfileStatus.pending.rawValue:
                return UserProfileStatus.pending
            case UserProfileStatus.notSent.rawValue:
                return UserProfileStatus.notSent
            default:
                print("default")
            }
        }
        return UserProfileStatus.notSent
    }
    
    class func saveProfileApprovalStatus(status: String) {
        defaults.set(status , forKey: "profileApprovalStatus")
    }
    
    class func saveProfile_lastUpdatedStatus(status: String) {
        defaults.set(status , forKey: "profileFormStatus")
    }
    
    class func getProfile_lastUpdatedStatus() -> UserProfile_lastUpdated {
        if let status_ = defaults.object(forKey: "profileFormStatus") as? String {
            
            switch status_ {
            case UserProfile_lastUpdated.step1.rawValue:
                return UserProfile_lastUpdated.step1
            case UserProfile_lastUpdated.step2.rawValue:
                return UserProfile_lastUpdated.step2
            case UserProfile_lastUpdated.step3.rawValue:
                return UserProfile_lastUpdated.step3
            case UserProfile_lastUpdated.paymentPending.rawValue:
                return UserProfile_lastUpdated.paymentPending
            case UserProfile_lastUpdated.paymentDone.rawValue:
                return UserProfile_lastUpdated.paymentDone
            default:
                print("default")
            }
        }
        return UserProfile_lastUpdated.noupdate
    }
    
    //MARK:- FCM Token
    class func saveFCMToken(token:String){
        defaults.set(token , forKey: "FCM_token")
    }
    
    class func getFCMoken() -> String {
        if let token = defaults.object(forKey: "FCM_token") as? String {
            return token
        }
        return ""
    }
    
    //MARK:- save user name
    class func saveUserName(name:String){
        defaults.set(name , forKey: "userName")
    }
    
    class func getUserName() -> String {
        if let name = defaults.object(forKey: "userName") as? String {
            return name
        }
        return ""
    }
    
    class func checkUserSavedInPhoneBook(number:String) ->Bool {
        if SessionManager.get_dictMyContacts() != nil {
            let no = number.components(separatedBy: CharacterSet.decimalDigits.inverted).joined()
            if let _ = SessionManager.get_dictMyContacts()?[no]{
                return true
            }
        }
        return false
    }
    
    //MARK:- MSG UNREAD COUNT
    class func save_unreadMsgCount(count:Int) {
        defaults.set(count , forKey: "msgUnreadCount")
    }
    
    class func get_unreadMsgCount() -> Int {
        if let count = defaults.object(forKey: "msgUnreadCount") as? Int {
            return count
        }
        return 0
    }
    
    
    //MARK:- MY PROFILE GENDER
    class func save_myProfileGender(value: String) {
        defaults.set(value , forKey: "myProfileGender")
    }
    
    class func get_myProfileGender_isMale() -> Bool {
        if let value = defaults.object(forKey: "myProfileGender") as? String {
            if value == "Male" {
                return true
            } else {
                return false
            }
        }
        return false
    }
    
    var timerValue : Int? {
        get {
            if let value = SessionManager.defaults.object(forKey: SessionManager.timerConstant) as? Int {
                return value
            } else {
                return 0
            }
        }
        set {
            SessionManager.defaults.set(newValue, forKey: SessionManager.timerConstant)
        }
        
    }
    
    var dailyApiCount : Int? {
        get {
            if let value = SessionManager.defaults.object(forKey: SessionManager.dailyApiCount) as? Int {
                return value
            } else {
                return 0
            }
        }
        set {
            SessionManager.defaults.set(newValue, forKey: SessionManager.dailyApiCount)
        }
    }
    
    var canHitApi : Bool {
        return dailyApiCount ?? 0 < 5 ? true : false
    }
    
    var oldTime: Date? {
        get {
            if let value = SessionManager.defaults.object(forKey: SessionManager.dateConstant) as? Date {
                return value
            } else {
                return Date()
            }
        }
        set {
            SessionManager.defaults.set(newValue, forKey: SessionManager.dateConstant)
        }
    }
    
    //MARK:- OFFLINE MESSAGES
    class func saveUnSentMsgs_ids(ids : String) {
        defaults.set(ids, forKey: "UnSentMsgs_ids")
    }
    
    class func getUnSentMsgs_ids() -> String?{
        if let about = defaults.object(forKey: "UnSentMsgs_ids") as? String {
            return about
        }
        return nil
    }
  
}

struct PlatformUtils {
    static let isSimulator: Bool = {
        var isSim = false
        #if arch(i386) || arch(x86_64)
            isSim = true
        #endif
        return isSim
    }()
}

