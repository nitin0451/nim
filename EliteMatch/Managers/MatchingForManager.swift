//
//  MatchingForManager.swift
//  EliteMatch
//
//  Created by Apple on 10/09/19.
//  Copyright © 2019 Kitlabs-M-0002. All rights reserved.
//

import UIKit

class MatchingForManager: NSObject {
    
    
    static let shared = MatchingForManager()
    
    func superMatctchingForViewHeight() ->CGFloat{
        return CGFloat((getNumberOfRows() * 60) + 60)
    }
   
    func getNumberOfRows() ->Int{
        if SessionManager.isChaperonUser() {
            if let dialogs = self.dialogs() {
                return dialogs.count + 1
            } else {
                return 1
            }
        }
        else {
            if let dialogs = self.dialogs() {
                return dialogs.count + 2
            } else {
                return 2
            }
        }
        
    }
    

    func dialogs() -> [QBChatDialog]? {
        
        if SessionManager.isChaperonUser() {
            var arrDialogs = ServicesManager.instance().chatService.dialogsMemoryStorage.dialogsSortByUpdatedAt(withAscending: false)
            for item in arrDialogs {
                if (item.name?.contains("Circle"))! {
                    
                    if !self.isRequestAcceptedByUser(dialog: item) {
                            if let index = arrDialogs.index(of: item) {
                                arrDialogs.remove(at: index)
                            }
                    }
                } else {
                    if let index = arrDialogs.index(of: item) {
                        arrDialogs.remove(at: index)
                    }
                }
            }
            return arrDialogs
        }
        else {
            var arrDialogs = ServicesManager.instance().chatService.dialogsMemoryStorage.dialogsSortByUpdatedAt(withAscending: false)
            for item in arrDialogs {
                
                if (item.name?.contains("Circle"))! {
                    if item.name == DialogManagerNIM.instance.getMyCircleName() {
                        if let index = arrDialogs.index(of: item) {
                            arrDialogs.remove(at: index)
                        }
                    }
                    else if !self.isRequestAcceptedByUser(dialog: item) {
                        if let index = arrDialogs.index(of: item) {
                            arrDialogs.remove(at: index)
                        }
                    }
                }else {
                    if let index = arrDialogs.index(of: item) {
                        arrDialogs.remove(at: index)
                    }
                }
            }
            return arrDialogs
        }
    }
    
    func isRequestAcceptedByUser(dialog: QBChatDialog) ->Bool{
        if let customData = dialog.data {
            if let strDialogData = customData["dialogData"] as? String {
                if let dic = CommonFunctions.shared.jsonToDic(jsonString: strDialogData) {
                    if let dicChaperon = dic["CircleRequestAccepted"] as? NSDictionary {
                        if let phoneNos = dicChaperon.object(forKey: "phoneNumber") as? String {
                            if phoneNos.contains(SessionManager.get_phonenumber()) {
                                return true
                            }
                        }
                    }
                }
            }
        }
        return false
    }
    
    
    //MARK:- Circle Pop-up
    func superCircleViewHeight() ->CGFloat{
        return CGFloat((getNumberOfRows_circlePopUp() * 60) + 65)
    }
    
    
    func getNumberOfRows_circlePopUp() ->Int{
        if let dialogs = self.dialogs_circlePopUP() {
            return dialogs.count
        } else {
            return 0
        }
    }
    
    
    func dialogs_circlePopUP() -> [QBChatDialog]? {
        
        if !SessionManager.isChaperonUser() {
            var arrDialogs = ServicesManager.instance().chatService.dialogsMemoryStorage.dialogsSortByUpdatedAt(withAscending: false)
            for item in arrDialogs {
                if (item.name?.contains("Circle"))! {
                    if item.name == DialogManagerNIM.instance.getMyCircleName() {
                        if let index = arrDialogs.index(of: item) {
                            arrDialogs.remove(at: index)
                            arrDialogs.insert(item, at: 0)
                        }
                    }
                    
                    else if !self.isRequestAcceptedByUser(dialog: item) {
                            if let index = arrDialogs.index(of: item) {
                                arrDialogs.remove(at: index)
                            }
                    }
                    
                } else {
                    if let index = arrDialogs.index(of: item) {
                        arrDialogs.remove(at: index)
                    }
                }
            }
            return arrDialogs
        }
        else {
            var arrDialogs = ServicesManager.instance().chatService.dialogsMemoryStorage.dialogsSortByUpdatedAt(withAscending: false)
            for item in arrDialogs {
                
                if (item.name?.contains("Circle"))! {
                    if !self.isRequestAcceptedByUser(dialog: item) {
                     if let index = arrDialogs.index(of: item) {
                     arrDialogs.remove(at: index)
                   }
                }
                }
               else {
                    if let index = arrDialogs.index(of: item) {
                        arrDialogs.remove(at: index)
                    }
                }
            }
            return arrDialogs
        }
    }
    
    
    //MARK:- Send Match Message
    func getUserAge(dob:String) ->String {
        if let date = CommonFunctions.shared.getDateFromString(dateFormat: "mm-dd-yyyy" , dateStr: dob) {
            if let age_ = CommonFunctions.shared.calculateAge(dob: date) {
                return ", \(age_)"
            }
        }
        return ""
    }
    
    func getCareerName(match:DailyMatch) ->String{
        if match.careerName != "" {
            return ", " + match.careerName
        }
        return ""
    }
    
    func sendMessage_Match(with image:UIImage , caption: String , dialogID:String , dialog: QBChatDialog ,match: DailyMatch , completionHandler: @escaping (Bool) -> Void) {
        
        let attachment = QBChatAttachment.imageAttachment(with: image)
        let messageText = "\(attachment.type?.capitalized ?? "") attachment"
        
        let message = QBChatMessage()
        
        if caption.count > 0 {
            message.text = caption
        } else {
            message.text = messageText
        }
        
        message.senderID = SessionManager.get_QuickBloxID() as! UInt
        message.markable = true
        message.deliveredIDs = [SessionManager.get_QuickBloxID() ?? 0]
        message.readIDs = [SessionManager.get_QuickBloxID() ?? 0]
        message.dialogID = dialogID
        message.dateSent = Date()
        message.attachments = [attachment]
        
        let dictCustomParam:NSMutableDictionary = ["matchID" : match.id , "matchPhNo": match.phoneNumber , "matchTitle":  match.name + self.getUserAge(dob: match.dob) , "matchLocation" : match.living_place + self.getCareerName(match: match)]
        
        message.customParameters = dictCustomParam
        
        ServicesManager.instance().chatService.sendAttachmentMessage(message , to: dialog , with: attachment ) { (error) in
            if error == nil {
                
                //Send Push Notification
                if let opponentID = NimUserManager.shared.getOpponentID(dialog: dialog) {
                    
                    NimUserManager.shared.sendPushNotification_chat(msg: caption , users: "\(opponentID)" , title: "New Message!", fromNo: SessionManager.getUserName() , toNo: "" , dialogID: dialogID)
                }
                completionHandler(true)
            } else {
                completionHandler(false)
            }
        }
    }
    
}
