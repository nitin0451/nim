//
//  DropDown_Manager.swift
//  EliteMatch
//
//  Created by Kitlabs-M-0002 on 8/6/19.
//  Copyright © 2019 Kitlabs-M-0002. All rights reserved.
//

import UIKit

class DropDown_Manager: NSObject {
    
    static let shared = DropDown_Manager()
    
    let regularFont = UIFont.init(name:"CircularStd-Book",size:15)
    let boldFont = UIFont.init(name:"CircularStd-Bold",size:15)
    
    
    func configure(title: String , preSelected: String , allowMultiSelection: Bool , arrItems: [String] , completionHandler:@escaping (String) -> () ) {
        
        print("preSelected" , preSelected)
        
        
        let Appearance = YBTextPickerAppearanceManager.init(
            pickerTitle         : title,
            titleFont           : boldFont,
            titleTextColor      : .white,
            titleBackground     : UIColor.black,
            searchBarFont       : regularFont,
            searchBarPlaceholder: title,
            closeButtonTitle    : "Cancel",
            closeButtonColor    : .darkGray,
            closeButtonFont     : regularFont,
            doneButtonTitle     : "Done",
            doneButtonColor     : UIColor.black,
            doneButtonFont      : boldFont,
            checkMarkPosition   : .Right,
            itemCheckedImage    : UIImage(named:"green_ic_checked"),
            itemUncheckedImage  : UIImage(named:"green_ic_unchecked"),
            itemColor           : .black,
            itemFont            : regularFont
        )
        
        let picker = YBTextPicker.init(with: arrItems , appearance: Appearance,
                                       onCompletion: { (selectedIndexes, selectedValues) in
                                        if selectedValues.count > 0{
                                            var values = [String]()
                                            for index in selectedIndexes{
                                                values.append(arrItems[index])
                                            }
                                            completionHandler(values.joined(separator: ", "))
                                        }else{
                                            completionHandler("")
                                        }
        },
                                       onCancel: {
                                        print("Cancelled")
        }
        )
        
        if preSelected.contains(","){
            picker.preSelectedValues = title.components(separatedBy: ", ")
        }
        picker.allowMultipleSelection = allowMultiSelection
        picker.show(withAnimation: .Fade)
    }

}
