//
//  InvitationManager.swift
//  EliteMatch
//
//  Created by Kitlabs-M-0002 on 7/24/19.
//  Copyright © 2019 Kitlabs-M-0002. All rights reserved.
//

import UIKit
import FTIndicator

class InvitationManager: NSObject {
    
    static let instance = InvitationManager()
    let webservice  = WebserviceSigleton ()
    
    func sendConnect_invitation(requestTo:String ,fromName:String , completionHandler:@escaping (Bool) -> ()){
        
        let dictParam : NSDictionary = ["from_id": SessionManager.get_Elite_userID() , "to_id": requestTo , "fromname": fromName] //SessionManager.get_Elite_userID()
        
        webservice.POSTServiceWithParameters_Elite(urlString: constantVC.WebserviceName.URL_SEND_INVITATION, params: dictParam as! Dictionary<String, AnyObject>, callback:{ (result,error) -> Void in
            
            if let dicResponse = result as NSDictionary? {
                if let msg = dicResponse["msg"] as? String {
                    if msg == responseStatus.success.rawValue {
                        completionHandler(true)
                    }
                    else {
                        completionHandler(false)
                    }
                }
            } else {
                completionHandler(false)
            }
        })
    }
    
    
    func getConnect_invitation(status:String , completionHandler:@escaping (Bool , NSArray) -> ()){
        
        webservice.GETService(urlString: constantVC.WebserviceName.URL_GET_INVITATIONS + "&to_id=\(SessionManager.get_Elite_userID())&status=\(status)" ) { (result , error) in
            if let dicResponse = result as NSDictionary? {
                if let msg = dicResponse["msg"] as? String {
                    if msg == responseStatus.success.rawValue {
                        if let data = dicResponse["data"] as? NSArray {
                            completionHandler(true , data)
                        }
                    }
                    else {
                        completionHandler(false , [])
                    }
                }
            } else {
                completionHandler(false , [])
            }
        }
    }
    
    
    func connectStatusChange_invitation(id:String ,fromName:String , invitationByID: String ,status: String , completionHandler:@escaping (Bool) -> ()){
        
        let dictParam : NSDictionary = ["id": "\(id)" , "status": "\(status)" , "fromname": fromName , "invitationByID": invitationByID ]
        
        webservice.POSTServiceWithParameters_Elite(urlString: constantVC.WebserviceName.URL_STATUS_INVITATION, params: dictParam as! Dictionary<String, AnyObject>, callback:{ (result,error) -> Void in
            
            if let dicResponse = result as NSDictionary? {
                if let msg = dicResponse["msg"] as? String {
                    if msg == responseStatus.success.rawValue {
                        completionHandler(true)
                    }
                    else {
                        completionHandler(false)
                    }
                }
            } else {
                completionHandler(false)
            }
        })
    }
    
    
    //MARK:- CHAPERON INVITATION
    func sendConnect_chaperon_invitation(dialogID: String , requestTo:String ,fromName:String , completionHandler:@escaping (Bool) -> ()){
        
        let dictParam : NSDictionary = ["from_id": SessionManager.get_Elite_userID() , "to_id": requestTo , "fromname":fromName , "dialogID": dialogID]
        
        webservice.POSTServiceWithParameters_Elite(urlString: constantVC.WebserviceName.URL_SEND_CHAPERON_INVITATION, params: dictParam as! Dictionary<String, AnyObject>, callback:{ (result,error) -> Void in
            
            if let dicResponse = result as NSDictionary? {
                if let msg = dicResponse["msg"] as? String {
                    if msg == responseStatus.success.rawValue {
                        completionHandler(true)
                    }
                    else {
                        completionHandler(false)
                    }
                }
            } else {
                completionHandler(false)
            }
        })
    }
    
    
    func getConnect_chaperon_invitation(status:String , completionHandler:@escaping (Bool , NSArray) -> ()){
        
        webservice.GETService(urlString: constantVC.WebserviceName.URL_GET_CHAPERON_INVITATION + "&to_id=\(SessionManager.get_Elite_userID())&status=\(status)" ) { (result , error) in
            if let dicResponse = result as NSDictionary? {
                if let msg = dicResponse["msg"] as? String {
                    if msg == responseStatus.success.rawValue {
                        if let data = dicResponse["data"] as? NSArray {
                            completionHandler(true , data)
                        }
                    }
                    else {
                        completionHandler(false , [])
                    }
                }
            } else {
                completionHandler(false , [])
            }
        }
    }
    
    
    func connectStatusChange_chaperon_invitation(id:String ,fromName:String ,invitationByID:String ,status: String , completionHandler:@escaping (Bool) -> ()){
        
        let dictParam : NSDictionary = ["id": "\(id)" , "status": "\(status)" , "fromname":fromName , "invitationByID": invitationByID]
        
        webservice.POSTServiceWithParameters_Elite(urlString: constantVC.WebserviceName.URL_CHAPERON_INVITATION_STATUS, params: dictParam as! Dictionary<String, AnyObject>, callback:{ (result,error) -> Void in
            
            if let dicResponse = result as NSDictionary? {
                if let msg = dicResponse["msg"] as? String {
                    if msg == responseStatus.success.rawValue {
                        completionHandler(true)
                    }
                    else {
                        completionHandler(false)
                    }
                }
            } else {
                completionHandler(false)
            }
        })
    }
    
    
    
    
    
    //MARK:- CHAPERON INVITATION
    func sendConnect_circle_invitation(dialogID: String , requestTo:String ,fromName:String , completionHandler:@escaping (Bool) -> ()){
        
        let dictParam : NSDictionary = ["from_id": SessionManager.get_Elite_userID() , "to_id": requestTo , "fromname":fromName , "dialogID": dialogID]
        
        webservice.POSTServiceWithParameters_Elite(urlString: constantVC.WebserviceName.URL_SEND_CIRCLE_INVITATION, params: dictParam as! Dictionary<String, AnyObject>, callback:{ (result,error) -> Void in
            
            if let dicResponse = result as NSDictionary? {
                if let msg = dicResponse["msg"] as? String {
                    if msg == responseStatus.success.rawValue {
                        completionHandler(true)
                    }
                    else {
                        completionHandler(false)
                    }
                }
            } else {
                completionHandler(false)
            }
        })
    }
    
    
    func getConnect_circle_invitation(status:String , completionHandler:@escaping (Bool , NSArray) -> ()){
        
        webservice.GETService(urlString: constantVC.WebserviceName.URL_GET_CIRCLE_INVITATION + "&to_id=\(SessionManager.get_Elite_userID())&status=\(status)" ) { (result , error) in
            if let dicResponse = result as NSDictionary? {
                if let msg = dicResponse["msg"] as? String {
                    if msg == responseStatus.success.rawValue {
                        if let data = dicResponse["data"] as? NSArray {
                            completionHandler(true , data)
                        }
                    }
                    else {
                        completionHandler(false , [])
                    }
                }
            } else {
                completionHandler(false , [])
            }
        }
    }
    
    
    func connectStatusChange_circle_invitation(id:String ,fromName:String ,invitationByID:String ,status: String , completionHandler:@escaping (Bool) -> ()){
        
        let dictParam : NSDictionary = ["id": "\(id)" , "status": "\(status)" , "fromname":fromName , "invitationByID": invitationByID]
        
        webservice.POSTServiceWithParameters_Elite(urlString: constantVC.WebserviceName.URL_CIRCLE_INVITATION_STATUS , params: dictParam as! Dictionary<String, AnyObject>, callback:{ (result,error) -> Void in
            
            if let dicResponse = result as NSDictionary? {
                if let msg = dicResponse["msg"] as? String {
                    if msg == responseStatus.success.rawValue {
                        completionHandler(true)
                    }
                    else {
                        completionHandler(false)
                    }
                }
            } else {
                completionHandler(false)
            }
        })
    }
    
}


