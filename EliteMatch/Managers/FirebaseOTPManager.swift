//
//  FirebaseOTPManager.swift
//  EliteMatch
//
//  Created by Kitlabs-M-0002 on 7/19/19.
//  Copyright © 2019 Kitlabs-M-0002. All rights reserved.
//

import UIKit
import Firebase
import FTIndicator
import PhoneNumberKit

class FirebaseOTPManager: NSObject {
    
    static let shared = FirebaseOTPManager()
    let phoneNumberKit = PhoneNumberKit()
    
    
    func sendVerificationOTP(phoneno:String , completionHandler:@escaping (Bool) -> ()){
        var phone = phoneno
        
        print("countryCode" , SessionManager.getCountryCode())
        
        do {
            let parsedPhNo = try phoneNumberKit.parse(phoneno , withRegion: SessionManager.getCountryCode() , ignoreType: true)
            if let formatPhNo = phoneNumberKit.format(parsedPhNo, toType: .international) as? String{
                phone = formatPhNo
            }
        } catch {
            print(error.localizedDescription)
        }
        
        PhoneAuthProvider.provider().verifyPhoneNumber( phone , uiDelegate: nil) { (verificationID , error) in
            if let error = error {
                FTIndicator.showToastMessage(error.localizedDescription)
                completionHandler(false)
                return
            }
            
            if let id = verificationID {
                SessionManager.saveOTPVerificationID(code: id)
            }
            completionHandler(true)
        }
    }
    
    
    
    func verfiyOTP(verificationCode:String , completionHandler:@escaping (Bool) -> ()){
        let credential = PhoneAuthProvider.provider().credential(withVerificationID: SessionManager.getOTPVerificationID() ,verificationCode: verificationCode)
        
        Auth.auth().signInAndRetrieveData(with: credential) { (authResult, error) in
            if let error = error {
                print(error.localizedDescription)
                completionHandler(false)
                return
            }
            completionHandler(true)
        }
    }
    
    
    func callToApi_updateFCMToken(completionHandler:@escaping (Bool) -> ()){
        
        let dictParam : NSDictionary = ["user_id":SessionManager.get_Elite_userID() , "fcm_token":SessionManager.getFCMoken()]
        
        WebserviceSigleton.shared.POSTServiceWithParameters(urlString: constantVC.WebserviceName.URL_UPDATE_FCM_TOKEN , params: dictParam as! Dictionary<String, AnyObject>) { (response , error) in
            if error == nil {
                completionHandler(true)
            } else {
                completionHandler(false)
            }
        }
        
    }
    
    
}
