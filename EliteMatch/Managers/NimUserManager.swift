//
//  NimUserManager.swift
//  EliteMatch
//
//  Created by Kitlabs-M-0002 on 7/18/19.
//  Copyright © 2019 Kitlabs-M-0002. All rights reserved.
//

import UIKit

class NimUserManager: NSObject {
    
    static let shared = NimUserManager()
    
    
    //MARK:- Registration Methods
    func logIn(user: QBUUser , completionHandler: @escaping (Bool,String) -> Void) {
        
        QBRequest.logIn(withUserLogin: user.login! , password: user.password! , successBlock: { response, userQ in
            if response.isSuccess {
                
                //save quick blox id
                if let id = userQ.id as? NSNumber {
                    SessionManager.save_QuickBloxID(id: id)
                }
                completionHandler(true , "login Successful")
            } else {
                completionHandler(false , "login UnSuccessful")
            }
            
        }, errorBlock: { response in
            
            print(response.error.debugDescription)
            
            self.signUp(user: user , completionHandler: { (success , msg) in
                if success {
                    completionHandler(true , msg)
                } else {
                    completionHandler(false , msg)
                }
            })
        })
    }
    
    
    func signUp(user: QBUUser , completionHandler: @escaping (Bool,String) -> Void) {
        
        QBRequest.signUp(user, successBlock: { response, userQ in
            
            if response.isSuccess {
                
                //save quick blox id
                if let id = userQ.id as? NSNumber {
                    SessionManager.save_QuickBloxID(id: id)
                }
                completionHandler(true , "SignUp Successful")
            } else {
                completionHandler(false , "SignUp UnSuccessful")
            }
        }, errorBlock: { response in
            
            print(response.error.debugDescription)
            completionHandler(false , response.error.debugDescription)
        })
    }
    
    func deleteUser(completionHandler: @escaping (Bool) -> Void) {
        
        QBRequest.deleteCurrentUser(successBlock: { (response) in
            completionHandler(true)
        }) { (error) in
            completionHandler(false)
        }
    }
    
    func updateUserInfo(){
        
        let updateParameters = QBUpdateUserParameters()
        updateParameters.phone = SessionManager.get_phonenumber()
        updateParameters.externalUserID = UInt(SessionManager.get_Elite_userID()) ?? 0
        updateParameters.fullName = SessionManager.getUserName()
        updateParameters.tags = ["profileUpdate"]
        
        let user = QBUUser()
        user.login = SessionManager.get_phonenumber()
        user.password = SessionManager.get_phonenumber()
        self.logIn(user: user) { (success , msg) in
            if success {
                QBRequest.updateCurrentUser(updateParameters, successBlock: { response, user in
                    // User updated successfully
                }, errorBlock: { response in
                    // Handle error
                })
            }
        }
    }
   
    
    //MARK:- Dialog Related Methods
    func loginAndGetDialogs() {
        
        if SessionManager.get_phonenumber() != "" {
            let user = QBUUser()
            user.login = SessionManager.get_phonenumber()
            user.password = SessionManager.get_phonenumber()
            
            ServicesManager.instance().logIn(with: user, completion: {
                [weak self] (success,  errorMessage) -> Void in
                
                guard success else {
                    // FTIndicator.showError(withMessage: errorMessage)
                    return
                }
                
                self?.updateUserInfo()
                
                //Core login
                Core.instance.currentUser = user
                Core.instance.loginWithCurrentUser()
                
                
                
                //save quick blox id
                guard let currentUser:QBUUser = ServicesManager.instance().currentUser  else {
                    return
                }
                
                if let id = currentUser.id as? NSNumber {
                    SessionManager.save_QuickBloxID(id: id)
                }
                
                self?.updateUserInfo()
                
                if (QBChat.instance.isConnected) {
                    self?.getDialogs()
                }
            })
        }
    }
    
    func getUserInfo(userID: String , completionHandler: @escaping (QBUUser) -> Void){
        ServicesManager.instance().usersService.getUserWithID( UInt(userID) ?? 0 , forceLoad: true).continueOnSuccessWith(block: { (task) -> Any? in
            if let user = task.result as? QBUUser {
                completionHandler(user)
            }
            return nil
        })
    }
    
    // MARK: - DataSource Action
    func getDialogs() {
        
        if let lastActivityDate = ServicesManager.instance().lastActivityDate {
            
            ServicesManager.instance().chatService.fetchDialogsUpdated(from: lastActivityDate as Date, andPageLimit: kDialogsPageLimit, iterationBlock: { (response, dialogObjects, dialogsUsersIDs, stop) -> Void in
                
            }, completionBlock: { (response) -> Void in
                
                if (response.isSuccess) {
                    
                    ServicesManager.instance().lastActivityDate = NSDate()
                }
            })
        }
        else {
            
            //  SVProgressHUD.show(withStatus: "SA_STR_LOADING_DIALOGS".localized, maskType: SVProgressHUDMaskType.clear)
            
            ServicesManager.instance().chatService.allDialogs(withPageLimit: kDialogsPageLimit, extendedRequest: nil, iterationBlock: { (response: QBResponse?, dialogObjects: [QBChatDialog]?, dialogsUsersIDS: Set<NSNumber>?, stop: UnsafeMutablePointer<ObjCBool>) -> Void in
                
            }, completion: { (response: QBResponse?) -> Void in
                
                guard response != nil && response!.isSuccess else {
                    //SVProgressHUD.showError(withStatus: "SA_STR_FAILED_LOAD_DIALOGS".localized)
                    return
                }
                
                // SVProgressHUD.showSuccess(withStatus: "SA_STR_COMPLETED".localized)
                ServicesManager.instance().lastActivityDate = NSDate()
            })
        }
    }
  
    //MARK:- Address Book Related Methods
    func retriveAddressBook(completionHandler: @escaping ([QBAddressBookContact]?) -> Void) {
        
        QBRequest.addressBook(withUdid: SessionManager.get_UUID() , successBlock: { (arrContacts) in
            
            completionHandler(arrContacts)
        }) { (response) in
            
            if !response.isSuccess {
                completionHandler(nil)
            }
        }
    }
    
    func updateAddressBook(addressBookQuick: NSMutableOrderedSet , completionHandler: @escaping (Bool) -> Void){
        
        let user = QBUUser()
        user.login = SessionManager.get_phonenumber()
        user.password = SessionManager.get_phonenumber()
        
        NimUserManager.shared.logIn(user: user) { (success , msg) in
            if success {
                //update address book
                QBRequest.uploadAddressBook(withUdid: SessionManager.getAndSave_UUID() , addressBook: addressBookQuick , force: true , successBlock: { (addressBookUpdates ) in
                    completionHandler(true)
                }, errorBlock: { (response) in
                    completionHandler(false)
                })
            } else {
                // FTIndicator.showError(withMessage: msg)
                completionHandler(false)
            }
        }
    }
    
    
    func AddNewContactToAddressBook(addressBookQuick: NSMutableOrderedSet , completionHandler: @escaping (Bool) -> Void){
        
        let user = QBUUser()
        user.login = SessionManager.get_phonenumber()
        user.password = SessionManager.get_phonenumber()
        
        NimUserManager.shared.logIn(user: user) { (success , msg) in
            if success {
                //update address book
                QBRequest.uploadAddressBook(withUdid: SessionManager.getAndSave_UUID() , addressBook: addressBookQuick , force: false , successBlock: { (addressBookUpdates ) in
                    completionHandler(true)
                }, errorBlock: { (response) in
                    completionHandler(false)
                })
            } else {
                //FTIndicator.showError(withMessage: msg)
                completionHandler(false)
            }
        }
    }
  
    //MARK:- Call Related Methods
    func callsAllowed() -> Bool {
        if !(Connectivity.isConnectedToInternet()) {
            return false
        }
        return true
    }
    
    func getOpponentsID(dialog: QBChatDialog?) -> [NSNumber]?{
        if let occupantIDS = dialog?.occupantIDs {
            let IDS = occupantIDS.filter { $0 != SessionManager.get_QuickBloxID() }
            return IDS
        }
        return nil
    }
    
    func getOpponentID(dialog: QBChatDialog?) -> NSNumber?{
        if let occupantIDS = dialog?.occupantIDs {
            let IDS = occupantIDS.filter { $0 != SessionManager.get_QuickBloxID() }
            return IDS[0]
        }
        return nil
    }
    
    func getNoFromMessage(message:String , replaceText: String) -> String{
        let no = message.replacingOccurrences(of: replaceText , with: "")
        return no
    }
    
    func parseDuration(_ timeString:String) -> TimeInterval {
        guard !timeString.isEmpty else {
            return 0
        }
        
        var interval:Double = 0
        
        let parts = timeString.components(separatedBy: ":")
        for (index, part) in parts.reversed().enumerated() {
            interval += (Double(part) ?? 0) * pow(Double(60), Double(index))
        }
        
        return interval
    }
    
    func _callNotificationMessage(for session: QBRTCSession?, state: QMCallNotificationState) -> QBChatMessage? {
        
        let senderID = ServicesManager.instance().currentUser.id
        
        let message = QBChatMessage()
        message.text = "Call notification"
        message.senderID = senderID
        message.markable = true
        message.dateSent = Date()
        message.callNotificationType = session?.conferenceType == QBRTCConferenceType.audio ? QMCallNotificationType.audio : QMCallNotificationType.video;
        message.callNotificationState = state
        
        let initiatorID = Int(session?.initiatorID ?? 0)
        let opponentID = Int(session?.opponentsIDs.first ?? 0)
        let calleeID: Int = initiatorID == senderID ? opponentID : initiatorID
        
        message.callerUserID = UInt(initiatorID)
        message.calleeUserIDs = NSIndexSet(index: calleeID) as IndexSet!
        
        message.recipientID = UInt(calleeID)
        return message
    }
    
    func sendCallNotificationMessage(with state: QMCallNotificationState, duration: TimeInterval , opponentNo: String) {
        
        let message: QBChatMessage? = _callNotificationMessage(for: constantVC.ActiveSession.session , state: state)
        
        if duration > 0 {
            
            message?.callDuration = duration
        }
        
        //custom parameters
        var callNotiType:String = ""
        var callNotiState: String = ""
        if message!.callNotificationType == QMCallNotificationType.audio {
            callNotiType = "audio"
        } else {
            callNotiType = "video"
        }
        
        if message!.callNotificationState == QMCallNotificationState.hangUp {
            callNotiState = "hangup"
        } else {
            callNotiState = "missedNoAnswer"
        }
        
        var dictCustomParam:NSMutableDictionary = ["callNotificationType" : callNotiType , "callDuration": duration , "callNotificationState": callNotiState]
        message?.customParameters = dictCustomParam
        
        
        if (constantVC.ActiveCall.isAudio == "false") {
            if constantVC.ActiveCall.isGroup == "true" {
                _sendNotificationMessage_Group(message)
            }
            else {
                _sendNotificationMessage(message , opponentNo: opponentNo)
            }
        } else {
            _sendNotificationMessage(message , opponentNo: opponentNo)
        }
    }
    
    
    func _sendNotificationMessage_Group(_ message: QBChatMessage?) {
        
        let chatDialog: QBChatDialog? = ServicesManager.instance().chatService.dialogsMemoryStorage.chatDialog(withID: constantVC.ActiveCall.dialogID)
        
        if chatDialog != nil {
            
            message?.dialogID = chatDialog?.id
            ServicesManager.instance().chatService.send(message!, to: chatDialog!, saveToHistory: true, saveToStorage: true)
        } else {
            ServicesManager.instance().chatService.loadDialog(withID: constantVC.ActiveCall.dialogID).continueWith(block: { (task) -> Any? in
                if task.isCompleted {
                    message?.dialogID = task.result?.id
                    ServicesManager.instance().chatService.send(message!, to: task.result!, saveToHistory: true, saveToStorage: true)
                }
                return nil
            })
        }
    }
    
    func _sendNotificationMessage(_ message: QBChatMessage? , opponentNo: String) {
        
        let chatDialog: QBChatDialog? = ServicesManager.instance().chatService.dialogsMemoryStorage.privateChatDialog(withOpponentID: (message?.recipientID)!)
        
        if chatDialog != nil {
            
            message?.dialogID = chatDialog?.id
            ServicesManager.instance().chatService.send(message!, to: chatDialog!, saveToHistory: true, saveToStorage: true)
            
            //send push notification
            self.sendPushNotificationForCall(msg: "Call Notification", dialog: chatDialog! , opponentNo: opponentNo)
            
        } else {
            
            
            /*ServicesManager.instance().chatService.createPrivateChatDialog(withOpponentID: (message?.recipientID)! , completion: { (response , chatDialog) in
             if response.isSuccess {
             
             if let dialog_ = chatDialog {
             QuickBloxManager().sendSystemMsg_updateDialog_toOccupants(dialog: dialog_)
             }
             
             message?.dialogID = chatDialog?.id
             ServicesManager.instance().chatService.send(message!, to: chatDialog!, saveToHistory: true, saveToStorage: true)
             //send push notification
             self.sendPushNotificationForCall(msg: "Call Notification", dialog: chatDialog!, opponentNo: opponentNo)
             }
             else {
             print(response.error.debugDescription)
             }
             })*/
        }
    }
    
    
    
    func sendPushNotificationForCall(msg: String , dialog: QBChatDialog , opponentNo: String){
        
        /*  guard let dialogID = dialog.id else {
         return
         }
         
         if dialog.type == QBChatDialogType.group {
         if let opponentIDs = QuickBloxManager().getOpponentsID(dialog: dialog) {
         var arrStrIDS = [String]()
         
         for id in opponentIDs {
         arrStrIDS.append("\(id)")
         }
         
         TamFirebaseManager.shared.getGroupMutedUsersId(collection: firebaseCollection.MuteGroup.rawValue , doc: dialogID) { (success , ids) in
         if success {
         if ids != "" {
         let arrMuteIds = ids.components(separatedBy: ",")
         
         //get ids not muted
         let newArrayID  = arrStrIDS.filter { (string) -> Bool in
         return !arrMuteIds.contains(string)
         }
         
         let strIDS = newArrayID.joined(separator: ",")
         
         //send push notification
         QuickBloxManager().sendPushNotification_chat(msg: msg , users: strIDS , title: "New Message!", fromNo: SessionManager.get_phonenumber() , toNo: "" , dialogID: dialogID)
         
         } else {
         let strIDS = arrStrIDS.joined(separator: ",")
         
         //send push notification
         QuickBloxManager().sendPushNotification_chat(msg: msg , users: strIDS , title: "New Message!", fromNo: SessionManager.get_phonenumber() , toNo: "" , dialogID: dialogID)
         }
         }
         else {
         //no muted
         let strIDS = arrStrIDS.joined(separator: ",")
         
         //send push notification
         QuickBloxManager().sendPushNotification_chat(msg: msg , users: strIDS , title: "New Message!", fromNo: SessionManager.get_phonenumber() , toNo: "" , dialogID: dialogID)
         }
         }
         }
         } else {
         
         let document = opponentNo + "-" + SessionManager.get_phonenumber()
         TamFirebaseManager.shared.getDocument(collection: firebaseCollection.Mute.rawValue , document: document) { (success) in
         if !(success) {
         //Not blocked - send custom notification
         if let opponentID = QuickBloxManager().getOpponentID(dialog: dialog) {
         
         QuickBloxManager().sendPushNotification_chat(msg: msg , users: "\(opponentID)" , title: "New Message!", fromNo: SessionManager.get_phonenumber() , toNo: opponentNo , dialogID: dialogID)
         }
         }
         }
         }*/
    }
    
    
    func sendPushNotification(msg:String , users: String , identity:String,title:String,fromNo:String , toNo:String, flag:String , postId:String , postImgUrl:String , comment:String){
        
        let message = msg
        let event = QBMEvent()
        event.notificationType = QBMNotificationType.push
        event.usersIDs = users
        event.type = QBMEventType.oneShot
        
        // custom params
        var payloadDict = ["message": message ,"identity": identity, "title": title,"FromNumber": fromNo , "ToNumber": toNo ,"flag": flag , "post_id": postId , "postImageUrl": postImgUrl , "comment": comment , "ios_badge": "1", "ios_sound": "default" , "ios_mutable_content": "1"]
        
        var error: Error? = nil
        let sendData: Data? = try? JSONSerialization.data(withJSONObject: payloadDict , options: .prettyPrinted)
        var jsonString: String? = nil
        if let aData = sendData {
            jsonString = String(data: aData, encoding: .utf8)
        }
        
        event.message = jsonString
        
        QBRequest.createEvent(event, successBlock: { response, events in
            
        }, errorBlock: { response in
            
        })
    }
    
    
    func sendPushNotification_chat(msg:String , users: String ,title:String,fromNo:String , toNo:String , dialogID: String){
        
        let message = msg
        let event = QBMEvent()
        event.notificationType = QBMNotificationType.push
        event.usersIDs = users
        event.type = QBMEventType.oneShot
        
        // custom params
        let payloadDict = ["flag": "chat" , "message": message , "title": message,"FromNumber": fromNo , "ToNumber": toNo , "dialog_id": dialogID , "ios_badge": "1", "ios_sound": "default" , "ios_mutable_content": "1"]
        
        var _: Error? = nil
        let sendData: Data? = try? JSONSerialization.data(withJSONObject: payloadDict , options: .prettyPrinted)
        var jsonString: String? = nil
        if let aData = sendData {
            jsonString = String(data: aData, encoding: .utf8)
        }
        
        event.message = jsonString
        
        QBRequest.createEvent(event, successBlock: { response, events in
            
        }, errorBlock: { response in
            
        })
    }
    
    
    func sendSilentPushNotification_updateProfile(msg:String , users: String ,fromNo:String){
        
        _ = msg
        let event = QBMEvent()
        event.notificationType = QBMNotificationType.push
        event.usersIDs = users
        event.type = QBMEventType.oneShot
        
        // custom params
        let payloadDict = ["FromNumber": fromNo , "ToNumber": users , "ios_content_available": "1" , "flag": "profile"]
        
        //["message": message , "title": message,"FromNumber": fromNo , "ToNumber": users , "ios_badge": "0", "ios_sound": "default" , "ios_mutable_content": "1" , "flag": "profile"]
        
        var _: Error? = nil
        let sendData: Data? = try? JSONSerialization.data(withJSONObject: payloadDict , options: .prettyPrinted)
        var jsonString: String? = nil
        if let aData = sendData {
            jsonString = String(data: aData, encoding: .utf8)
        }
        
        event.message = jsonString
        
        QBRequest.createEvent(event, successBlock: { response, events in
            
        }, errorBlock: { response in
            
        })
    }
    
    
    //MARK:- QB system Message
    
    func sendSystemMessageQB(dialog: QBChatDialog?){
        
        if let occupantIDS = NimUserManager.shared.getOpponentsID(dialog: dialog) {
            for id in occupantIDS {
                
                var message: QBChatMessage = QBChatMessage()
                var params = NSMutableDictionary()
                params["notification_type"] = "2"
                params["_id"] = dialog?.id
                params["room_jid"] = dialog?.roomJID
                params["occupants_ids"] = dialog?.occupantIDs
                params["name"] = dialog?.name
                params["type"] = dialog?.type.rawValue
                params["photo"] = dialog?.photo
                
                message.dialogID = dialog?.id
                message.customParameters = params
                message.recipientID = UInt(truncating: id)
                
                QBChat.instance.sendSystemMessage(message) { (error) in
                    if error != nil {
                        print(error.debugDescription)
                    }
                }
            }
        }
    }
    
    //MARK:- Privacy Manager
    func retrievePrivacyList(){
        QBChat.instance.retrievePrivacyList(withName: "public")
    }
    
    //MARK:- BLOCK USER
    func blockAUser(userQuickBloxID:String , userServerID: String , completionHandler:@escaping (Bool) -> ()){
        
        var userID:String = ""
        if let id = SessionManager.get_QuickBloxID() {
            userID = "\(id)"
        }
        
        NimUserManager.shared.addBlockedUserToSession(userQuickBloxID: userQuickBloxID)
        NimUserManager.shared.blockUserQuickBlox(userQuickBloxID: userQuickBloxID)
        completionHandler(true)
        
        /*let webservice  = WebserviceSigleton ()
        let dictParam : NSDictionary = ["login_users_id":SessionManager.get_Elite_userID() , "other_users_id":userServerID, "loginQuickBloxID": userID , "otherQuickBloxID": userQuickBloxID , "status": "1"]
        webservice.POSTServiceWithParameters(urlString: constantVC.WebserviceName.URL_BLOCKUSER, params: dictParam as! Dictionary<String, AnyObject>, callback:{ (result,error) -> Void in
            
            if result != nil{
                let response = result! as Dictionary
                let resultMessage = response["msg"] as! String
                if resultMessage == "success"{
                    NimUserManager.shared.addBlockedUserToSession(userQuickBloxID: userQuickBloxID)
                    completionHandler(true)
                    NimUserManager.shared.blockUserQuickBlox(userQuickBloxID: userQuickBloxID)
                }
                else{
                    completionHandler(false)
                }
            }
            else{
                completionHandler(false)
            }
        })*/
    }
    
    
    func addBlockedUserToSession(userQuickBloxID: String){
        if let arrIDs = SessionManager.get_chatBlockedUsersID() {
            var arr = arrIDs
            arr.append(userQuickBloxID)
            SessionManager.save_chatBlockedUsersID(IDS: arr)
        } else {
            var arrIds:[String] = []
            arrIds.append(userQuickBloxID)
            SessionManager.save_chatBlockedUsersID(IDS: arrIds)
        }
    }
    
    
    func removeBlockedUserToSession(userQuickBloxID: String) {
        if let arrIDs = SessionManager.get_chatBlockedUsersID() {
            let arr = arrIDs.filter{$0 != userQuickBloxID}
            SessionManager.save_chatBlockedUsersID(IDS: arr)
        }
    }
    
    func UnblockUser(userQuickBloxID:String , userServerID: String , completionHandler:@escaping (Bool) -> ()) {
        
        var userID:String = ""
        if let id = SessionManager.get_QuickBloxID() {
            userID = "\(id)"
        }
        
        NimUserManager.shared.removeBlockedUserToSession(userQuickBloxID: userQuickBloxID)
        NimUserManager.shared.UnblockUserQuickBlox(userQuickBloxID: userQuickBloxID)
        completionHandler(true)
        
       /* let webservice  = WebserviceSigleton ()
        let dictParam : NSDictionary = ["login_users_id":SessionManager.get_Elite_userID() , "other_users_id":userServerID, "loginQuickBloxID": userID , "otherQuickBloxID": userQuickBloxID ,"status": "0"]
        webservice.POSTServiceWithParameters(urlString: constantVC.WebserviceName.URL_BLOCKUSER, params: dictParam as! Dictionary<String, AnyObject>, callback:{ (result,error) -> Void in
            
            if result != nil{
                let response = result! as Dictionary
                let resultMessage = response["msg"] as! String
                if resultMessage == "success"{
                    NimUserManager.shared.removeBlockedUserToSession(userQuickBloxID: userQuickBloxID)
                    completionHandler(true)
                    NimUserManager.shared.UnblockUserQuickBlox(userQuickBloxID: userQuickBloxID)
                }
                else{
                    completionHandler(false)
                }
            }
            else{
                completionHandler(false)
            }
        })*/
    }
    
    func loadBlockUsersList() {
        
        let webservice  = WebserviceSigleton ()
        let dictParam : NSDictionary = ["login_users_id": SessionManager.get_Elite_userID(), "detail_type":"full"]
        webservice.POSTServiceWithParameters(urlString: constantVC.WebserviceName.URL_GETBLOCKED, params: dictParam as! Dictionary<String, AnyObject>, callback:{ (result,error) -> Void in
            if result != nil{
                let response = result! as Dictionary
                let resultMessage = response["msg"] as! String
                if resultMessage == "success"{
                    
                    var arrIDS:[String] = []
                    
                    if let userDetails = response["user_details"] as? [[String:Any]] {
                        for item in userDetails {
                            
                            if let id = item["quickbloxID"] {
                                arrIDS.append("\(id)")
                            }
                        }
                    }
                    
                    //save blocked users to defaults
                    if arrIDS.count > 0 {
                        SessionManager.save_chatBlockedUsersID(IDS: arrIDS)
                    } else {
                        SessionManager.save_chatBlockedUsersID(IDS: [])
                    }
                }
                else{
                    SessionManager.save_chatBlockedUsersID(IDS: [])
                }
            }
            else{
                SessionManager.save_chatBlockedUsersID(IDS: [])
            }
        })
    }
    
    
    func blockUserQuickBlox(userQuickBloxID:String) {
        
        let item: QBPrivacyItem = QBPrivacyItem(privacyType: .userID , userID: UInt(userQuickBloxID) ?? 0 , allow: false)
        item.mutualBlock = true
        let list: QBPrivacyList = QBPrivacyList(name: "public", items: [item])
        QBChat.instance.setPrivacyList(list)
        QBChat.instance.setDefaultPrivacyListWithName("public")
        constantVC.ActiveDataSource.isNeedToRefreshPost = true
    }
    
    
    func UnblockUserQuickBlox(userQuickBloxID:String) {
        let item: QBPrivacyItem = QBPrivacyItem(privacyType: .userID , userID: UInt(userQuickBloxID) ?? 0, allow: true)
        item.mutualBlock = false
        let list: QBPrivacyList = QBPrivacyList(name: "public", items: [item])
        QBChat.instance.setPrivacyList(list)
        QBChat.instance.setDefaultPrivacyListWithName("public")
        constantVC.ActiveDataSource.isNeedToRefreshPost = true
    }
    
    //MARK:- Check blocked user
    func isUserBlocked(dialog: QBChatDialog , document:String , completionHandler:@escaping (Bool) -> ()) {
        
        //        if dialog.type == QBChatDialogType.private {
        //            TamFirebaseManager.shared.getDocument(collection: firebaseCollection.Block.rawValue , document: document) { (success) in
        //                if success {
        //                    completionHandler(true)
        //                } else {
        //                    completionHandler(false)
        //                }
        //            }
        //        }  else {
        //            completionHandler(false)
        //        }
    }
    
    //MARK:- Serialize Methods
    func convertDictionaryToJsonString(dict: NSMutableDictionary) -> String {
        let jsonData = try? JSONSerialization.data(withJSONObject: dict, options: JSONSerialization.WritingOptions())
        if let jsonString = NSString(data: jsonData!, encoding: String.Encoding.utf8.rawValue) {
            return "\(jsonString)"
        }
        return ""
    }
    
    func convertJsonStringToDictionary(text: String) -> [String: Any]? {
        if let data = text.replacingOccurrences(of: "\n", with: "").data(using: String.Encoding.utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String:AnyObject]
            } catch {
                print(error.localizedDescription)
            }
        }
        return nil
    }
    
    
    //MARK:- Clear chat - Update dialog
    func clearChat_UpdateDialog(dialog: QBChatDialog , completionHandler:@escaping (Bool) -> ()){
        if let customData = dialog.data {
            var data = customData
            
            if let d = data["number"] as? String {
                
                if let customDic = self.convertJsonStringToDictionary(text: d) {
                    
                    var dic = customDic
                    
                    if let ids = customDic["clearChat"] as? String {
                        if ids != "" {
                            dic["clearChat"] = ids + "," + SessionManager.get_phonenumber()
                        }
                        else {
                            dic["clearChat"] = SessionManager.get_phonenumber()
                        }
                    }
                    
                    let dictNSMutable = NSMutableDictionary(dictionary: dic)
                    
                    if let jsonStr = self.convertDictionaryToJsonString(dict: dictNSMutable) as? String {
                        data["number"] = jsonStr
                    }
                    
                }
                else {
                    let newDic = NSMutableDictionary()
                    newDic.setValue(d, forKey: "number")
                    newDic.setValue(SessionManager.get_phonenumber(), forKey: "clearChat")
                    
                    if let jsonStr = self.convertDictionaryToJsonString(dict: newDic) as? String {
                        data["number"] = jsonStr
                    }
                }
            }
            else { //for group
                let newDic = NSMutableDictionary()
                newDic.setValue(SessionManager.get_phonenumber(), forKey: "clearChat")
                
                if let jsonStr = self.convertDictionaryToJsonString(dict: newDic) as? String {
                    data["number"] = jsonStr
                }
            }
            
            
            dialog.data = data
        }
        
        QBRequest.update(dialog, successBlock: { (response , dialogNew) in
            
            //update in cache
            self.sendSystemMsg_updateDialog(dialog: dialogNew)
            
            completionHandler(true)
        }) { (error) in
            completionHandler(false)
        }
    }
    
    
    func UnClearChat_updateDialog(dialog: QBChatDialog , completionHandler:@escaping (Bool) -> ()){
        if let customData = dialog.data {
            var data = customData
            
            if let d = data["number"] as? String {
                
                if let customDic = self.convertJsonStringToDictionary(text: d) {
                    
                    var dic = customDic
                    
                    if let ids = customDic["clearChat"] as? String {
                        
                        let arrID = ids.components(separatedBy: ",")
                        let arrNew = arrID.filter{$0 != SessionManager.get_phonenumber()}
                        
                        if arrNew.count > 0 {
                            let str = arrNew.joined(separator: ",")
                            dic["clearChat"] = str
                        } else {
                            dic["clearChat"] = ""
                        }
                    }
                    
                    let dictNSMutable = NSMutableDictionary(dictionary: dic)
                    
                    if let jsonStr = self.convertDictionaryToJsonString(dict: dictNSMutable) as? String {
                        data["number"] = jsonStr
                    }
                }
            }
            
            dialog.data = data
        }
        
        QBRequest.update(dialog, successBlock: { (response , dialogNew) in
            
            //update in cache
            self.sendSystemMsg_updateDialog(dialog: dialogNew)
            
            completionHandler(true)
        }) { (error) in
            completionHandler(false)
        }
    }
    
    
    func sendSystemMsg_updateDialog(dialog: QBChatDialog){
        
        if let id = SessionManager.get_QuickBloxID() {
            
            var message: QBChatMessage = QBChatMessage()
            message.dialogID = dialog.id
            message.customParameters = NSMutableDictionary(dictionary: dialog.data!)
            message.recipientID = UInt(truncating: id)
            message.text = "updateDialog"
            
            QBChat.instance.sendSystemMessage(message) { (error) in
                if error != nil {
                    print(error.debugDescription)
                }
            }
        }
    }
    
    
    func sendSystemMsg_updateDialog_toOccupants(dialog: QBChatDialog){
        
        if let id = NimUserManager.shared.getOpponentID(dialog: dialog) {
            
            var message: QBChatMessage = QBChatMessage()
            message.dialogID = dialog.id
            message.recipientID = UInt(truncating: id)
            message.text = "createNewDialog"
            
            QBChat.instance.sendSystemMessage(message) { (error) in
                if error != nil {
                    print(error.debugDescription)
                }
            }
        }
    }
    
    
    func sendPushNotification_profileUpdate_tags(strUrl: String){
        
        let payload = ["flag": "profileUpdate" , "imgUrl": strUrl , "ios_voip": "1", UsersConstant.alert: "1"]
        let data = try? JSONSerialization.data(withJSONObject: payload,
                                               options: .prettyPrinted)
        var message = ""
        if let data = data {
            message = String(data: data, encoding: .utf8) ?? ""
        }
        let event = QBMEvent()
        event.notificationType = QBMNotificationType.push
        event.usersTagsAll = "profileUpdate"
        event.type = QBMEventType.oneShot
        event.message = message
        QBRequest.createEvent(event, successBlock: { response, events in
            if response.isSuccess {
                print("success")
            } else {
                print("error")
            }
            
        }, errorBlock: { response in
            print(response.error?.debugDescription)
        })
        
        
        /*let event = QBMEvent()
        event.notificationType = QBMNotificationType.push
        event.usersTagsAll = "profileUpdate"
        event.type = QBMEventType.oneShot
        
        // custom params
        var payloadDict = ["flag": "profileUpdate" , "imgUrl": strUrl , "ios_voip": "1"]
        
        var error: Error? = nil
        let sendData: Data? = try? JSONSerialization.data(withJSONObject: payloadDict , options: .prettyPrinted)
        var jsonString: String? = nil
        if let aData = sendData {
            jsonString = String(data: aData, encoding: .utf8)
        }
        
        event.message = jsonString
        QBRequest.createEvent(event, successBlock: { response, events in
            
        }, errorBlock: { response in
            
        })*/
    }
   
    
    func isClearChat(dialog: QBChatDialog) -> Bool{
        
        if let customData = dialog.data {
            if let d = customData["number"] as? String {
                if let customDic = NimUserManager.shared.convertJsonStringToDictionary(text: d) {
                    if let ids = customDic["clearChat"] as? String {
                        
                        let arr = ids.components(separatedBy: ",")
                        if arr.contains(SessionManager.get_phonenumber()) {
                            return true
                        }
                        else {
                            return false
                        }
                    }
                }
            }
        }
        return false
    }
    
    
    //MARK:- Permission
    func UnknownUserPopUp_UpdateDialog(status: String , dialog: QBChatDialog , completionHandler:@escaping (Bool) -> ()){
        if let customData = dialog.data {
            var data = customData
            
            if let isAllowed = data["isAllowed"] as? String {
                var statusToUpdate = isAllowed
                statusToUpdate = status
                data["isAllowed"] = statusToUpdate
            }
            
            dialog.data = data
            
            QBRequest.update(dialog, successBlock: { (response , dialogNew) in
                
                //update dialog in memory
                self.sendSystemMsg_updateDialog(dialog: dialogNew)
                
                completionHandler(true)
            }) { (error) in
                completionHandler(false)
            }
        }
    }
    
    
    func sendPushNotification_forScreenShotTaken(users: String , fromNo:String , toNo:String , dialogID: String , msg: String){
        
        let message = msg
        let event = QBMEvent()
        event.notificationType = QBMNotificationType.push
        event.usersIDs = users
        event.type = QBMEventType.oneShot
        
        // custom params
        let payloadDict = ["flag": "screenshot" , "message": message , "title": message,"FromNumber": fromNo , "ToNumber": toNo , "dialog_id": dialogID , "ios_badge": "1", "ios_sound": "default" , "ios_mutable_content": "1"]
        
        var error: Error? = nil
        let sendData: Data? = try? JSONSerialization.data(withJSONObject: payloadDict , options: .prettyPrinted)
        var jsonString: String? = nil
        if let aData = sendData {
            jsonString = String(data: aData, encoding: .utf8)
        }
        
        event.message = jsonString
        QBRequest.createEvent(event, successBlock: { response, events in
            
        }, errorBlock: { response in
            
        })
    }
    
 
}


struct QuickBloxAccount {
    static let kQBApplicationID:UInt = 74784
    static let kQBAuthKey = "QTewSvgec63kFA3"
    static let kQBAuthSecret = "ZEcVX4gDrDJtdnc"
    static let kQBAccountKey = "FznULugPRAsSGf3_eM1s"
    static let kQBApiEndPoint = "https://apitamaaas.quickblox.com"
    static let kQBchatEndPoint = "chattamaaas.quickblox.com"
}

    
class MatchTimer:NSObject {

    static let shared = MatchTimer()
    
    
    func getTimeLeft_minutes(lastUpdatedTime: String) ->Int{
        
        if let lastUpdatedUTCDate = CommonFunctions.shared.getUTC(date: lastUpdatedTime) {
            
            let calendar = NSCalendar.current
            let endate = calendar.date(byAdding: .hour , value: 24 , to: lastUpdatedUTCDate)
            
            let formatter = DateFormatter()
            formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
            let currentDateStr = formatter.string(from: Date())
            
            if let currentUTCDate = CommonFunctions.shared.getUTC(date: currentDateStr) {
                let diff = Int(endate!.timeIntervalSince1970 - currentUTCDate.timeIntervalSince1970)
                let hours = diff / 3600
                let minutes = (diff - hours * 3600) / 60
                let totalMins = minutes + (hours * 60)
                return totalMins
            }
        }
        
       /* if let lastUpdatedDate = CommonFunctions.shared.UTCToLocal(date: lastUpdatedTime) {
            
            let calendar = NSCalendar.current
            let endate = calendar.date(byAdding: .hour , value: 24 , to: lastUpdatedDate)
            
            if let startDate = CommonFunctions.shared.UTCToLocal(date: self.getCurrentTimeString()) {
                
                let diff = Int(endate!.timeIntervalSince1970 - startDate.timeIntervalSince1970)
                let hours = diff / 3600
                let minutes = (diff - hours * 3600) / 60
                return minutes
            }
        }*/
      return 0
    }
    
    
    func getCurrentTimeString() ->String{
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let currentTime = formatter.string(from: Date())
        return currentTime
    }

}
