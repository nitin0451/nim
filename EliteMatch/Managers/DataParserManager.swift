//
//  DataParserManager.swift
//  EliteMatch
//
//  Created by Kitlabs-M-0002 on 7/22/19.
//  Copyright © 2019 Kitlabs-M-0002. All rights reserved.
//

import UIKit

class DataParserManager: NSObject {
    
    static let shared = DataParserManager()
    
    func getDailyMatch(arrData: NSArray) ->[DailyMatch]?{
        var arrDailyMatch:[DailyMatch] = []
        for (index , item) in arrData.enumerated() {
            
            if let dicData = item as? NSDictionary {
                
                let objDailyMatch = DailyMatch()
                
                if let dicProfile = dicData.object(forKey: "userData") as? NSDictionary {
                
                    if let id = dicProfile.object(forKey: "id") as? String {
                       objDailyMatch.id = id
                    }
                    if let phoneNumber = dicProfile.object(forKey: "phoneNumber") as? String {
                        objDailyMatch.phoneNumber = phoneNumber
                    }
                    if let user_is = dicProfile.object(forKey: "user_is") as? String {
                        objDailyMatch.user_is = user_is
                    }
                    if let user_looking_for = dicProfile.object(forKey: "user_looking_for") as? String {
                        objDailyMatch.user_looking_for = user_looking_for
                    }
                    if let instagram_id = dicProfile.object(forKey: "instagram_id") as? String {
                        objDailyMatch.instagram_id = instagram_id
                    }
                    if let insta_url = dicProfile.object(forKey: "insta_url") as? String {
                        objDailyMatch.insta_url = insta_url
                    }
                    if let linkedin_id = dicProfile.object(forKey: "linkedin_id") as? String {
                        objDailyMatch.linkedin_id = linkedin_id
                    }
                    if let linkedin_url = dicProfile.object(forKey: "linkedin_url") as? String {
                        objDailyMatch.linkedin_url = linkedin_url
                    }
                    if let name = dicProfile.object(forKey: "name") as? String {
                        objDailyMatch.name = name
                    }
                    if let email = dicProfile.object(forKey: "email") as? String {
                        objDailyMatch.email = email
                    }
                    if let dob = dicProfile.object(forKey: "dob") as? String {
                        objDailyMatch.dob = dob
                    }
                    if let about_me = dicProfile.object(forKey: "about_me") as? String {
                        objDailyMatch.about_me = about_me
                    }
                    if let languages = dicProfile.object(forKey: "languages") as? String {
                        objDailyMatch.languages = languages
                    }
                    if let religion = dicProfile.object(forKey: "religion") as? String {
                        objDailyMatch.religion = religion
                    }
                    if let living_place = dicProfile.object(forKey: "living_place") as? String {
                        objDailyMatch.living_place = living_place
                    }
                    if let travelPlaces = dicProfile.object(forKey: "travelPlaces") as? String {
                        objDailyMatch.travelPlaces = travelPlaces
                    }
                    if let education = dicProfile.object(forKey: "education") as? String {
                        objDailyMatch.education = education
                    }
                    if let lifestyle = dicProfile.object(forKey: "lifestyle") as? String {
                        objDailyMatch.lifestyle = lifestyle
                    }
                    if let height = dicProfile.object(forKey: "height") as? String {
                        objDailyMatch.height = height
                    }
                    if let fav_shows = dicProfile.object(forKey: "fav_shows") as? String {
                        objDailyMatch.fav_shows = fav_shows
                    }
                    if let fav_dessert = dicProfile.object(forKey: "fav_dessert") as? String {
                        objDailyMatch.fav_dessert = fav_dessert
                    }
                    if let fav_music = dicProfile.object(forKey: "fav_music") as? String {
                        objDailyMatch.fav_music = fav_music
                    }
                    if let fav_food = dicProfile.object(forKey: "fav_food") as? String {
                        objDailyMatch.fav_food = fav_food
                    }
                    if let zoadicSign = dicProfile.object(forKey: "zoadicSign") as? String {
                        objDailyMatch.zoadicSign = zoadicSign
                    }
                    if let ethnicity = dicProfile.object(forKey: "ethnicity") as? String {
                        objDailyMatch.ethnicity = ethnicity
                    }
                    if let petpeeve = dicProfile.object(forKey: "petpeeve") as? String {
                        objDailyMatch.petpeeve = petpeeve
                    }
                    if let isDrink = dicProfile.object(forKey: "isDrink") as? String {
                        objDailyMatch.isDrink = isDrink
                    }
                    if let isSmoke = dicProfile.object(forKey: "isSmoke") as? String {
                        objDailyMatch.isSmoke = isSmoke
                    }
                    if let isHaveChildren = dicProfile.object(forKey: "isHaveChildren") as? String {
                        objDailyMatch.isHaveChildren = isHaveChildren
                    }
                    if let isMarriedBefore = dicProfile.object(forKey: "isMarriedBefore") as? String {
                        objDailyMatch.isMarriedBefore = isMarriedBefore
                    }
                    if let favHoliday = dicProfile.object(forKey: "favHoliday") as? String {
                        objDailyMatch.favHoliday = favHoliday
                    }
                    if let isPreferred_TallerPartner = dicProfile.object(forKey: "isPreferred_TallerPartner") as? String {
                        objDailyMatch.isPreferred_TallerPartner = isPreferred_TallerPartner
                    }
                    if let insta_imgs = dicProfile.object(forKey: "insta_imgs") as? String {
                        objDailyMatch.insta_imgs = insta_imgs
                    }
                    if let lastupdated_status = dicProfile.object(forKey: "lastupdated_status") as? String {
                        objDailyMatch.lastupdated_status = lastupdated_status
                    }
                    if let inSchool = dicProfile.object(forKey: "inSchool") as? String {
                        objDailyMatch.inSchool = inSchool
                    }
                    if let careerName = dicProfile.object(forKey: "careerName") as? String {
                        objDailyMatch.careerName = careerName
                    }
                    if let countyCode = dicProfile.object(forKey: "countyCode") as? String {
                        objDailyMatch.countyCode = countyCode
                    }
                    if let inCareer = dicProfile.object(forKey: "inCareer") as? String {
                        objDailyMatch.inCareer = inCareer
                    }
                    if let isWorkOutRoutinely = dicProfile.object(forKey: "isWorkOutRoutinely") as? String {
                        objDailyMatch.isWorkOutRoutinely = isWorkOutRoutinely
                    }
                    if let isOnlyEatHalal = dicProfile.object(forKey: "isOnlyEatHalal") as? String {
                        objDailyMatch.isOnlyEatHalal = isOnlyEatHalal
                    }
                    if let religionCount = dicProfile.object(forKey: "religionCount") as? String {
                        objDailyMatch.religionCount = religionCount
                    }
                    if let ethnicityCount = dicProfile.object(forKey: "ethnicityCount") as? String {
                        objDailyMatch.ethnicityCount = ethnicityCount
                    }
                    if let status = dicProfile.object(forKey: "status") as? String {
                        objDailyMatch.status = status
                    }
                    if let score = dicProfile.object(forKey: "score") as? String {
                        objDailyMatch.score = score
                    }
                    
                    //invitations
                    if let invites = dicProfile.object(forKey: "invetation") as? NSArray {
                        for item in invites {
                            let objInvites = Invitation()
                            
                            if let dic = item as? NSDictionary {
                                if let id = dic.object(forKey: "id") as? String {
                                    objInvites.id = id
                                }
                                if let from_id = dic.object(forKey: "from_id") as? String {
                                    objInvites.from_id = from_id
                                }
                                if let to_id = dic.object(forKey: "to_id") as? String {
                                    objInvites.to_id = to_id
                                }
                                if let status = dic.object(forKey: "status") as? String {
                                    objInvites.status = status
                                }
                            }
                            objDailyMatch.invitations.append(objInvites)
                        }
                    }
                    
                    //user Images
                    if let images = dicProfile.object(forKey: "images") as? NSArray {
                        
                        var arrImgs:[Image] = []
                        for item in images {
                            let objimage = Image()
                            
                            if let dic = item as? NSDictionary {
                                if let id = dic.object(forKey: "id") as? String {
                                    objimage.id = id
                                }
                                if let user_id = dic.object(forKey: "user_id") as? String {
                                    objimage.user_id = user_id
                                }
                                if let filename = dic.object(forKey: "filename") as? String {
                                    objimage.filename = filename
                                }
                                if let order = dic.object(forKey: "orderID") as? String {
                                    objimage.order = order
                                }
                            }
                            arrImgs.append(objimage)
                        }
                        
                        //arrange images according to order
                        arrImgs = arrImgs.sorted(by: { $0.order ?? "" < $1.order ?? "" })
                        objDailyMatch.userImages.append(contentsOf: arrImgs)
                    }
                    
                    arrDailyMatch.append(objDailyMatch)
                }
            }
        }
        return arrDailyMatch
    }
    
    
    func getConnectInvitation(arrData: NSArray) ->[Invitation]?{
        var arrInvites:[Invitation] = []
        
        for invite in arrData {
            let objInvites = Invitation()
            
            if let dic = invite as? NSDictionary {
                
                if let id = dic.object(forKey: "dialogID") as? String {
                    objInvites.dialogID = id
                }
                if let id = dic.object(forKey: "id") as? String {
                    objInvites.id = id
                }
                if let from_id = dic.object(forKey: "from_id") as? String {
                    objInvites.from_id = from_id
                }
                if let to_id = dic.object(forKey: "to_id") as? String {
                    objInvites.to_id = to_id
                }
                if let status = dic.object(forKey: "status") as? String {
                    objInvites.status = status
                }
                if let name = dic.object(forKey: "name") as? String {
                    objInvites.name = name
                }
                if let from_phone_number = dic.object(forKey: "from_phone_number") as? String {
                    objInvites.from_phone_number = from_phone_number
                }
                if let to_phone_number = dic.object(forKey: "to_phone_number") as? String {
                    objInvites.to_phone_number = to_phone_number
                }
                if let createdAt = dic.object(forKey: "createdAt") as? String {
                    objInvites.createdAt = createdAt
                }
                if let image = dic.object(forKey: "image") as? String {
                    objInvites.image = image
                }
            }
            arrInvites.append(objInvites)
        }
        return arrInvites
    }

}
