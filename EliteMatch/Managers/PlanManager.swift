//
//  PlanManager.swift
//  EliteMatch
//
//  Created by Apple on 11/10/19.
//  Copyright © 2019 Kitlabs-M-0002. All rights reserved.
//

import UIKit

class PlanManager: NSObject {
    
    
    static let shared = PlanManager()
    
    
    func updateMembership(startDate:String , endDate: String , planID:String , completionHandler:@escaping (Bool) -> ()){
        
        let dictParam : NSDictionary = ["plan_start_date": startDate , "plan_end_date": endDate , "plan_id": planID , "user_id": SessionManager.get_Elite_userID()]
        
        WebserviceSigleton().POSTServiceWithParameters(urlString: constantVC.WebserviceName.URL_UPDATE_PLAN_DETAILS , params: dictParam as! Dictionary<String, AnyObject>) { (result , error) in
            
            if result != nil{
                let response = result! as NSDictionary
                print(response)
                completionHandler(true)
            }
            else {
                completionHandler(false)
            }
        }
        
    }
    
    
    func PlanstartDate() ->String{
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let startDate = formatter.string(from: Date())
        return startDate
    }
    
    
    func PlanEndDate() ->String{
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let endDate = (Calendar.current as NSCalendar).date(byAdding: .day, value: 30, to: Date(), options: [])!
        let strEndDate = formatter.string(from: endDate)
        return strEndDate
    }
    
    
    func stringToDate(strDate: String) ->Date? {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        dateFormatter.timeZone = TimeZone(abbreviation: "GMT+0:00") //Current time zone
        //according to date format your date string
        guard let date = dateFormatter.date(from: strDate) else {
            return nil
        }
       return date
    }
    

}
