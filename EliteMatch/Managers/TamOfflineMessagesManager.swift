//
//  TamOfflineMessagesManager.swift
//  Tamaas
//
//  Created by Apple on 19/09/19.
//  Copyright © 2019 Krescent Global. All rights reserved.
//

import UIKit

class TamOfflineMessagesManager: NSObject {
    
    static let shared = TamOfflineMessagesManager()
    
    func saveUnsentMsgsIds(msgID:String){
        if let ids = SessionManager.getUnSentMsgs_ids() {
            var arr = ids.components(separatedBy: ",")
            if !arr.contains(msgID) {
                arr.append(msgID)
            }
            let str = arr.joined(separator: ",")
            SessionManager.saveUnSentMsgs_ids(ids: str)
        }
        else {
            SessionManager.saveUnSentMsgs_ids(ids: msgID)
        }
    }
    
    
    func removeUnsentMsgsIds(msgID:String) {
        if let ids = SessionManager.getUnSentMsgs_ids() {
            var arr = ids.components(separatedBy: ",")
            if let index = arr.index(of: msgID) {
                arr.remove(at: index)
                let str = arr.joined(separator: ",")
                SessionManager.saveUnSentMsgs_ids(ids: str)
            }
        }
    }
    
    
    func isOfflineAddedMsg(msgID:String) ->Bool{
        if let ids = SessionManager.getUnSentMsgs_ids() {
            let arr = ids.components(separatedBy: ",")
            if arr.contains(msgID) {
                return true
            }
        }
        return false
    }
    
    
    func countOfUnsentMsgs() ->Int{
        if let ids = SessionManager.getUnSentMsgs_ids() {
            let arr = ids.components(separatedBy: ",")
            return arr.count
        }
        return 0
    }
   
  
}
