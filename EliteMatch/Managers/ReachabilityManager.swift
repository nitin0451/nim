//
//  ReachabilityManager.swift
//  Tamaas
//
//  Created by Krescent Global on 05/09/18.
//  Copyright © 2018 Krescent Global. All rights reserved.
//

import UIKit
import FTIndicator
import Reachability

class ReachabilityManager: NSObject {
    
    static let shared = ReachabilityManager()
    let reachability = Reachability()!
    var isCallConnect:Bool = false
    
    func startMonitoring() {
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.reachabilityChanged),
                                               name: .reachabilityChanged,
                                               object: reachability)
        do{
            try reachability.startNotifier()
        } catch {
            debugPrint("Could not start reachability notifier")
        }
    }
    
    func stopMonitoring(){
        reachability.stopNotifier()
        NotificationCenter.default.removeObserver(self,
                                                  name: .reachabilityChanged,
                                                  object: reachability)
    }
    
    @objc func reachabilityChanged(notification: Notification) {
        let reachability = notification.object as! Reachability
        switch reachability.connection {
        case .none:
            debugPrint("Network became unreachable")
            self.handle_internetNOTActive()
        case .wifi:
            debugPrint("Network reachable through WiFi")
            self.handle_internetActive()
        case .cellular:
            debugPrint("Network reachable through Cellular Data")
            self.handle_internetActive()
        }
    }
    
    func handle_internetActive() {
        NotificationCenter.default.post(name: constantVC.NSNotification_name.NSNotification_InternetConnected , object: nil)
    }
    
    func handle_internetNOTActive() {
         NotificationCenter.default.post(name: constantVC.NSNotification_name.NSNotification_InternetDisconnected , object: nil)
    }

    
    class func isConnectedToNetwork() -> Bool {
        
        var zeroAddress = sockaddr_in()
        
        zeroAddress.sin_len = UInt8(MemoryLayout.size(ofValue: zeroAddress))
        
        zeroAddress.sin_family = sa_family_t(AF_INET)
        
        let defaultRouteReachability = withUnsafePointer(to: &zeroAddress) {
            
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {zeroSockAddress in
                
                SCNetworkReachabilityCreateWithAddress(nil, zeroSockAddress)
                
            }
        }
        
        var flags = SCNetworkReachabilityFlags()
        
        if !SCNetworkReachabilityGetFlags(defaultRouteReachability!, &flags) {
            
            return false
            
        }
        
        let isReachable = (flags.rawValue & UInt32(kSCNetworkFlagsReachable)) != 0
        
        let needsConnection = (flags.rawValue & UInt32(kSCNetworkFlagsConnectionRequired)) != 0
        
        return (isReachable && !needsConnection)
        
    }
    
}
