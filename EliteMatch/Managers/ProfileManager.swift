//
//  ProfileManager.swift
//  EliteMatch
//
//  Created by Kitlabs-M-0002 on 8/2/19.
//  Copyright © 2019 Kitlabs-M-0002. All rights reserved.
//

import UIKit
import YYWebImage
import Alamofire

class ProfileManager: NSObject {
    
    
    static let shared = ProfileManager()
    
    
    func getUserProfile_phoneNo(phoneNo: String , completionHandler:@escaping (Bool , UserProfile?) -> () ) {
        
        var idchaperon:String = ""
        if SessionManager.isChaperonUser() {
            idchaperon = "1"
        }
        else {
            idchaperon = "0"
        }
        
        let dictParam : NSDictionary = ["phoneNumber": phoneNo , "idchaperon": idchaperon]
        
        WebserviceSigleton().GETServiceWithParameters(urlString: constantVC.GeneralConstants.MATCH_BASE_URL + constantVC.WebserviceName.URL_GET_PROFILE_BY_PHONENO   , params: dictParam as! Dictionary<String, AnyObject> , callback: { (result,error) -> Void in
            if result != nil{
                let response = result! as NSDictionary
                
                if let data = response["data"] as? [String : Any] {
                    
                    let user = UserProfile(JSON: data)
                    
                    //arrange userImages
                    if var images = user?.images {
                        let arrImgs = self.arrangeProfileAccordingToOrder(arrImages: images)
                        images = arrImgs
                        
                        //save user profile
                        user?.images = images
                    }
                    
                    //save user name
                    if phoneNo == SessionManager.get_phonenumber() {
                        if let name = user?.name {
                            SessionManager.saveUserName(name: name)
                        }
                        if let gender = user?.user_is {
                            SessionManager.save_myProfileGender(value: gender)
                        }
                        
                        //save user approval status
                        if let status = user?.status {
                            if status == "1" {
                                SessionManager.saveProfileApprovalStatus(status: UserProfileStatus.approved.rawValue)
                            }
                        }
                    }
                    
                    completionHandler(true , user)
                }
                else {
                    completionHandler(false , nil)
                }
            }
            else{
                completionHandler(false , nil)
            }
        })
    }
    
    
    func arrangeProfileAccordingToOrder(arrImages: [UserImage]) -> [UserImage] {
        return arrImages.sorted(by: { $0.order ?? "" < $1.order ?? "" })
    }
    
    
    func UpdateProfile(param:NSDictionary , completionHandler:@escaping (Bool) -> ()){
        
        WebserviceSigleton().POSTServiceWithParameters_Elite(urlString: constantVC.WebserviceName.URL_EDIT_PROFILE, params: param as! Dictionary<String, AnyObject>, callback:{ (result,error) -> Void in
            if result != nil{
                let resultMessage = result!["msg"] as! String
                if resultMessage == "success"{
                    completionHandler(true)
                }
                else{
                   completionHandler(false)
                }
            }else {
                completionHandler(false)
            }
        })
    }
    
    
    func getMyProfileUrl() ->URL{
        return URL.init(string: constantVC.GeneralConstants.GET_IMAGE + SessionManager.get_phonenumber() + ".jpeg")!
    }
    
    func getOpponentProfileUrl(opponentNo:String) ->URL{
        return URL.init(string: constantVC.GeneralConstants.GET_IMAGE + opponentNo + ".jpeg")!
    }
    
    func clearMyProfileCache(){
        SDImageCache.shared().removeImage(forKey: "\(self.getMyProfileUrl())" , withCompletion: nil)
        YYWebImageManager.shared().cache?.removeImage(forKey: "\(self.getMyProfileUrl())")
    }
    
    func getPlaceHolderUserImage() ->UIImage {
        return UIImage(named: "contactCard")!
    }

    func delete_profilePhoto(param:NSDictionary , completionHandler:@escaping (Bool) -> ()){
        
        WebserviceSigleton().POSTServiceWithParameters_Elite(urlString: constantVC.WebserviceName.URL_DELETE_PROFILE_PHOTO, params: param as! Dictionary<String, AnyObject>, callback:{ (result,error) -> Void in
            if result != nil{
                let resultMessage = result!["msg"] as! String
                if resultMessage == "success"{
                    completionHandler(true)
                }
                else{
                    completionHandler(false)
                }
            }else {
                completionHandler(false)
            }
        })
    }
    
    
    func updateOrder_profilePhoto(param:NSDictionary , completionHandler:@escaping (Bool) -> ()){
        
        WebserviceSigleton().POSTServiceWithParameters_EliteTEST(urlString: constantVC.WebserviceName.URL_UPDATE_PROFILES_ORDER, params: param as! Dictionary<String, AnyObject>, callback:{ (result,error) -> Void in
            if result != nil{
                let resultMessage = result!["msg"] as! String
                if resultMessage == "success"{
                    completionHandler(true)
                }
                else{
                    completionHandler(false)
                }
            }else {
                completionHandler(false)
            }
        })
    }
    
    
    func webService_addUserImage(order: String , id:String , imgdata:Data , completionHandler:@escaping (Bool , String) -> ()){
        
        Alamofire.upload(
            multipartFormData: { multipartFormData in
                //update profile images
                multipartFormData.append(order.data(using: String.Encoding.utf8)!, withName: "order_id")
                multipartFormData.append(id.data(using: String.Encoding.utf8)!, withName: "user_id")
                multipartFormData.append(imgdata, withName: "profileImage", fileName: "\(String(NSDate().timeIntervalSince1970).replacingOccurrences(of: ".", with: "")).jpeg", mimeType: "image/jpeg")
        },
            to:"\(constantVC.GeneralConstants.MATCH_BASE_URL)\(constantVC.WebserviceName.URL_ADD_PROFILE_PHOTO)",
            encodingCompletion: { encodingResult in
                
                switch encodingResult {
                case .success(let upload, _, _):
                    upload.responseJSON { response in
                        
                        guard response.result.error == nil else {
                            print("Error for POST :\(response.result.error!)")
                            completionHandler(false , "")
                            return
                        }
                        
                        if let value = response.result.value {
                            
                            if let result = value as? Dictionary<String, AnyObject> {
                                
                                print(result)
                                let resultMessage = result["msg"] as! String
                                
                                if resultMessage == "success"{
                                    
                                    if let rowdata = result["rowdata"] as? NSDictionary {
                                        if let filename = rowdata.object(forKey: "filename") as? String {
                                            completionHandler(true , filename)
                                        }
                                    }
                                }
                                else{
                                    completionHandler(false , "")
                                }
                            }
                        }
                        }
                        .uploadProgress { progress in // main queue by default
                            print("Upload Progress: \(progress.fractionCompleted)")
                    }
                    return
                case .failure(let encodingError):
                    LoadingIndicatorView.hide()
                    debugPrint(encodingError)
                }
        })
    }
    
    
    /* func getImage(url:URL ,imageView:UIImageView , completionHandler:@escaping (UIImage?) -> () ) {
        let targetSize: CGSize? = imageView.bounds.size
        let transform = QMImageTransform(size: targetSize! , isCircle: false)
        
        let cachedImage: UIImage? = QMImageLoader.instance.imageCache?.imageFromCache(forKey: transform.key(with: url))
        
        if cachedImage != nil {
            transform.apply(for: (cachedImage)!) { (img) in
                completionHandler(img)
            }
        } else {
            
            QMImageLoader.instance.downloadImage(with: url , transform: transform , options: [.highPriority , .continueInBackground , .allowInvalidSSLCertificates ], progress: { (receivedSize , expectedSize , targetURL) in
            }) { (image, transfomedImage , error , cacheType , finished , imageURL) in
                if transfomedImage != nil {
                    completionHandler(transfomedImage)
                }
                if error != nil {
                    completionHandler(nil)
                }
            }
        }
    }*/
    
    
    func getRefreshImage(url:URL ,imageView:UIImageView , completionHandler:@escaping (UIImage?) -> () ) {
        let targetSize: CGSize? = imageView.bounds.size
        let transform = QMImageTransform(size: targetSize! , isCircle: false)
        
        YYWebImageManager.shared().requestImage(with: url , options: currentYYImgotions.currentYYImg , progress: nil , transform: nil) { (image , url , type , stage , error) in
            if error == nil , let img = image {
                transform.apply(for: img) { (img_) in
                    completionHandler(img_)
                }
            }
            else {
                completionHandler(nil)
            }
        }
    }
    
    func clearMyProfileCache_onrequest(strUrl: String){
        YYWebImageManager.shared().cache?.removeImage(forKey: strUrl)
    }
   
   
    
}
