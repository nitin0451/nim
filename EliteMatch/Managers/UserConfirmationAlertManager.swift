//
//  UserConfirmationAlertManager.swift
//  EliteMatch
//
//  Created by Kitlabs-M-0002 on 7/25/19.
//  Copyright © 2019 Kitlabs-M-0002. All rights reserved.
//

import UIKit

class UserConfirmationAlertManager: NSObject {
    
    static let instance = UserConfirmationAlertManager()
    
    
    func showAlert(title: String , subTitle: String ,okTitle:String , cancelTitle:String , completionHandler:@escaping (Bool) -> () ){
        
        let alert = UIAlertController(title: title, message: subTitle, preferredStyle: .alert)
        
        // add the actions (buttons)
        let action1 = UIAlertAction(title: okTitle , style: .default) { (action) in
            completionHandler(true)
        }
        let action2 = UIAlertAction(title: cancelTitle, style: .default) { (action) in
             completionHandler(false)
        }
        
        alert.addAction(action2)
        alert.addAction(action1)
        
        if let topController = UIApplication.topViewController() {
            topController.present(alert, animated: true, completion: nil)
        }
        
    }
    
    
    func showAlert_testAccount(completionHandler:@escaping (Bool) -> ()){
        
        let alert = UIAlertController(title: "NIM Test Account", message: "You can proceed with this test account", preferredStyle: .alert)
        let action1 = UIAlertAction(title: "Ok" , style: .default) { (action) in
            completionHandler(true)
        }
        alert.addAction(action1)
        if let topController = UIApplication.topViewController() {
            topController.present(alert, animated: true, completion: nil)
        }
    }
    
    
    func showAlert_reportSubmitted(completionHandler:@escaping (Bool) -> ()){
        
        let alert = UIAlertController(title: "NIM", message: "Your report has been received and we will take the necessary actions within 24hrs. Thank you.", preferredStyle: .alert)
        let action1 = UIAlertAction(title: "Ok" , style: .default) { (action) in
            completionHandler(true)
        }
        alert.addAction(action1)
        if let topController = UIApplication.topViewController() {
            topController.present(alert, animated: true, completion: nil)
        }
    }
    
    
    

}
