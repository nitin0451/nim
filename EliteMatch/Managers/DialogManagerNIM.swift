//
//  DialogManagerNIM.swift
//  EliteMatch
//
//  Created by Kitlabs-M-0002 on 8/14/19.
//  Copyright © 2019 Kitlabs-M-0002. All rights reserved.
//

import UIKit
import FTIndicator

class DialogManagerNIM: NSObject {
    
    static let instance = DialogManagerNIM()
    
    
    func StartChat_createDialog(opponentID: String , opponentNo:String  , completionHandler: @escaping (Bool , QBChatDialog?) -> Void) {
        
        if opponentID != "0" {
            // Creating or fetching private chat.
            if let privateChatDialog = ServicesManager.instance().chatService.dialogsMemoryStorage.privateChatDialog(withOpponentID: UInt(opponentID)!) {
                completionHandler(true , privateChatDialog)
                
            } else {
                
                let strNo = SessionManager.get_phonenumber() + "-" + opponentNo
                let dic:[String:String] = ["number": strNo]
                let dicData:[String:Any] = ["dialogData": dic]
                
                ServicesManager.instance().chatService.createPrivateChatDialog(withOpponentID: UInt(opponentID)! , data: CommonFunctions.shared.dicToJson(dic: dicData)!) { (response , dialog) in
                    if response.isSuccess {
                        completionHandler(true , dialog!)
                    } else {
                        completionHandler(false , nil)
                    }
                }
            }
        }
    }
    
    func CreatePrivateGroupChat(user: QBUUser , completionHandler: @escaping (Bool , QBChatDialog?) -> Void){
        
        let strNoCombo = SessionManager.get_phonenumber() + "-" + user.login!
        
        let dic:[String:String] = ["phoneNumber": ""]
        let dicData:[String:Any] = ["ChaperonRequestAccepted": dic]
        
        print(CommonFunctions.shared.dicToJson(dic: dicData)!)
        
        ServicesManager.instance().chatService.createGroupChatDialog(withName: strNoCombo , photo: "" , occupants: [user], data: CommonFunctions.shared.dicToJson(dic: dicData)!) { (response , chatDialog) in
            if response.isSuccess {
                completionHandler(true , chatDialog!)
            } else {
                completionHandler(false , nil)
            }
        }
    }
    
    
    func joinChaperonToPrivateGroupChat(arrOpponentId: [NSNumber] , dialog: QBChatDialog , completionHandler: @escaping (Bool) -> Void){
        
        ServicesManager.instance().chatService.joinOccupants(withIDs: arrOpponentId , to: dialog) { (response , dialog) in
            if response.isSuccess {
                completionHandler(true)
            } else {
                completionHandler(false)
            }
        }
        
    }
    
    
    func UpdateDialogStatus_chaperonAcceptRequest(dialog: QBChatDialog , completionHandler: @escaping (Bool) -> Void){
        if let customData = dialog.data {
            if let strDialogData = customData["dialogData"] as? String {
                if let dic = CommonFunctions.shared.jsonToDic(jsonString: strDialogData) {
                    if let dicChaperon = dic["ChaperonRequestAccepted"] as? NSDictionary {
                        if let phoneNos = dicChaperon.object(forKey: "phoneNumber") as? String {
                            var arrPhonNos = phoneNos.components(separatedBy: ",")
                            arrPhonNos.append(SessionManager.get_phonenumber())
                            
                            let strUpdatedPhoNos = arrPhonNos.joined(separator: ",")
                            
                            var dialogToUpdate = dialog
                            
                            let dic:[String:String] = ["phoneNumber": strUpdatedPhoNos]
                            let dicData:[String:Any] = ["ChaperonRequestAccepted": dic]
                            
                            let updateCustomData:[String:Any] = ["class_name": "PrivateDialog" , "dialogData": CommonFunctions.shared.dicToJson(dic: dicData)]
                            dialogToUpdate.data = updateCustomData
                            
                            ServicesManager.instance().chatService.dialogsMemoryStorage.add(dialogToUpdate , andJoin: true)
                            
                            QBRequest.update(dialogToUpdate , successBlock: { (response , chatDialog) in
                                if response.isSuccess {
                                    completionHandler(true)
                                }else {
                                    completionHandler(false)
                                }
                            }) { (error) in
                                completionHandler(false)
                            }
                        }
                    }
                }
            }
        }
    }
    
    func UpdateDialogStatus_CIRCLEAcceptRequest(dialog: QBChatDialog , completionHandler: @escaping (Bool) -> Void){
        if let customData = dialog.data {
            if let strDialogData = customData["dialogData"] as? String {
                if let dic = CommonFunctions.shared.jsonToDic(jsonString: strDialogData) {
                    if let dicChaperon = dic["CircleRequestAccepted"] as? NSDictionary {
                        if let phoneNos = dicChaperon.object(forKey: "phoneNumber") as? String {
                            var arrPhonNos = phoneNos.components(separatedBy: ",")
                            arrPhonNos.append(SessionManager.get_phonenumber())
                            
                            let strUpdatedPhoNos = arrPhonNos.joined(separator: ",")
                            
                            var dialogToUpdate = dialog
                            
                            let dic:[String:String] = ["phoneNumber": strUpdatedPhoNos]
                            let dicData:[String:Any] = ["CircleRequestAccepted": dic]
                            
                            let updateCustomData:[String:Any] = ["class_name": "PrivateDialog" , "dialogData": CommonFunctions.shared.dicToJson(dic: dicData)]
                            dialogToUpdate.data = updateCustomData
                            
                            ServicesManager.instance().chatService.dialogsMemoryStorage.add(dialogToUpdate , andJoin: true)
                            
                            QBRequest.update(dialogToUpdate , successBlock: { (response , chatDialog) in
                                if response.isSuccess {
                                    completionHandler(true)
                                }else {
                                    completionHandler(false)
                                }
                            }) { (error) in
                                completionHandler(false)
                            }
                        }
                    }
                }
            }
        }
    }
    
    
    func getOpponentID(dialog: QBChatDialog?) -> NSNumber?{
        if let occupantIDS = dialog?.occupantIDs {
            let IDS = occupantIDS.filter { $0 != SessionManager.get_QuickBloxID() }
            if IDS.indices.contains(0) {
                return IDS[0]
            }
        }
        return nil
    }
    
    func getOpponentNo(dialog:QBChatDialog) ->String?{
        if let customData = dialog.data {
            if let str = customData["dialogData"] as? String {
                if let dic = CommonFunctions.shared.jsonToDic(jsonString: str) {
                    if let dicNo = dic["dialogData"] as? NSDictionary{
                        if let number = dicNo.object(forKey: "number") as? String {
                            let arrNo = number.components(separatedBy: "-")
                            for no in arrNo {
                                if no != SessionManager.get_phonenumber() {
                                    return no
                                }
                            }
                        }
                    }
                }
            }
        }
        return nil
    }
    
    func getUserName(dailog:QBChatDialog , completionHandler:@escaping (String) -> ()){
        
        if let opponentId = DialogManagerNIM.instance.getOpponentID(dialog: dailog) {
            
            if let user = ServicesManager.instance().usersService.usersMemoryStorage.user(withID: UInt(truncating: opponentId)) as? QBUUser {
                if let name = user.fullName {
                    completionHandler(name)
                }
            }else {
                ServicesManager.instance().usersService.getUserWithID(UInt(truncating: opponentId)).continueOnSuccessWith(block: { (task) -> Any? in
                    if task.isCompleted {
                        if let user = task.result {
                            if let name = user.fullName {
                                completionHandler(name)
                            }
                        }
                    }
                    return nil
                })
            }
        }
        
    }
    
    
    func getUserName_fromNo(number:String , completionHandler:@escaping (String) -> ()){
        
        if let users = ServicesManager.instance().usersService.usersMemoryStorage.users(withLogins: [number]) as? [QBUUser] {
            if users.indices.contains(0) {
                if let name = users[0].fullName {
                    completionHandler(name)
                }
            }
        }else {
            ServicesManager.instance().usersService.getUsersWithLogins([number]).continueOnSuccessWith(block: { (task) -> Any? in
                if task.isCompleted {
                    if let users = task.result as? [QBUUser] {
                        if users.indices.contains(0) {
                            if let name = users[0].fullName {
                                completionHandler(name)
                            }
                        }
                    }
                }
                return nil
            })
        }
    }
    
    
    
    func getOpponentNo_fromGroupName(dialog:QBChatDialog) ->String? {
        
        if let gname = dialog.name {
            let arrNo = gname.components(separatedBy: "-")
            for item in arrNo {
                if item != SessionManager.get_phonenumber() {
                    return item
                }
            }
        }
        return nil
    }
    
    
    
    //MARK:- CIRCLE
    func isMyCircleCreated(completionHandler: @escaping (Bool , QBChatDialog?) -> Void){
        
        let extendedRequest = ["name" : self.getMyCircleName()]
        let page = QBResponsePage(limit: 1, skip: 0)
        
        QBRequest.dialogs(for: page , extendedRequest: extendedRequest , successBlock: { (response , arrDialogs , nmber , page ) in
            if response.isSuccess {
                if arrDialogs.indices.contains(0) {
                    completionHandler(true , arrDialogs[0])
                } else {
                    completionHandler(false , nil)
                }
            }else {
                completionHandler(false , nil)
            }
        }, errorBlock: { (error) in
            print(error.description)
            completionHandler(false , nil)
        })
    }
    
    func createMyCircle(users: [QBUUser] , completionHandler: @escaping (Bool , QBChatDialog?) -> Void){
        
            let dic:[String:String] = ["phoneNumber": ""]
            let dicData:[String:Any] = ["CircleRequestAccepted": dic]
            
            ServicesManager.instance().chatService.createGroupChatDialog(withName: self.getMyCircleName() , photo: "" , occupants: users , data: CommonFunctions.shared.dicToJson(dic: dicData)!) { (response , chatDialog) in
                if response.isSuccess {
                    completionHandler(true , chatDialog!)
                } else {
                    completionHandler(false , nil)
                }
            }
    }
    
    func addUsersTo_MyCircle(arrOpponentId: [NSNumber] , dialog: QBChatDialog , completionHandler: @escaping (Bool) -> Void){
        
        ServicesManager.instance().chatService.joinOccupants(withIDs: arrOpponentId , to: dialog) { (response , dialog) in
            if response.isSuccess {
                completionHandler(true)
            } else {
                completionHandler(false)
            }
        }
    }
    
    func getMyCircleName() ->String{
        return SessionManager.getUserName() + " Circle" + "-\(SessionManager.get_phonenumber())"
    }
}



