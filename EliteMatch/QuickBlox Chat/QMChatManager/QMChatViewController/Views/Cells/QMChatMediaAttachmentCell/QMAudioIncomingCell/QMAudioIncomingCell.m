//
//  QMAudioIncomingCell.m
//  Pods
//
//  Created by Vitaliy Gurkovsky on 2/13/17.
//
//

#import "QMAudioIncomingCell.h"

@implementation QMAudioIncomingCell

- (void)awakeFromNib {
    
    [super awakeFromNib];
    
    _progressView.layer.masksToBounds = YES;
    self.layer.masksToBounds = YES;
    [_slider setThumbImage:[UIImage imageNamed:@"blueRound"]  forState:UIControlStateNormal];
}

- (void)prepareForReuse {
    
    [super prepareForReuse];
    
    [self.progressView setProgress:0
                          animated:NO];
    [self.slider setValue:0.0];
    
}


- (void)layoutSubviews {
    
    [super layoutSubviews];
    
    UIImage *stretchableImage = self.containerView.backgroundImage;
    
    _progressView.layer.mask = [self maskLayerFromImage:stretchableImage
                                              withFrame:_progressView.bounds];
}

- (void)setCurrentTime:(NSTimeInterval)currentTime {

    [super setCurrentTime:currentTime];
    
    NSInteger duration = self.duration;
    
    NSString *timeStamp = [self timestampString:currentTime
                                    forDuration:duration];
    
    self.durationLabel.text = timeStamp;
    
    if (duration > 0) {
        BOOL animated = self.viewState == QMMediaViewStateActive && currentTime > 0;
//        [self.progressView setProgress:currentTime/duration
//                              animated:animated];
        [self.slider setValue:currentTime/duration];
    }
}

+ (QMChatCellLayoutModel)layoutModel {
    
    QMChatCellLayoutModel defaultLayoutModel = [super layoutModel];
    defaultLayoutModel.avatarSize = CGSizeMake(0, 0);
    defaultLayoutModel.containerInsets = UIEdgeInsetsMake(0, 8, 0, 0);
    defaultLayoutModel.staticContainerSize = CGSizeMake(250, 70);
    
    return defaultLayoutModel;
}

@end
