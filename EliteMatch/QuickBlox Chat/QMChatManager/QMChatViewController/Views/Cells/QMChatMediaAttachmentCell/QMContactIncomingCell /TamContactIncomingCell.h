//
//  TamContactIncomingCell.h
//  Tamaas
//
//  Created by Kitlabs-M-0002 on 12/22/18.
//  Copyright © 2018 Krescent Global. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "QMMediaIncomingCell.h"
#import "QMProgressView.h"

@interface TamContactIncomingCell : QMMediaIncomingCell

@property (weak, nonatomic) IBOutlet QMProgressView *progressView;
@property (weak, nonatomic) IBOutlet UISlider *slider;

@property (strong, nonatomic) NSMutableArray *arrContacts;


@property (weak, nonatomic) IBOutlet UIView *vwReplyContainer;
@property (weak, nonatomic) IBOutlet UILabel *lblUserName;
@property (weak, nonatomic) IBOutlet UILabel *lblReplyToMsg;
@property (weak, nonatomic) IBOutlet UIImageView *imgVwThumbnail;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraint_replyContainerHeight;

@end



