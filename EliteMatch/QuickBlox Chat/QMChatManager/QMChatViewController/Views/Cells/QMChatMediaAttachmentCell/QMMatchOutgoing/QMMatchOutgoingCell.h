//
//  QMMatchOutgoingCell.h
//  EliteMatch
//
//  Created by Apple on 13/09/19.
//  Copyright © 2019 Kitlabs-M-0002. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "QMMediaOutgoingCell.h"

NS_ASSUME_NONNULL_BEGIN

@interface QMMatchOutgoingCell : QMMediaOutgoingCell

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraint_replyContainerHeight;
@property (weak, nonatomic) IBOutlet TTTAttributedLabel *lblCaption;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraint_lblCaptionHeight;

@end

NS_ASSUME_NONNULL_END
