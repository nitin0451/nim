//
//  QMVideoOutgoingCell.h
//  Pods
//
//  Created by Vitaliy Gurkovsky on 2/13/17.
//
//

#import "QMMediaOutgoingCell.h"

@interface QMVideoOutgoingCell : QMMediaOutgoingCell

@property (weak, nonatomic) IBOutlet UIView *vwReplyContainer;
@property (weak, nonatomic) IBOutlet UILabel *lblUserName;
@property (weak, nonatomic) IBOutlet UILabel *lblReplyToMsg;
@property (weak, nonatomic) IBOutlet UIImageView *imgVwThumbnail;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraint_replyContainerHeight;

@property (weak, nonatomic) IBOutlet TTTAttributedLabel *lblCaption;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraint_lblCaptionHeight;

@end
