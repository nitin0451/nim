//
//  TMTextMessageReplyView.swift
//  Tamaas
//
//  Created by Kitlabs-M-0002 on 5/9/19.
//  Copyright © 2019 Krescent Global. All rights reserved.
//

import UIKit

class TMTextMessageReplyView: UIView {
    
    @IBOutlet var contentView: UIView!
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var msgText: UILabel!
    @IBOutlet weak var btnClose: UIButton!
    @IBOutlet weak var imgVwThumbnail: UIImageView!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    
    private func commonInit() {
        Bundle.main.loadNibNamed("TMTextMessageReplyView", owner: self, options: nil)
        addSubview(contentView)
        contentView.frame = self.bounds
        contentView.autoresizingMask = [.flexibleWidth , .flexibleHeight]
    }
    
}
