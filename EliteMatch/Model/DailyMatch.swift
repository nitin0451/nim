//
//  DailyMatch.swift
//  EliteMatch
//
//  Created by Kitlabs-M-0002 on 7/22/19.
//  Copyright © 2019 Kitlabs-M-0002. All rights reserved.
//

import UIKit

class DailyMatch: NSObject {
    
    var id:String = ""
    var phoneNumber:String = ""
    var user_is:String = ""
    var user_looking_for:String = ""
    var instagram_id:String = ""
    var insta_url:String = ""
    var linkedin_id:String = ""
    var linkedin_url:String = ""
    var name:String = ""
    var email:String = ""
    var dob:String = ""
    var about_me:String = ""
    var languages:String = ""
    var religion:String = ""
    var living_place:String = ""
    var travelPlaces:String = ""
    var education:String = ""
    var lifestyle:String = ""
    var height:String = ""
    var fav_shows:String = ""
    var fav_dessert:String = ""
    var fav_music:String = ""
    var fav_food:String = ""
    var zoadicSign:String = ""
    var ethnicity:String = ""
    var petpeeve:String = ""
    var isDrink:String = ""
    var isSmoke:String = ""
    var isHaveChildren:String = ""
    var isMarriedBefore:String = ""
    var favHoliday:String = ""
    var isPreferred_TallerPartner:String = ""
    var insta_imgs:String = ""
    var lastupdated_status:String = ""
    var inSchool:String = ""
    var careerName:String = ""
    var countyCode:String = ""
    var inCareer:String = ""
    var isWorkOutRoutinely:String = ""
    var isOnlyEatHalal:String = ""
    var religionCount:String = ""
    var ethnicityCount:String = ""
    var status:String = ""
    var score:String = ""
    var invitations:[Invitation] = []
    var userImages:[Image] = []
    var selectedMatchImage:UIImage?
}


class Invitation: NSObject {
    var id:String = ""
    var from_id:String = ""
    var to_id:String = ""
    var status:String = ""
    var name:String = ""
    var from_phone_number:String = ""
    var to_phone_number:String = ""
    var createdAt:String = ""
    var image:String = ""
    var dialogID:String = ""
}


class Image: NSObject {
    var id:String = ""
    var user_id:String = ""
    var filename:String = ""
    var order:String = ""
}


class MediaPreview {
    var image: UIImage!
    var caption:String = ""
    var MediaType:String = ""
    var videoUrl:String = ""
}
