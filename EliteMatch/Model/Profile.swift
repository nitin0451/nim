//
//  Profile.swift
//  EliteMatch
//
//  Created by Kitlabs-M-0002 on 12/5/18.
//  Copyright © 2018 Kitlabs-M-0002. All rights reserved.
//

import UIKit
import ObjectMapper

class Profile: NSObject {
     var fullName:String = ""
     var email:String = ""
     var linkedInID:String = ""
     var instagramID:String = ""
     var linkedInUrl:String = ""
     var instagramUrl:String = ""
     var instaImages:String = ""
     var dob:String = ""
     var languages:String = ""
     var inSchool:Bool = false
     var inCareer:Bool = false
     var religion:String = ""
     var living_place:String = ""
     var travel:String = ""
     var ethnicity:String = ""
     var education:String = ""
     var Lifestyle:String = ""
     var isWorkOutRoutinely:Bool = false
     var isSmoke:Bool = false
     var isDrink:Bool = false
     var isOnlyEatHalal:Bool = false
     var isMarriedBefore:Bool = false
     var isHaveChildren:Bool = false
     var isPreferred_TallerPartner:Bool = false
     var height:String = ""
     var petpeeve:String = ""
     var favHoliday:String = ""
     var favShow:String = ""
     var favDessert:String = ""
     var favMusic:String = ""
     var favFood:String = ""
     var religionCount:String = ""
     var ethnicityCount:String = ""
     var about:String = ""
     var userIs:String = ""
     var userLookingFor:String = ""
     var careerName:String = ""
     var imagesUrls:[String] = []
     var images:[Image] = []
}



class UserProfile:Mappable {
    
    var id:String?
    var phoneNumber:String?
    var user_is:String?
    var user_looking_for:String?
    var instagram_id:String?
    var insta_url:String?
    var linkedin_id:String?
    var linkedin_url:String?
    var name:String?
    var email:String?
    var dob:String?
    var about_me:String?
    var languages:String?
    var religion:String?
    var living_place:String?
    var travelPlaces:String?
    var education:String?
    var lifestyle:String?
    var height:String?
    var fav_shows:String?
    var fav_dessert:String?
    var fav_music:String?
    var fav_food:String?
    var zoadicSign:String?
    var ethnicity:String?
    
    var petpeeve:String?
    var isDrink:String?
    var isSmoke:String?
    var isHaveChildren:String?
    var isMarriedBefore:String?
    var favHoliday:String?
    var isPreferred_TallerPartner:String?
    var insta_imgs:String?
    var lastupdated_status:String?
    var inSchool:String?
    var careerName:String?
    var countyCode:String?
    var inCareer:String?
    var isWorkOutRoutinely:String?
    var isOnlyEatHalal:String?
    var religionCount:String?
    var ethnicityCount:String?
    var status:String?
    var score:String?
    var idchaperon:String?
    var quickBloxId:String?
    var religionMatterCount:String?
    var isBingeWatchShows:String?
    var plan_id:String?
    var plan_start_date:String?
    var plan_end_date:String?
    
    
    var images:[UserImage]?
    //Nitin
    var approvalDate: String?
   
    required init?(map: Map) {
        
    }
    
    // Mappable
    func mapping(map: Map) {
        
        id    <- map["id"]
        phoneNumber         <- map["phoneNumber"]
        user_is      <- map["user_is"]
        user_looking_for       <- map["user_looking_for"]
        instagram_id  <- map["instagram_id"]
        linkedin_id  <- map["linkedin_id"]
        linkedin_url     <- map["linkedin_url"]
        name    <- map["name"]
        email         <- map["email"]
        dob      <- map["dob"]
        about_me       <- map["about_me"]
        languages  <- map["languages"]
        religion  <- map["religion"]
        living_place     <- map["living_place"]
        travelPlaces    <- map["travelPlaces"]
        education         <- map["education"]
        lifestyle      <- map["lifestyle"]
        height       <- map["height"]
        fav_shows  <- map["fav_shows"]
        fav_dessert  <- map["fav_dessert"]
        fav_music     <- map["fav_music"]
        fav_food    <- map["fav_food"]
        zoadicSign         <- map["zoadicSign"]
        ethnicity      <- map["ethnicity"]
        petpeeve       <- map["petpeeve"]
        isDrink  <- map["isDrink"]
        isSmoke  <- map["isSmoke"]
        isHaveChildren     <- map["isHaveChildren"]
        isMarriedBefore    <- map["isMarriedBefore"]
        favHoliday         <- map["favHoliday"]
        isPreferred_TallerPartner      <- map["isPreferred_TallerPartner"]
        insta_imgs       <- map["insta_imgs"]
        lastupdated_status  <- map["lastupdated_status"]
        inSchool  <- map["inSchool"]
        careerName     <- map["careerName"]
        countyCode  <- map["countyCode"]
        inCareer     <- map["inCareer"]
        isWorkOutRoutinely    <- map["isWorkOutRoutinely"]
        isOnlyEatHalal         <- map["isOnlyEatHalal"]
        religionCount      <- map["religionCount"]
        ethnicityCount       <- map["ethnicityCount"]
        status  <- map["status"]
        score  <- map["score"]
        idchaperon     <- map["idchaperon"]
        quickBloxId  <- map["quickBloxId"]
        images     <- map["images"]
        isBingeWatchShows  <- map["isBingeWatchShows"]
        religionMatterCount     <- map["religionMatterCount"]
        plan_id     <- map["plan_id"]
        plan_start_date  <- map["plan_start_date"]
        plan_end_date     <- map["plan_end_date"]
        approvalDate <- map["approvalDate"]
    }
    
}



class UserImage: Mappable {
    var id:String?
    var user_id:String?
    var filename:String?
    var image:UIImage?
    var order:String?
    
    required init?(map: Map) {
        
    }
    
    // Mappable
    func mapping(map: Map) {
        id          <- map["id"]
        user_id     <- map["user_id"]
        filename    <- map["filename"]
        image       <- map["image"]
        order       <- map["orderID"]
    }
}


class imageWithId: NSObject{
    var id:String?
    var image:UIImage?
    var order:String?
    var imgName:String?
}


func getMapper_UserImage(jsonStr: String) ->UserImage?{
    let user = Mapper<UserImage>().map(JSONString: jsonStr)
    if user != nil {
        return user
    }
    return nil
}
