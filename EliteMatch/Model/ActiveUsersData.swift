//
//  ActiveUsersData.swift
//  EliteMatch
//
//  Created by Apple on 18/02/20.
//  Copyright © 2020 Kitlabs-M-0002. All rights reserved.
//

import Foundation
import ObjectMapper

class ActiveUsersModalClass: Mappable {
    
    var match:String?
    var userData:UserProfile?

    required init?(map: Map) {
        
    }
    
    // Mappable
    func mapping(map: Map) {
        match          <- map["match"]
        userData     <- map["userData"]
    }
}
