//
//  CallActiveView_Video.swift
//  Tamaas
//
//  Created by Kitlabs-M-0002 on 6/4/19.
//  Copyright © 2019 Krescent Global. All rights reserved.
//

import UIKit

class CallActiveView_Video: UIView {
    
    @IBOutlet weak var vwSuper: UIView!
    @IBOutlet weak var vwSmall: UIView!
    @IBOutlet weak var btnFullView: UIButton!
    @IBOutlet weak var lblCallName: UILabel!
    

    class func instanceFromNib() -> UIView {
        return UINib(nibName: "CallActiveView_Video", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! UIView
    }
    
    @IBAction func didPress_fullView(_ sender: UIButton) {
        self.OpenVideoCallVC()
    }
    
    func OpenVideoCallVC(){
        //remove background active view
        CallActiveInBackgroundManager.shared.hideActiveVideoCallView()
        
        if let topController = UIApplication.topViewController() {
            if let callViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "TamVideoCall_VC") as? TamVideoCall_VC {
                callViewController.session = constantVC.ActiveSession.session
                callViewController.callUUID = constantVC.ActiveSession.activeCallUUID
                callViewController.opponentNo = constantVC.ActiveCall.receiverNo
                callViewController.isGroup = constantVC.ActiveCall.isGroup
                callViewController.opponentName = constantVC.ActiveCall.nameToShow
                topController.present(callViewController , animated: true , completion: nil)
            }
        }
    }
    

}
