//
//  CallActiveInBackgroundManager.swift
//  Tamaas
//
//  Created by Kitlabs-M-0002 on 6/4/19.
//  Copyright © 2019 Krescent Global. All rights reserved.
//

import UIKit

class CallActiveInBackgroundManager: NSObject {
    
    
    //MARK:- Variables
    static let shared = CallActiveInBackgroundManager()
    var panGesture = UIPanGestureRecognizer()
    var activeView:CallActiveView_Video!
    var activeAudioView: CallActiveView_Audio!
    
    
    //MARK:- Configure Video Active View
    func showActiveVideoCallView(){
        constantVC.ActiveSession.isBackgroundViewActive = true
        activeView = CallActiveView_Video.instanceFromNib() as? CallActiveView_Video
        
        if let superView = UIApplication.topViewController()?.view {
            activeView.frame = CGRect(x: superView.frame.size.width - 170 , y: superView.frame.size.height - 300 , width: 150 , height: 210)
            panGesture = UIPanGestureRecognizer(target: self, action: #selector(self.draggedView_VIDEO(_:)))
            activeView.isUserInteractionEnabled = true
            activeView.addGestureRecognizer(panGesture)
            UIApplication.shared.keyWindow?.insertSubview(activeView , at: 1)
        }
        self.configureVideoCallBackgroundView()
        self.setActiveViewToFront()
    }
    
    func configureVideoCallBackgroundView(){
        
        activeView.vwSuper.layer.cornerRadius = 6.0
        activeView.vwSuper.clipsToBounds = true
        activeView.vwSmall.layer.cornerRadius = 6.0
        activeView.vwSmall.clipsToBounds = true
        
        self.setCallName()
        
        if constantVC.ActiveSession.isCallActive {
            if constantVC.ActiveCall.isGroup != "true" {                    //one to one
                self.setLocalView(superView: activeView.vwSmall)
                self.opponentView(superView: activeView.vwSuper)
            } else {                                                       //group
                self.setLocalView(superView: activeView.vwSuper)
            }
        } else {
            self.setLocalView(superView: activeView.vwSuper)
        }
    }
    
    func hideActiveVideoCallView(){
        if self.activeView != nil {
            self.activeView.removeFromSuperview()
            self.activeView = nil
        }
    }
    
    func setCallName(){
        if constantVC.ActiveCall.isGroup == "true" {
            self.activeView.lblCallName.text = constantVC.ActiveCall.groupName
        } else {
            self.activeView.lblCallName.text = constantVC.ActiveCall.nameToShow
        }
    }
    
    
    //MARK:- Add drag to active video view
    @objc func draggedView_VIDEO(_ sender:UIPanGestureRecognizer){
        let superView = UIApplication.topViewController()?.view
        superView?.bringSubview(toFront: activeView)
        let translation = sender.translation(in: superView)
        activeView.center = CGPoint(x: activeView.center.x + translation.x, y: activeView.center.y + translation.y)
        sender.setTranslation(CGPoint.zero, in: superView)
    }
    
    @objc func draggedView_AUDIO(_ sender:UIPanGestureRecognizer){
        let superView = UIApplication.topViewController()?.view
        superView?.bringSubview(toFront: activeAudioView)
        let translation = sender.translation(in: superView)
        activeAudioView.center = CGPoint(x: activeAudioView.center.x + translation.x, y: activeAudioView.center.y + translation.y)
        sender.setTranslation(CGPoint.zero, in: superView)
    }
    
    
    //MARK:- Set Video Active Call Views
    func opponentView(superView: UIView){
        if constantVC.ActiveSession.remoteActiveUsers.count > 0 {
            if let user = constantVC.ActiveSession.remoteActiveUsers[0] as? User {
                if let opponentVw = userView(userID: user.userID) as? UIView {
                    DispatchQueue.main.async {
                        opponentVw.frame = superView.bounds
                        superView.addSubview(opponentVw)
                    }
                }
            }
        }
    }
    
    
    func setLocalView(superView: UIView){
        
        if let cameraCapture = constantVC.ActiveSession.cameraCapture {
            
            let localView = LocalVideoView(withPreviewLayer: cameraCapture.previewLayer)
            localView.frame = superView.bounds
            superView.insertSubview(localView , at: 0)
            // localView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
            //superView.autoresizesSubviews = true
            localView.translatesAutoresizingMaskIntoConstraints = false
            localView.leftAnchor.constraint(equalTo: superView.leftAnchor).isActive = true
            localView.rightAnchor.constraint(equalTo: superView.rightAnchor).isActive = true
            localView.topAnchor.constraint(equalTo: superView.topAnchor).isActive = true
            localView.bottomAnchor.constraint(equalTo: superView.bottomAnchor).isActive = true
            localView.layoutIfNeeded()
        }
    }
    
    //MARK:- Helpers - Video Call
    private func userView(userID: UInt) -> UIView? {
        if let result = constantVC.ActiveSession.videoViews[userID] {
            return result
        }
        
        if let remoteVideoTraсk = constantVC.ActiveSession.session?.remoteVideoTrack(withUserID: NSNumber(value: userID)) {
            
            QBRTCRemoteVideoView.preferMetal = false
            
            let remoteView :QBRTCRemoteVideoView = QBRTCRemoteVideoView()
            remoteView.videoGravity = AVLayerVideoGravity.resizeAspect.rawValue
            remoteView.clipsToBounds = true
            remoteView.setVideoTrack(remoteVideoTraсk)
            return remoteView
            
            //Opponents
            /* let remoteVideoView = QBRTCRemoteVideoView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width , height: self.view.frame.height))
             remoteVideoView.videoGravity = AVLayerVideoGravity.resizeAspectFill.rawValue
             videoViews[userID] = remoteVideoView
             remoteVideoView.setVideoTrack(remoteVideoTraсk)*/
            
        }
        return nil
    }
    
    func isActiveVideoView() ->Bool{
        if let topVC = UIApplication.topViewController() {
            if topVC.isKind(of: TamVideoCall_VC.self) {
                return true
            }
        }
        return false
    }
    
    func isActiveChatWindow() ->Bool {
        if let topVC = UIApplication.topViewController() {
            if topVC.isKind(of: ChatViewController.self) && constantVC.ActiveCall.dialogID == ServicesManager.instance().currentDialogID {
                return true
            }
        }
        return false
    }
    
    func isActiveAudioView() ->Bool{
        if let topVC = UIApplication.topViewController() {
            if topVC.isKind(of: TamAudioCall_VC.self) {
                return true
            }
        }
        return false
    }
    
    func setActiveViewToFront(){
        if let activeVw = self.activeView {
            activeVw.superview?.bringSubview(toFront: activeVw)
        }
        if let activeVw = self.activeAudioView {
            activeVw.superview?.bringSubview(toFront: activeVw)
        }
    }
    
    
    //MARK:- Call Timer
    func configureCallTimer(){
        constantVC.ActiveSession.callTimer = Timer.scheduledTimer(timeInterval: CallConstant.refreshTimeInterval,
                                                                  target: self,
                                                                  selector: #selector(self.refreshCallTime(_:)),
                                                                  userInfo: nil,
                                                                  repeats: true)
    }
    
    func invalidateCallTimer(){
        constantVC.ActiveSession.callTimer?.invalidate()
    }
    
    
    @objc func refreshCallTime(_ sender: Timer?) {
        constantVC.ActiveSession.timeDuration += CallConstant.refreshTimeInterval
        constantVC.ActiveCall.duration =  CommonFunctions.shared.string(withTimeDuration: constantVC.ActiveSession.timeDuration)
        NotificationCenter.default.post(name: constantVC.NSNotification_name.NSNotification_updateTimer, object: nil)
    }
    
    
    func closeCall(){
        QBRTCClient.instance().remove(TamVideoCall_VC.self() as QBRTCClientDelegate)
        // stopping camera session
        constantVC.ActiveSession.cameraCapture?.stopSession(nil)
    }
    
    func closeCallAudio(){
        QBRTCClient.instance().remove(TamAudioCall_VC.self() as QBRTCClientDelegate)
    }
    
    
    //MARK:- Configure Audio Active View
    func showActiveAudioCallView(){
        constantVC.ActiveSession.isBackgroundViewActive = true
        activeAudioView = CallActiveView_Audio.instanceFromNib() as? CallActiveView_Audio
        
        if let superView = UIApplication.topViewController()?.view {
            activeAudioView.frame = CGRect(x: superView.frame.size.width - 120 , y: superView.frame.size.height - 190 , width: 100 , height: 100)
            panGesture = UIPanGestureRecognizer(target: self, action: #selector(self.draggedView_AUDIO(_:)))
            activeAudioView.isUserInteractionEnabled = true
            activeAudioView.addGestureRecognizer(panGesture)
            UIApplication.shared.keyWindow?.insertSubview(activeAudioView , at: 1)
        }
        
        self.configureAudioCallBackgroundView()
        self.setActiveViewToFront()
    }
    
    func configureAudioCallBackgroundView(){
        activeAudioView.layer.cornerRadius = activeAudioView.frame.size.width / 2.0
        activeAudioView.clipsToBounds = true
        activeAudioView.lblCallName.text = constantVC.ActiveCall.nameToShow
        activeAudioView.userImage.yy_setImage(with: URL.init(string: "\(constantVC.GeneralConstants.GET_IMAGE)\(constantVC.ActiveCall.receiverNo).jpeg") , placeholder: UIImage(named: "contactCard") , options: currentYYImgotions.currentYYImg , completion: nil)
    }
    
    func hideActiveAudioCallView(){
        if self.activeAudioView != nil {
            self.activeAudioView.removeFromSuperview()
            self.activeAudioView = nil
        }
    }
    
    
    
    
    
}
