//
//  TamAudioCall_VC.swift
//  Tamaas
//
//  Created by Kitlabs-M-0002 on 1/2/19.
//  Copyright © 2019 Krescent Global. All rights reserved.
//

import UIKit
import Quickblox
import QuickbloxWebRTC
import SVProgressHUD
import FTIndicator
import CallKit

class TamAudioCall_VC: UIViewController {
   
    //MARK: - Internal Properties
   // private var timeDuration: TimeInterval = 0.0
   // private var callTimer: Timer?
    private var beepTimer: Timer?
    
    //Managers
    private let core = Core.instance
    private let settings = Settings.instance
    
    //Camera
    var session: QBRTCSession?
    var callUUID: UUID?
    
    //Containers
    private var users = [User]()
    
    //Views
    private var localVideoView: LocalVideoView?
    
    //States
    private var shouldGetStats = false
    private var didStartPlayAndRecord = false
    
    private var muteAudio = false {
        didSet {
            session?.localMediaStream.audioTrack.isEnabled = !muteAudio
        }
    }
    private var muteVideo = false {
        didSet {
            session?.localMediaStream.videoTrack.isEnabled = !muteVideo
        }
    }
    
    private var state = CallViewControllerState.connected {
        didSet {
            switch state {
            case .disconnected:
                title = CallStateConstant.disconnected
            case .connecting:
                title = CallStateConstant.connecting
            case .connected:
                title = CallStateConstant.connected
            case .disconnecting:
                title = CallStateConstant.disconnecting
            }
        }
    }
    
    override var prefersStatusBarHidden : Bool {
        return true
    }
    
    //MARK:- Outlets
    @IBOutlet weak var userImage: UIImageView!
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var lblDuration: UILabel!
    @IBOutlet weak var background_imageView: UIImageView!

    //MARK:-Variables
    var opponentNo:String = ""
    var opponentName:String = ""
    var userPhoto:String = ""
    var isUserBlocked:Bool = false
    var blockTimer = Timer()
    var blockSeconds = 60
    var isSpeakerBlock:Bool = false
    var currentUser:QBUUser?
    var dialogID:String = ""
    
    
    //MARK: - Life Cycle
    override func awakeFromNib() {
        super.awakeFromNib()
        
        if !self.isUserBlocked {
            QBRTCClient.instance().add(self as QBRTCClientDelegate)
            QBRTCAudioSession.instance().addDelegate(self)
        }
    }
    
    deinit {
        debugPrint("deinit \(self)")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.currentUser = ServicesManager.instance().currentUser
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.handleNotification_updateCallTimer), name: constantVC.NSNotification_name.NSNotification_updateTimer , object: nil)
        
        if !constantVC.ActiveSession.isBackgroundViewActive {
            //when user blocked
            if !self.isUserBlocked {
                self.showActiveCallView()
            } else {
                self.showBlockedView()
            }
        } else {
            self.setCallInfo(isRemoveCalling: true)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        //nitin
        if #available(iOS 13.0, *) {
            self.isModalInPresentation = true // available in IOS13
        }
        self.background_imageView.layer.cornerRadius = self.background_imageView.frame.width/2
        self.background_imageView.layer.maskedCorners = [.layerMinXMinYCorner,.layerMaxXMinYCorner]
    }
    
    func showBlockedView(){
        //blocked
        self.userName.text = "Calling " + self.opponentName
        self.userImage.yy_setImage(with: URL.init(string: "\(constantVC.GeneralConstants.GET_IMAGE)\(self.opponentNo).jpeg") , placeholder: UIImage(named: "contactCard") , options: [.progressiveBlur , .setImageWithFadeAnimation ] , completion: nil)
        self.lblDuration.text = "00:00"
        
        //Begin play calling sound
        beepTimer = Timer.scheduledTimer(timeInterval: QBRTCConfig.dialingTimeInterval(),
                                         target: self,
                                         selector: #selector(playCallingSound(_:)),
                                         userInfo: nil, repeats: true)
        blockTimer = Timer.scheduledTimer(timeInterval: 1, target: self,   selector: (#selector(self.updateBlockTimer)), userInfo: nil, repeats: true)
        
    }
    
    func showActiveCallView(){
        let currentConferenceUser = User(user: currentUser!)
        users = [currentConferenceUser]
        
        let audioSession = QBRTCAudioSession.instance()
        if audioSession.isInitialized == false {
            audioSession.initialize { configuration in
                // adding blutetooth support
                configuration.categoryOptions.insert(AVAudioSessionCategoryOptions.allowBluetooth)
                configuration.categoryOptions.insert(AVAudioSessionCategoryOptions.allowBluetoothA2DP)
                // adding airplay support
                configuration.categoryOptions.insert(AVAudioSessionCategoryOptions.allowAirPlay)
                debugPrint("\(configuration.categoryOptions)")
                guard let session = self.session else { return }
                if session.conferenceType == QBRTCConferenceType.video {
                    // setting mode to video chat to enable airplay audio and speaker only
                    configuration.mode = AVAudioSessionModeVideoChat
                }
            }
        }
        
        //update GUI
        state = .connecting
        
        guard let session = self.session else { return }
        
        var conferenceUsers = [User]()
        
        for uID in session.opponentsIDs {
            if uID != SessionManager.get_QuickBloxID() {
                let user = createConferenceUser(userID: uID.uintValue)
                conferenceUsers.insert(user, at: 0)
            }
        }
        
        self.users = conferenceUsers
        
        if let userID = SessionManager.get_QuickBloxID() {
            
            constantVC.ActiveCall.senderID = "\(userID)"
            
            let isInitiator = userID.uintValue == session.initiatorID.uintValue
            if isInitiator == true {
                startCall()
                self.setCallInfo(isRemoveCalling: false)
            } else {
                acceptCall()
                self.setCallInfo(isRemoveCalling: true)
            }
        }
        
        title = CallStateConstant.connecting
        
        if CallKitManager.isCallKitAvailable() == true,
            session.initiatorID.uintValue == currentUser?.id {
            CallKitManager.instance.updateCall(with: callUUID, connectingAt: Date())
        }
        
        //set caller data
        constantVC.ActiveCall.senderNo = SessionManager.get_phonenumber()
        constantVC.ActiveCall.receiverNo = self.opponentNo
        constantVC.ActiveCall.isGroup = "false"
        constantVC.ActiveCall.isAudio = "true"
        constantVC.ActiveCall.groupName = ""
        constantVC.ActiveCall.groupPhoto = ""
        constantVC.ActiveCall.nameToShow = self.opponentName
    }
    
    
    
    @objc func updateBlockTimer() {
        self.blockSeconds -= 1
        if self.blockSeconds < 0 {
            
            if let beepTimer = beepTimer {
                beepTimer.invalidate()
                self.beepTimer = nil
                SoundProvider.stopSound()
            }
            
            self.dismiss(animated: true , completion: nil)
        }
    }
    
    @objc func handleNotification_updateCallTimer(_ notificationObj: NSNotification) {
        self.lblDuration.text = String(describing: CommonFunctions.shared.string(withTimeDuration: constantVC.ActiveSession.timeDuration))
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        //self.closeCall(withTimeout: true)
    }
    
    private func closeCall(withTimeout timeout: Bool) {
        // removing delegate on close call so we don't get any callbacks
        // that will force collection view to perform updates
        // while controller is deallocating
        QBRTCClient.instance().remove(self as QBRTCClientDelegate)
        state = .disconnected
        
        //        if timeout {`
        //            SVProgressHUD.showError(withStatus: CallConstant.sessionDidClose)
        //        } else {
        //            // dismissing progress hud if needed
        //            SVProgressHUD.dismiss()
        //        }
        // navigationController?.popToRootViewController(animated: true)
    }
    
    
    // MARK: - Actions
    func startCall() {
        //Begin play calling sound
        beepTimer = Timer.scheduledTimer(timeInterval: QBRTCConfig.dialingTimeInterval(),
                                         target: self,
                                         selector: #selector(playCallingSound(_:)),
                                         userInfo: nil, repeats: true)
        
        //Start call
        let userInfo = ["callByName": self.opponentName , "name": "Test", "callBy": SessionManager.get_phonenumber() , "isGroup": "false" , "dialogId": self.dialogID]
        session?.startCall(userInfo)
    }
    
    func acceptCall() {
        SoundProvider.stopSound()
        //Accept call
        let userInfo = ["acceptCall": "userInfo"]
        session?.acceptCall(userInfo)
    }
    
    // MARK: - Timers actions
    @objc func playCallingSound(_ sender: Any?) {
        SoundProvider.playSound(type: .calling)
    }
    
    private func createConferenceUser(userID: UInt) -> User {
        let user = QBUUser()
        user.id = userID
        return User(user: user)
    }
   
    func setCallInfo(isRemoveCalling:Bool){
        
        if isRemoveCalling {
            self.userName.text = self.opponentName
        }
        else {
            self.userName.text = "Calling " + self.opponentName
        }
        self.userImage.yy_setImage(with: URL.init(string: "\(constantVC.GeneralConstants.GET_IMAGE)\(self.opponentNo).jpeg") , placeholder: UIImage(named: "contactCard") , options: [.progressiveBlur , .setImageWithFadeAnimation ] , completion: nil)
    }
    
    func openChatWindow(){
        constantVC.openPrivateChat.isActiveOpenPrivateChat = true
        constantVC.openPrivateChat.isActiveOpenChatWithActiveCall = true
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "ChatViewController") as! ChatViewController
        
        if constantVC.ActiveSession.activeDialog != nil {
            controller.dialog = constantVC.ActiveSession.activeDialog
            
            if let topVC = UIApplication.topViewController() {
                topVC.present(controller , animated: false , completion: {
                    CallActiveInBackgroundManager.shared.showActiveAudioCallView()
                })
            }
        } else {
            
            //get dialog from server
            ServicesManager.instance().chatService.loadDialog(withID: constantVC.ActiveCall.dialogID).continueOnSuccessWith { (task) -> Any? in
                if task.isCompleted{
                    if let dialog = task.result {
                        controller.dialog = dialog
                        constantVC.ActiveSession.activeDialog = dialog
                        
                        DispatchQueue.main.async {
                            if let topVC = UIApplication.topViewController() {
                                topVC.present(controller , animated: false , completion: {
                                    CallActiveInBackgroundManager.shared.showActiveAudioCallView()
                                })
                            }
                        }
                    }
                } else {
                    FTIndicator.showToastMessage("Error in open Chat Window!")
                }
                return nil
            }
        }
    }
  
    //MARK:- UIButton Actions
    @IBAction func didPress_chatWindow(_ sender: UIButton) {
        self.dismiss(animated: true , completion: {
            if CallActiveInBackgroundManager.shared.isActiveChatWindow() {
                CallActiveInBackgroundManager.shared.showActiveAudioCallView()
            } else {
                self.openChatWindow()
            }
        })
    }
    
    @IBAction func didPress_back(_ sender: UIButton) {
        self.dismiss(animated: true , completion: {
            CallActiveInBackgroundManager.shared.showActiveAudioCallView()
        })
    }
    
    @IBAction func didPress_Mute(_ sender: UIButton) {
        
        if let muteAudio = self.muteAudio as? Bool {
            self.muteAudio = !muteAudio
        }
        
        if !(self.muteAudio) {
            sender.setImage(UIImage(named: "muteunsel"), for: .normal)
        } else {
            sender.setImage(UIImage(named: "mutesel"), for: .normal)
        }
        
    }
    
    @IBAction func didPress_endCall(_ sender: UIButton) {
        
        if !self.isUserBlocked {
            CallActiveInBackgroundManager.shared.invalidateCallTimer()
            self.session?.hangUp(["hangup": "hang up"])
            self.dismiss(animated: true, completion: nil)
        } else {
            self.blockSeconds = 0
            self.updateBlockTimer()
        }
        
    }
    
    @IBAction func didPress_speaker(_ sender: UIButton) {
        
        if self.isUserBlocked {
            if self.isSpeakerBlock {
                self.isSpeakerBlock = false
                sender.setImage(UIImage(named: "speakerunsel"), for: UIControlState.normal)
            } else {
                self.isSpeakerBlock = true
                sender.setImage(UIImage(named: "speakersel"), for: UIControlState.normal)
            }
        }else {
            if QBRTCAudioSession.instance().currentAudioDevice == .speaker {
                self.isSpeakerBlock = false
                QBRTCAudioSession.instance().currentAudioDevice = .receiver
                sender.setImage(UIImage(named: "speakerunsel"), for: UIControlState.normal)
            }
            
            if QBRTCAudioSession.instance().currentAudioDevice == .receiver {
                self.isSpeakerBlock = true
                QBRTCAudioSession.instance().currentAudioDevice = .speaker
                sender.setImage(UIImage(named: "speakersel"), for: UIControlState.normal)
            }
        }
        
       
    }
    
    @IBAction func didPress_dialPad(_ sender: UIButton) {
        constantVC.GlobalVariables.isDialPadLaunched = true
        self.openDialPad()
    }
    
    func openDialPad(){
        let controller = self.storyboard?.instantiateViewController(withIdentifier: "DialPad_VC") as! DialPad_VC
        let transition = CATransition()
        transition.duration = 0.30
        transition.type = kCATransitionPush
        transition.subtype = kCAOnOrderOut
        self.view.window?.layer.add(transition, forKey: kCATransition)
        self.present(controller, animated: false , completion: nil)
    }
    
    
}


extension TamAudioCall_VC: QBRTCAudioSessionDelegate {
    //MARK: QBRTCAudioSessionDelegate
    func audioSession(_ audioSession: QBRTCAudioSession, didChangeCurrentAudioDevice updatedAudioDevice: QBRTCAudioDevice) {
        let isSpeaker = updatedAudioDevice == QBRTCAudioDevice.speaker
        // dynamicButton.pressed = isSpeaker
    }
}

extension TamAudioCall_VC: QBRTCClientDelegate {
    // MARK: QBRTCClientDelegate
    
    func session(_ session: QBRTCBaseSession, connectionClosedForUser userID: NSNumber) {
        // remove user from the collection
        if let index = users.index(where: { $0.userID == userID.uintValue }) {
            users.remove(at: index)
        }
    }
    
    /**
     *  Called in case when connection state changed
     */
    func session(_ session: QBRTCBaseSession, didChange state: QBRTCConnectionState, forUser userID: NSNumber) {
        guard let session = session as? QBRTCSession,
            session == self.session, let index = users.index(where: { $0.userID == userID.uintValue }) else {
                return
        }
        let user = users[index]
        user.connectionState = state
    }
    
    /**
     *  Called in case when receive remote video track from opponent
     */
    func session(_ session: QBRTCBaseSession,
                 receivedRemoteVideoTrack videoTrack: QBRTCVideoTrack,
                 fromUser userID: NSNumber) {
        guard let session = session as? QBRTCSession,
            session == self.session else {
                return
        }
    }
    
    func sessionDidClose(_ session: QBRTCSession) {
        
        if session == self.session {
            if CallKitManager.isCallKitAvailable() == true {
                CallKitManager.instance.endCall(with: callUUID) {
                    debugPrint("endCall")
                }
            }
            
            let audioSession = QBRTCAudioSession.instance()
            if audioSession.isInitialized == true,
                audioSession.audioSessionIsActivatedOutside(AVAudioSession.sharedInstance()) == false {
                debugPrint("Deinitializing QBRTCAudioSession in CallViewController.")
                audioSession.deinitialize()
            }
            
            if let beepTimer = beepTimer {
                beepTimer.invalidate()
                self.beepTimer = nil
                SoundProvider.stopSound()
            }
            
            if let callTimer = constantVC.ActiveSession.callTimer {
                callTimer.invalidate()
                constantVC.ActiveSession.callTimer = nil
            }
        }
    }
    
    /**
     *  Called in case when connection is established with opponent
     */
    func session(_ session: QBRTCBaseSession, connectedToUser userID: NSNumber) {
        guard let session = session as? QBRTCSession,
            session == self.session else {
                return
        }
        
        
        if self.isSpeakerBlock {
            QBRTCAudioSession.instance().currentAudioDevice = .speaker
        }else {
            QBRTCAudioSession.instance().currentAudioDevice = .receiver
        }
        
        constantVC.ActiveSession.isCallActive = true
        self.setCallInfo(isRemoveCalling: true)
        
        if let beepTimer = beepTimer {
            beepTimer.invalidate()
            self.beepTimer = nil
            SoundProvider.stopSound()
        }
        
        if constantVC.ActiveSession.callTimer == nil {
            
            if CallKitManager.isCallKitAvailable(),
                let currentUser = core.currentUser,
                session.initiatorID.uintValue == currentUser.id {
                CallKitManager.instance.updateCall(with: callUUID, connectedAt: Date())
            }
            
            CallActiveInBackgroundManager.shared.configureCallTimer()
        }
    }
}
