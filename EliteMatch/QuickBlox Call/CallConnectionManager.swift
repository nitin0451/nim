//
//  CallConnectionManager.swift
//  Tamaas
//
//  Created by Kitlabs-M-0002 on 6/5/19.
//  Copyright © 2019 Krescent Global. All rights reserved.
//

import UIKit
import FTIndicator

class CallConnectionManager: NSObject {
    
    //MARK: shared Instance
    static let instance: CallConnectionManager = {
        let core = CallConnectionManager()
        return core
    }()
    
    
    //MARK:- Call Connection Methods
    func call(with conferenceType: QBRTCConferenceType , dialog: QBChatDialog , groupName: String , callTo_:String , callTo_Name:String) {
        
        if constantVC.ActiveSession.session != nil {
            return
        }
        
        if hasConnectivity() {
            CallPermissions.check(with: conferenceType) { granted in
                if granted {
                    
                    constantVC.ActiveSession.activeDialog = dialog
                    var opponentsIDs:[NSNumber] = []
                    
                    if let opponentIDs_ = NimUserManager.shared.getOpponentsID(dialog: dialog) {
                        opponentsIDs = opponentIDs_
                        
                        constantVC.ActiveCall.opponentsIDs = opponentIDs_
                        
                        var arrStrIDS = [String]()
                        
                        for id in opponentsIDs {
                            arrStrIDS.append("\(id)")
                        }
                        
                        let strIDS = arrStrIDS.joined(separator: ",")
                        
                        if let senderID = SessionManager.get_QuickBloxID() {
                            constantVC.ActiveCall.membersID = strIDS + "," + "\(senderID)"
                        }
                    }
                    
                    if let id = dialog.id {
                        constantVC.ActiveCall.dialogID = id
                    }
                    
                    if let photo = dialog.photo {
                        constantVC.ActiveCall.groupPhoto = photo
                    }
                    
                    //Create new session
                    let session = QBRTCClient.instance().createNewSession(withOpponents: opponentsIDs, with: conferenceType)
                    if session.id.isEmpty == false {
                        constantVC.ActiveSession.session = session
                        
                        var callTo:String = ""
                        var callBy:String = ""
                        var isGroup:String = ""
                        var isAudio:String = ""
                        
                        if dialog.type == QBChatDialogType.group {
                            callTo = groupName
                            callBy = groupName
                            isGroup = "true"
                        } else {
                            callTo = callTo_
                            callBy = SessionManager.get_phonenumber()
                            isGroup = "false"
                        }
                        
                        var uuid: UUID? = nil
                        if CallKitManager.isCallKitAvailable() == true {
                            uuid = UUID()
                            constantVC.ActiveSession.activeCallUUID = uuid
                            CallKitManager.instance.startCall(withUserIDs: opponentsIDs, session: session, uuid: uuid , callTo: callTo)
                        }
                        
                        
                        if conferenceType == .audio {
                            isAudio = "true"
                            if let callViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "TamAudioCall_VC") as? TamAudioCall_VC {
                                callViewController.session = constantVC.ActiveSession.session
                                callViewController.callUUID = uuid
                                callViewController.opponentNo = callTo
                                callViewController.opponentName = callTo_Name
                                callViewController.userPhoto = callTo
                                callViewController.dialogID = constantVC.ActiveCall.dialogID
                                if let topVC = UIApplication.topViewController() {
                                    topVC.present(callViewController , animated: true , completion: {
                                        constantVC.ActiveSession.isStartedConnectingCall = false
                                    })
                                }
                            }
                        }
                        else {
                            isAudio = "false"
                            if let callViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "TamVideoCall_VC") as? TamVideoCall_VC {
                                callViewController.session = constantVC.ActiveSession.session
                                callViewController.callUUID = uuid
                                callViewController.opponentNo = callTo
                                callViewController.opponentName = callTo_Name
                                callViewController.isGroup = isGroup
                                callViewController.callBy = callBy
                                callViewController.dialogID = constantVC.ActiveCall.dialogID
                                if let topVC = UIApplication.topViewController() {
                                    topVC.present(callViewController , animated: true , completion: {
                                        constantVC.ActiveSession.isStartedConnectingCall = false
                                    })
                                }
                            }
                        }
                        
                        let payload = ["message": callBy,
                                       "ios_voip": "1", UsersConstant.voipEvent: "1" , "callingFrom": callBy , "isGroup": isGroup , "isAudio": isAudio]
                        let data = try? JSONSerialization.data(withJSONObject: payload,
                                                               options: .prettyPrinted)
                        var message = ""
                        if let data = data {
                            message = String(data: data, encoding: .utf8) ?? ""
                        }
                        let event = QBMEvent()
                        event.notificationType = QBMNotificationType.push
                        let arrayUserIDs = opponentsIDs.map({"\($0)"})
                        event.usersIDs = arrayUserIDs.joined(separator: ",")
                        event.type = QBMEventType.oneShot
                        event.message = message
                        QBRequest.createEvent(event, successBlock: { response, events in
                            if response.isSuccess {
                                print("success")
                            } else {
                                print("error")
                            }
                            
                        }, errorBlock: { response in
                            print(response.error?.debugDescription)
                        })
                    } else {
                        
                        FTIndicator.showToastMessage("SA_STR_CONNECTING_TO_CHAT")
                        //SVProgressHUD.showError(withStatus: UsersAlertConstant.shouldLogin)
                    }
                }
            }
        }
    }
    
    func hasConnectivity() -> Bool {
        let status = Core.instance.networkConnectionStatus()
        guard status != NetworkConnectionStatus.notConnection else {
            FTIndicator.showToastMessage(UsersAlertConstant.checkInternet)
            // showAlertView(message: UsersAlertConstant.checkInternet)
            return false
        }
        return true
    }
    
    
    func prepareCall_VIDEO(opponentNo: String , dialog: QBChatDialog , groupName: String , opponentName:String){
        
       /* if self.isAuthorizedUser(dialog: dialog) {
            
            let document = opponentNo + "-" + SessionManager.get_phonenumber()
            QuickBloxManager().isUserBlocked(dialog: dialog , document: document, completionHandler: { (success) in
                if !success {
                    self.call(with: QBRTCConferenceType.video , dialog: dialog , groupName: groupName , callTo_: opponentNo)
                } else {
                    self.navigateTamVideoCall_blockedVC(opponentNo: opponentNo , dialog: dialog , groupName: groupName)
                }
            })
        } else {
            self.showUserBlockedInfo()
        }*/
        
        self.call(with: QBRTCConferenceType.video , dialog: dialog , groupName: groupName , callTo_: opponentNo, callTo_Name: opponentName)
    }
    
    
    func prepareCall_AUDIO(opponentNo: String , dialog: QBChatDialog , groupName: String , opponentName:String){
        
       /* if self.isAuthorizedUser(dialog: dialog) {
            
            let document = opponentNo + "-" + SessionManager.get_phonenumber()
            QuickBloxManager().isUserBlocked(dialog: dialog , document: document, completionHandler: { (success) in
                if !success {
                    self.call(with: QBRTCConferenceType.audio , dialog: dialog , groupName: groupName , callTo_: opponentNo)
                } else {
                    self.navigateTamAudioCall_blockedVC(opponentNo: opponentNo)
                }
            })
        } else {
            self.showUserBlockedInfo()
        }*/
        
        self.call(with: QBRTCConferenceType.audio , dialog: dialog , groupName: groupName , callTo_: opponentNo, callTo_Name: opponentName)
    }
    
    func isAuthorizedUser(dialog: QBChatDialog) ->Bool{
        if dialog.type == QBChatDialogType.private {
            if let occupantID = NimUserManager.shared.getOpponentID(dialog: dialog) {
                if let isBlock = SessionManager.isChatUserBlocked(userID: "\(occupantID)") as? Bool {
                    if isBlock {
                        return false
                    }
                }
            }
        }
        return true
    }
    
    func showUserBlockedInfo(){
        FTIndicator.showToastMessage("First UnBlock User")
    }
    
    func navigateTamAudioCall_blockedVC(opponentNo: String){

        if let callViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "TamAudioCall_VC") as? TamAudioCall_VC {
            callViewController.opponentNo = opponentNo
            callViewController.userPhoto = opponentNo
            callViewController.isUserBlocked = true
            if let topVC = UIApplication.topViewController() {
                topVC.present(callViewController , animated: true , completion: nil)
            }
        }
    }
    
    
    func navigateTamVideoCall_blockedVC(opponentNo: String , dialog: QBChatDialog , groupName: String){
        
        var callTo:String = ""
        var isGroup:String = ""
        
        if dialog.type == QBChatDialogType.group {
            callTo = groupName
            isGroup = "true"
        } else {
            callTo = opponentNo
            isGroup = "false"
        }
        
        if let callViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "TamVideoCall_VC") as? TamVideoCall_VC {
            callViewController.opponentNo = callTo
            callViewController.isGroup = isGroup
            callViewController.isUserBlocked = true
            if let topVC = UIApplication.topViewController() {
                topVC.present(callViewController , animated: true , completion: nil)
            }
        }
        
    }
}


