//
//  NotificationService.swift
//  Service
//
//  Created by Kitlabs-M-0002 on 8/22/19.
//  Copyright © 2019 Kitlabs-M-0002. All rights reserved.
//

import UserNotifications
import Contacts
import UIKit

/*userInfo {
 FromNumber = 912222222222;
 ToNumber = 913333333333;
 aps =     {
 alert = Yffguc;
 badge = 1;
 "mutable-content" = 1;
 sound = default;
 };
 "dialog_id" = 5d5e60f4d94c22509e24f397;
 title = Yffguc;
 }*/

class NotificationService: UNNotificationServiceExtension {
    
    var contentHandler: ((UNNotificationContent) -> Void)?
    var bestAttemptContent: UNMutableNotificationContent?
    
    override func didReceive(_ request: UNNotificationRequest, withContentHandler contentHandler: @escaping (UNNotificationContent) -> Void) {
        
        
        self.contentHandler = contentHandler
        bestAttemptContent = (request.content.mutableCopy() as? UNMutableNotificationContent)
        
        if let bestAttemptContent = bestAttemptContent {
            
            if let userInfo = bestAttemptContent.userInfo as? [String:Any] {
                
                print(userInfo)
                
                    //chat
                    if let fromNo = userInfo["FromNumber"] as? String {
                        bestAttemptContent.title = fromNo
                    }
                    
                    if let newMsg = userInfo["title"] as? String {
                        
                        if newMsg.contains("TAMSTICKER*") {
                            bestAttemptContent.body = "Sent you a Sticker"
                        }
                        else if newMsg == "Image attachment" || newMsg == "Audio attachment" || newMsg == "Video attachment"{
                            bestAttemptContent.body = "Sent you a Media"
                        }
                        else {
                            bestAttemptContent.body = newMsg
                        }
                    } else {
                        bestAttemptContent.body = "Sent you a new message!"
                    }
                
                contentHandler(bestAttemptContent)
            }
        }
    }
    
    
    func allowedContactKeys() -> [CNKeyDescriptor]{
        //We have to provide only the keys which we have to access. We should avoid unnecessary keys when fetching the contact. Reducing the keys means faster the access.
        return [CNContactNamePrefixKey as CNKeyDescriptor,
                CNContactGivenNameKey as CNKeyDescriptor,
                CNContactFamilyNameKey as CNKeyDescriptor,
                CNContactOrganizationNameKey as CNKeyDescriptor,
                CNContactBirthdayKey as CNKeyDescriptor,
                CNContactImageDataKey as CNKeyDescriptor,
                CNContactThumbnailImageDataKey as CNKeyDescriptor,
                CNContactImageDataAvailableKey as CNKeyDescriptor,
                CNContactPhoneNumbersKey as CNKeyDescriptor,
                CNContactEmailAddressesKey as CNKeyDescriptor,
        ]
    }
    
    
    func getContactName(userNumber:String) -> String {
        let contactStore = CNContactStore()
        var userName:String = ""
        do {
            try contactStore.enumerateContacts(with: CNContactFetchRequest(keysToFetch: allowedContactKeys())) {
                (contact, cursor) -> Void in
                for number in contact.phoneNumbers {
                    let bookphNo = number.value.stringValue.components(separatedBy: CharacterSet.decimalDigits.inverted).joined()
                    if userNumber.range(of: bookphNo) != nil {
                        userName = contact.givenName
                    } else {
                        if userNumber.contains(bookphNo) {
                            userName = contact.givenName
                        }
                    }
                }
            }
        }
        catch{
            print("Handle the error please")
        }
        
        return userName
    }
    
    override func serviceExtensionTimeWillExpire() {
        // Called just before the extension will be terminated by the system.
        // Use this as an opportunity to deliver your "best attempt" at modified content, otherwise the original push payload will be used.
        if let contentHandler = contentHandler, let bestAttemptContent =  bestAttemptContent {
            contentHandler(bestAttemptContent)
        }
    }
    
}
